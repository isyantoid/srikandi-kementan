<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{title}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}export_data" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-file-excel-o"></i> Export Data</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-bordered">
                <thead>
                    <th width="10" class="text-center">No</th>
                    <th>Nomor STO</th>
                    <th>Tanggal</th>
                    <th>Luas Kebun</th>
                    <th>Provinsi</th>
                </thead>
                <tbody>
                    <?php if($laporan): ?>
                        <?php $no=1; foreach($laporan as $row): ?>
                            <tr>
                                <td class="text-center"><?= $no ?></td>
                                <td><?= $row['nomor_registrasi'] ?></td>
                                <td><?= $row['tanggal_permohonan'] ?></td>
                                <td><?= $row['luas_kebun'] . ' ' . $row['satuan_luas_kebun'] ?></td>
                                <td><?= $row['provinsi_nama'] ?></td>
                            </tr>
                        <?php $no++; endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
        </div>
    </div>
</div>