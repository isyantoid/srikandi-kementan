<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                    <thead>
                        <tr>
                            <th rowspan="2" style="text-align:center; vertical-align: middle;">No</th>
                            <th rowspan="2" style="text-align:center; vertical-align: middle;">Komoditas</th>
                            <?php foreach ($show_query_provinsi as $sqp) : ?>
                            <th colspan="2" style="text-align:center; vertical-align: middle;">
                                <?php echo $sqp->provinsi_nama; ?></th>
                            <?php endforeach; ?>
                            <th colspan="2" style="text-align:center; vertical-align: middle;">
                                Total</th>
                        </tr>
                        <tr>
                            <?php for ($no = 1; $no <= $no_query_provinsi; $no++) : ?>
                            <td style="text-align:center">Kampung</td>
                            <td style="text-align:center">Luasan(ha)</td>
                            <?php endfor; ?>
                            <td style="text-align:center">Kampung</td>
                            <td style="text-align:center">Luasan(ha)</td>
                        </tr>
                        <?php $number = 1;
                        foreach ($show_query_komoditi as $sqk) :
                            $totalkampung = 0;
                            $totalkomoditi = 0;
                        ?>
                        <tr>
                            <td style="text-align:center"><?php echo $number; ?></td>
                            <td style="text-align:center"><?php echo $sqk->jenis_komoditi_nama; ?></td>
                            <?php foreach ($show_query_provinsi as $sqp) : ?>
                            <td style="text-align:center">
                                <?php
                                        $totalkampung +=  count_kampung($sqk->jenis_komoditi_nama, $sqp->provinsi_nama);
                                        echo count_kampung($sqk->jenis_komoditi_nama, $sqp->provinsi_nama); ?>
                            </td>
                            <td style="text-align:center">
                                <?php
                                        $totalkomoditi += count_luas($sqk->jenis_komoditi_nama, $sqp->provinsi_nama);
                                        echo count_luas($sqk->jenis_komoditi_nama, $sqp->provinsi_nama); ?>
                            </td>
                            <?php endforeach;
                                $number++; ?>
                            <td style="text-align:center"><?php echo $totalkampung; ?></td>
                            <td style="text-align:center"><?php echo $totalkomoditi; ?></td>
                        </tr>
                        <?php
                        endforeach; ?>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->