<div class="row">
    <div class="col-md-8">
        <div class="kt-portlet kt-margin-top-30">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title"> {title} </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div style="height:300px; overflow:hidden;">
                    <img src="{dist_path}carousel/images/person_<?php echo $id ?>.jpg" alt="images" class="img-fluid w-100">
                </div>
                <div class="mt-5 text-justify">
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et voluptas dolorum esse! Maxime tempore dignissimos architecto minus consectetur nesciunt sint incidunt aliquam ea repellat, nam deserunt laudantium illo blanditiis in. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae neque dignissimos, molestiae blanditiis saepe maiores repellendus vel reiciendis dolorem excepturi quos laboriosam ipsam provident nesciunt repudiandae. Est suscipit dignissimos provident!</p>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et voluptas dolorum esse! Maxime tempore dignissimos architecto minus consectetur nesciunt sint incidunt aliquam ea repellat, nam deserunt laudantium illo blanditiis in. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae neque dignissimos, molestiae blanditiis saepe maiores repellendus vel reiciendis dolorem excepturi quos laboriosam ipsam provident nesciunt repudiandae. Est suscipit dignissimos provident!</p>
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et voluptas dolorum esse! Maxime tempore dignissimos architecto minus consectetur nesciunt sint incidunt aliquam ea repellat, nam deserunt laudantium illo blanditiis in. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae neque dignissimos, molestiae blanditiis saepe maiores repellendus vel reiciendis dolorem excepturi quos laboriosam ipsam provident nesciunt repudiandae. Est suscipit dignissimos provident!</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="kt-portlet kt-margin-top-30">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title"> Artikel Populer </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <?php for($i=1; $i<=5; $i++): ?>
                    <div class="kt-widget kt-widget--user-profile-3">
                        <div class="kt-widget__top pt-3 pb-3">
                            <div class="kt-widget__media kt-hidden-">
                                <img src="{dist_path}carousel/images/person_<?php echo $i ?>.jpg" alt="image">
                            </div>
                            <div class="kt-widget__content">
                                <div class="kt-widget__head">
                                    <a href="{site_url}mockup/baca_artikel/<?php echo $i ?>" class="kt-widget__username"> Judul Artikel <?php echo $i ?></a>
                                </div>
                                <div class="kt-widget__info">
                                    <div class="kt-widget__desc">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-widget__bottom mt-3"></div>
                    </div>
                <?php endfor ?>
            </div>
        </div>
    </div>
</div>