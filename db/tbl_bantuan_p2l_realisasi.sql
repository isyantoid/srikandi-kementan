/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 19/06/2023 17:14:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_bantuan_p2l_realisasi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bantuan_p2l_realisasi`;
CREATE TABLE `tbl_bantuan_p2l_realisasi`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `bantuan_p2l_id` int NULL DEFAULT NULL,
  `nama_realisasi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_bantuan_id` int NULL DEFAULT NULL,
  `jumlah` int NULL DEFAULT NULL,
  `nilai` decimal(10, 0) NULL DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '1. realisasi fisik sarana perbenihan\r\n2. realisasi fisik demplop\r\n3. realisasi fisik pertanaman',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
