<div class="row">
    <div class="col-md-6">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> JENIS KOMODITAS TAHUN {tahun}</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div id="chart_1" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> JENIS BANTUAN TAHUN {tahun}</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div id="chart_2" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> BANTUAN TAHUN {tahun}</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div id="chart_3" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    am4core.ready(function() {

        am4core.useTheme(am4themes_animated);

        var chart = am4core.create("chart_1", am4charts.PieChart);
        chart.dataSource.url = "{segment1}chart_1?tahun={tahun}&provinsi_kode={provinsi_kode}&kabupaten_kode={kabupaten_kode}";
        chart.dataSource.parser = new am4core.JSONParser();
        chart.innerRadius = 0;
        var label = chart.seriesContainer.createChild(am4core.Label);
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "total_nilai_bantuan";
        pieSeries.dataFields.jenis_komoditi_nama = "jenis_komoditi_nama";
        pieSeries.dataFields.total_nilai_bantuan = "total_nilai_bantuan";
        var templateText = "[bold]{jenis_komoditi_nama}[/] {value.percent.formatNumber('#.0')}% \n[bold]Rp. {total_nilai_bantuan.formatNumber('#,###')}[/]";
        pieSeries.labels.template.fontSize = 9;
        pieSeries.labels.template.text = templateText;


        var chart2 = am4core.create("chart_2", am4charts.PieChart);
        chart2.dataSource.url = "{segment1}chart_2?tahun={tahun}&provinsi_kode={provinsi_kode}&kabupaten_kode={kabupaten_kode}";
        chart2.dataSource.parser = new am4core.JSONParser();
        chart2.innerRadius = 0;
        var label = chart2.seriesContainer.createChild(am4core.Label);
        var pieSeries2 = chart2.series.push(new am4charts.PieSeries());
        pieSeries2.dataFields.value = "total_nilai_bantuan";
        pieSeries2.dataFields.total_nilai_bantuan = "nama_bantuan";
        pieSeries2.dataFields.total_nilai_bantuan = "total_nilai_bantuan";
        var templateText2 = "[bold]{nama_bantuan}[/] {value.percent.formatNumber('#.0')}% \n[bold]Rp. {total_nilai_bantuan.formatNumber('#,###')}[/]";
        pieSeries2.labels.template.fontSize = 9;
        pieSeries2.labels.template.text = templateText2;


        var chart = am4core.create("chart_3", am4charts.XYChart);

        chart.dataSource.url = "{segment1}chart_3?tahun={tahun}&provinsi_kode={provinsi_kode}&kabupaten_kode={kabupaten_kode}";

        // Create axes

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "kabupaten_nama";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
            if (target.dataItem && target.dataItem.index & 2 == 2) {
                return dy + 25;
            }
            return dy;
        });

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "total_nilai_bantuan";
        series.dataFields.categoryX = "kabupaten_nama";
        series.name = "Total Nilai Bantuan";
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;

    });
</script>