<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model', 'model');
		if(!is_user()) {
            redirect('beranda/login');
        }
	}


	public function json_nasional() {
        $geojsonPath = './dist/json/indonesia-prov.geojson';
        $geojsonContent = file_get_contents($geojsonPath);
        $geojson = json_decode($geojsonContent, true);

        foreach ($geojson['features'] as &$feature) {
            $checkProv = $this->db->where('kode',$feature['properties']['kode'])->get('tbl_provinsi')->row_array();
			$count = $this->model->get_count_alokasi($feature['properties']['kode']);
            $feature['properties']['value'] = $count; // Ganti dengan properti dan nilai yang sesuai
        }
        if(file_put_contents($geojsonPath, json_encode($geojson, JSON_PRETTY_PRINT))) {
			echo json_encode($geojson, JSON_PRETTY_PRINT);
        }

    }

	public function get_peta_champion() {
        // $this->db->where('champ.provinsi_kode', $provinsiKode);
		$tahun = ($this->input->get('tahun')) ? $this->input->get('tahun') : date('Y');

        $this->db->select('
            champ.*,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi2.jenis_komoditi_nama as jenis_komoditi_nama2,
			champ_komitmen.stok,
        ');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('tbl_jenis_komoditi', 'champ.jenis_komoditi_id = tbl_jenis_komoditi.id');
        $this->db->join('tbl_jenis_komoditi as tbl_jenis_komoditi2', 'champ.jenis_komoditi_kedua_id = tbl_jenis_komoditi2.id', 'left');
        $this->db->join('champ_komitmen', 'champ.user_id = champ_komitmen.user_id', 'left');

		$champ_bamer = [13, 14];
		$champ_cabai = [15, 16];
		$user_level_id = $this->session->userdata('session_user_level_id');
		if(champ_user() == 'bamer') {
			$this->db->where('champ.jenis_komoditi_id', '68');
		} else if(champ_user() == 'cabai') {
			$this->db->where('champ.jenis_komoditi_id !=', '68');
		}
		
        $res = $this->db->get('champ');
        $data = $res->result_array();
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
    }



	public function index()
	{
		akses_user('read');
		$tahun = ($this->input->get('tahun')) ? $this->input->get('tahun') : date('Y');
		$data['tahun'] = $tahun;
		$data['list_index_komoditi'] = $this->model->list_index_komoditi_dashboard($tahun);
		$data['title'] = 'Dashboard';

		$allowed_levels_p2l = [9, 10, 11, 12];
		$allowed_levels_champ = [13, 14, 15, 16, 17, 18,19];
		$user_level_id = $this->session->userdata('session_user_level_id');
		if(in_array($user_level_id, $allowed_levels_p2l)) {
			$data['content'] = 'dashboard/index_p2l';
		} else if(in_array($user_level_id, $allowed_levels_champ)) {
			$data['content'] = 'dashboard/index_champ';
		} else {
			$data['content'] = 'dashboard/index';
		}
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}
	
	public function detail_kampung()
	{
		$jenis_komoditi_id = $this->input->get('jenis_komoditi_id');
		$tahun = $this->input->get('tahun');
		if($jenis_komoditi_id && $tahun) {
			$data['tahun'] = $tahun;
			$data['jenis_komoditi_id'] = $jenis_komoditi_id;
			$data['detail_kampung'] = $this->model->detail_kampung($tahun, $jenis_komoditi_id);
			$data['title'] = 'Detail Kampung';
			$data['content'] = 'dashboard/detail_kampung';
			$data = array_merge($data, path_info());
			$this->parser->parse('admin_template/main', $data);
		}
	}

	public function summary_pusat()
	{
		$data['title'] = 'Summary Executive Report';
		$data['content'] = 'dashboard/summary_komoditi_pusat';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}

	public function result_summary()
	{
		$tipe = $this->input->post('tipe');
		$image = $this->input->post('image');
		$provinsi = $this->input->post('provinsi');
		$komoditi = $this->input->post('komoditi');
		if ($tipe == 'provinsi') {
			$data['komoditi'] = $komoditi;
			$data['image'] = $image;
			$data['title'] = $komoditi . ' - Dalam Provinsi';
			$page = 'dashboard/result_summary_komoditi_provinsi';
		} else if ($tipe == 'kabupaten') {
			$data['provinsi'] = $provinsi;
			$data['komoditi'] = $komoditi;
			$data['image'] = $image;
			$data['title'] = $komoditi . ' - Provinsi ' . $provinsi;
			$page = 'dashboard/result_summary_komoditi_kabupaten';
		} else {
			$page = 'dashboard/result_summary_komoditi_pusat';
			$data['title'] = 'Komoditi Pusat';
		}
		$data = array_merge($data, path_info());
		$this->parser->parse($page, $data);
	}

	public function chart_pusat()
	{
		$data['show_kelompok_komoditi'] = $this->db->get('tbl_kelompok_komoditi')->result();
		$data['show_jenis_komoditi'] = $this->db->get('tbl_jenis_komoditi')->result();

		$data['title'] = 'Chart Information System';
		$data['content'] = 'dashboard/chart_pusat';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}

	public function result_chart_pusat()
	{
		$params = $this->input->post();

		$show_jenis_komoditi = $this->db->get('tbl_jenis_komoditi')->result();

		foreach ($show_jenis_komoditi as $sjk) :

			if ($this->count_total_chart1($params, $sjk->jenis_komoditi_kode) > 0) :
				$array[] = array(
					'sector' => $sjk->jenis_komoditi_nama, 'size' => $this->count_total_chart1($params, $sjk->jenis_komoditi_kode)
				);
			else :
				$array[] = array();
			endif;

		endforeach;

		$show_jenis_bantuan = $this->db->get('tbl_jenis_bantuan')->result();
		foreach ($show_jenis_bantuan as $sjb) :

			if ($this->count_total_chart2($params, $sjb->id) > 0) :
				$array_jns_bntuan[] = array(
					'sector' => $sjb->nama_bantuan, 'size' => $this->count_total_chart2($params, $sjb->id)
				);
			else :
				$array_jns_bntuan[] = array();
			endif;

		endforeach;


		$data['show_data_chart1'] = json_encode($this->check_not_empty_array($array));
		$data['show_data_chart2'] = json_encode($this->check_not_empty_array($array_jns_bntuan));

		$data['title'] = 'Komoditi Dalam Grafik';
		$data['tahun'] = $this->input->post('tahun');
		$data['provinsi_kode'] = $this->input->post('provinsi_kode');
		$data['kabupaten_kode'] = $this->input->post('kabupaten_kode');
		$page = 'dashboard/result_chart_pusat';
		$data = array_merge($data, path_info());
		$this->parser->parse($page, $data);
	}


	public function chart_1() {
		$data = $this->model->chart_1();
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
		
	}
	public function chart_2() {
		$data = $this->model->chart_2();
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
		
	}
	public function chart_3() {
		$data = $this->model->chart_3();
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
		
	}

	function check_not_empty_array($key)
	{
		$data[0] = '';
		foreach (array_filter($key) as $d) :
			$data[] = array(
				'sector' => $d['sector'],
				'size' => $d['size']
			);
		endforeach;

		return $data;
	}

	function count_total_chart1($params, $key)
	{
		$this->db->select('sum(total_nilai_bantuan) as total_nilai_bantuan');

		(!empty($params['tahun'])) ?  $this->db->like('tanggal_kirim', $params['tahun']) : '';

		(!empty($params['provinsi_kode'])) ?  $data['provinsi_kode'] = $params['provinsi_kode'] : '';

		(!empty($params['kabupaten_kode'])) ?  $data['kabupaten_kode'] = $params['kabupaten_kode'] : '';

		$data['jenis_komoditi_kode'] = $key;

		$this->db->where($data);

		$row = $this->db->get('tbl_bantuan')->row();

		if (!empty($row)) :
			return ($row->total_nilai_bantuan == null) ? 0 : $row->total_nilai_bantuan;
		else :
			return '0';
		endif;
	}

	public function count_total_chart2($params, $key)
	{
		$this->db->select('sum(tbl_bantuan_detail.total) as total');
		(!empty($params['tbl_bantuan.tahun'])) ?  $this->db->like('tanggal_kirim', $params['tahun']) : '';

		(!empty($params['tbl_bantuan.provinsi_kode'])) ?  $data['provinsi_kode'] = $params['provinsi_kode'] : '';

		(!empty($params['tbl_bantuan.kabupaten_kode'])) ?  $data['kabupaten_kode'] = $params['kabupaten_kode'] : '';

		(!empty($key)) ?  $data['tbl_bantuan_detail.jenis_bantuan_id'] = $key : '';

		$this->db->where($data);

		$this->db->join('tbl_bantuan', 'tbl_bantuan_detail.bantuan_id = tbl_bantuan.id');
		$this->db->from('tbl_bantuan_detail');
		$row = $this->db->get()->row();

		if (!empty($row)) :
			return ($row->total == null) ? 0 : $row->total;
		else :
			return '0';
		endif;
	}

    public function gis_pusat()
	{
		$data['title'] = 'Geographic Information System';
		$data['content'] = 'dashboard/gis_pusat';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}

// 	public function get_lat_lng_permohonan()
// 	{
// 		$array_data['kelompok_komoditi_id'] = $this->input->get('kelompok_komoditi_id');
// 		$array_data['jenis_komoditi_id'] = $this->input->get('jenis_komoditi_id');
// 		$array_data['provinsi_kode'] = $this->input->get('provinsi_kode');
// 		$array_data['kabupaten_kode'] = $this->input->get('kabupaten_kode');
// 		$result = $this->model->get_lat_lng_permohonan($array_data);
// 		$this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_PRETTY_PRINT));
// 	}

public function get_lat_lng_permohonan()
	{
		$array_data['kelompok_komoditi_id'] = ($this->input->get('kelompok_komoditi_id') == 'null') ? '' : $this->input->get('kelompok_komoditi_id');
		$array_data['jenis_komoditi_id'] = ($this->input->get('jenis_komoditi_id') == 'null') ? '' : $this->input->get('jenis_komoditi_id');
		$array_data['provinsi_kode'] = ($this->input->get('provinsi_kode') == 'null') ? '' : $this->input->get('provinsi_kode');
		$array_data['kabupaten_kode'] = ($this->input->get('kabupaten_kode') == 'null') ? '' : $this->input->get('kabupaten_kode');
		$result = $this->model->get_lat_lng_permohonan($array_data);
	
		$this->output->set_content_type('application/json')->set_output(json_encode($result, JSON_PRETTY_PRINT));
	}

	public function map_pusat()
	{
		//edited hendra
		$data['kelompok_komoditi_id'] = $this->input->get('kelompok_komoditi_id');
		$data['jenis_komoditi_id'] = $this->input->get('jenis_komoditi_id');
		$data['provinsi_kode'] = $this->input->get('provinsi_kode');
		$data['kabupaten_kode'] = $this->input->get('kabupaten_kode');
		//edited hendra
		$data['title'] = 'Komoditi';
		$page = 'dashboard/map_pusat';
		$data = array_merge($data, path_info());
		$this->parser->parse($page, $data);
	}


	public function map_pusat_detail($id)
	{
		if ($id) {
			$data = $this->model->detail_permohonan($id);
			$anggota_tani = getResultArray('tbl_anggota_tani', array('id_permohonan' => $id));
			$arrayPolygon = array();
			foreach ($anggota_tani as $at) {
				$arrayPolygon[] = json_decode($at['polygon']);
			}
			$data['array_polygon'] = $arrayPolygon;
			$data['title'] = 'Komoditi';
			$page = 'dashboard/map_pusat_detail';
			$data = array_merge($data, path_info());
			$this->parser->parse($page, $data);
		}
	}

	public function map_pusat_detail_polygon($id)
	{
		if ($id) {
			$anggota_tani = $this->model->get_lat_lng_anggota($id);
			if ($anggota_tani) {
				foreach ($anggota_tani as $row) :
					$res['id'] = $row['id'];
					$res['nama_anggota'] = $row['nama_anggota'];
					$res['nik_anggota'] = $row['nik_anggota'];
					$res['no_hp'] = $row['no_hp'];
					$res['luas'] = $row['luas'];
					$res['latitude'] = (float) $row['latitude'];
					$res['longitude'] = (float) $row['longitude'];
					$res['tanggal_tanam'] = formatTanggal($row['tanggal_tanam']);
					$res['tanggal_panen'] = formatTanggal($row['tanggal_panen']);
					$res['jenis_komoditi_nama'] = $row['jenis_komoditi_nama'];

					$polygon = json_decode($row['polygon']);
					$res['coordinates'] = $polygon;
					$results[] = $res;
				endforeach;
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($results, JSON_PRETTY_PRINT));
		} else {
		}
		if ($this->input->is_ajax_request()) {
		}
	}

	public function chart_total_kampung()
	{
		for ($i = 1; $i <= 12; $i++) {
			$dateObj = DateTime::createFromFormat('!m', $i);
			$data['month'] = $dateObj->format('M');
			$data['total'] = rand(1500, 9999);
			$res[] = $data;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function chart_total_luas()
	{
		for ($i = 1; $i <= 12; $i++) {
			$dateObj = DateTime::createFromFormat('!m', $i);
			$data['month'] = $dateObj->format('M');
			$data['total'] = rand(500, 4900);
			$res[] = $data;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}


	public function test_json()
	{
		$json = file_get_contents('./polygon.json');
		$dec = json_decode($json, true);
		$this->output->set_content_type('application/json')->set_output(json_encode($dec, JSON_PRETTY_PRINT));
	}


	public function parsing_kml()
	{
		$kml = simplexml_load_file(FCPATH . 'file.kml');
		$json = json_encode($kml);
		$array = json_decode($json, TRUE);


		$placemark = json_encode($array['Document']['Folder'][0]['Placemark']);
		$placemarkArray = json_decode($placemark, TRUE);
		foreach ($placemarkArray as $p) {
			$coordinates = isset($p['Polygon']['outerBoundaryIs']['LinearRing']['coordinates']);
			if ($coordinates) {
				$item['name'] = $p['name'];
				$item['styleUrl'] = $p['styleUrl'];
				$coordinates = trim(preg_replace('/\s+/', '', $p['Polygon']['outerBoundaryIs']['LinearRing']['coordinates']));
				$exp = explode(",0", $coordinates);
				for ($i = 0; $i < count($exp); $i++) {
					$item['coordinates'][] = $exp[$i];
				}
				$results[] = $item;
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($results, JSON_PRETTY_PRINT));
	}



	public function kelompok_tani()
	{
		$data['tanggal_awal'] = date('Y-m-01');
		$data['tanggal_akhir'] = date('Y-m-t');
		$data['title'] = 'Kelompok Tani';
		$data['content'] = 'dashboard/kelompok_tani';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}

	public function kelompok_tani_result()
	{
		$data['tanggal_awal'] = $this->input->post('tanggal_awal');
		$data['tanggal_akhir'] = $this->input->post('tanggal_akhir');
		$data['title'] = 'Kelompok Tani';
		$data = array_merge($data, path_info());
		$this->parser->parse('dashboard/kelompok_tani_result', $data);
	}
	
	public function luas_kampung()
	{
		$data['tanggal_awal'] = date('Y-m-01');
		$data['tanggal_akhir'] = date('Y-m-t');
		$data['title'] = 'Luas Kampung';
		$data['content'] = 'dashboard/luas_kampung';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}

	public function luas_kampung_result()
	{
		$data['tanggal_awal'] = $this->input->post('tanggal_awal');
		$data['tanggal_akhir'] = $this->input->post('tanggal_akhir');
		$data['title'] = 'Luas Kampung';
		$data = array_merge($data, path_info());
		$this->parser->parse('dashboard/luas_kampung_result', $data);
	}
	
	public function jumlah_kampung()
	{
		$data['tanggal_awal'] = date('Y-m-01');
		$data['tanggal_akhir'] = date('Y-m-t');
		$data['title'] = 'Jumlah Kampung';
		$data['content'] = 'dashboard/jumlah_kampung';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}

	public function jumlah_kampung_result()
	{
		$data['tanggal_awal'] = $this->input->post('tanggal_awal');
		$data['tanggal_akhir'] = $this->input->post('tanggal_akhir');
		$data['title'] = 'Jumlah Kampung';
		$data = array_merge($data, path_info());
		$this->parser->parse('dashboard/jumlah_kampung_result', $data);
	}
	
	public function tanam_panen()
	{
		$data['tanggal_awal'] = date('Y-m-01');
		$data['tanggal_akhir'] = date('Y-m-t');
		$data['title'] = 'Tanam & Panen';
		$data['content'] = 'dashboard/tanam_panen';
		$data = array_merge($data, path_info());
		$this->parser->parse('admin_template/main', $data);
	}
	
	public function tanam_panen_result()
	{
		$data['tanggal_awal'] = $this->input->post('tanggal_awal');
		$data['tanggal_akhir'] = $this->input->post('tanggal_akhir');
		$data['title'] = 'Tanam & Panen';
		$data = array_merge($data, path_info());
		$this->parser->parse('dashboard/tanam_panen_result', $data);
	}

	public function perkiraan_panen_testing() {
		$data = $this->model->view_dokumentasi_perkiraan_panen();
		$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
		
	}
}