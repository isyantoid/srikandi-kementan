<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}" class="btn btn-danger btn-elevate btn-icon-sm">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-borderless table-hover table-checkable w-50">
                <tbody>
                    <tr>
                        <td>Nomor Registrasi STO</td>
                        <td>:</td>
                        <td class="font-weight-bold">{nomor_registrasi}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Registrasi STO</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_permohonan}</td>
                    </tr>
                    <tr>
                        <td>Nomor Registrasi Horti</td>
                        <td>:</td>
                        <td class="font-weight-bold">{no_reg_horti}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Pengesahan Registrasi Horti</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_reg_horti}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>:</td>
                        <td class="font-weight-bold">{provinsi_nama}</td>
                    </tr>
                    <tr>
                        <td>Kabupaten</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kabupaten_nama}</td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kecamatan_nama}</td>
                    </tr>
                    <tr>
                        <td>Desa</td>
                        <td>:</td>
                        <td class="font-weight-bold">{desa_nama}</td>
                    </tr>
                    <tr>
                        <td>Garis Lintang</td>
                        <td>:</td>
                        <td class="font-weight-bold">{lat}</td>
                    </tr>
                    <tr>
                        <td>Garis Bujur</td>
                        <td>:</td>
                        <td class="font-weight-bold">{lng}</td>
                    </tr>
                    <tr>
                        <td>Kelompok Komoditas</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kelompok_komoditi_nama}</td>
                    </tr>
                    <tr>
                        <td>Jenis Komoditas</td>
                        <td>:</td>
                        <td class="font-weight-bold">{jenis_komoditi_nama}</td>
                    </tr>
                    <tr>
                        <td>Sub Komoditi</td>
                        <td>:</td>
                        <td class="font-weight-bold">{sub_komoditi_nama}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Tanam</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_tanam}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Perkiraan Panen</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_perkiraan_panen}</td>
                    </tr>
                    <tr>
                        <td>Luas Kebun</td>
                        <td>:</td>
                        <td class="font-weight-bold">{luas_kebun} {satuan_luas_kebun}</td>
                    </tr>
                </tbody>
            </table>

            <div class="alert alert-secondary" role="alert">
                <div class="alert-icon"><i class="flaticon-info kt-font-brand"></i></div>
                <div class="alert-text font-weight-bold">Kelompok & Anggota Tani</div>
            </div>

            <table class="table table-bordered">
                <?php $kelompok_tani = $this->model->get_kelompok_tani($id) ?>
                <?php if($kelompok_tani): ?>
                    <?php foreach($kelompok_tani as $k): ?>
                        <tr>
                            <td>
                                <p>Nama Kelompok : <?php echo $k['nama_kelompok'] ?></p>
                                <p>Nama Ketua : <?php echo $k['nama_ketua'] ?></p>
                                <p>NIK Ketua : <?php echo $k['nik_ketua'] ?></p>
                                <p>NO HP : <?php echo $k['no_hp'] ?></p>
                                <p>Luas : <?php echo $k['luas'] ?> Hektar</p>
                                <p>Terdaftar Simluhtan : <?php echo ($k['terdaftar_simluhtan'] == '1') ? 'Ya' : 'Tidak' ?></p>
                            </td>
                            <td class="p-3">
                                <?php $anggota_tani = $this->model->get_anggota_tani($k['id']) ?>
                                <?php if($anggota_tani): ?>
                                    <?php $noA=1; foreach($anggota_tani as $a): ?>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                                        <p>Nama Anggota : <?php echo $a['nama_anggota'] ?></p>
                                        <p>NIK Anggota : <?php echo $a['nik_anggota'] ?></p>
                                        <p>NO HP : <?php echo $a['no_hp'] ?></p>
                                        <p>Luas : <?php echo $a['luas'] ?> Hektar</p>
                                        <p>Garis Lintang : <?php echo $a['latitude'] ?></p>
                                        <p>Garis Bujur : <?php echo $a['longitude'] ?></p>
                                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                                        <?php $noA++ ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

<script>
var table;

table = $('#kt_table_1').DataTable({
    ajax: {
        url: '{segment1}datatable'
    },
    responsive: true,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 10,
    order: [
        [0, 'desc']
    ],
    columns: [{
            data: 'id',
            visible: false
        },
        {
            data: 'id',
            width: 25,
            className: 'text-center'
        },
        { data: 'nomor_registrasi' },
        { data: 'tanggal_permohonan' },
        { data: 'kelompok_komoditi_nama' },
        { data: 'jenis_komoditi_nama' },
        { data: 'sub_komoditi_nama' },
        { 
            data: 'luas_kebun',
            render: function(data, type, row) {
                return data + ' ' + row.satuan_luas_kebun;
            }
        },
        {
            targets: -1,
            width: 100,
            title: 'Actions',
            className: 'text-center',
            orderable: false,
            render: function(data, type, row, meta) {
                aksi = '<span class="dropdown">';
                aksi +=
                    '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">';
                aksi += '<i class="la la-ellipsis-h"></i>';
                aksi += '</a>';
                aksi += '<div class="dropdown-menu dropdown-menu-right">';
                aksi += '<a class="dropdown-item" href="{segment1}detail/' + row.id + '"><i class="la la-eye"></i> Detail</a>';
                aksi += '<a class="dropdown-item" href="javascript:deleteData(' + row.id + ')"><i class="la la-trash"></i> Delete</a>';
                aksi += '</div>';
                aksi += '</span>';
                return aksi;
            },
        },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        var index = iDisplayIndex + 1;
        $('td:eq(0)', nRow).html(index);
        return nRow;
    }
});

function deleteData(id) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data yang telah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus data!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '{segment1}delete/' + id
            });
            table.ajax.reload();
            Swal.fire('Sukses!', 'Data berhasil dihapus.', 'success');
        }
    });
}
</script>