<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}create" class="btn btn-brand btn-elevate btn-icon-sm"><i
                                class="la la-plus"></i> New Record</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                    <tr>
                        <th>Record ID</th>
                        <th>#</th>
                        <th>Kode Komoditi</th>
                        <th>Nama Komoditi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

<script>
var table;

table = $('#kt_table_1').DataTable({
    ajax: {
        url: 'https://registrasilahanhorti.id/api/master_data/jenis_komoditi_datatable',
        method: 'post'
    },
    responsive: true,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 10,
    order: [
        [0, 'desc']
    ],
    columns: [{
            data: 'komoditi_jenis_id',
            visible: false
        },
        {
            data: 'komoditi_jenis_id',
            width: 25,
            className: 'text-center'
        },
        {
            data: 'komoditi_jenis_kode'
        },
        {
            data: 'komoditi_jenis_nama'
        },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        var index = iDisplayIndex + 1;
        $('td:eq(0)', nRow).html(index);
        return nRow;
    }
});

function deleteData(id) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data yang telah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus data!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '{segment1}delete/' + id
            });
            table.ajax.reload();
            Swal.fire('Sukses!', 'Data berhasil dihapus.', 'success');
        }
    });
}
</script>