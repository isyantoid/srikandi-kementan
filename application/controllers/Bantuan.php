<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bantuan extends CI_Controller
{

    private $title = 'Bantuan';
    private $view = 'Bantuan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');

        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_bantuan.id,
            tbl_bantuan.tanggal_kirim,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_bantuan.total_nilai_bantuan,
            tbl_tipe_bantuan.tipe_bantuan,
            tbl_kelompok_tani.nama_kelompok,
            tbl_permohonan.no_reg_horti,
        ');
        $this->datatables_server_side->join('tbl_tipe_bantuan','tbl_bantuan.tipe_bantuan_id = tbl_tipe_bantuan.id', 'left');
        $this->datatables_server_side->join('tbl_kelompok_tani','tbl_bantuan.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->datatables_server_side->join('tbl_permohonan','tbl_bantuan.permohonan_id = tbl_permohonan.id', 'left');
        
        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->where('tbl_permohonan.app_id', '2');
        $this->datatables_server_side->from('tbl_bantuan');
        return print_r($this->datatables_server_side->generate());
    }


    public function datatable_permohonan()
    {
        akses_user('read');
        $provinsi_kode = $this->input->post('provinsi_kode');
        $kabupaten_kode = $this->input->post('kabupaten_kode');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_permohonan.id,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_permohonan.status_reg_horti,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.sub_komoditi_nama,
        ');


        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->where('tbl_permohonan.app_id', '2');
        $this->datatables_server_side->from('tbl_permohonan');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    
    public function form_kelompok()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['nomor'] = $this->input->post('nomor');
        $data = array_merge($data, path_info());
        $this->parser->parse($this->view . '/form_kelompok', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = $this->model->get_bantuan($id);
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Bantuan';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }
    
    public function detail($id = null)
    {
        akses_user('read');
        if ($id) {
            $data = $this->model->get_bantuan($id);
            if ($data) {
                $data['detail_bantuan'] = $this->model->get_detail_bantuan($id);
                $data['title'] = $this->title;
                $data['subTitle'] = 'Detail Bantuan';
                $data['content'] = $this->view . '/detail';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function get_harga_jenis_bantuan($id = null) {
        if($this->input->is_ajax_request()) {
            if($id) {
                $jenisBantuan = getRowArray('tbl_jenis_bantuan', array('id' => $id));
                if($jenisBantuan) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($jenisBantuan));
                }
            }
        }
    }

    public function get_permohonan($id = null) {
        if($this->input->is_ajax_request()) {
            if($id) {
                $permohonan = getRowArray('tbl_permohonan', array('id' => $id));
                if($permohonan) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($permohonan));
                }
            }
        }
    }

    public function save()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nomor_registrasi', 'nomorRegistrasi', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            jsonOutputError('nomor registrasi horti wajib diisi');
        } else {
            $this->model->save();
        }
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }

    public function get_nomor_urut_registrasi() {
        if($this->input->is_ajax_request()) {
            $search = $this->input->post('search');
            $this->db->like('nomor_registrasi', $search);
            $count = $this->db->get('tbl_permohonan')->num_rows();
            $data['nomor_urut'] = str_pad($count + 1, 4, "0", STR_PAD_LEFT);
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
        
    }



    public function export_data()
    {
        $fileName = 'bantuan_' . date('d_m_Y') . '.xlsx';
        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $styleForTitle = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 16,
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForHeader = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 10,
            ],
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'DDDDDD'],
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForBody = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
                'name' => 'Cambria',
            ],
            'borders' => [
                'allBorders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => ['rgb' => '444444']
                ]
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1')->getStyle('A1')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A2:H2')->getStyle('A2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A3:H3')->getStyle('A1:A3')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'LAPORAN BANTUAN');

        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Tanggal Penerimaan');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Nama Kelompok');
        $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Tipe Bantuan');
        $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Jenis Bantuan');
        $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Satuan');
        $objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Harga');
        $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Total');
        $objPHPExcel->getActiveSheet()->SetCellValue('J4', 'Foto');
        foreach (range('A', 'J') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A4:J4')->applyFromArray($styleForHeader);

        $regKampung = $this->model->exportExcel();
        if($regKampung) {
            $rowCount = 5;
            $no=1;
            foreach($regKampung as $row) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no)->getStyle('A' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['tanggal_bantuan'])->getStyle('B' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('c' . $rowCount, $row['nama_kelompok'])->getStyle('c' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['tipe_bantuan'])->getStyle('D' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['jenis_bantuan'])->getStyle('E' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['jumlah'])->getStyle('F' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['satuan'])->getStyle('G' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['harga'])->getStyle('H' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['total'])->getStyle('I' . $rowCount)->applyFromArray($styleForBody);
                if($row['foto']) {
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $rowCount, 'Klik untuk lihat foto')->getStyle('J' . $rowCount)->applyFromArray($styleForBody);
                    $objPHPExcel->getActiveSheet()->getCell('J' . $rowCount)->getHyperlink()->setUrl(base_url('dist/images/bantuan/' . $row['foto']));
                } else {
                    $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, null)->getStyle('J' . $rowCount)->applyFromArray($styleForBody);
                }
                $rowCount++;
                $no++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}