<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Realisasi_fisik_sarana_pembenihan_model extends CI_Model {

    private $tableName = 'tbl_bantuan_p2l';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $id = $this->uri->segment(3);
            if($id) {


                $this->db->set('registrasi_p2l_id', $this->input->post('registrasi_p2l_id'));
                $this->db->set('tanggal_penerima_bantuan', $this->input->post('tanggal_penerima_bantuan'));
                $this->db->set('realisasi_pemanfaatan_anggaran', $this->input->post('realisasi_pemanfaatan_anggaran'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
                $this->db->where('id', $id);
                $this->db->update('tbl_bantuan_p2l');
                return jsonOutputSuccess();
            } else {
                
                $this->db->set('registrasi_p2l_id', $this->input->post('registrasi_p2l_id'));
                $this->db->set('tanggal_penerima_bantuan', $this->input->post('tanggal_penerima_bantuan'));
                $this->db->set('realisasi_pemanfaatan_anggaran', $this->input->post('realisasi_pemanfaatan_anggaran'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
                $this->db->insert('tbl_bantuan_p2l');
                return jsonOutputSuccess();
            }
            
        }


        
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('tbl_bantuan_p2l_realisasi');
    }
}
