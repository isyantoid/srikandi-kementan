<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Registrasi_p2l_model extends CI_Model {

    private $tableName = 'user';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $noreg = getNomorRegistrasiP2L();

            $id = $this->uri->segment(3);
            if($id) {


                $this->db->set('tanggal_registrasi_p2l', $this->input->post('tanggal_registrasi_p2l'));
                $this->db->set('kelompok_tani_id', $this->input->post('kelompok_tani_id'));
                $this->db->set('nama_kelompok', $this->input->post('nama_kelompok'));
                $this->db->set('nama_ketua', $this->input->post('nama_ketua'));
                $this->db->set('provinsi_kode', $this->input->post('provinsi_kode'));
                $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                $this->db->set('kecamatan_kode', $this->input->post('kecamatan_kode'));
                $this->db->set('desa_kode', $this->input->post('desa_kode'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('jumlah_penerima_manfaat', $this->input->post('jumlah_penerima_manfaat'));
                $this->db->set('terdaftar_simluhtan', $this->input->post('terdaftar_simluhtan'));
                $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
                $this->db->where('id', $id);
                $this->db->update('tbl_registrasi_p2l');
                return jsonOutputSuccess();
            } else {

                // $check = isDuplicate('tbl_registrasi_p2l', 'nomor_registrasi_p2l', $this->input->post('nomor_registrasi_p2l'));
                // if($check) {
                //     return jsonOutputError('Nomor registrasi sudah ada!!');
                // }
                
                $this->db->set('nomor_registrasi_p2l', $noreg);
                $this->db->set('tanggal_registrasi_p2l', $this->input->post('tanggal_registrasi_p2l'));
                $this->db->set('kelompok_tani_id', $this->input->post('kelompok_tani_id'));
                $this->db->set('nama_kelompok', $this->input->post('nama_kelompok'));
                $this->db->set('nama_ketua', $this->input->post('nama_ketua'));
                $this->db->set('provinsi_kode', $this->input->post('provinsi_kode'));
                $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                $this->db->set('kecamatan_kode', $this->input->post('kecamatan_kode'));
                $this->db->set('desa_kode', $this->input->post('desa_kode'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('jumlah_penerima_manfaat', $this->input->post('jumlah_penerima_manfaat'));
                $this->db->set('terdaftar_simluhtan', $this->input->post('terdaftar_simluhtan'));
                $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
                $this->db->insert('tbl_registrasi_p2l');
                return jsonOutputSuccess();
            }
            
        }


        
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('tbl_registrasi_p2l');
    }

    public function detail_registrasi($id) {
        $this->db->select('
            tbl_registrasi_p2l.*,
            tbl_provinsi.nama as provinsi_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_desa.nama as desa_nama,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_registrasi_p2l.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->join('tbl_kecamatan', 'tbl_registrasi_p2l.kecamatan_kode = tbl_kecamatan.kode', 'left');
        $this->db->join('tbl_desa', 'tbl_registrasi_p2l.desa_kode = tbl_desa.kode', 'left');
        $this->db->where('tbl_registrasi_p2l.id', $id);
        $this->db->from('tbl_registrasi_p2l');
        return $this->db->get()->row_array();
    }

    public function get_kelompok_tani($id_permohonan) {
        $this->db->select(' tbl_kelompok_tani.*');
        $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function get_anggota_tani($id_kelompok_tani) {
        $this->db->select(' tbl_anggota_tani.*');
        $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
        $this->db->from('tbl_anggota_tani');
        $get = $this->db->get();
        return $get->result_array();
    }

    public function getRegistrasiKampung() {
        $this->db->select('
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_kelompok_tani.nama_kelompok,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_kelompok_tani.terdaftar_simluhtan,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->order_by('tbl_permohonan.nomor_registrasi ASC', 'tbl_jenis_komoditi.jenis_komoditi_nama ASC', 'tbl_permohonan.tanggal_permohonan DESC');
        $this->db->from('tbl_kelompok_tani');
        $res = $this->db->get();
        return $res->result_array();
    }
}
