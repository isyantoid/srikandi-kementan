<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{

    private $title = 'Kabupaten';
    private $view = 'Kabupaten';

    public function __construct()
    {
        parent::__construct();
        //$this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
}