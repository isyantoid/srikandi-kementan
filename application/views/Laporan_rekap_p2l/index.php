<style>
.custom-infobox {
  color: black;
  padding: 0px;
  padding-top: 10px;
  font-size: 14px;
  text-transform: lowercase;
  font-weight: bold;
  text-align: center;
}

.custom-infobox-value {
  color: white;
  padding: 0px;
  padding-top: 10px;
  font-size: 14px;
  text-transform: lowercase;
  font-weight: bolder;
  text-align: center;
}

</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">List Data</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button type="button" id="saveButton" class="btn btn-brand btn-elevate btn-icon-sm">Export</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Alokasi (Kel)</th>
                    </thead>
                    <tbody>
                        <?php if($get_laporan): ?>
                        <?php $no=1; foreach($get_laporan as $row): ?>
                        <tr>
                            <td class="text-center"><?= $no ?></td>
                            <td>
                                <a class="font-weight-bolder"
                                    href="{segment1}rekap_provinsi/<?= $row['provinsi_kode'] ?>"><?= $row['provinsi_nama'] ?></a>
                            </td>
                            <td><?= $row['kabupaten_nama'] ?></td>
                            <td id="kodeprov-<?= $row['provinsi_kode'] ?>"><?= $row['alokasi'] ?></td>
                        </tr>
                        <?php $no++; endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>


            <div class="mt-5" id="map" style="width: 2400px; height: 800px; margin:0; padding:0;"></div>

        </div>
    </div>
</div>

<script>
    var baseUrl = '{base_url}';
</script>
<script src="https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chroma-js/2.1.1/chroma.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap" async defer></script>
<!-- <script src="{base_url}dist/json/script.js"></script> -->
<script src="{base_url}dist/json/map.js"></script>

<script>


</script>