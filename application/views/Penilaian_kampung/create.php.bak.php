<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Provinsi:</label>
                        <select class="form-control provinsi_kode" name="provinsi_kode" style="width:100%;"></select>
                        <input type="hidden" name="provinsi_nama" class="provinsi_nama">
                    </div>
                    <div class="col-md-4">
                        <label>Kabupaten:</label>
                        <select class="form-control kabupaten_kode" name="kabupaten_kode" style="width:100%;"></select>
                        <input type="hidden" name="kabupaten_nama" class="kabupaten_nama">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Kecamatan:</label>
                        <select class="form-control kecamatan_kode" name="kecamatan_kode" style="width:100%;"></select>
                        <input type="hidden" name="kecamatan_nama" class="kecamatan_nama">
                    </div>
                    <div class="col-md-4">
                        <label>Desa:</label>
                        <select class="form-control desa_kode" name="desa_kode" style="width:100%;"></select>
                        <input type="hidden" name="desa_nama" class="desa_nama">
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Kelompok Komoditas:</label>
                        <select class="form-control kelompok_komoditi_id" name="kelompok_komoditi_id" style="width:100%;"></select>
                    </div>
                    <div class="col-md-4">
                        <label>Jenis Komoditas:</label>
                        <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                    </div>
                    <div class="col-md-4">
                        <label>Sub Komoditas:</label>
                        <select class="form-control sub_komoditi_id" name="sub_komoditi_id" style="width:100%;"></select>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi:</label>
                        <select class="form-control nomor_registrasi" name="nomor_registrasi" style="width:100%;" required></select>
                    </div>
                </div>



                <div id="form_penilaian"></div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');

        ajaxSelect({
            id: '.provinsi_kode',
            url: '{site_url}ajax_selectbox/provinsi_select',
        });

        $('.provinsi_kode').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
                ajaxSelect({
                    id: '.kabupaten_kode',
                    url: '{site_url}ajax_selectbox/kabupaten_select',
                    optionalSearch: {
                        provinsi_kode: val
                    }
                });
                $('.provinsi_nama').val($(this).text());
            }
        });

        $('.provinsi_kode').change(function() {
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                get_nomor_registrasi();
            }
        });

        $('.kabupaten_kode').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                $('.kecamatan_kode, .desa_kode').empty();
                ajaxSelect({
                    id: '.kecamatan_kode',
                    url: '{site_url}ajax_selectbox/kecamatan_select',
                    optionalSearch: {
                        kabupaten_kode: val
                    }
                });
                $('.kabupaten_nama').val($(this).text());
                get_nomor_registrasi();
            }
        });

        $('.kecamatan_kode').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                $('.desa_kode').empty();
                ajaxSelect({
                    id: '.desa_kode',
                    url: '{site_url}ajax_selectbox/desa_select',
                    optionalSearch: {
                        kecamatan_kode: val
                    }
                });
                $('.kecamatan_nama').val($(this).text());
                get_nomor_registrasi();
            }
        });

        $('.desa_kode').change(function() {
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                $('.desa_nama').val($(this).text());
                get_nomor_registrasi();
            }
        });

        $('.kelompok_komoditi_id').change(function() {
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                get_nomor_registrasi();
            }
        });

        ajaxSelect({
            id: '.kelompok_komoditi_id',
            url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
        });

        $('.kelompok_komoditi_id').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                $('.jenis_komoditi_id').empty();
                ajaxSelect({
                    id: '.jenis_komoditi_id',
                    url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                    optionalSearch: {
                        kelompok_komoditi_id: val
                    }
                });
                get_nomor_registrasi();
            }
        });

        $('.jenis_komoditi_id').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if (!nomor_registrasi) {
                $('.sub_komoditi_id').empty();
                ajaxSelect({
                    id: '.sub_komoditi_id',
                    url: '{site_url}ajax_selectbox/sub_komoditi_id',
                    optionalSearch: {
                        jenis_komoditi_id: val
                    }
                });
                get_nomor_registrasi();
            }
        });

        $('.nomor_registrasi').change(function() {
            var val = $(this).val();
            $('#form_penilaian').load('{segment1}form_penilaian', {
                id_permohonan: val
            }, function() {});
        });
    });


    ajaxSelect({
        allowClear: true,
        id: '.nomor_registrasi',
        url: '{site_url}ajax_selectbox/nomor_registrasi',
    });





    $('.nomor_registrasi').change(function() {
        var val = $(this).val();

        $.ajax({
            url: '{segment1}get_permohonan/' + val,
            dataType: 'json',
            method: 'get',
            success: function(data) {
                ajaxSelect({
                    id: '.provinsi_kode',
                    url: '{site_url}ajax_selectbox/provinsi_select',
                    selected: data.provinsi_kode
                });

                $('.provinsi_kode').change(function() {
                    var val = $(this).val();
                    $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
                    ajaxSelect({
                        id: '.kabupaten_kode',
                        url: '{site_url}ajax_selectbox/kabupaten_select',
                        optionalSearch: {
                            provinsi_kode: val
                        },
                        selected: data.kabupaten_kode
                    });

                    $('.provinsi_nama').val($(this).text());
                });

                $('.kabupaten_kode').change(function() {
                    var val = $(this).val();
                    $('.kecamatan_kode, .desa_kode').empty();
                    ajaxSelect({
                        id: '.kecamatan_kode',
                        url: '{site_url}ajax_selectbox/kecamatan_select',
                        optionalSearch: {
                            kabupaten_kode: val
                        },
                        selected: data.kecamatan_kode
                    });
                    $('.kabupaten_nama').val($(this).text());
                });

                $('.kecamatan_kode').change(function() {
                    var val = $(this).val();
                    $('.desa_kode').empty();
                    ajaxSelect({
                        id: '.desa_kode',
                        url: '{site_url}ajax_selectbox/desa_select',
                        optionalSearch: {
                            kecamatan_kode: val
                        },
                        selected: data.desa_kode,
                    });
                    $('.kecamatan_nama').val($(this).text());
                });

                $('.desa_kode').change(function() {
                    $('.desa_nama').val($(this).text());
                });

                ajaxSelect({
                    id: '.kelompok_komoditi_id',
                    url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
                    selected: data.kelompok_komoditi_id,
                });

                $('.kelompok_komoditi_id').change(function() {
                    var val = $(this).val();
                    $('.jenis_komoditi_id').empty();
                    ajaxSelect({
                        id: '.jenis_komoditi_id',
                        url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                        optionalSearch: {
                            kelompok_komoditi_id: val
                        },
                        selected: data.jenis_komoditi_id,
                    });
                });

                $('.jenis_komoditi_id').change(function() {
                    var val = $(this).val();
                    var nomor_registrasi = $('.nomor_registrasi').val();
                    $('.sub_komoditi_id').empty();
                    ajaxSelect({
                        id: '.sub_komoditi_id',
                        url: '{site_url}ajax_selectbox/sub_komoditi_id',
                        optionalSearch: {
                            jenis_komoditi_id: val
                        },
                        selected: data.sub_komoditi_id,
                    });
                });
            }
        })

    });

    function get_nomor_registrasi() {
        $('#form_penilaian').html('');

        var provinsi_kode = $('.provinsi_kode').val();
        var kabupaten_kode = $('.kabupaten_kode').val();
        var kecamatan_kode = $('.kecamatan_kode').val();
        var desa_kode = $('.desa_kode').val();
        var kelompok_komoditi_id = $('.kelompok_komoditi_id').val();
        var jenis_komoditi_id = $('.jenis_komoditi_id').val();
        var sub_komoditi_id = $('.sub_komoditi_id').val();

        $('.nomor_registrasi').empty();
        ajaxSelect({
            allowClear: true,
            id: '.nomor_registrasi',
            url: '{site_url}ajax_selectbox/nomor_registrasi',
            optionalSearch: {
                provinsi_kode: provinsi_kode,
                kabupaten_kode: kabupaten_kode,
                kecamatan_kode: kecamatan_kode,
                desa_kode: desa_kode,
                kelompok_komoditi_id: kelompok_komoditi_id,
                jenis_komoditi_id: jenis_komoditi_id,
                sub_komoditi_id: sub_komoditi_id,
            }
        });
    }



    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
                btnSpinnerHide();
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>