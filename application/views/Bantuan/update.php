<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Tipe Bantuan:</label>
                        <select class="form-control tipe_bantuan_id" name="tipe_bantuan_id" style="width:100%;" required></select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi Horti:</label>
                        <div class="input-group">
                            <input type="hidden" name="nomor_registrasi" class="nomor_registrasi form-control" value="{permohonan_id}" required readonly>
                            <input type="text" name="no_reg_horti" class="no_reg_horti form-control" value="{no_reg_horti}" required readonly>
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-modal-table" data-toggle="modal" data-target="#modal-table-permohonan" type="button">Cari</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Kelompok Petani:</label>
                        <select class="form-control kelompok_tani_id" name="kelompok_tani_id" style="width:100%;" required></select>
                    </div>
                </div>


                <div class="detail-permohonan" hidden>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Provinsi:</label>
                            <input type="text" class="form-control provinsi" disabled>
                        </div>
                        <div class="col-md-4">
                            <label>Kabupaten:</label>
                            <input type="text" class="form-control kabupaten" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Kecamatan:</label>
                            <input type="text" class="form-control kecamatan" disabled>
                        </div>
                        <div class="col-md-4">
                            <label>Desa:</label>
                            <input type="text" class="form-control desa" disabled>
                        </div>
                    </div>
    
    
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Kelompok Komoditas:</label>
                            <input type="text" class="form-control kelompok_komoditi" disabled>
                        </div>
                        <div class="col-md-4">
                            <label>Jenis Komoditas:</label>
                            <input type="text" class="form-control jenis_komoditi" disabled>
                        </div>
                        <div class="col-md-4">
                            <label>Sub Komoditas:</label>
                            <input type="text" class="form-control sub_komoditi" disabled>
                        </div>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>TOTAL NILAI BANTUAN:</label>
                        <input type="text" class="form-control form-control-lg total_nilai_bantuan" name="total_nilai_bantuan" required placeholder="0" readonly value="<?= number_format($total_nilai_bantuan) ?>">
                    </div>
                </div>


                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless table_kelompok">
                                <tbody>

                                    <?php $bantuan_detail = getResultArray('tbl_bantuan_detail', array('bantuan_id' => $id)) ?>
                                    <?php if($bantuan_detail): ?>
                                        <?php $no=1; foreach($bantuan_detail as $row) : ?>
                                            <tr class="header bg-light" data-header="<?= $no ?>">
                                                <input type="hidden" required class="form-control" name="id[]" value="<?= $row['id'] ?>">
                                                <input type="hidden" required class="form-control" name="nomor[]" value="<?= $no ?>">
                                                <td class="text-center">
                                                    <a href="javascript:(0)" class="text-danger btn_delete_bantuan" title="delete bantuan"><i class="fa fa-trash"></i></a>
                                                </td>
                                                <td> <label class="small pl-1">Tanggal Bantuan</label> <input type="text" required class="form-control tanggal_bantuan" placeholder="Tanggal Bantuan" name="tanggal_bantuan[]" value="<?= $row['tanggal_bantuan'] ?>"> </td>
                                                <td> <label class="small pl-1">Jenis Bantuan</label> <select required class="form-control jenis_bantuan_id jenis_bantuan_id_<?= $no ?>" data-val="<?= $row['jenis_bantuan_id'] ?>" name="jenis_bantuan_id[]" style="width:100%;"></select> </td>
                                                <script>
                                                    ajaxSelect({
                                                        id: '.jenis_bantuan_id_<?= $no ?>',
                                                        url: '{site_url}ajax_selectbox/jenis_bantuan_id',
                                                        selected: '<?= $row['jenis_bantuan_id'] ?>'
                                                    });
                                                </script>
                                                
                                                <td> <label class="small pl-1">Satuan</label> <input type="text" required class="form-control satuan" placeholder="Satuan" name="satuan[]" readonly value="<?= $row['satuan'] ?>"> </td>
                                            </tr>
                                            <tr class="header bg-light" data-header="<?= $no ?>">
                                                <td>&nbsp;</td>
                                                <td> <label class="small pl-1">Jumlah</label> <input type="text" required class="form-control jumlah_detail" placeholder="Jumlah" name="jumlah[]" value="<?= number_format($row['jumlah']) ?>"> </td>
                                                <td> <label class="small pl-1">Harga</label> <input type="text" required class="form-control harga_detail harga_detail_<?= $no ?>" readonly placeholder="Harga satuan" name="harga[]" value="<?= number_format($row['harga']) ?>"> </td>
                                                <td> <label class="small pl-1">Total</label> <input type="text" required class="form-control total_detail" readonly placeholder="Total nilai" name="total[]" value="<?= number_format($row['total']) ?>"> </td>
                                                <td>
                                                    <label class="small pl-1">Foto</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="foto_detail_<?= $no ?>" id="customFile">
                                                        <label class="custom-file-label text-left" for="customFile">Foto bantuan</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $no++ ?>
                                        <?php endforeach ?>
                                    <?php else : ?>
                                        <tr class="header bg-light" data-header="1">
                                            <input type="hidden" required class="form-control" name="nomor[]" value="1">
                                            <td class="text-center">
                                                <a href="javascript:(0)" class="text-danger btn_delete_bantuan" title="delete bantuan"><i class="fa fa-trash"></i></a>
                                            </td>
                                            <td> <label class="small pl-1">Tanggal Bantuan</label> <input type="text" required class="form-control tanggal_bantuan" placeholder="Tanggal Bantuan" name="tanggal_bantuan[]"> </td>
                                            <td> <label class="small pl-1">Jenis Bantuan</label> <select required class="form-control jenis_bantuan_id" name="jenis_bantuan_id[]" style="width:100%;" required></select> </td>
                                            <td> <label class="small pl-1">Satuan</label> <input type="text" required class="form-control satuan" placeholder="Satuan" name="satuan[]" readonly> </td>
                                        </tr>
                                        <tr class="header bg-light" data-header="1">
                                            <td>&nbsp;</td>
                                            <td> <label class="small pl-1">Jumlah</label> <input type="text" required class="form-control jumlah_detail" placeholder="Jumlah" name="jumlah[]"> </td>
                                            <td> <label class="small pl-1">Harga</label> <input type="text" required class="form-control harga_detail harga_detail_1" readonly placeholder="Harga satuan" name="harga[]"> </td>
                                            <td> <label class="small pl-1">Total</label> <input type="text" required class="form-control total_detail" readonly placeholder="Total nilai" name="total[]"> </td>
                                            <td>
                                                <label class="small pl-1">Foto</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="foto_detail_1" id="customFile">
                                                    <label class="custom-file-label text-left" for="customFile">Foto bantuan</label>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endif ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info btn_add_kelompok">Tambah Data</button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <a href="{segment1}" class="btn btn-secondary">Batal</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<div class="modal fade" id="modal-table-permohonan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrasi Kampung</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-light alert-elevate" role="alert">
                    <div class="alert-icon"><i class="flaticon-info kt-font-info"></i></div>
                    <div class="alert-text">Klik baris pada table di bawah ini untuk memilih nomor registrasi horti! Gunakan fitur pencarian untuk memudahkan menemukan data.</div>
                </div>

                <table class="table table-sm table-bordered" id="table-permohonan">
                    <thead>
                        <th>#</th>
                        <th>#</th>
                        <th>Nomor Registrasi Horti</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kelompok Komoditas</th>
                        <th>Jenis Komoditas</th>
                        <th>Luas</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

    var table;

    datatable()

    function datatable() {
        table = $('#table-permohonan').DataTable({
            ajax: {
                url: '{segment1}datatable_permohonan',
                method: 'post',
            },
            select: 'single',
            stateSave: true,
            responsive: true,
            serverSide: true,
            lengthMenu: [5, 10, 25, 50],
            pageLength: 25,
            destroy: true,
            order: [
                [0, 'desc']
            ],
            columns: [{
                    data: 'id',
                    visible: false,
                },
                {
                    data: 'id',
                    width: 25,
                    className: 'text-center'
                },
                { data: 'no_reg_horti', className: 'text-left' },
                { data: 'provinsi_nama', className: 'text-left' },
                { data: 'kabupaten_nama', className: 'text-left' },
                { data: 'kelompok_komoditi_nama' },
                { data: 'jenis_komoditi_nama' },
                { 
                    data: 'luas_kebun', className: 'text-center',
                    render: function(data, type, row) {
                        return data + row.satuan_luas_kebun;
                    }
                },
            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var index = iDisplayIndex + 1;
                $('td:eq(0)', nRow).html(index);
                return nRow;
            }
        });
        
    }


    table.on( 'select', function ( e, dt, type, indexes ) {
        var rowData = table.rows( indexes ).data().toArray();
        var id = rowData[0].id;
        $('.nomor_registrasi').val(id)
        $('.no_reg_horti').val(rowData[0].no_reg_horti)
        $('.kelompok_tani_id').empty().val('')
        ajaxSelect({
            id: '.kelompok_tani_id',
            url: '{site_url}ajax_selectbox/kelompok_tani_id',
            optionalSearch: {
                id_permohonan: id
            }
        });

        $('#modal-table-permohonan').modal('hide')
    });

    ajaxSelect({
        id: '.kelompok_tani_id',
        url: '{site_url}ajax_selectbox/kelompok_tani_id',
        optionalSearch: {
            id_permohonan: '{permohonan_id}'
        },
        selected: '{kelompok_tani_id}',
    });

    ajaxSelect({
        id: '.tipe_bantuan_id',
        url: '{site_url}ajax_selectbox/tipe_bantuan_id',
        selected: '{tipe_bantuan_id}'
    });

    $('.tanggal_bantuan').datepicker({
        format: 'yyyy-mm-dd',
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        autoclose: true
    });




    var tableKelompokTani = $('.table_kelompok');
    // get_total_head()
    

    $('.btn_add_kelompok').click(function() {

        var nomorHeader = new Date().getTime();

        var html = `<tr class="header bg-light" data-header="` + nomorHeader + `">
        <input type="hidden" required class="form-control" name="id[]" value="">
        <input type="hidden" required class="form-control" name="nomor[]" value="` + nomorHeader + `">
        <td class="text-center">
            <a href="javascript:(0)" class="text-danger btn_delete_bantuan" title="delete bantuan"><i class="fa fa-trash"></i></a>
        </td>
        <td> <label class="small pl-1">Tanggal Bantuan</label> <input type="text" required class="form-control tanggal_bantuan" placeholder="Tanggal Bantuan" name="tanggal_bantuan[]"> </td>
        <td> <label class="small pl-1">Jenis Bantuan</label> <select required class="form-control jenis_bantuan_id" name="jenis_bantuan_id[]" style="width:100%;" required></select> </td>
        <td> <label class="small pl-1">Satuan</label> <input type="text" required class="form-control satuan" placeholder="Satuan" name="satuan[]" readonly> </td>
        </tr>
        <tr class="header bg-light" data-header="` + nomorHeader + `">
        <td>&nbsp;</td>
        <td> <label class="small pl-1">Jumlah</label> <input type="text" required class="form-control jumlah_detail" placeholder="Jumlah" name="jumlah[]"> </td>
        <td> <label class="small pl-1">Harga</label> <input type="text" required class="form-control harga_detail harga_detail_`+nomorHeader+`" placeholder="Harga satuan" name="harga[]"> </td>
        <td> <label class="small pl-1">Total</label> <input type="text" required class="form-control total_detail" readonly placeholder="Total nilai" name="total[]"> </td>
        <td>
        <label class="small pl-1">Foto</label>
        <div class="custom-file">
        <input type="file" class="custom-file-input" id="customFile" name="foto_detail_` + nomorHeader + `">
        <label class="custom-file-label text-left" for="customFile">Foto bantuan</label>
        </div>
        </td>
        </tr>`;

        tableKelompokTani.find('tbody').append(html);

        ajaxSelect({
            id: '.jenis_bantuan_id',
            url: '{site_url}ajax_selectbox/jenis_bantuan_id',
        });

        $('.tanggal_bantuan').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });
    });

    tableKelompokTani.on('keyup', '.jumlah_detail', function() {
        var row = $(this).closest('tr');
        var val = numeral($(this).val()).format();
        $(this).val(val);
        get_total_detail(row);
    });
    tableKelompokTani.on('keyup', '.harga_detail', function() {
        var row = $(this).closest('tr');
        var val = numeral($(this).val()).format();
        $(this).val(val);
        get_total_detail(row);
    });

    tableKelompokTani.on('click', '.btn_delete_bantuan', function() {
        var row = $(this).closest('tr');
        var nomorHeader = row.data('header');
        tableKelompokTani.find('tbody tr[data-header=' + nomorHeader + ']').each(function() {
            $(this).remove();
        });
        get_total_head();
    });


    function get_total_detail(row) {
        var harga_detail = numeral(row.find('.harga_detail').val()).value();
        var jumlah_detail = numeral(row.find('.jumlah_detail').val()).value();
        row.find('.total_detail').val(numeral(harga_detail * jumlah_detail).format());
        get_total_head();
    }

    function get_total_head() {
        var sum = 0;
        tableKelompokTani.find('tbody tr').filter(function() {
            var total_detail = numeral($(this).closest('tr').find('input.total_detail').val()).value();
            total_detail = isNaN(total_detail) ? 0 : total_detail;
            sum += total_detail;
        });
        $('.total_nilai_bantuan').val(numeral(sum).format());
    } 


    tableKelompokTani.on('change', '.jenis_bantuan_id', function() {
        var row = $(this).closest('tr');
        var text = $(':selected', this).text();
        var val = $(':selected', this).val();
        var arr = text.split(' - ');
        row.find('.satuan').val(arr[1]);

        $.ajax({
            url: '{site_url}bantuan/get_harga_jenis_bantuan/' + val,
            method: 'GET',
            dataType: 'JSON',
            success: function(res) {
                var nomorHeader = row.data('header');
                $('.harga_detail_' + nomorHeader).val(res.harga_satuan) 
                // tableKelompokTani.find('tbody tr[data-header=' + nomorHeader + ']').each(function() {
                //     $('.harga_detail').val(res.harga_satuan) 
                // });

            }
        });
    });


    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save/{id}',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
                btnSpinnerHide();
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }


</script>