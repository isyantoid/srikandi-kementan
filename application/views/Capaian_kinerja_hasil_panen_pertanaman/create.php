
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi P2L:</label>
                        <select class="form-control registrasi_p2l_id" name="registrasi_p2l_id" style="width:100%;"
                            required></select>
                    </div>
                    <div class="col-md-4">
                        <label>Tanggal Registrasi P2L:</label>
                        <input type="text" class="form-control tanggal_registrasi_p2l" name="tanggal_registrasi_p2l"
                            disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nama Kelompok Tani:</label>
                        <input type="text" class="form-control nama_kelompok_tani" name="nama_kelompok_tani" readonly>
                    </div>
                    <div class="col-md-4">
                        <label>Nama Ketua:</label>
                        <input type="text" class="form-control nama_ketua" name="nama_ketua" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Komoditas:</label>
                        <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"
                            required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Tanggal Capaian Kinerja:</label>
                        <input type="text" class="form-control tanggal_capaian_kinerja" name="tanggal_capaian_kinerja" required>
                    </div>
                    <div class="col-md-4">
                        <label>Jumlah:</label>
                        <input type="number" class="form-control jumlah" name="jumlah" required>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>
$(document).ready(function() {

    $(document).on('change', '.registrasi_p2l_id', function() {
        var val = $(this).val();
        $.ajax({
            url: '{segment1}detail_registrasi/' + val,
            dataType: 'json',
            method: 'post',
            success: function(res) {
                $('.tanggal_registrasi_p2l').val(res.tanggal_registrasi_p2l)
                $('.nama_kelompok_tani').val(res.nama_kelompok)
                $('.nama_ketua').val(res.nama_ketua)

                setTimeout(() => {
                    
                    ajaxSelect({
                        id: '.jenis_komoditi_id',
                        url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                    });
                }, 100);

            }
        });
    })
    

    $('.tanggal_capaian_kinerja')
        .datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });

    $('.jenis_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');


    ajaxSelect({
        id: '.registrasi_p2l_id',
        url: '{site_url}ajax_selectbox/registrasi_p2l_id',
    });
    $('.kelompok_tani_id').change(function() {
        var val = $(this).val();
        if (val) {
            $.ajax({
                url: '{segment1}get_nama_ketua_kelompok_tani/' + val,
                method: 'post',
                dataType: 'json',
                success: function(res) {
                    $('.nama_ketua').val(res.nama_ketua);
                }
            });
        }
    });
})


function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
            btnSpinnerHide();
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>