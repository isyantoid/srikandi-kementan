<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}" class="btn btn-danger btn-elevate btn-icon-sm">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-borderless table-hover table-checkable">
                <tbody>
                    <tr>
                        <td>Tanggal Kirim</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_kirim}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>:</td>
                        <td class="font-weight-bold">{provinsi_nama}</td>
                    </tr>
                    <tr>
                        <td>Kabupaten</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kabupaten_nama}</td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kecamatan_nama}</td>
                    </tr>
                    <tr>
                        <td>Desa</td>
                        <td>:</td>
                        <td class="font-weight-bold">{desa_nama}</td>
                    </tr>
                    <tr>
                        <td>Nama Kelompok</td>
                        <td>:</td>
                        <td class="font-weight-bold">{nama_kelompok}</td>
                    </tr>
                    <tr>
                        <td>Total Nilai Bantuan</td>
                        <td>:</td>
                        <td class="font-weight-bold">Rp. <?php echo number_format($total_nilai_bantuan) ?></td>
                    </tr>
                </tbody>
            </table>

            <div class="alert alert-secondary" role="alert">
                <div class="alert-icon"><i class="flaticon-info kt-font-brand"></i></div>
                <div class="alert-text font-weight-bold">Detail Bantuan</div>
            </div>

            <table class="table table-bordered">
                <thead>
                    <th class="text-center">Foto</th>
                    <th>Tanggal Bantuan</th>
                    <th>Jenis Bantuan</th>
                    <th>Satuan</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    <?php if($detail_bantuan): ?>
                        <?php foreach($detail_bantuan as $k): ?>
                            <tr>
                                <?php if($k['foto']) : ?>
                                    <td class="text-center">
                                        <a href="javascript:view_image('<?php echo getImage('bantuan/' . $k['foto']) ?>')">
                                            <img src="<?php echo getImage('bantuan/' . $k['foto']) ?>" width="48">
                                        </a>
                                    </td>
                                <?php else : ?>
                                    <td class="text-center"><img src="<?php echo base_url('dist/images/no_image.png') ?>" width="48"></td>
                                <?php endif ?>
                                <td><?php echo $k['tanggal_bantuan'] ?></td>
                                <td><?php echo $k['nama_bantuan'] ?></td>
                                <td><?php echo $k['satuan'] ?></td>
                                <td><?php echo number_format($k['jumlah']) ?></td>
                                <td><?php echo number_format($k['harga']) ?></td>
                                <td><?php echo number_format($k['total']) ?></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                </tbody>
            </table>
            
            <!--end: Datatable -->

        </div>
    </div>
</div>
<!-- end:: Content -->

<div class="modal fade" id="modal_view_image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <img src="#" class="view_image" width="100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

function view_image(src) {
    $('#modal_view_image').modal('show');
    $('.view_image').attr('src', src);
}
    
var table;

table = $('#kt_table_1').DataTable({
    ajax: {
        url: '{segment1}datatable'
    },
    responsive: true,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 10,
    order: [
        [0, 'desc']
    ],
    columns: [{
            data: 'id',
            visible: false
        },
        {
            data: 'id',
            width: 25,
            className: 'text-center'
        },
        { data: 'nomor_registrasi' },
        { data: 'tanggal_permohonan' },
        { data: 'kelompok_komoditi_nama' },
        { data: 'jenis_komoditi_nama' },
        { data: 'sub_komoditi_nama' },
        { 
            data: 'luas_kebun',
            render: function(data, type, row) {
                return data + ' ' + row.satuan_luas_kebun;
            }
        },
        {
            targets: -1,
            width: 100,
            title: 'Actions',
            className: 'text-center',
            orderable: false,
            render: function(data, type, row, meta) {
                aksi = '<span class="dropdown">';
                aksi +=
                    '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">';
                aksi += '<i class="la la-ellipsis-h"></i>';
                aksi += '</a>';
                aksi += '<div class="dropdown-menu dropdown-menu-right">';
                aksi += '<a class="dropdown-item" href="{segment1}detail/' + row.id + '"><i class="la la-eye"></i> Detail</a>';
                aksi += '<a class="dropdown-item" href="javascript:deleteData(' + row.id + ')"><i class="la la-trash"></i> Delete</a>';
                aksi += '</div>';
                aksi += '</span>';
                return aksi;
            },
        },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        var index = iDisplayIndex + 1;
        $('td:eq(0)', nRow).html(index);
        return nRow;
    }
});

function deleteData(id) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data yang telah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus data!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '{segment1}delete/' + id
            });
            table.ajax.reload();
            Swal.fire('Sukses!', 'Data berhasil dihapus.', 'success');
        }
    });
}
</script>