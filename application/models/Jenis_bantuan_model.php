<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_bantuan_model extends CI_Model
{

    private $tableName = 'tbl_jenis_bantuan';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['kode_jenis'] != $this->input->post('kode_jenis')) {
                    if (isDuplicate($this->tableName, 'kode_jenis', $this->input->post('kode_jenis'))) {
                        return jsonOutputError('Kode Jenis sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate($this->tableName, 'kode_jenis', $this->input->post('kode_jenis'))) {
                    return jsonOutputError('Kode Jenis sudah ada sebelumnya.');
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}