<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Tipe Bantuan:</label>
                        <select class="form-control tipe_bantuan_id" name="tipe_bantuan_id" style="width:100%;" required></select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Provinsi:</label>
                        <select class="form-control provinsi_kode" name="provinsi_kode" style="width:100%;" required></select>
                    </div>
                    <div class="col-md-4">
                        <label>Kabupaten:</label>
                        <select class="form-control kabupaten_kode" name="kabupaten_kode" style="width:100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Kecamatan:</label>
                        <select class="form-control kecamatan_kode" name="kecamatan_kode" style="width:100%;" required></select>
                    </div>
                    <div class="col-md-4">
                        <label>Desa:</label>
                        <select class="form-control desa_kode" name="desa_kode" style="width:100%;" required></select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Kelompok Komoditas:</label>
                        <select class="form-control kelompok_komoditi_id" name="kelompok_komoditi_id" style="width:100%;" required></select>
                    </div>
                    <div class="col-md-4">
                        <label>Jenis Komoditas:</label>
                        <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;" required></select>
                    </div>
                    <div class="col-md-4">
                        <label>Sub Komoditas:</label>
                        <select class="form-control sub_komoditi_id" name="sub_komoditi_id" style="width:100%;" required></select>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi:</label>
                        <select class="form-control nomor_registrasi" name="nomor_registrasi" style="width:100%;" required></select>
                    </div>
                    <div class="col-md-4">
                        <label>Kelompok Tani:</label>
                        <select class="form-control kelompok_tani_id" name="kelompok_tani_id" style="width:100%;" required></select>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>TOTAL NILAI BANTUAN:</label>
                        <input type="text" class="form-control form-control-lg total_nilai_bantuan" name="total_nilai_bantuan" value="" required placeholder="0" readonly>
                    </div>
                </div>


                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless table_kelompok">
                                <tbody>

                                    <tr class="header bg-light" data-header="1">
                                        <input type="hidden" required class="form-control" name="nomor[]" value="1">
                                        <td class="text-center">
                                            <a href="javascript:(0)" class="text-danger btn_delete_bantuan" title="delete bantuan"><i class="fa fa-trash"></i></a>
                                        </td>
                                        <td> <label class="small pl-1">Tanggal Bantuan</label> <input type="text" required class="form-control tanggal_bantuan" placeholder="Tanggal Bantuan" name="tanggal_bantuan[]"> </td>
                                        <td> <label class="small pl-1">Jenis Bantuan</label> <select required class="form-control jenis_bantuan_id" name="jenis_bantuan_id[]" style="width:100%;" required></select> </td>
                                        <td> <label class="small pl-1">Satuan</label> <input type="text" required class="form-control satuan" placeholder="Satuan" name="satuan[]" readonly> </td>
                                    </tr>
                                    <tr class="header bg-light" data-header="1">
                                        <td>&nbsp;</td>
                                        <td> <label class="small pl-1">Jumlah</label> <input type="text" required class="form-control jumlah_detail" placeholder="Jumlah" name="jumlah[]"> </td>
                                        <td> <label class="small pl-1">Harga</label> <input type="text" required class="form-control harga_detail" placeholder="Harga satuan" name="harga[]"> </td>
                                        <td> <label class="small pl-1">Total</label> <input type="text" required class="form-control total_detail" placeholder="Total nilai" name="total[]"> </td>
                                        <td>
                                            <label class="small pl-1">Foto</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="foto_detail_1" id="customFile">
                                                <label class="custom-file-label text-left" for="customFile">Foto bantuan</label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info btn_add_kelompok">Tambah Data</button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <a href="{segment1}" class="btn btn-secondary">Batal</a>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->


<script>
    $(document).ready(function() {


        $('.tanggal_bantuan').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });


        $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id, .kelompok_tani_id').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');


        ajaxSelect({
            allowClear: true,
            id: '.nomor_registrasi',
            url: '{site_url}ajax_selectbox/no_reg_horti',
        });

        ajaxSelect({
            id: '.provinsi_kode',
            url: '{site_url}ajax_selectbox/provinsi_select',
        });

        $('.provinsi_kode').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if(!nomor_registrasi) {
                $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
                ajaxSelect({
                    id: '.kabupaten_kode',
                    url: '{site_url}ajax_selectbox/kabupaten_select',
                    optionalSearch: {
                        provinsi_kode: val
                    }
                });
    
                $('.provinsi_nama').val($(this).text());
                get_nomor_registrasi();
            }
        });

        $('.kabupaten_kode').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if(!nomor_registrasi) {
                $('.kecamatan_kode, .desa_kode').empty();
                ajaxSelect({
                    id: '.kecamatan_kode',
                    url: '{site_url}ajax_selectbox/kecamatan_select',
                    
                    optionalSearch: {
                        kabupaten_kode: val
                    }
                });
                $('.kabupaten_nama').val($(this).text());
            }
        });

        $('.kecamatan_kode').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if(!nomor_registrasi) {
                $('.desa_kode').empty();
                ajaxSelect({
                    id: '.desa_kode',
                    url: '{site_url}ajax_selectbox/desa_select',
                    optionalSearch: {
                        kecamatan_kode: val
                    }
                });
                $('.kecamatan_nama').val($(this).text());
            }
        });

        $('.desa_kode').change(function() {
            var nomor_registrasi = $('.nomor_registrasi').val();
            if(!nomor_registrasi) {
                $('.desa_nama').val($(this).text());
            }
        });

        ajaxSelect({
            id: '.kelompok_komoditi_id',
            url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
        });

        $('.kelompok_komoditi_id').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if(!nomor_registrasi) {
                $('.jenis_komoditi_id').empty();
                ajaxSelect({
                    id: '.jenis_komoditi_id',
                    url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                    optionalSearch: {
                        kelompok_komoditi_id: val
                    }
                });
                get_nomor_registrasi();
            }
        });

        $('.jenis_komoditi_id').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            if(!nomor_registrasi) {
                $('.sub_komoditi_id').empty();
                ajaxSelect({
                    id: '.sub_komoditi_id',
                    url: '{site_url}ajax_selectbox/sub_komoditi_id',
                    optionalSearch: {
                        jenis_komoditi_id: val
                    }
                });
                get_nomor_registrasi();
            }
        });

        $('.nomor_registrasi').change(function() {
            var val = $(this).val();
            var nomor_registrasi = $('.nomor_registrasi').val();
            $('.kelompok_tani_id').empty();
            ajaxSelect({
                id: '.kelompok_tani_id',
                url: '{site_url}ajax_selectbox/kelompok_tani_id',
                optionalSearch: {
                    id_permohonan: val
                }
            });
        });

        ajaxSelect({
            id: '.tipe_bantuan_id',
            url: '{site_url}ajax_selectbox/tipe_bantuan_id',
        });

        ajaxSelect({
            id: '.jenis_bantuan_id',
            url: '{site_url}ajax_selectbox/jenis_bantuan_id',
        });


        $('.nomor_registrasi').change(function(){
            var val = $(this).val();
            
            $.ajax({
                url: '{segment1}get_permohonan/' + val,
                dataType: 'json',
                method: 'get',
                success: function(data) {
                    ajaxSelect({
                        id: '.provinsi_kode',
                        url: '{site_url}ajax_selectbox/provinsi_select',
                        selected: data.provinsi_kode
                    });

                    $('.provinsi_kode').change(function() {
                        var val = $(this).val();
                        $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
                        ajaxSelect({
                            id: '.kabupaten_kode',
                            url: '{site_url}ajax_selectbox/kabupaten_select',
                            optionalSearch: {
                                provinsi_kode: val
                            },
                            selected: data.kabupaten_kode
                        });

                        $('.provinsi_nama').val($(this).text());
                    });

                    $('.kabupaten_kode').change(function() {
                        var val = $(this).val();
                        $('.kecamatan_kode, .desa_kode').empty();
                        ajaxSelect({
                            id: '.kecamatan_kode',
                            url: '{site_url}ajax_selectbox/kecamatan_select',
                            optionalSearch: {
                                kabupaten_kode: val
                            },
                            selected: data.kecamatan_kode
                        });
                        $('.kabupaten_nama').val($(this).text());
                    });

                    $('.kecamatan_kode').change(function() {
                        var val = $(this).val();
                        $('.desa_kode').empty();
                        ajaxSelect({
                            id: '.desa_kode',
                            url: '{site_url}ajax_selectbox/desa_select',
                            optionalSearch: {
                                kecamatan_kode: val
                            },
                            selected: data.desa_kode,
                        });
                        $('.kecamatan_nama').val($(this).text());
                    });

                    $('.desa_kode').change(function() {
                        $('.desa_nama').val($(this).text());
                    });

                    ajaxSelect({
                        id: '.kelompok_komoditi_id',
                        url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
                        selected: data.kelompok_komoditi_id,
                    });

                    $('.kelompok_komoditi_id').change(function() {
                        var val = $(this).val();
                        $('.jenis_komoditi_id').empty();
                        ajaxSelect({
                            id: '.jenis_komoditi_id',
                            url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                            optionalSearch: {
                                kelompok_komoditi_id: val
                            },
                            selected: data.jenis_komoditi_id,
                        });
                    });

                    $('.jenis_komoditi_id').change(function() {
                        var val = $(this).val();
                        var nomor_registrasi = $('.nomor_registrasi').val();
                        $('.sub_komoditi_id').empty();
                        ajaxSelect({
                            id: '.sub_komoditi_id',
                            url: '{site_url}ajax_selectbox/sub_komoditi_id',
                            optionalSearch: {
                                jenis_komoditi_id: val
                            },
                            selected: data.sub_komoditi_id,
                        });
                    });
                }
            })

        });

        var tableKelompokTani = $('.table_kelompok');
        $('.btn_add_kelompok').click(function() {

            var nomorHeader = new Date().getTime();

            var html = `<tr class="header bg-light" data-header="` + nomorHeader + `">
            <input type="hidden" required class="form-control" name="nomor[]" value="` + nomorHeader + `">
            <td class="text-center">
                <a href="javascript:(0)" class="text-danger btn_delete_bantuan" title="delete bantuan"><i class="fa fa-trash"></i></a>
            </td>
            <td> <label class="small pl-1">Tanggal Bantuan</label> <input type="text" required class="form-control tanggal_bantuan" placeholder="Tanggal Bantuan" name="tanggal_bantuan[]"> </td>
            <td> <label class="small pl-1">Jenis Bantuan</label> <select required class="form-control jenis_bantuan_id" name="jenis_bantuan_id[]" style="width:100%;" required></select> </td>
            <td> <label class="small pl-1">Satuan</label> <input type="text" required class="form-control satuan" placeholder="Satuan" name="satuan[]" readonly> </td>
            </tr>
            <tr class="header bg-light" data-header="` + nomorHeader + `">
            <td>&nbsp;</td>
            <td> <label class="small pl-1">Jumlah</label> <input type="text" required class="form-control jumlah_detail" placeholder="Jumlah" name="jumlah[]"> </td>
            <td> <label class="small pl-1">Harga</label> <input type="text" required class="form-control harga_detail" placeholder="Harga satuan" name="harga[]"> </td>
            <td> <label class="small pl-1">Total</label> <input type="text" required class="form-control total_detail" placeholder="Total nilai" name="total[]"> </td>
            <td>
            <label class="small pl-1">Foto</label>
            <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile" name="foto_detail_` + nomorHeader + `">
            <label class="custom-file-label text-left" for="customFile">Foto bantuan</label>
            </div>
            </td>
            </tr>`;

            tableKelompokTani.find('tbody').append(html);

            ajaxSelect({
                id: '.jenis_bantuan_id',
                url: '{site_url}ajax_selectbox/jenis_bantuan_id',
            });
            $('.tanggal_bantuan').datepicker({
                format: 'yyyy-mm-dd',
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true
            });
        });
    });

    var tableKelompokTani = $('.table_kelompok');


    tableKelompokTani.on('change', '.jenis_bantuan_id', function() {
        var row = $(this).closest('tr');
        var val = $(':selected', this).text();
        var arr = val.split(' - ');
        row.find('.satuan').val(arr[1]);
    });

    tableKelompokTani.on('keyup', '.jumlah_detail', function() {
        var row = $(this).closest('tr');
        var val = numeral($(this).val()).format();
        $(this).val(val);
        get_total_detail(row);
    });
    tableKelompokTani.on('keyup', '.harga_detail', function() {
        var row = $(this).closest('tr');
        var val = numeral($(this).val()).format();
        $(this).val(val);
        get_total_detail(row);
    });

    tableKelompokTani.on('click', '.btn_delete_bantuan', function() {
        var row = $(this).closest('tr');
        var nomorHeader = row.data('header');
        tableKelompokTani.find('tbody tr[data-header=' + nomorHeader + ']').each(function() {
            $(this).remove();
        });
        get_total_head();
    });


    function get_total_detail(row) {
        var harga_detail = numeral(row.find('.harga_detail').val()).value();
        var jumlah_detail = numeral(row.find('.jumlah_detail').val()).value();
        row.find('.total_detail').val(numeral(harga_detail * jumlah_detail).format());
        get_total_head();
    }

    function get_total_head() {
        var sum = 0;
        tableKelompokTani.find('tbody tr').filter(function() {
            var total_detail = numeral($(this).closest('tr').find('input.total_detail').val()).value();
            total_detail = isNaN(total_detail) ? 0 : total_detail;
            sum += total_detail;
        });
        $('.total_nilai_bantuan').val(numeral(sum).format());
    }    

    function get_nomor_registrasi() {
        $('#form_penilaian').html('');

        var provinsi_kode = $('.provinsi_kode').val();
        var kabupaten_kode = $('.kabupaten_kode').val();
        var kecamatan_kode = $('.kecamatan_kode').val();
        var desa_kode = $('.desa_kode').val();
        var kelompok_komoditi_id = $('.kelompok_komoditi_id').val();
        var jenis_komoditi_id = $('.jenis_komoditi_id').val();
        var sub_komoditi_id = $('.sub_komoditi_id').val();

        $('.nomor_registrasi').empty();
        ajaxSelect({
            allowClear: true,
            id: '.nomor_registrasi',
            url: '{site_url}ajax_selectbox/no_reg_horti',
            optionalSearch: {
                provinsi_kode: provinsi_kode,
                kabupaten_kode: kabupaten_kode,
                kecamatan_kode: kecamatan_kode,
                desa_kode: desa_kode,
                kelompok_komoditi_id: kelompok_komoditi_id,
                jenis_komoditi_id: jenis_komoditi_id,
                sub_komoditi_id: sub_komoditi_id,
            }
        });
    }    

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
                btnSpinnerHide();
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }

    function generate_nomor_registrasi() {
        var static = 'RK.01.';
        var kabupaten_kode = $('.kabupaten_kode').val();
        var kelompok_komoditi_id = romawiKelompokKomoditi($('.kelompok_komoditi_id').val());
        var jenis_komoditi_id = ($('.jenis_komoditi_id').val()) ? $('.jenis_komoditi_id').val() : '';
        var year = new Date().getFullYear().toString().substr(-2);
        var nomor_registrasi = static + kabupaten_kode + '.' + kelompok_komoditi_id + '.' + jenis_komoditi_id + '.' + year;

        $.ajax({
            url: '{segment1}get_nomor_urut_registrasi',
            dataType: 'json',
            method: 'post',
            data: {
                search: nomor_registrasi
            },
            success: function(res) {
                nomor_registrasi += '.' + res.nomor_urut;
                $('.nomor_registrasi').val(nomor_registrasi);
            }
        })

    }
</script>