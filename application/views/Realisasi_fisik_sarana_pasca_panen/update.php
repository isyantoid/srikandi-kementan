<style>
.pac-container {
    z-index: 1050;
    position: fixed !important;
    top: 15% !important;
}
</style>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi P2L:</label>
                        <select class="form-control registrasi_p2l_id" name="registrasi_p2l_id" style="width:100%;"
                            disabled></select>
                    </div>
                    <div class="col-md-4">
                        <label>Tanggal Registrasi P2L:</label>
                        <input type="text" class="form-control tanggal_registrasi_p2l" name="tanggal_registrasi_p2l"
                            disabled>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nama Kelompok Tani:</label>
                        <input type="text" class="form-control nama_kelompok_tani" name="nama_kelompok_tani" disabled>
                    </div>
                    <div class="col-md-4">
                        <label>Nama Ketua:</label>
                        <input type="text" class="form-control nama_ketua" name="nama_ketua" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Tanggal Bantuan:</label>
                        <select class="form-control bantuan_p2l_id" name="bantuan_p2l_id" style="width:100%;"
                            disabled></select>
                    </div>
                    <div class="col-md-4">
                        <label>Nilai Bantuan:</label>
                        <input type="text" class="form-control nilai_bantuan" name="nilai_bantuan" disabled>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit">
                </div>
                <div class="mt-3 form_detail"></div>
                <!-- <table class="table" id="realisasi_fisik_sarana_pembenihan">
                    <tbody></tbody>
                </table> -->
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>
$(document).ready(function() {

    generate_form_realisasi('.form_detail', 0);

    function generate_form_realisasi(elementID, btnDelete) {
        $.ajax({
            url: '{segment1}generate_form/{id}?btnDelete=' + btnDelete,
            method: 'get',
            success: function(html) {
                $(elementID).append(html)
            }
        });
    }

    $(document).on('change', '.bantuan_p2l_id', function() {
        var val = $('option:selected', this).text();
        if (val) {
            var splitText = val.split(' | ');
            $('.nilai_bantuan').val(splitText[1]);
        }
    });

    $(document).on('change', '.registrasi_p2l_id', function() {
        var val = $(this).val();
        $.ajax({
            url: '{segment1}detail_registrasi/' + val,
            dataType: 'json',
            method: 'post',
            success: function(res) {
                $('.tanggal_registrasi_p2l').val(res.tanggal_registrasi_p2l)
                $('.nama_kelompok_tani').val(res.nama_kelompok)
                $('.nama_ketua').val(res.nama_ketua)
            }
        });
    })


    $('.tanggal_registrasi_p2l, .tanggal_penerima_bantuan')
        .datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });

    ajaxSelect({
        id: '.bantuan_p2l_id',
        url: '{segment1}list_bantuan',
        selected: '{bantuan_p2l_id}'
    });


    ajaxSelect({
        id: '.registrasi_p2l_id',
        url: '{site_url}ajax_selectbox/registrasi_p2l_id',
        selected: '{registrasi_p2l_id}',
    });
})


function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save/{id}',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
            btnSpinnerHide();
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>