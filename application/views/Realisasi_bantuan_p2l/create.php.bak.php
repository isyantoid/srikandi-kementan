<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="alert alert-secondary" role="alert">
                    <div class="alert-icon"><i class="flaticon-info kt-font-brand"></i></div>
                    <div class="alert-text font-weight-bold">A. Data Pemohon</div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Tanggal permohonan:</label>
                        <input type="text" class="form-control tanggal_permohonan" name="tanggal_permohonan" value="" required placeholder="Masukan Tanggal permohonan">
                    </div>
                    <div class="col-md-4">
                        <label class="">Diajukan oleh:</label>
                        <div class="kt-radio-inline">
                            <label class="kt-radio kt-radio--solid">
                                <input type="radio" name="diajukan_oleh" checked="" value="Perorangan"> Perorangan
                                <span></span>
                            </label>
                            <label class="kt-radio kt-radio--solid">
                                <input type="radio" name="diajukan_oleh" value="Kelompok"> Kelompok
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label>Nama kampung:</label>
                        <input type="text" class="form-control nama_kampung" name="nama_kampung" value="" required placeholder="Masukan Nama kampung">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nama Pemohon:</label>
                        <input type="text" class="form-control nama_pemohon" name="nama_pemohon" value="" required placeholder="Masukan Nama Pemohon">
                    </div>
                    <div class="col-md-4">
                        <label>Alamat Pemohon:</label>
                        <input type="text" class="form-control alamat_pemohon" name="alamat_pemohon" value="" required placeholder="Masukan Alamat Pemohon">
                    </div>
                    <div class="col-md-4">
                        <label>Telepon Pemohon:</label>
                        <input type="text" class="form-control telepon_pemohon" name="telepon_pemohon" value="" required placeholder="Masukan telepon Pemohon">
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>


                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Provinsi:</label>
                        <select class="form-control provinsi_kode" name="provinsi_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Kabupaten:</label>
                        <select class="form-control kabupaten_kode" name="kabupaten_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Kecamatan:</label>
                        <select class="form-control kecamatan_kode" name="kecamatan_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Desa:</label>
                        <select class="form-control desa_kode" name="desa_kode" style="width:100%;"></select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Alamat lengkap kebun:</label>
                        <textarea class="form-control alamat_kebun" name="alamat_kebun" placeholder="Masukan alamat lengkap kebun"></textarea>
                    </div>
                    <div class="col-md-2">
                        <label>Latitude:</label>
                        <input type="text" class="form-control lat" name="lat" value="" required placeholder="Latitude" readonly>
                    </div>
                    <div class="col-md-2">
                        <label>Longitude:</label>
                        <input type="text" class="form-control lng" name="lng" value="" required placeholder="Longitude" readonly>
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <button type="button" class="btn btn-sm btn-info form-control btn_modal_map1"><i class="fa fa-globe"></i> Open maps</button>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Jenis Komoditi:</label>
                        <select class="form-control jenis_komoditi_kode" name="jenis_komoditi_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Komoditi:</label>
                        <select class="form-control komoditi_kode" name="komoditi_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal tanam:</label>
                        <input type="text" class="form-control tanggal_tanam" name="tanggal_tanam" value="" required placeholder="Masukan Tanggal tanam">
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal perkiraan panen:</label>
                        <input type="text" class="form-control tanggal_perkiraan_panen" name="tanggal_perkiraan_panen" value="" required placeholder="Masukan Tanggal perkiraan panen">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Varietas:</label>
                        <select class="form-control varietas_kode" name="varietas_kode" style="width:100%;"></select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Jenis Komoditi Pendamping:</label>
                        <select class="form-control jenis_komoditi_pendamping_kode" name="jenis_komoditi_pendamping_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Komoditi Pendamping:</label>
                        <select class="form-control komoditi_pendamping_kode" name="komoditi_pendamping_kode" style="width:100%;"></select>
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal tanam:</label>
                        <input type="text" class="form-control tanggal_tanam_pendamping" name="tanggal_tanam_pendamping" value="" required placeholder="Masukan Tanggal tanam">
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal perkiraan panen:</label>
                        <input type="text" class="form-control tanggal_perkiraan_panen_pendamping" name="tanggal_perkiraan_panen_pendamping" value="" required placeholder="Masukan Tanggal perkiraan panen">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Luas kebun:</label>
                        <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="0" readonly>
                    </div>
                </div>


                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

                <div class="row">
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info btn_add_kelompok">Tambah Kelompok Tani</button>
                    </div>
                </div>
                <div class="m-3 table-responsive">
                    <table class="table table-borderedless table_kelompok_tani">
                        <tbody></tbody>
                    </table>
                </div>



                


            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<!--begin::Modal-->
<div class="modal fade" id="modal_map1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map1()" id="formMap1">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div style="height:350px;" id="map1"></div>
                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai latitude dan longitude</p>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Longitude:</label>
                            <input type="text" class="form-control latMap1" name="latMap1" id="latMap1" required placeholder="Longitude" readonly>
                        </div>
                        <div class="col-md-6">
                            <label>Longitude:</label>
                            <input type="text" class="form-control lngMap1" name="lngMap1" id="lngMap1" required placeholder="Longitude" readonly>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu67fpz02di0x7OTI-ICN4tLDWxCoJzIk&libraries=places&callback=initMap"></script>

<script>
    $(document).ready(function() {

        $('.tanggal_permohonan, .tanggal_tanam, .tanggal_perkiraan_panen, .tanggal_tanam_pendamping, .tanggal_perkiraan_panen_pendamping').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
        });
        
        $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .komoditi_kode, .komoditi_pendamping_kode').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');

        ajaxSelect({
            id: '.provinsi_kode',
            url: 'https://registrasilahanhorti.id/api/master_data/provinsi_select',
        });

        $('.provinsi_kode').change(function() {
            var val = $(this).val();
            $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
            ajaxSelect({
                id: '.kabupaten_kode',
                url: 'https://registrasilahanhorti.id/api/master_data/kabupaten_select',
                optionalSearch: {
                    provinsi_kode: val
                }
            });
        });

        $('.kabupaten_kode').change(function() {
            var val = $(this).val();
            $('.kecamatan_kode, .desa_kode').empty();
            ajaxSelect({
                id: '.kecamatan_kode',
                url: 'https://registrasilahanhorti.id/api/master_data/kecamatan_select',
                optionalSearch: {
                    kabupaten_kode: val
                }
            });
        });

        $('.kecamatan_kode').change(function() {
            var val = $(this).val();
            $('.desa_kode').empty();
            ajaxSelect({
                id: '.desa_kode',
                url: 'https://registrasilahanhorti.id/api/master_data/desa_select',
                optionalSearch: {
                    kecamatan_kode: val
                }
            });
        });

        // open map1 
        $('.btn_modal_map1').click(function() {
            $('#modal_map1').modal('show');
        })

        ajaxSelect({
            id: '.jenis_komoditi_kode',
            url: 'https://registrasilahanhorti.id/api/master_data/komoditi_select',
        });

        $('.jenis_komoditi_kode').change(function() {
            var val = $(this).val();
            $('.komoditi_kode').empty();
            ajaxSelect({
                id: '.komoditi_kode',
                url: 'https://registrasilahanhorti.id/api/master_data/komoditi_jenis_select',
                optionalSearch: {
                    komoditi_kode: val
                }
            });
        });

        ajaxSelect({
            id: '.jenis_komoditi_pendamping_kode',
            url: 'https://registrasilahanhorti.id/api/master_data/komoditi_select',
        });

        $('.jenis_komoditi_pendamping_kode').change(function() {
            var val = $(this).val();
            $('.komoditi_pendamping_kode').empty();
            ajaxSelect({
                id: '.komoditi_pendamping_kode',
                url: 'https://registrasilahanhorti.id/api/master_data/komoditi_jenis_select',
                optionalSearch: {
                    komoditi_kode: val
                }
            });
        });

        var tableKelompokTani = $('.table_kelompok_tani');
        $('.btn_add_kelompok').click(function() {

            var nomor = new Date().getTime();

            var html = `<tr class="header bg-warning">
                <td class="text-center" hidden><input type="hidden" class="form-control nomor" name="kelompok_tani_nomor[]" readonly value="` + nomor + `">` + nomor + `</td>
                <td style="" class="text-center">
                    <span class="form-text small text-white">&nbsp;</span>
                    <div class="btn-group btn-group-sm button_aksi">
                        <button type="button" class="btn btn-danger btn_delete_kelompok_tani" title="delete kelompok tani"><i class="fa fa-trash"></i></button>
                        <button type="button" class="btn btn-success btn_add_anggota_kelompok_tani" title="add anggota kelompok tani"><i class="fa fa-users"></i></button>
                    </div>
                </td>
                <td style="">
                    <span class="form-text small text-white">NIK</span>
                    <input type="text" required class="form-control" name="kelompok_tani_nik[]">
                </td>
                <td style="">
                    <span class="form-text small text-white">Nama Kelompok</span>
                    <input type="text" required class="form-control" name="kelompok_tani_nama_kelompok[]">
                </td>
                <td style="">
                    <span class="form-text small text-white">Nama Ketua</span>
                    <input type="text" required class="form-control" name="kelompok_tani_nama_ketua[]">
                </td>
                <td style="">
                    <span class="form-text small text-white">Nomor HP</span>
                    <input type="text" required class="form-control" name="kelompok_tani_no_hp[]">
                </td>
                <td style="">
                    <span class="form-text small text-white">Luas</span>
                    <input type="text" required class="form-control kelompok_tani_luas" name="kelompok_tani_luas[]" readonly>
                </td>
            </tr>`;

            tableKelompokTani.find('tbody').append(html);
            // kalkulasi();
        });


        tableKelompokTani.on('click', '.btn_add_anggota_kelompok_tani', function() {
            var row = $(this).closest('tr');
            var index = row.index();
            var nomorHeader = row.find('input.nomor').val();

            var html = `<tr class="sub">
                <td class="text-center" hidden><input type="text" required class="form-control nomor_sub" name="` + nomorHeader + `_nomor[]" readonly value="` + nomorHeader + `"></td>
                <td style="min-width:200px;" class="text-center">
                    <span class="form-text small text-white">&nbsp;</span>
                    <div class="btn-group btn-group-sm button_aksi">
                        <button type="button" class="btn btn-danger btn_delete_anggota_tani" title="delete anggota tani"><i class="fa fa-trash"></i></button>
                    </div>
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">NIK</span>
                    <input type="text" required class="form-control nik_anggota" name="` + nomorHeader + `_nik[]">
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">Nama Lengkap</span>
                    <input type="text" required class="form-control nama_lengkap_anggota" name="` + nomorHeader + `_nama_lengkap[]">
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">No. Hp</span>
                    <input type="text" required class="form-control no_hp_anggota" name="` + nomorHeader + `_no_hp[]">
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">Luas/Hektar</span>
                    <input type="text" required class="form-control luas_anggota" name="` + nomorHeader + `_luas[]">
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">Latitude</span>
                    <input type="text" required class="form-control latitude_anggota" name="` + nomorHeader + `_latitude[]">
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">Longitude</span>
                    <input type="text" required class="form-control longitude_anggota" name="` + nomorHeader + `_longitude[]">
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">Jenis Komoditi</span>
                    <select name="` + nomorHeader + `_jenis_komoditi_anggota[]" id="" class="form-control jenis_komoditi_anggota" required>
                        <option value="">-- Pilih Jenis Komoditas --</option>
                    </select>
                </td>
                <td style="min-width:200px;">
                    <span class="form-text small text-muted">Tanggal Tanam</span>
                    <input type="text" required class="form-control tanggal_tanam_anggota" name="` + nomorHeader + `_tanggal_tanam_anggota[]">
                </td>
            </tr>`;

            tableKelompokTani.find('tbody tr').eq(index).after(html);
            $('.tanggal_tanam_anggota').datepicker({
                format: 'yyyy-mm-dd',
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "top left",
            });
            kalkulasi();
        });
    });

    var tableKelompokTani = $('.table_kelompok_tani');

    tableKelompokTani.on('click', '.btn_delete_kelompok_tani', function() {
        var row = $(this).closest('tr');
        var index = row.index();
        var nomorHeader = row.find('input.nomor').val();

        console.log(nomorHeader);


        tableKelompokTani.find('tbody tr').each(function() {
            var head = $(this).find('input.nomor').val();
            var sub = $(this).find('input.nomor_sub').val();
            if (head == nomorHeader || sub == nomorHeader) {
                $(this).remove();
            }
        });
        kalkulasi();
    });

    tableKelompokTani.on('click', '.btn_delete_anggota_tani', function() {
        var row = $(this).closest('tr');
        row.remove();
        kalkulasi();
    });

    tableKelompokTani.on('change', '.luas_anggota', function() {
        var row = $(this).closest('tr');
        var index = row.index();
        var nomorHeader = row.find('input.nomor_sub').val();

        var sum = 0;
        tableKelompokTani.find('tbody tr.sub input.nomor_sub').filter(function() {
            if ($(this).val() === nomorHeader) {
                var luas = parseFloat($(this).closest('tr').find('input.luas_anggota').val());
                sum += luas;
            }
        });

        tableKelompokTani.find('tbody tr.header input.nomor').filter(function() {
            if ($(this).val() === nomorHeader) {
                $(this).closest('tr').find('input.kelompok_tani_luas').val(sum);
            }
        });
        kalkulasi();
    });


    function kalkulasi() {
        totalLuas();
        totalKebun();
    }

    function totalLuas() {
        sum = 0;
        tableKelompokTani.find('tbody tr.sub').each(function() {
            var luas = $(this).find('.luas_anggota').val() ? $(this).find('.luas_anggota').val() : 0;
            sum += parseFloat(luas);
        });
        $('.luas_kebun').val(sum);
    }


    function totalKebun() {
        var totalKebun = tableKelompokTani.find('tbody tr.sub').length;
        $('.total_kebun').val(totalKebun);
    }

    function save_lat_lng_map1() {
        var form = $('#formMap1')[0];
        var formData = new FormData(form);

        if (!formData.get('latMap1') && !formData.get('lngMap1')) {
            alert('Latitude dan longitude wajib diisi')
            return false;
        } else {
            $('.lat').val(formData.get('latMap1'));
            $('.lng').val(formData.get('lngMap1'));
            $('#modal_map1').modal('hide');
        }
    }

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }




    // MAP1 
    var map;

    function initialize() {
        var mapOptions = {
            zoom: 5.2,
            center: new google.maps.LatLng(-2.0423904, 102.8983498),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map1'),
            mapOptions
        );
        google.maps.event.addListener(map, 'click', function(event) {
            document.getElementById('latMap1').value = event.latLng.lat();
            document.getElementById('lngMap1').value = event.latLng.lng();
            console.log(event);
        });


    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>