<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_level_model extends CI_Model {

    private $tableName = 'tbl_user_level';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);

            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['nama'] != $this->input->post('nama')) {
                    if (isDuplicate($this->tableName, 'nama', $this->input->post('nama'))) {
                        return jsonOutputError('Nama user level sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate($this->tableName, 'nama', $this->input->post('nama'))) {
                    return jsonOutputError('Nama user level sudah ada sebelumnya.');
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->set('app_id', '2');
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }

        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}
