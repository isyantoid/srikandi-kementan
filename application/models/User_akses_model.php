<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_akses_model extends CI_Model {

    private $tableName = 'tbl_user_akses';
    private $primaryKey = 'id';

    public function head_menu($userLevelID) {
        $get = $this->db
        ->select('
            tbl_menu.*,
            tbl_user_akses.show,
            tbl_user_akses.read,
            tbl_user_akses.create,
            tbl_user_akses.update,
            tbl_user_akses.delete,
        ')
        ->join('tbl_user_akses', 'user_level_id = '.$userLevelID.' and menu_id = tbl_menu.id', 'left')
        ->where('subId', null)
        ->order_by('nomor_urut', 'asc')
        ->get('tbl_menu');
        return $get->result_array();
    }

    public function sub_menu($userLevelID, $subID) {
        $this->db
        ->select('
            tbl_menu.*,
            tbl_user_akses.show,
            tbl_user_akses.read,
            tbl_user_akses.create,
            tbl_user_akses.update,
            tbl_user_akses.delete,
        ')
        ->join('tbl_user_akses', 'user_level_id = '.$userLevelID.' and menu_id = tbl_menu.id', 'left')
        ->order_by('nomor_urut', 'asc')
        ->where('subId', $subID);
        $get = $this->db->get('tbl_menu');
        return $get->result_array();
    }


    public function list_menu($userLevelID) {
        $this->db
        ->select('
            tbl_menu.*,
            tbl_user_akses.read,
            tbl_user_akses.create,
            tbl_user_akses.update,
            tbl_user_akses.delete,
        ')
        ->join('tbl_user_akses', 'user_level_id = '.$userLevelID.' and menu_id = tbl_menu.id', 'left');
        $get = $this->db->get('tbl_menu');
        return $get->result_array();
    }
    
    public function cek_akses_user($menuID, $userLevelID) {
        $this->db->where('menu_id', $menuID);
        $this->db->where('user_level_id', $userLevelID);
        $get = $this->db->get('tbl_user_akses');
        return $get->row_array();
    }
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);

            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['nama'] != $this->input->post('nama')) {
                    if (isDuplicate($this->tableName, 'nama', $this->input->post('nama'))) {
                        return jsonOutputError('Nama user level sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate($this->tableName, 'nama', $this->input->post('nama'))) {
                    return jsonOutputError('Nama user level sudah ada sebelumnya.');
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->set('app_id', '2');
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }

        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}
