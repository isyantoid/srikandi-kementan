<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Desa extends CI_Controller
{

    private $title = 'Desa';
    private $view = 'Desa';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('tbl_desa', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Desa';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            return $this->model->delete();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}