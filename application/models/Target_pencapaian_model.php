<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Target_pencapaian_model extends CI_Model
{
    private $tableName = 'tbl_target_pencapaian';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {

                $dataExists = getRowArray('tbl_target_pencapaian', ['id' => $id]);
                if($dataExists['tahun'] != $this->input->post('tahun')) {
                    if(isDuplicate('tbl_target_pencapaian', 'tahun', $this->input->post('tahun'))) {
                        return jsonOutputError('jenis komoditi pada tahun yang dipilih sudah ada sebelumnya!');    
                    }
                }
                if($dataExists['jenis_komoditi_id'] != $this->input->post('jenis_komoditi_id')) {
                    if(isDuplicate('tbl_target_pencapaian', 'jenis_komoditi_id', $this->input->post('jenis_komoditi_id'))) {
                        return jsonOutputError('jenis komoditi pada tahun yang dipilih sudah ada sebelumnya!');    
                    }
                }
                
                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {
                if(isDuplicate('tbl_target_pencapaian', 'tahun', $this->input->post('tahun'))) {
                    return jsonOutputError('jenis komoditi pada tahun yang dipilih sudah ada sebelumnya!');    
                }
                if(isDuplicate('tbl_target_pencapaian', 'jenis_komoditi_id', $this->input->post('jenis_komoditi_id'))) {
                    return jsonOutputError('jenis komoditi pada tahun yang dipilih sudah ada sebelumnya!');    
                }
                
                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}