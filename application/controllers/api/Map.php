<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Map extends Api {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('','');
    }

    public function jenis_komoditi_get() {
        $this->db->select('
            tbl_permohonan.jenis_komoditi_kode,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_icon,
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->group_by('tbl_permohonan.jenis_komoditi_kode');
        $get = $this->db->get('tbl_permohonan');
        if($get->num_rows() > 0) {
            $this->set_response(dataApiRespon(true, 'Data jenis provinsi', $get->result_array()), REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data'), REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function provinsi_get() {
        $filter = $this->get('filter');
        $limit = $this->get('limit');
        $this->db->order_by('provinsi_nama', 'asc');
        $this->db->limit($limit);
        if($filter) $this->db->like('provinsi_nama', $filter);
        $jenis_komoditi = $this->db->get('tbl_provinsi');
        if($jenis_komoditi->num_rows() > 0) {
            $this->set_response(dataApiRespon(true, 'Data jenis provinsi', $jenis_komoditi->result_array()), REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data'), REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function total_kampung_indo_get() {
        $get = $this->db->get('tbl_permohonan')->num_rows();
        $data['total'] = $get;
        $this->set_response($get, REST_Controller::HTTP_OK);
    }
    
    public function total_kelompok_tani_indo_get() {
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $get = $this->db->get('tbl_kelompok_tani')->num_rows();
        $data['total'] = $get;
        $this->set_response($get, REST_Controller::HTTP_OK);
    }
    
    public function total_luas_indo_get() {
        $this->db->select_sum('luas', 'total_luas');
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $get = $this->db->get('tbl_kelompok_tani');
        $data['total'] = $get->row()->total_luas;
        $this->set_response((int) $get->row()->total_luas, REST_Controller::HTTP_OK);
    }

    public function markers_map_get() {
        $filter = $this->get('filter');
        $this->db->select('
            tbl_permohonan.id as idpermintaan,
            "2" as type,
            tbl_permohonan.lat as lintang,
            tbl_permohonan.lng as bujur,
            "plant.png" as icon,
            tbl_permohonan.nomor_registrasi as nama,
            tbl_permohonan.nomor_registrasi as pemilik,
            tbl_permohonan.jenis_komoditi_kode as jenis_material_id,
            tbl_permohonan.id,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
            tbl_permohonan.desa_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_pin,
            (select COALESCE(sum(total_nilai_bantuan),0) from tbl_bantuan where permohonan_id = tbl_permohonan.id) total_nilai_bantuan,
            (select count(*) from tbl_kelompok_tani where id_permohonan = tbl_permohonan.id) total_kelompok_tani,
            (select count(*) from tbl_anggota_tani where id_permohonan = tbl_permohonan.id) total_anggota_tani,
            coalesce((select nilai_rating_persen from tbl_penilaian where permohonan_id = tbl_permohonan.id), 0) as nilai_rating_persen,
        ');
        if($filter) {
            $this->db->like('tbl_permohonan.nomor_registrasi', $filter, 'before');
            $this->db->where('lng !=', null);
            $this->db->where('lat !=', null);
        } else {
            $this->db->where('lat !=', null);
            $this->db->where('lng !=', null);
            $this->db->or_where('lat !=', 0);
            $this->db->or_where('lng !=', 0);
        }
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $get = $this->db->get('tbl_permohonan');
        if($get->num_rows() > 0) {
            $this->set_response(dataApiRespon(true, 'Data markers', $get->result_array()), REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data'), REST_Controller::HTTP_NOT_FOUND);
        }
    }


    public function permohonan_get() {
        $filter = $this->get('filter');
        $provinsi_kode = $this->get('provinsi_kode');
        $kabupaten_kode = $this->get('kabupaten_kode');
        $limit = $this->get('limit');
        $start = $this->get('start');

        $this->db->select('
            tbl_permohonan.id,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
            tbl_permohonan.desa_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_pin,
            (select COALESCE(sum(total_nilai_bantuan),0) from tbl_bantuan where permohonan_id = tbl_permohonan.id) total_nilai_bantuan,
            (select count(*) from tbl_kelompok_tani where id_permohonan = tbl_permohonan.id) total_kelompok_tani,
            (select count(*) from tbl_anggota_tani where id_permohonan = tbl_permohonan.id) total_anggota_tani,
            coalesce((select nilai_rating_persen from tbl_penilaian where permohonan_id = tbl_permohonan.id), 0) as nilai_rating_persen,
        ');
        if($provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $provinsi_kode);
        if($kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $kabupaten_kode);
        $this->db->limit($start,$limit);

        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $get = $this->db->get('tbl_permohonan');
        if($get->num_rows() > 0) {
            $data['status'] = true;
            $data['total'] = $get->num_rows();
            $data['data'] = $get->result_array();
            $this->set_response(
                // dataApiRespon(true, 'Data markers', $get->result_array()
                $data, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data'), REST_Controller::HTTP_NOT_FOUND);
        }
    }
}