<div class="kt-portlet kt-margin-top-30">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                PESAN ANDA
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>NAMA</label>
                        <input type="text" class="form-control" aria-describedby="nameHelp" placeholder="Masukan nama anda">
                    </div>
                    <div class="form-group">
                        <label>EMAIL</label>
                        <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Masukan email anda">
                    </div>
                    <div class="form-group">
                        <label>LAMPIRAN</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="lampiran" name="lampiran">
                            <label class="custom-file-label" for="lampiran">Pilih lampiran</label>
                            <span class="form-text text-muted">Silahkan memilih berkas jpg, jpeg, png dan pdf .</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">PESAN ANDA</label>
                        <textarea class="form-control" id="exampleTextarea" rows="5"></textarea>
                        <span class="form-text text-muted">Masukan pesan anda disini</span>
                    </div>
                </div>

                <div class="col-md-6 p-5">
                    <h4> Direktorat Jenderal Hortikultura </h4>
                    <h4> Kementerian Pertanian Republik Indonesia </h4>
                    <h4 class="mt-5">Jl. AUP No. 3, RT/RW 009/010 Pasar Minggu, Jakarta Selatan, DKI Jakarta, 12520</h4>
                    <h4 class="mt-3">
                        <i class="fa fa-phone"></i> Telp: 021-7806775 <br>
                        <i class="fa fa-fax"></i> Fax: 021-78844037 <br>
                        <i class="fa fa-mail-bulk"></i> email : hortikultura@pertanian.go.id <br>
                        <i class="fa fa-globe"></i> Web : hortikultura.pertanian.go.id
                    </h4>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-md-6">
                        <button type="reset" class="btn btn-primary">KIRIM PESAN</button>&nbsp;
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!--end::Form-->
</div>