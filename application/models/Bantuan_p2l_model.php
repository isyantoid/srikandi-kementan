<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bantuan_p2l_model extends CI_Model {

    private $tableName = 'tbl_bantuan_p2l';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $id = $this->uri->segment(3);
            if($id) {


                $this->db->set('registrasi_p2l_id', $this->input->post('registrasi_p2l_id'));
                $this->db->set('tanggal_penerima_bantuan', $this->input->post('tanggal_penerima_bantuan'));
                $this->db->set('realisasi_pemanfaatan_anggaran', $this->input->post('realisasi_pemanfaatan_anggaran'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
                $this->db->where('id', $id);
                $this->db->update('tbl_bantuan_p2l');
                return jsonOutputSuccess();
            } else {
                
                $this->db->set('registrasi_p2l_id', $this->input->post('registrasi_p2l_id'));
                $this->db->set('tanggal_penerima_bantuan', $this->input->post('tanggal_penerima_bantuan'));
                $this->db->set('realisasi_pemanfaatan_anggaran', $this->input->post('realisasi_pemanfaatan_anggaran'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
                $this->db->insert('tbl_bantuan_p2l');
                return jsonOutputSuccess();
            }
            
        }


        
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('tbl_bantuan_p2l');
    }

    public function detail_registrasi($id) {
        $this->db->select('
            tbl_bantuan_p2l.id,
            tbl_registrasi_p2l.nomor_registrasi_p2l,
            tbl_registrasi_p2l.tanggal_registrasi_p2l,
            tbl_registrasi_p2l.jumlah_penerima_manfaat,
            tbl_registrasi_p2l.terdaftar_simluhtan,
            tbl_bantuan_p2l.tanggal_penerima_bantuan,
            tbl_registrasi_p2l.kelompok_tani_id,
            tbl_bantuan_p2l.realisasi_pemanfaatan_anggaran,
            tbl_bantuan_p2l.lat,
            tbl_bantuan_p2l.lng,
            tbl_bantuan_p2l.registrasi_p2l_id,
            tbl_registrasi_p2l.nama_kelompok,
            tbl_registrasi_p2l.nama_ketua,
            tbl_provinsi.nama as provinsi_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_desa.nama as desa_nama,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_registrasi_p2l.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_registrasi_p2l', 'tbl_bantuan_p2l.registrasi_p2l_id = tbl_registrasi_p2l.id', 'left');
        $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->join('tbl_kecamatan', 'tbl_registrasi_p2l.kecamatan_kode = tbl_kecamatan.kode', 'left');
        $this->db->join('tbl_desa', 'tbl_registrasi_p2l.desa_kode = tbl_desa.kode', 'left');
        $this->db->where('tbl_bantuan_p2l.id',$id);
        $this->db->from('tbl_bantuan_p2l');
        return $this->db->get()->row_array();
    }

    public function get_kelompok_tani($id_permohonan) {
        $this->db->select(' tbl_kelompok_tani.*');
        $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function get_anggota_tani($id_kelompok_tani) {
        $this->db->select(' tbl_anggota_tani.*');
        $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
        $this->db->from('tbl_anggota_tani');
        $get = $this->db->get();
        return $get->result_array();
    }

    public function getRegistrasiKampung() {
        $this->db->select('
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_kelompok_tani.nama_kelompok,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_kelompok_tani.terdaftar_simluhtan,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->order_by('tbl_permohonan.nomor_registrasi ASC', 'tbl_jenis_komoditi.jenis_komoditi_nama ASC', 'tbl_permohonan.tanggal_permohonan DESC');
        $this->db->from('tbl_kelompok_tani');
        $res = $this->db->get();
        return $res->result_array();
    }
}
