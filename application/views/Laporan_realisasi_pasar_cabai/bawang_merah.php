
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title"><?= $title ?></h3>
            </div>
            <!-- <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}export_excel?tahun={tahun}&jenis_komoditi_id={jenis_komoditi_id}" class="btn btn-brand btn-elevate btn-icon-sm">Export Excel</a>
                        <a href="{segment1}export_pdf?tahun={tahun}&jenis_komoditi_id={jenis_komoditi_id}" class="btn btn-brand btn-elevate btn-icon-sm">Export Pdf</a>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="kt-portlet__body">
            <form action="{segment1}?">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Pilih Tahun</label>
                            <input class="form-control tahun" type="text" value="{tahun}" readonly name="tahun" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Champion</label>
                            <select class="form-control champ_id" name="champ_id" style="width:100%;"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10">
                        <button type="submit" class="btn btn-success">Filter</button>
                    </div>
                </div>
            </form>
            <?php $persen = $persentase['persen']; ?>
            
            <div class="table-responsive mt-5">
                <table class="table table-sm table-bordered">
                    <thead class="bg-light">
                        <th class="text-center">Bulan</th>
                        <th class="text-center">Komitmen <br>Stok Baru</th>
                        <th class="text-center">Update <br>Stok Bulanan</th>
                        <?php foreach($pasar as $row): ?>
                            <th class="text-center"><?= $row['nama'] ?></th>
                        <?php endforeach ?>
                        <th class="text-center">Total</th>
                        <th class="text-center">Stok Akhir</th>
                        <th class="text-center">Carry Over <br> <?= $persen ?>%</th>
                    </thead>
                    <tbody>
                        <?php $totalKomitmenStok = 0; ?>
                        <?php $stokAwal = 0; ?>
                        <?php $stokAkhir = 0; ?>
                        <?php $stokAkhir = 0; ?>
                        <?php $stokUpdate = 0; ?>
                        <?php $totalPasar = 0; ?>
                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                            <?php $bulan = date('M', mktime(0, 0, 0, $i, 1)); ?>
                            <?php $komitmenStok = $this->model->get_stok_awal($tahun, $jenis_komoditi_id, $bulan, $champ_id) ?>
                            
                            <?php $co = $stokAkhir*80/100; ?>
                            <?php $stokUpdate = $komitmenStok+$co ?>
                            
                            <tr>
                                <td class="text-center align-middle"><?= $bulan ?>-{tahun}</td>
                                <td class="text-center align-middle"><?= number_format($komitmenStok, 0) ?></td>
                                <td class="text-center align-middle"><?= number_format($stokUpdate, 0) ?></td>
                                <?php foreach($pasar as $row): ?>
                                    <?php $rp = $this->model->get_realisasi_pasar($tahun, $i, $jenis_komoditi_id, $row['id'], $champ_id); ?>
                                    <td class="text-center align-middle"><?= number_format($rp['jumlah']) ?></td>
                                    <?php $totalPasar =+ $rp['jumlah'] ?>
                                    <?php $stokAkhir = $stokUpdate - $rp['jumlah'] ?>
                                <?php endforeach ?>
                                <td class="text-center align-middle"><?= number_format($totalPasar, 0) ?></td>
                                <td class="text-center align-middle"><?= number_format($stokAkhir, 0) ?></td>
                                <td class="text-center align-middle"><?= number_format($co, 0) ?></td>
                            </tr>
                            
                            <?php $stokAwal = $stokUpdate ?>
                            <?php $totalKomitmenStok = $totalKomitmenStok + $komitmenStok; ?>
                        <?php endfor ?>
                    </tbody>
                    
                    
                </table>
            </div>

            <div class="mt-2">
                <table class="table w-25 font-weight-bold">
                    <tbody>
                        <tr>
                            <td>Champion</td>
                            <td>{jenis_komoditi_nama}</td>
                        </tr>
                        <tr>
                            <td>Luas</td>
                            <td><?= number_format($this->model->get_luas_jenis_komoditi($tahun, $jenis_komoditi_id, $champ_id),2) ?> Ha</td>
                        </tr>
                        <tr>
                            <td>Komitmen Stok</td>
                            <td><?= number_format($totalKomitmenStok) ?> Ton</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<script>
    var baseUrl = '{base_url}';
    $(".tahun").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
    });
    ajaxSelect({
        id: '.champ_id',
        url: '{site_url}ajax_selectbox/champ_bawang_merah',
        selected: '{champ_id}',
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chroma-js/2.1.1/chroma.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap" async defer></script>
<!-- <script src="{base_url}dist/json/script.js"></script> -->
<script src="{base_url}dist/json/map.js"></script>

<script>


</script>