<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bantuan_model extends CI_Model {
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $id = $this->uri->segment(3);
            
            $permohonan = getRowArray('tbl_permohonan', ['id' => $this->input->post('nomor_registrasi')]);

            $this->db->trans_begin();
            
            if($id) {
                $this->db->set('tipe_bantuan_id', $this->input->post('tipe_bantuan_id'));
                $this->db->set('provinsi_kode', $permohonan['provinsi_kode']);
                $this->db->set('kabupaten_kode', $permohonan['kabupaten_kode']);
                $this->db->set('kecamatan_kode', $permohonan['kecamatan_kode']);
                $this->db->set('desa_kode', $permohonan['desa_kode']);
                $this->db->set('kelompok_komoditi_id', $permohonan['kelompok_komoditi_id']);
                $this->db->set('jenis_komoditi_id', $permohonan['jenis_komoditi_id']);
                $this->db->set('sub_komoditi_id', $permohonan['sub_komoditi_id']);
                $this->db->set('permohonan_id', $this->input->post('nomor_registrasi'));
                $this->db->set('kelompok_tani_id', $this->input->post('kelompok_tani_id'));
                $this->db->set('total_nilai_bantuan', removeComma($this->input->post('total_nilai_bantuan')));
                $this->db->where('id', $id);
                $this->db->update('tbl_bantuan');
                $id_bantuan = $id;

                $detail = $this->input->post('nomor');
                $count_detail = count($detail);
                if($count_detail > 0) {
                    
                    for($i=0; $i<$count_detail; $i++) {
                        $nomor = $detail[$i];

                        if($this->input->post('id')[$i]) {
                            $this->db->set('bantuan_id',$id_bantuan);
                            $this->db->set('tanggal_bantuan',$this->input->post('tanggal_bantuan')[$i]);
                            $this->db->set('jenis_bantuan_id',$this->input->post('jenis_bantuan_id')[$i]);
                            $this->db->set('satuan',$this->input->post('satuan')[$i]);
                            $this->db->set('jumlah',removeComma($this->input->post('jumlah')[$i]));
                            $this->db->set('harga',removeComma($this->input->post('harga')[$i]));
                            $this->db->set('total',removeComma($this->input->post('total')[$i]));
    
                            
                            $config = $this->config_file_upload();                    
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            
                            if ( $this->upload->do_upload('foto_detail_' . $nomor)){
                                $data_upload = $this->upload->data();
                                $this->db->set('foto', $data_upload['file_name']);
                            }
                            
                            
    
                            $this->db->where('id', $this->input->post('id')[$i]);
                            $this->db->update('tbl_bantuan_detail');
                        } else {

                            $this->db->set('bantuan_id',$id_bantuan);
                            $this->db->set('tanggal_bantuan',$this->input->post('tanggal_bantuan')[$i]);
                            $this->db->set('jenis_bantuan_id',$this->input->post('jenis_bantuan_id')[$i]);
                            $this->db->set('satuan',$this->input->post('satuan')[$i]);
                            $this->db->set('jumlah',removeComma($this->input->post('jumlah')[$i]));
                            $this->db->set('harga',removeComma($this->input->post('harga')[$i]));
                            $this->db->set('total',removeComma($this->input->post('total')[$i]));
    
                            
                            $config = $this->config_file_upload();                    
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            
                            if ( $this->upload->do_upload('foto_detail_' . $nomor)){
                                $data_upload = $this->upload->data();
                                $this->db->set('foto', $data_upload['file_name']);
                            } 
                            
                            
    
                            $this->db->insert('tbl_bantuan_detail');
                        }
                    }
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return jsonOutputError('Data gagal disimpan');
                } else {
                    $this->db->trans_commit();
                    return jsonOutputSuccess();
                }

            } else {
                $this->db->trans_begin();

                $this->db->set('tanggal_kirim', date('Y-m-d'));
                $this->db->set('tipe_bantuan_id', $this->input->post('tipe_bantuan_id'));
                $this->db->set('provinsi_kode', $permohonan['provinsi_kode']);
                $this->db->set('kabupaten_kode', $permohonan['kabupaten_kode']);
                $this->db->set('kecamatan_kode', $permohonan['kecamatan_kode']);
                $this->db->set('desa_kode', $permohonan['desa_kode']);
                $this->db->set('kelompok_komoditi_id', $permohonan['kelompok_komoditi_id']);
                $this->db->set('jenis_komoditi_id', $permohonan['jenis_komoditi_id']);
                $this->db->set('sub_komoditi_id', $permohonan['sub_komoditi_id']);
                $this->db->set('permohonan_id', $this->input->post('nomor_registrasi'));
                $this->db->set('kelompok_tani_id', $this->input->post('kelompok_tani_id'));
                $this->db->set('total_nilai_bantuan', removeComma($this->input->post('total_nilai_bantuan')));
                $this->db->insert('tbl_bantuan');
                $id_bantuan = $this->db->insert_id();

                $detail = $this->input->post('nomor');
                $count_detail = count($detail);
                if($count_detail > 0) {
                    for($i=0; $i<$count_detail; $i++) {
                        $nomor = $detail[$i];

                        $this->db->set('bantuan_id',$id_bantuan);
                        $this->db->set('tanggal_bantuan',$this->input->post('tanggal_bantuan')[$i]);
                        $this->db->set('jenis_bantuan_id',$this->input->post('jenis_bantuan_id')[$i]);
                        $this->db->set('satuan',$this->input->post('satuan')[$i]);
                        $this->db->set('jumlah',removeComma($this->input->post('jumlah')[$i]));
                        $this->db->set('harga',removeComma($this->input->post('harga')[$i]));
                        $this->db->set('total',removeComma($this->input->post('total')[$i]));

                        
                        $config = $this->config_file_upload();                    
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        
                        if ( $this->upload->do_upload('foto_detail_' . $nomor)){
                            $data_upload = $this->upload->data();
                            $this->db->set('foto', $data_upload['file_name']);
                        } else {
                            $error = $this->upload->display_errors();
                            return jsonOutputError($error);
                        }
                        
                        

                        $this->db->insert('tbl_bantuan_detail');
                    }
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return jsonOutputError('Data gagal disimpan');
                } else {
                    $this->db->trans_commit();
                    return jsonOutputSuccess();
                }
            }
        }

        
    }

    public function config_file_upload() {
        $config['upload_path'] = './dist/images/bantuan/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|webp';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('tbl_bantuan');
        
        $this->db->where('bantuan_id', $id);
        $this->db->delete('tbl_bantuan_detail');
    }
    
    public function get_bantuan($id) {
        $this->db->select('
            tbl_bantuan.*,
            tbl_tipe_bantuan.tipe_bantuan,
            tbl_kelompok_tani.nama_kelompok,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.no_reg_horti,
        ');
        $this->db->join('tbl_tipe_bantuan','tbl_bantuan.tipe_bantuan_id = tbl_tipe_bantuan.id', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_bantuan.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->join('tbl_permohonan','tbl_bantuan.permohonan_id = tbl_permohonan.id', 'left');
        $this->db->where('tbl_bantuan.id', $id);
        $this->db->from('tbl_bantuan');
        $get = $this->db->get();
        return $get->row_array();
    }
    
    public function get_detail_bantuan($bantuan_id) {
        $this->db->select('
            tbl_bantuan_detail.*,
            tbl_jenis_bantuan.nama_bantuan
        ');
        $this->db->join('tbl_jenis_bantuan','tbl_bantuan_detail.jenis_bantuan_id = tbl_jenis_bantuan.id');
        $this->db->where('tbl_bantuan_detail.bantuan_id', $bantuan_id);
        $this->db->from('tbl_bantuan_detail');
        $get = $this->db->get();
        return $get->result_array();
    }


    public function exportExcel() {
        $this->db->select('
            tbl_kelompok_tani.nama_kelompok,
            tbl_tipe_bantuan.tipe_bantuan,
            tbl_bantuan.provinsi_nama,
            tbl_bantuan.kecamatan_nama,
            tbl_bantuan.kabupaten_nama,
            tbl_bantuan.desa_nama,
            tbl_bantuan_detail.tanggal_bantuan,
            tbl_jenis_bantuan.nama_bantuan as jenis_bantuan,
            tbl_bantuan_detail.satuan,
            tbl_bantuan_detail.jumlah,
            tbl_bantuan_detail.harga,
            tbl_bantuan_detail.total,
            tbl_bantuan_detail.foto
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_bantuan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_bantuan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_jenis_bantuan', 'tbl_bantuan_detail.jenis_bantuan_id = tbl_jenis_bantuan.id', 'left');
        $this->db->join('tbl_bantuan', 'tbl_bantuan_detail.bantuan_id = tbl_bantuan.id', 'left');
        $this->db->join('tbl_tipe_bantuan', 'tbl_bantuan.tipe_bantuan_id = tbl_tipe_bantuan.id', 'left');
        $this->db->join('tbl_kelompok_tani', 'tbl_bantuan.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->order_by('tbl_tipe_bantuan.tipe_bantuan ASC', 'tbl_bantuan.tanggal_kiriim DESC');
        $this->db->from('tbl_bantuan_detail');
        $res = $this->db->get();
        return $res->result_array();
    }
}
