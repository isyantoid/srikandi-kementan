<style>
    .gm-style .gm-style-iw-c {
    position: absolute;
    box-sizing: border-box;
    overflow: hidden;
    top: 0;
    left: 0;
    transform: translate3d(-50%,-100%,0);
    background-color: #2786fb;
    color: white;
    border-radius: 8px;
    padding: 12px !important;
    box-shadow: 0 2px 7px 1px rgb(0 0 0 / 30%);
}
.gm-style .gm-style-iw-d {
    overflow: auto !important;
}
</style>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet" data-ktportlet="true" id="collapse">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-list-3"></i>
                        </span>
                        <h3 class="kt-portlet__head-title"> Filter {title}</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form kt-form--fit kt-form--label-right" action="javascript:filter()" id="form1">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Komoditi:</label>
                                <div class="col-lg-3">
                                    <select class="form-control id_komoditi" name="id_komoditi"></select>
                                </div>
                                <label class="col-lg-2 col-form-label">Jenis Komoditi:</label>
                                <div class="col-lg-3">
                                    <select class="form-control id_jenis_komoditi" name="id_jenis_komoditi"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Tanggal Awal:</label>
                                <div class="col-lg-3">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control tanggal_awal" name="tanggal_awal" placeholder="Tanggal Awal">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
                                    </div>
                                </div>
                                <label class="col-lg-2 col-form-label">Tanggal Akhir:</label>
                                <div class="col-lg-3">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control tanggal_akhir" name="tanggal_akhir" placeholder="Tanggal Akhir">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Provinsi:</label>
                                <div class="col-lg-3">
                                    <select class="form-control id_provinsi" name="id_provinsi"></select>
                                </div>
                                <label class="col-lg-2 col-form-label">Kabupaten:</label>
                                <div class="col-lg-3">
                                    <select class="form-control id_kabupaten" name="id_kabupaten"></select>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot kt-portlet__foot--fit-x">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-success">Tampilkan Hasil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="result"></div>
</div>

<script>
    var portlet = new KTPortlet('collapse');
    // portlet.collapse('hide');

    $('.id_komoditi').select2({
        placeholder: 'Pilih komoditi'
    });
    $('.id_jenis_komoditi').select2({
        placeholder: 'Pilih jenis komoditi'
    });
    $('.id_kabupaten').select2({
        placeholder: 'Pilih kabupaten'
    });
    $('.id_provinsi').select2({
        placeholder: 'Pilih provinsi'
    });

    $('.tanggal_awal, .tanggal_akhir').datepicker({
        todayHighlight: true,
        orientation: "bottom left",
    });

    $(document).on('click', '.result_summary_kabupaten', function(e) {
        e.preventDefault();
        var komoditi = $(this).data('komoditi');
        var provinsi = $(this).data('provinsi');
        var image = $(this).data('image');
        var url = '{site_url}dashboard/result_summary';

        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            $('.result').load(url, {
                komoditi: komoditi,
                image: image,
                provinsi: provinsi,
                tipe: 'kabupaten'
            }, function() {
                unblockPage();
            });
        }, 1000);
    });

    $(document).on('click', '.result_summary_provinsi', function(e) {
        e.preventDefault();
        var komoditi = $(this).data('komoditi');
        var image = $(this).data('image');
        var url = '{site_url}dashboard/result_summary';

        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            $('.result').load(url, {
                komoditi: komoditi,
                image: image,
                tipe: 'provinsi'
            }, function() {
                unblockPage();
            });
        }, 1000);
    });

    function filter() {
        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            portlet.collapse('hide');
            $('.result').load('{site_url}dashboard/map_pusat', function() {
                unblockPage();
            });
        }, 1000);
    }
    
</script>