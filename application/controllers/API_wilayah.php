<?php

defined('BASEPATH') or exit('No direct script access allowed');

class API_wilayah extends CI_Controller
{

    private $url_api_wilayah;
    private $tahun;

    public function __construct() {
        parent::__construct();
        $this->url_api_wilayah = $this->config->item('url_api_wilayah');
        $this->tahun = date('Y');
    }


    public function sipedas_provinsi() {
        $this->load->library('curl');
        $result = $this->curl->simple_get('https://sipedas.pertanian.go.id/api/wilayah/list_wilayah?thn=2022&lvl=10&lv2=11');
        $result = json_decode($result, True);
        // var_dump($result);

        $data = array();
        foreach($result as $key => $val) {
            $item['id'] = $key;
            $item['text'] = $val;
            $data[] = $item;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    
    public function list_provinsi_datatable() {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_provinsi.id,
            tbl_provinsi.kode,
            tbl_provinsi.nama,
        ');
        $this->datatables_server_side->from('tbl_provinsi');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function list_kabupaten_datatable($prov=null) {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_kabupaten.id,
            tbl_kabupaten.kode,
            tbl_kabupaten.nama,
        ');
        $this->datatables_server_side->from('tbl_kabupaten');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function list_kecamatan_datatable($prov=null, $kab=null) {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_kecamatan.id,
            tbl_kecamatan.kode,
            tbl_kecamatan.nama,
        ');
        $this->datatables_server_side->from('tbl_kecamatan');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function list_desa_datatable($prov=null, $kab=null, $kec=null) {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_desa.id,
            tbl_desa.kode,
            tbl_desa.nama,
        ');
        $this->datatables_server_side->from('tbl_desa');
        return print_r($this->datatables_server_side->generate());
    }
}
