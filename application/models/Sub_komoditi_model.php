<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sub_komoditi_model extends CI_Model {

    private $tableName = 'tbl_sub_komoditi';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['sub_komoditi_kode'] != $this->input->post('sub_komoditi_kode')) {
                    if (isDuplicate($this->tableName, 'sub_komoditi_kode', $this->input->post('sub_komoditi_kode'))) {
                        return jsonOutputError('Kode sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate($this->tableName, 'sub_komoditi_kode', $this->input->post('sub_komoditi_kode'))) {
                    return jsonOutputError('Kode sudah ada sebelumnya.');
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }

        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}
