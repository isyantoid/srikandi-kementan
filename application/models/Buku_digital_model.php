<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Buku_digital_model extends CI_Model
{
    private $tableName = 'tbl_buku_digital';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $params = array(
                    'nama_ebook' => $this->input->post('nama_ebook', TRUE),
                    'id_jenis_dokumen' => $this->input->post('id_jenis_dokumen', TRUE),
                    'keterangan' => $this->input->post('keterangan', TRUE)
                );
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName, $params);

                if (!empty($_FILES['file_upload'])) :
                    $this->delete_all_file($id);

                    $nama_pdf = $this->upload_pdf($id);
                    $this->db->where($this->primaryKey, $id);
                    $this->db->update($this->tableName, array('file_upload' => $nama_pdf));
                endif;

                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {
                $params = array(
                    'nama_ebook' => $this->input->post('nama_ebook', TRUE),
                    'id_jenis_dokumen' => $this->input->post('id_jenis_dokumen', TRUE),
                    'keterangan' => $this->input->post('keterangan', TRUE)
                );

                $insert = $this->db->insert($this->tableName, $params);
                $id = $this->db->insert_id();

                if (!empty($_FILES['file_upload'])) :
                    $nama_pdf = $this->upload_pdf($id);
                    $this->db->where($this->primaryKey, $id);
                    $this->db->update($this->tableName, array('file_upload' => $nama_pdf));
                endif;


                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);

        $this->delete_all_file($id);

        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }

    //delete image and folder
    function delete_all_file($id)
    {
        $dir = './uploads/buku_digital/' . $id;
        if (!is_dir($dir)) {
            $file = "direktori folder kosong";
        } else {
            $data = scandir($dir);

            $num = count($data) - 1;
            if ($num > 1) {
                foreach (scandir($dir) as $item) {
                    if ($item == '.' || $item == '..') {
                        continue;
                    }

                    unlink($dir . '/' . $item);
                }
                rmdir($dir);
            } else {
                $file = "file kosong";
            }
        }
    }

    public function upload_pdf($id)
    {

        $path = './uploads/buku_digital/' . $id;
        if (!is_dir($path)) {
            //Directory does not exist, so lets create it.
            mkdir($path, 0755, true);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 0;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_upload')) {
            $error = array('error' => $this->upload->display_errors());
            $nama_file = 'ccccc';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $nama_file =  $data['upload_data']['file_name'];
        }

        return $nama_file;
    }
}