<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_komitmen_cabai extends CI_Controller
{

    private $title = 'Lap. Komitmen Stok Champion Cabai';
    private $view = 'Laporan_komitmen_cabai';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }
    
    public function index()
    {
        akses_user('read');
        $data['tahun'] = $this->input->get('tahun') ? $this->input->get('tahun') : date('Y');
        $data['jenis_komoditi_id'] = $this->input->get('jenis_komoditi_id');
        $data['data'] = $this->model->laporan_komitmen($data['tahun']);
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function export_pdf()
    {

        akses_user('read');

        $tahun = $this->input->get('tahun') ? $this->input->get('tahun') : date('Y');

        if($tahun) {
            $data = $this->model->laporan_komitmen($tahun);

            $css = file_get_contents('./dist/assets/css/pdf_style.css');
            $this->load->library('pdfgenerator');
            $this->data['data'] = $this->model->laporan_komitmen($tahun);
            $this->data['css'] = $css;
            $this->data['title'] = 'Laporan Champion Komitmen Cabai ' . $tahun;
            $file_pdf = 'laporan_champion_komitmen';
            $paper = 'A3';
            $orientation = "portrait";
            $html = $this->load->view($this->view . '/pdf',$this->data, true);
            $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);
        }


    }

    public function export_excel() {
        $tahun = $this->input->get('tahun') ? $this->input->get('tahun') : date('Y');
        
        $fileName = 'laporan_champion_' . date('Y') . '.xlsx';
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        

        $highestColumn = $objPHPExcel->getActiveSheet()->getHighestColumn();
        $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();

        $cellRange = 'A1:' . $highestColumn . $highestRow;
        $objPHPExcel->getActiveSheet()->getStyle($cellRange)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '1E90FF')
                )
            )
        );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $columnRange = range('B', 'D');
        foreach ($columnRange as $column) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(18);
        }
        $columnRange = range('E', 'Q');
        foreach ($columnRange as $column) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(7);
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(18);

        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'NO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B2', 'CHAMPION');
        $objPHPExcel->getActiveSheet()->SetCellValue('C2', 'KECAMATAN');
        $objPHPExcel->getActiveSheet()->SetCellValue('D2', 'KABUPATEN');
        $objPHPExcel->getActiveSheet()->SetCellValue('E2', 'STOK');
        $objPHPExcel->getActiveSheet()->SetCellValue('F2', 'JAN');
        $objPHPExcel->getActiveSheet()->SetCellValue('G2', 'FEB');
        $objPHPExcel->getActiveSheet()->SetCellValue('H2', 'MAR');
        $objPHPExcel->getActiveSheet()->SetCellValue('I2', 'APR');
        $objPHPExcel->getActiveSheet()->SetCellValue('J2', 'MAY');
        $objPHPExcel->getActiveSheet()->SetCellValue('K2', 'JUN');
        $objPHPExcel->getActiveSheet()->SetCellValue('L2', 'JUL');
        $objPHPExcel->getActiveSheet()->SetCellValue('M2', 'AUG');
        $objPHPExcel->getActiveSheet()->SetCellValue('N2', 'SEP');
        $objPHPExcel->getActiveSheet()->SetCellValue('O2', 'OCT');
        $objPHPExcel->getActiveSheet()->SetCellValue('P2', 'NOV');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q2', 'DEC');
        $objPHPExcel->getActiveSheet()->SetCellValue('R2', 'TOTAL KUMULATIF');
        $objPHPExcel->getActiveSheet()->SetCellValue('S2', 'KONTAK DINAS');

        $data = $this->model->laporan_komitmen($tahun);

        if($data) {
            $rowCount = 6;
            $no = 1;
            foreach($data as $row) {
                $rowCount = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow() + 1;

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['champion_nama']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['kecamatan_nama']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['kabupaten_nama']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['stok']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['jan']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['feb']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['mar']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['apr']);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $row['may']);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row['jun']);
                $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row['jul']);
                $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $row['aug']);
                $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $row['sep']);
                $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $row['oct']);
                $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $row['nov']);
                $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, $row['dec']);
                $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $row['stok']);
                $objPHPExcel->getActiveSheet()->SetCellValue('S' . $rowCount, $row['kontak_dinas_nama']);
                
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}