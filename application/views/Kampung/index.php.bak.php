<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#data-laporan" role="tab">
                            <i class="flaticon2-heart-rate-monitor" aria-hidden="true"></i>Pencarian Data Lahan
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="data-laporan" role="tabpanel">
                    <form class="kt-form" action="" id="form1">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Periode Registrasi</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control tanggal_permohonan_awal datepicker"
                                                name="tanggal_permohonan_awal" value="">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control tanggal_permohonan_akhir datepicker"
                                                name="tanggal_permohonan_akhir" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Periode Panen</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control perkiraan_panen_awal datepicker"
                                                name="perkiraan_panen_awal" value="">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control perkiraan_panen_akhir datepicker"
                                                name="perkiraan_panen_akhir" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Provinsi</label>
                                    <select class="form-control kt-select2 provinsi_kode" name="provinsi_kode"
                                        style="width:100%;"></select>
                                </div>
                                <div class="col-md-6">
                                    <label>Kabupaten</label>
                                    <select class="form-control kt-select2 kabupaten_kode" name="kabupaten_kode"
                                        style="width:100%;"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Jenis Komoditi</label>
                                    <select class="form-control kt-select2 kelompok_komoditi_id" name="kelompok_komoditi_id"
                                        style="width:100%;"></select>
                                </div>
                                <div class="col-md-6">
                                    <label>Komoditi</label>
                                    <select class="form-control kt-select2 jenis_komoditi_id" name="jenis_komoditi_id"
                                        style="width:100%;"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Varietas</label>
                                    <select class="form-control kt-select2 sub_komoditi_id" name="sub_komoditi_id"
                                        style="width:100%;"></select>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="filter" value="1">
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{site_url}/kampung" class="btn btn-secondary">Clear</a>
                            </div>
                        </div>

                        <?php if ($get_laporan) : ?>

                        <div class="kt-portlet__body">
                            <div class="kt-form__actions mb-5">
                                <?php
                                    $urlGETS = $this->input->get();
                                    $get = '';
                                    foreach ($urlGETS as $key => $val) {
                                        $get .= $key . '=' . $val . '&';
                                    }
                                    $get = substr($get, 0, -1);
                                    ?>
                                <!-- <a href="<?php echo site_url('kampung/print_pdf?' . $get) ?>"
                                    class="btn btn-warning">EXPORT PDF</a> -->
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped- table-bordered table-hover table-checkable"
                                    id="kt_table_1">
                                    <thead>
                                        <tr>
                                            <?php if ($this->session->userdata('level') == 1) : ?>
                                            <th style="min-width:50px;">Aksi</th>
                                            <?php endif ?>
                                            <th style="min-width:50px;">No.</th>
                                            <th style="min-width:150px;">No. Reg</th>
                                            <th style="min-width:150px;">Provinsi</th>
                                            <th style="min-width:150px;">Kabupaten</th>
                                            <th style="min-width:50px;">Luas/M2</th>
                                            <th style="min-width:150px;">Kampung</th>
                                            <th style="min-width:150px;">Kecamatan</th>
                                            <th style="min-width:150px;">Desa</th>
                                            <th style="min-width:150px;">Kelompok</th>
                                            <th style="min-width:150px;">Ketua</th>
                                            <th style="min-width:150px;">NIK</th>
                                            <th style="min-width:150px;">HP</th>
                                            <th style="min-width:150px;">Komoditas</th>
                                            <th style="min-width:150px;">Vaietas</th>
                                            <th style="min-width:50px;">Luas/M2</th>
                                            <th style="min-width:50px;">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                            $nomor = 1;
                                            $no = 1;
                                            $id_pemohon = null;
                                            $prov_nama = '';
                                            $prov_volume = 0;
                                            $nomor_registrasi = null;
                                            $kab_nama = '';
                                            $kab_volume = 0;
                                            $jumlah_kampung = 0;
                                            $nama_kampung = 0;

                                            $jenis_nama = '';
                                            $jenis_volume = 0;

                                            $countLaporan = count($get_laporan);
                                            ?>
                                        <?php foreach ($get_laporan as $row) : ?>

                                        <?php if ($kab_nama != $row['kabupaten_nama'] || $jenis_nama != $row['komoditi_jenis_nama']) : ?>
                                        <?php if ($no > 1) : ?>
                                        <tr class="bg-info text-white">
                                            <td colspan="5" class="text-left">TOTAL LUAS KOMODITI <span
                                                    class="font-weight-bold"><?php echo strtoupper($jenis_nama) ?></span>
                                                DI <?php echo strtoupper($kab_nama) ?></td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($kab_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="9">&nbsp;</td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($kab_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <?php endif ?>
                                        <?php $kab_volume = 0 ?>
                                        <?php endif ?>
                                        <?php $kab_nama = $row['kabupaten_nama'] ?>
                                        <?php $kab_volume += $row['volume'] ?>



                                        <?php if ($jenis_nama != $row['komoditi_jenis_nama']) : ?>
                                        <?php if ($no > 1) : ?>
                                        <tr class="bg-warning text-dark">
                                            <td colspan="5" class="text-left">TOTAL LUAS KOMODITI <span
                                                    class="font-weight-bold"><?php echo strtoupper($jenis_nama) ?></span>
                                            </td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($prov_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="9">DARI <span
                                                    class="font-weight-bold"><?php echo $jumlah_kampung ?></span>
                                                KAMPUNG</td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($prov_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <?php endif ?>
                                        <?php $jumlah_kampung = 0 ?>
                                        <?php $prov_volume = 0 ?>
                                        <?php endif ?>
                                        <?php $jenis_nama = $row['komoditi_jenis_nama'] ?>


                                        <?php if ($prov_nama != $row['provinsi_nama'] || $jenis_nama != $row['komoditi_jenis_nama']) : ?>
                                        <?php if ($no > 1) : ?>
                                        <tr class="bg-dark text-white">
                                            <td colspan="5" class="text-left">TOTAL LUAS KOMODITI <span
                                                    class="font-weight-bold"><?php echo strtoupper($jenis_nama) ?></span>
                                                DI <?php echo strtoupper($prov_nama) ?></td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($prov_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="9">&nbsp;</td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($prov_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <?php endif ?>
                                        <?php $prov_volume = 0 ?>
                                        <?php endif ?>
                                        <?php $prov_nama = $row['provinsi_nama'] ?>
                                        <?php $prov_volume += $row['volume'] ?>

                                        <tr>
                                            <?php if ($this->session->userdata('level') == 1) : ?>
                                            <td class="text-center">
                                                <?php if ($id_pemohon != $row['id_pemohon']) : ?>
                                                <a href="<?php echo site_url('kampung/edit/' . $row['id_pemohon'] . '?' . $get) ?>"
                                                    class="btn btn-primary">Edit</a>
                                                <?php endif ?>
                                            </td>
                                            <?php endif ?>
                                            <td class="text-center">
                                                <?php echo ($id_pemohon == $row['id_pemohon']) ? '' : $no ?></td>
                                            <td class="text-left">
                                                <?php echo ($id_pemohon == $row['id_pemohon']) ? '' : $row['nomor_registrasi'] ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo ($id_pemohon == $row['id_pemohon']) ? '' : $row['provinsi_nama'] ?>
                                            </td>
                                            <td class="text-left">
                                                <?php echo ($id_pemohon == $row['id_pemohon']) ? '' : $row['kabupaten_nama'] ?>
                                            </td>
                                            <td class="text-center"><?php echo number_format($row['volume'] * 1000) ?>
                                            </td>
                                            <td class="text-left"><?php echo $row['komoditi_jenis_nama'] ?></td>
                                            <td class="text-left"><?php echo $row['kecamatan_nama'] ?></td>
                                            <td class="text-left"><?php echo $row['desa_nama'] ?></td>
                                            <td class="text-left"><?php echo $row['nama_kelompok'] ?></td>
                                            <td class="text-left"><?php echo $row['nama_ketua'] ?></td>
                                            <td class="text-left"><?php echo $row['nik'] ?></td>
                                            <td class="text-left"><?php echo $row['no_hp'] ?></td>
                                            <td class="text-left">
                                                <?php echo $row['komoditi_jenis_kode'] . ' - ' . $row['komoditi_jenis_nama'] ?>
                                            </td>
                                            <td class="text-left"><?php echo $row['sub_komoditi_nama']
                                                                            ?></td>
                                            <td class="text-center"><?php echo number_format($row['volume'] * 1000) ?>
                                            </td>
                                            <?php $jumlah = $this->model->get_jumlah($row['id_pemohon']); ?>
                                            <td class="text-center">
                                                <?php echo ($id_pemohon == $row['id_pemohon']) ? '' : $jumlah ?></td>
                                        </tr>



                                        <?php if ($id_pemohon != $row['id_pemohon']) : ?>
                                        <?php $no++ ?>
                                        <?php $jumlah_kampung++ ?>
                                        <?php endif ?>
                                        <?php if ($countLaporan == $nomor) : ?>
                                        <tr class="bg-warning text-dark">
                                            <td colspan="5" class="text-left">TOTAL LUAS <span
                                                    class="font-weight-bold"><?php echo strtoupper($jenis_nama) ?></span>
                                            </td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($prov_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="9">DARI <span
                                                    class="font-weight-bold"><?php echo $jumlah_kampung ?></span>
                                                KAMPUNG</td>
                                            <td colspan="1" class="text-center"><span
                                                    class="font-weight-bold"><?php echo number_format($prov_volume * 1000) ?></span>
                                            </td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <?php endif ?>

                                        <?php $id_pemohon = $row['id_pemohon'] ?>
                                        <?php $jenis_nama = $row['komoditi_jenis_nama'] ?>
                                        <?php $nomor_registrasi = $row['nomor_registrasi'] ?>
                                        <?php $nomor++ ?>



                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php endif ?>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <script>

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });

        $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');
        
        
        ajaxSelect({
            allowClear: true,
            id: '.provinsi_kode',
            url: '{site_url}ajax_selectbox/provinsi_select',
        });

        $('.provinsi_kode').change(function() {
            var val = $(this).val();
            $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
            ajaxSelect({
                id: '.kabupaten_kode',
                url: '{site_url}ajax_selectbox/kabupaten_select',
                optionalSearch: {
                    provinsi_kode: val
                }
            });
        });

        ajaxSelect({
            id: '.kelompok_komoditi_id',
            url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
            selected: '<?php echo $this->input->get('kelompok_komoditi_id') ?>'
        });

        $('.kelompok_komoditi_id').change(function() {
            var val = $(this).val();
            $('.jenis_komoditi_id').empty();
            ajaxSelect({
                id: '.jenis_komoditi_id',
                url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                optionalSearch: {
                    kelompok_komoditi_id: val
                }
            });
        });

        $('.jenis_komoditi_id').change(function() {
            var val = $(this).val();
            $('.sub_komoditi_id').empty();
            ajaxSelect({
                id: '.sub_komoditi_id',
                url: '{site_url}ajax_selectbox/sub_komoditi_id',
                optionalSearch: {
                    jenis_komoditi_id: val
                }
            });
        });

    $('.sub_komoditi_id').select2({
        allowClear: true,
        placeholder: 'Pilih Varietas',
    }).val('').trigger('change');

    $('.status').select2({
        allowClear: true,
        placeholder: 'Pilih Status',
    }).val('').trigger('change');

    $('.jenis_registrasi').select2({
        allowClear: true,
        placeholder: 'Pilih Jenis Registrasi',
    }).val('').trigger('change');

    function filter() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();
        setTimeout(() => {
            $.ajax({
                url: '{segment1}get_view_filter',
                method: 'post',
                data: formData,
                contentType: false,
                processData: false,
                success: function(data) {
                    $('#form_result_filter').html(data);
                    btnSpinnerHide();
                },
            });
        }, 500);

    }
    </script>