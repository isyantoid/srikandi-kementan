<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_komitmen extends CI_Controller
{

    private $title = 'Champion Komitmen';
    private $view = 'Champ_komitmen';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function bawang_merah()
    {
        akses_user('read');
        $data['title'] = $this->title . ' Bawang Merah';
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index_bawang_merah';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable_bawang_merah()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('champ_komitmen.*, tbl_user.nama as user_nama');
        $this->datatables->join('tbl_user','champ_komitmen.user_id = tbl_user.id');
        if($this->session->userdata('session_user_level_id') == 13) {
            $this->datatables->where('tbl_user.kabupaten_kode',$this->session->userdata('session_kabupaten_kode'));
        } else {
            $this->datatables->where('champ_komitmen.user_id',$this->session->userdata('session_user_id'));
        }
        $this->datatables->from('champ_komitmen');
        return print_r($this->datatables->generate());
    }
    
    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('champ_komitmen.*, tbl_user.nama as user_nama');
        $this->datatables->join('tbl_user','champ_komitmen.user_id = tbl_user.id');
        if($this->session->userdata('session_user_level_id') == 13) {
            $this->datatables->where('tbl_user.kabupaten_kode',$this->session->userdata('session_kabupaten_kode'));
        } else {
            $this->datatables->where('champ_komitmen.user_id',$this->session->userdata('session_user_id'));
        }
        $this->datatables->from('champ_komitmen');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('champ_komitmen', array('id' => $id));
            if ($data) {
                $data['data'] = $data;
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit ' . $this->title;
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}