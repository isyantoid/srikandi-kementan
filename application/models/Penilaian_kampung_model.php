<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Penilaian_kampung_model extends CI_Model
{

    private $tableName = 'tbl_penilaian';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $permohonan = getRowArray('tbl_permohonan', ['id' => $this->input->post('nomor_registrasi')]);
            
            
            
            $id = $this->uri->segment(3);
            if ($id) {
                $penilaian_id = $id;

                $kriteria = $this->input->post('kriteria_id');
                $countKriteria = count($kriteria);
                
                $nilai_total = 0;

                if ($countKriteria > 0) {
                    for ($i = 0; $i < $countKriteria; $i++) {
                        $id_penilaian_detail = $this->input->post('id_penilaian_detail')[$i];
                        $kriteria_id = $this->input->post('kriteria_id')[$i];
                        $nilai = $this->input->post('nilai')[$kriteria_id];
                        if($nilai == '1') $nilai_total = $nilai_total + 1;

                        $this->db->set('kriteria_id', $kriteria_id);
                        $this->db->set('nilai', $nilai);
                        $this->db->set('penilaian_id', $penilaian_id);
                        $this->db->where('id', $id_penilaian_detail);
                        $this->db->update('tbl_penilaian_detail');
                    }
                }

                $this->db->set('nilai_total', $nilai_total);
                $this->db->set('nilai_rating', '`nilai_total` / `nilai_pembagi`', false);
                $this->db->set('nilai_rating_persen', '20 * `nilai_total` / `nilai_pembagi`', false);
                $this->db->where('id', $penilaian_id);
                $this->db->update('tbl_penilaian');
                return jsonOutputSuccess();
            } else {
                $cekPenilaianRegistrasi = getRowArray('tbl_penilaian', array('permohonan_id' => $this->input->post('nomor_registrasi')));
                if($cekPenilaianRegistrasi) {
                    $this->db->delete('tbl_penilaian_detail', array('penilaian_id' => $cekPenilaianRegistrasi['id']));
                    $this->db->delete('tbl_penilaian', array('id' => $cekPenilaianRegistrasi['id']));
                }
                
                $this->db->set('tanggal_penilaian', date('Y-m-d'));
                $this->db->set('permohonan_id', $this->input->post('nomor_registrasi'));
                $this->db->set('provinsi_kode', $permohonan['provinsi_kode']);
                $this->db->set('kabupaten_kode', $permohonan['kabupaten_kode']);
                $this->db->set('kecamatan_kode', $permohonan['kecamatan_kode']);
                $this->db->set('desa_kode', $permohonan['desa_kode']);
                $this->db->set('kelompok_komoditi_id', $permohonan['kelompok_komoditi_id']);
                $this->db->set('jenis_komoditi_id', $permohonan['jenis_komoditi_id']);
                $this->db->set('sub_komoditi_id', $permohonan['sub_komoditi_id']);
                $this->db->insert('tbl_penilaian');
                $penilaian_id = $this->db->insert_id();

                $kriteria = $this->input->post('kriteria_id');
                $countKriteria = count($kriteria);
                
                $nilai_total = 0;

                if ($countKriteria > 0) {
                    for ($i = 0; $i < $countKriteria; $i++) {
                        $kriteria_id = $this->input->post('kriteria_id')[$i];
                        $nilai = $this->input->post('nilai')[$kriteria_id];
                        if($nilai == '1') $nilai_total = $nilai_total + 1;

                        $this->db->set('kriteria_id', $kriteria_id);
                        $this->db->set('nilai', $nilai);
                        $this->db->set('penilaian_id', $penilaian_id);
                        $this->db->insert('tbl_penilaian_detail');
                    }
                }

                $this->db->set('nilai_total', $nilai_total);
                $this->db->set('nilai_rating', '`nilai_total` / `nilai_pembagi`', false);
                $this->db->set('nilai_rating_persen', '20 * `nilai_total` / `nilai_pembagi`', false);
                $this->db->where('id', $penilaian_id);
                $this->db->update('tbl_penilaian');
                return jsonOutputSuccess();
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('tbl_penilaian');
        
        $this->db->where('penilaian_id', $id);
        $this->db->delete('tbl_penilaian_detail');
    }

    public function detail_permohonan($id_permohonan)
    {
        $this->db->select('
            tbl_permohonan.*,
            tbl_kelompok_komoditi.kelompok_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_sub_komoditi.sub_komoditi_nama,
        ');
        $this->db->join('tbl_kelompok_komoditi', 'tbl_permohonan.kelompok_komoditi_id = tbl_kelompok_komoditi.id', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->join('tbl_sub_komoditi', 'tbl_permohonan.sub_komoditi_id = tbl_sub_komoditi.id', 'left');
        $this->db->where('tbl_permohonan.id', $id_permohonan);
        $this->db->from('tbl_permohonan');
        $get = $this->db->get();
        return $get->row_array();
    }

    public function get_kelompok_tani($id_permohonan)
    {
        $this->db->select(' tbl_kelompok_tani.*');
        $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $get = $this->db->get();
        return $get->result_array();
    }

    public function get_anggota_tani($id_kelompok_tani)
    {
        $this->db->select(' tbl_anggota_tani.*');
        $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
        $this->db->from('tbl_anggota_tani');
        $get = $this->db->get();
        return $get->result_array();
    }

    public function penilaian_export() {
        $this->db->select('
            tbl_penilaian.id,
            tbl_permohonan.nomor_registrasi,
            tbl_penilaian.tanggal_penilaian,
            tbl_penilaian.nilai_rating_persen,
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_penilaian.permohonan_id = tbl_permohonan.id', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->order_by('tbl_jenis_komoditi.jenis_komoditi_nama ASC');
        $this->db->from('tbl_penilaian');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function penilaian_detail($penilaian_id) {
        $this->db->select('
            concat(tbl_indikator_penilaian.nama_indikator,' - ', tbl_kriteria_penilaian.nama_kriteria) as indikator,
            tbl_penilaian_detail.nilai
            from tbl_penilaian_detail
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_penilaian.permohonan_id = tbl_permohonan.id', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->order_by('tbl_jenis_komoditi.jenis_komoditi_nama ASC');
        $this->db->from('tbl_penilaian');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function get_kriteria_penilaian() {
        $this->db->select('
            tbl_kriteria_penilaian.indikator_id,
            tbl_kriteria_penilaian.id as kriteria_id,
            tbl_indikator_penilaian.nama_indikator,
            tbl_kriteria_penilaian.nama_kriteria
        ');
        $this->db->join('tbl_indikator_penilaian', 'tbl_kriteria_penilaian.indikator_id = tbl_indikator_penilaian.id', 'left');
        $this->db->from('tbl_kriteria_penilaian');
        $res = $this->db->get();
        return $res->result_array();
    }
}
