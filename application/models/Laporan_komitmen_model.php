<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_komitmen_model extends CI_Model
{

    private $tableName = 'champ_realisasi_pasar';
    private $primaryKey = 'id';

    

    public function laporan_komitmen($tahun = '', $jenis_komoditi_id = '')
    {
        $id = $this->uri->segment(3);
        $this->db->select('
            champ_komitmen.*,
            champ.nama as champion_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            champ_kontak_dinas.nama as kontak_dinas_nama,
            champ_kontak_dinas.nomor_hp as kontak_dinas_nomor_hp,
        ');
        $this->db->join('champ', 'champ_komitmen.user_id = champ.user_id', 'left');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('champ_kontak_dinas', 'champ_kontak_dinas.kabupaten_kode = champ.kabupaten_kode', 'left');
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->where('champ.jenis_komoditi_id', $jenis_komoditi_id);
        $get = $this->db->get('champ_komitmen');
        return $get->result_array();
    }
}