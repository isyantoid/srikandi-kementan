<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}" class="btn btn-danger btn-elevate btn-icon-sm">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="row">
                    <div class="col-5">
                        <table class="table table-borderless table-hover table-checkable">
                            <tbody>
                                <tr>
                                    <td>Nomor Registrasi Horti</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{nomor_registrasi}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Kirim Penilaian</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{tanggal_penilaian}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{provinsi_nama}</td>
                                </tr>
                                <tr>
                                    <td>Kabupaten</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{kabupaten_nama}</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{kecamatan_nama}</td>
                                </tr>
                                <tr>
                                    <td>Desa</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{desa_nama}</td>
                                </tr>
                                <tr>
                                    <td>Kelompok Komoditas</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{kelompok_komoditi_nama}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Komoditas</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{jenis_komoditi_nama}</td>
                                </tr>
                                <tr>
                                    <td>Sub Komoditas</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{sub_komoditi_nama}</td>
                                </tr>
                                <tr>
                                    <td>Total Penilaian</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{nilai_total}</td>
                                </tr>
                                <tr>
                                    <td>Rating</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">
                                        <span id="score"></span>
                                        <br><small class="description-score"></small>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-7">
                        <table class="table table-sm">
                            <thead>
                                <th>#</th>
                                <th>Indikator</th>
                                <th>Ya/Tidak</th>
                            </thead>
                            <tbody>
                                <?php $indikator = getResultArray('tbl_indikator_penilaian') ?>
                                <?php $no = 1;
                                foreach ($indikator as $row) : ?>
                                    <tr>
                                        <td><?php echo $no ?></td>
                                        <td class="font-weight-bold" colspan="2"><?php echo $row['nama_indikator'] ?></td>
                                    </tr>
                                    <?php $kriteria = getResultArray('tbl_kriteria_penilaian', array('indikator_id' => $row['id'])) ?>
                                    <?php foreach ($kriteria as $k) : ?>
                                        <?php
                                            $penilaian_detail = getRowArray('tbl_penilaian_detail', [
                                                'penilaian_id' => $id,
                                                'indikator_id' => $row['id'],
                                                'kriteria_id' => $k['id'],
                                            ]);
                                        ?>
                                        <input type="hidden" name="id_penilaian_detail[]" value="<?php echo $penilaian_detail['id'] ?>">
                                        <input type="hidden" name="kriteria_id[]" value="<?php echo $k['id'] ?>">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td><?php echo $k['nama_kriteria'] ?></td>
                                            <td>
                                                <?php if($penilaian_detail['nilai'] == 1) : ?>
                                                    <label class="badge badge-success">YA</label>
                                                    <?php else: ?>
                                                        <label class="badge badge-danger">Tidak</label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                    <?php $no++ ?>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                



                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                
            </div>

        </form>
    </div>
</div>

<script>

    addScore('{nilai_rating_persen}', $('#score'));
    $('.description-score').text(descriptionScore('{nilai_rating_persen}'));
</script>