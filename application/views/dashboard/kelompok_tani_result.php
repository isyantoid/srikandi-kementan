<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> Kampung</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Nomor</th>
                                <th>Nama Kelompok</th>
                                <th>Ketua</th>
                                <th>NIK</th>
                                <th>NO HP</th>
                                <th>Luas</th>
                                <th>Terdaftar Simluhtan</th>
                            </thead>
                            <tbody>
                                <?php $jenis_komoditi_nama = '' ?>
                                <?php if($this->model->total_kelompok_tani_detail()) : ?>
                                    <?php $totalLuas=0; $no=1; foreach($this->model->total_kelompok_tani_detail() as $row): ?>
                                        
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $row['nomor_registrasi'] ?></td>
                                            <td><?php echo $row['nama_kelompok'] ?></td>
                                            <td><?php echo $row['nama_ketua'] ?></td>
                                            <td><?php echo $row['nik_ketua'] ?></td>
                                            <td><?php echo $row['no_hp'] ?></td>
                                            <td><?php echo $row['luas'] ?>Ha</td>
                                            <td><?php echo ($row['terdaftar_simluhtan'] == '1') ? 'YA' : 'TIDAK' ?></td>
                                        </tr>


                                    <?php $no++ ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>