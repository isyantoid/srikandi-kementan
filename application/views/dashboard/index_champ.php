<style>
.custom-infobox {
    color: black;
    padding: 0px;
    padding-top: 10px;
    font-size: 14px;
    text-transform: lowercase;
    font-weight: bold;
    text-align: center;
}

.custom-infobox-value {
    color: white;
    padding: 0px;
    padding-top: 10px;
    font-size: 14px;
    text-transform: lowercase;
    font-weight: bolder;
    text-align: center;
}

.kt-widget.kt-widget--user-profile-2 .kt-widget__body .kt-widget__item {
    padding: 2px;
}
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <form class="kt-form kt-form--label-right" action="{site_url}dashboard" id="form1">




        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet kt-portlet--mobile">
                    <div class="kt-portlet__head kt-portlet__head--lg">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon">
                                <i class="kt-font-brand flaticon2-line-chart"></i>
                            </span>
                            <h3 class="kt-portlet__head-title">Peta Geografis Champion</h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="mt-0" id="map1" style="width: 100%; height: 400px; margin:0; padding:0;"></div>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group mb-3">
                    <select class="form-control tahun" name="tahun" style="width:100%;" required>
                        <?php for($i=2019; $i<=2030; $i++): ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-success">Filter</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet" style="background-color: #ffffff;">
                    <div class="kt-portlet__body">
                        <div class="row">
                            <?php $listChampion = $this->model->get_champion($tahun); ?>
                            <?php foreach($listChampion as $row): ?>
                            <div class="col-md-3">
                                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--skin-solid p-0"
                                    style="box-shadow: 5px 5px 20px 0px rgb(0 0 0)">
                                    <div class="kt-portlet__head kt-portlet__head--noborder">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body p-3">
                                        <div class="kt-widget kt-widget--user-profile-2">
                                            <div class="kt-widget__head">
                                                <div class="kt-widget__media">
                                                    <img style="height:80px; width:80px;"
                                                        class="kt-widget__img kt-hidden-"
                                                        src="{uploads_path}users/<?php echo $row['foto'] ?>"
                                                        alt="image">
                                                </div>
                                                <div class="kt-widget__info">
                                                    <span href="#" class="kt-widget__username result_summary_provinsi">
                                                        <?php echo $row['nama'] ?> </span>
                                                    <span class="kt-widget__desc">
                                                        <p class="text-sm"><?php echo $row['kecamatan_nama'] ?>,
                                                            <?php echo $row['kabupaten_nama'] ?></p>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__body">
                                                <div class="kt-widget__item mt-3">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Champion:</span>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo $row['jenis_komoditi_nama'] ?></span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Luas Lahan:</span>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($row['luas'], 2) ?>
                                                            Ha</span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Komitmen Stok:</span>
                                                        <?php $komitmenStok = $this->model->get_champion_komitmen_stok($tahun, $row['id']) ?>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($komitmenStok['stok'], 2) ?>
                                                            Ton</span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Saldo Stok:</span>
                                                        <?php $saldoStok = $this->model->get_champion_saldo_stok($tahun, $row['id']) ?>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($komitmenStok['stok']-$saldoStok['jumlah'], 2) ?>
                                                            Ton</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet" style="background-color: #ffffff;">
                    <div class="kt-portlet__body">
                        <div class="row">
                            <?php $listChampion = $this->model->get_champion_cabai($tahun); ?>
                            <?php foreach($listChampion as $row): ?>
                            <div class="col-md-3">
                                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--skin-solid p-0"
                                    style="box-shadow: 5px 5px 20px 0px rgb(0 0 0)">
                                    <div class="kt-portlet__head kt-portlet__head--noborder">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body p-3">
                                        <div class="kt-widget kt-widget--user-profile-2">
                                            <div class="kt-widget__head">
                                                <div class="kt-widget__media">
                                                    <img style="height:80px; width:80px;"
                                                        class="kt-widget__img kt-hidden-"
                                                        src="{uploads_path}users/<?php echo $row['foto'] ?>"
                                                        alt="image">
                                                </div>
                                                <div class="kt-widget__info">
                                                    <span href="#" class="kt-widget__username result_summary_provinsi">
                                                        <?php echo $row['nama'] ?> </span>
                                                    <span class="kt-widget__desc">
                                                        <p class="text-sm"><?php echo $row['kecamatan_nama'] ?>,
                                                            <?php echo $row['kabupaten_nama'] ?></p>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__body">
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Luas Lahan <?= $row['jenis_komoditi_nama'] ?>:</span>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($row['luas'], 2) ?>
                                                            Ha</span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Komitmen Stok <?= $row['jenis_komoditi_nama'] ?>:</span>
                                                        <?php $komitmenStok = $this->model->get_champion_komitmen_stok_cabai($tahun, $row['id'], $row['jenis_komoditi_id']) ?>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($komitmenStok['stok'], 2) ?>
                                                            Ton</span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Stok <?= $row['jenis_komoditi_nama'] ?>:</span>
                                                        <?php $saldoStok = $this->model->get_champion_saldo_stok_cabai($tahun, $row['id'], $row['jenis_komoditi_id']) ?>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($komitmenStok['stok']-$saldoStok['jumlah'], 2) ?>
                                                            Ton</span>
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Luas Lahan <?= $row['jenis_komoditi_nama2'] ?>:</span>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($row['luas_kedua'], 2) ?>
                                                            Ha</span>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Komitmen Stok <?= $row['jenis_komoditi_nama2'] ?>:</span>
                                                        <?php $komitmenStok = $this->model->get_champion_komitmen_stok_cabai($tahun, $row['id'], $row['jenis_komoditi_kedua_id']) ?>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($komitmenStok['stok'], 2) ?>
                                                            Ton</span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item m-0">
                                                    <div class="kt-widget__contact">
                                                        <span class="kt-widget__label">Stok <?= $row['jenis_komoditi_nama2'] ?>:</span>
                                                        <?php $saldoStok = $this->model->get_champion_saldo_stok_cabai($tahun, $row['id'], $row['jenis_komoditi_kedua_id']) ?>
                                                        <span
                                                            class="kt-widget__data font-weight-bold"><?php echo number_format($komitmenStok['stok']-$saldoStok['jumlah'], 2) ?>
                                                            Ton</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>

<script>
var baseUrl = '{base_url}';

$('.tahun').select2().val('<?= $tahun ?>').trigger('change');
</script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap" async
    defer></script>
<!-- <script src="{base_url}dist/json/script.js"></script> -->
<!-- <script src="{base_url}dist/json/map_dashboard_champ.js"></script> -->
<script>
function initMap() {
    const map = new google.maps.Map(document.getElementById("map1"), {
        zoom: 4.65,
        center: {
            lat: -2.0423904,
            lng: 122.8983498
        },
    });
    setMarkers(map);
}

function setMarkers(map) {
    const image = {
        url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
        size: new google.maps.Size(20, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32),
    };


    $.ajax({
        url: "{site_url}dashboard/get_peta_champion",
        dataType: 'json',
        method: 'post',
        success: function(res) {
            console.log(res)
            for (let i = 0; i < res.length; i++) {

                const row = res[i];

                const marker = new google.maps.Marker({
                    position: {
                        lat: parseFloat(row.lat),
                        lng: parseFloat(row.lng)
                    },
                    map,
                    icon: {
                        url: '{uploads_path}users/' + row.foto,
                        scaledSize: new google.maps.Size(30, 30),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(0, 0)
                    },
                    title: row.nama,
                    shape: {
                        coords: [20, 20, 20], // Koordinat untuk area interaktif, misalnya klik
                        type: 'circle' // Jenis bentuk, dalam hal ini lingkaran
                    }
                });

                // new CustomMarker(new google.maps.LatLng(parseFloat(row.lat), parseFloat(row.lng)), map, '{uploads_path}users/' + row.foto)

                let contentString;
                if(row.jenis_komoditi_id != '68') {
                    contentString = `
                        <div class="text-dark">
                        <p class="m-0 small">Champion: ` + row.nama + `</p>
                        <p class="m-0 small">Komoditi: ` + row.jenis_komoditi_nama + ` ` +row.luas+ `ha </p>`;
                        if(row.jenis_komoditi_nama2 != 'null') {
                            contentString += `<p class="m-0 small">Komoditi: ` + row.jenis_komoditi_nama2 + ` ` +row.luas_kedua+ `ha </p>`;
                        }
                        contentString += `<p class="m-0 small">Kecamatan: ` + row.kecamatan_nama + `</p>
                        <p class="m-0 small">Kabupaten: ` + row.kabupaten_nama + `</p>
                        </div>`;
                } else {
                    contentString = `
                        <div class="text-dark">
                        <p class="m-0 small">Champion: ` + row.nama + `</p>
                        <p class="m-0 small">Komoditi: ` + row.jenis_komoditi_nama + `</p>
                        <p class="m-0 small">Kecamatan: ` + row.kecamatan_nama + `</p>
                        <p class="m-0 small">Kabupaten: ` + row.kabupaten_nama + `</p>
                        <p class="m-0 small">Luas Lahan: ` + row.luas + ` Ha</p>
                        </div>`;
                }

                var infowindow = new google.maps.InfoWindow();
                marker.addListener("click", () => {
                    infowindow.setContent(contentString);
                    infowindow.setOptions({
                        maxWidth: 300
                    });
                    infowindow.open({
                        anchor: marker,
                        map
                    });
                });

            }
        }
    });
}
</script>