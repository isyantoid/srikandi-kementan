<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ajax_selectbox extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if(!is_user()) {
            redirect('beranda/login');
        }
    }
    
    public function kelompok_komoditi_id($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('tbl_kelompok_komoditi.id as id, concat(tbl_kelompok_komoditi.romawi, " - ", tbl_kelompok_komoditi.kelompok_komoditi_nama) as text');
            $data = $this->db->where('tbl_kelompok_komoditi.id', $id)->get('tbl_kelompok_komoditi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_kelompok_komoditi.id as id, concat(tbl_kelompok_komoditi.romawi, " - ", tbl_kelompok_komoditi.kelompok_komoditi_nama) as text');
            $this->db->limit(50);
            $this->db->where_in('tbl_kelompok_komoditi.id', [1,2]);
            $this->db->order_by('tbl_kelompok_komoditi.romawi', 'asc');
            if ($term) $this->db->like('tbl_kelompok_komoditi.kelompok_komoditi_nama', $term);

            $data = $this->db->get('tbl_kelompok_komoditi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function jenis_komoditi_id($id = null)
    {
        $q = $this->input->get('q');
        $kelompok_komoditi_id = $this->input->get('kelompok_komoditi_id');
        if ($id) {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.kelompok_komoditi_id', [1,2]);
            $data = $this->db->where('tbl_jenis_komoditi.id', $id)->get('tbl_jenis_komoditi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.kelompok_komoditi_id', [1,2]);
            $this->db->limit(50);
            $this->db->order_by('tbl_jenis_komoditi.id', 'asc');
            if ($kelompok_komoditi_id) $this->db->where('tbl_jenis_komoditi.kelompok_komoditi_id', $kelompok_komoditi_id);
            if ($q) $this->db->like('tbl_jenis_komoditi.jenis_komoditi_nama', $q);
            $data = $this->db->get('tbl_jenis_komoditi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function cabe_id($id = null)
    {
        $q = $this->input->get('q');
        $kelompok_komoditi_id = $this->input->get('kelompok_komoditi_id');
        if ($id) {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.kelompok_komoditi_id', [1,2]);
            $data = $this->db->where('tbl_jenis_komoditi.id', $id)->get('tbl_jenis_komoditi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.id', [76,77]);
            $this->db->limit(50);
            $this->db->order_by('tbl_jenis_komoditi.id', 'asc');
            if ($kelompok_komoditi_id) $this->db->where('tbl_jenis_komoditi.kelompok_komoditi_id', $kelompok_komoditi_id);
            if ($q) $this->db->like('tbl_jenis_komoditi.jenis_komoditi_nama', $q);
            $data = $this->db->get('tbl_jenis_komoditi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function cabe_2_id($id = null)
    {
        $q = $this->input->get('q');
        $withoutId = $this->input->get('withoutId');
        $kelompok_komoditi_id = $this->input->get('kelompok_komoditi_id');
        if ($id) {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.kelompok_komoditi_id', [1,2]);
            $data = $this->db->where('tbl_jenis_komoditi.id', $id)->get('tbl_jenis_komoditi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.id', [76,77]);
            $this->db->limit(50);
            $this->db->order_by('tbl_jenis_komoditi.id', 'asc');
            if ($withoutId) $this->db->where('tbl_jenis_komoditi.id !=', $withoutId);
            if ($kelompok_komoditi_id) $this->db->where('tbl_jenis_komoditi.kelompok_komoditi_id', $kelompok_komoditi_id);
            if ($q) $this->db->like('tbl_jenis_komoditi.jenis_komoditi_nama', $q);
            $data = $this->db->get('tbl_jenis_komoditi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function bamer_id($id = null)
    {
        $q = $this->input->get('q');
        $kelompok_komoditi_id = $this->input->get('kelompok_komoditi_id');
        if ($id) {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.kelompok_komoditi_id', [1,2]);
            $data = $this->db->where('tbl_jenis_komoditi.id', $id)->get('tbl_jenis_komoditi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_jenis_komoditi.id as id, concat(tbl_jenis_komoditi.jenis_komoditi_kode, " - ", tbl_jenis_komoditi.jenis_komoditi_nama) as text');
            $this->db->where_in('tbl_jenis_komoditi.id', [68]);
            $this->db->limit(50);
            $this->db->order_by('tbl_jenis_komoditi.id', 'asc');
            if ($kelompok_komoditi_id) $this->db->where('tbl_jenis_komoditi.kelompok_komoditi_id', $kelompok_komoditi_id);
            if ($q) $this->db->like('tbl_jenis_komoditi.jenis_komoditi_nama', $q);
            $data = $this->db->get('tbl_jenis_komoditi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function sub_komoditi_id($id = null)
    {
        $q = $this->input->get('q');
        $jenis_komoditi_id = $this->input->get('jenis_komoditi_id');
        if ($id) {
            $this->db->select('tbl_sub_komoditi.id as id, concat(tbl_sub_komoditi.sub_komoditi_kode, " - ", tbl_sub_komoditi.sub_komoditi_nama) as text');
            $data = $this->db->where('tbl_sub_komoditi.id', $id)->get('tbl_sub_komoditi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_sub_komoditi.id as id, concat(tbl_sub_komoditi.sub_komoditi_kode, " - ", tbl_sub_komoditi.sub_komoditi_nama) as text');
            $this->db->limit(50);
            $this->db->order_by('tbl_sub_komoditi.id', 'asc');
            if ($jenis_komoditi_id) $this->db->where('tbl_sub_komoditi.jenis_komoditi_id', $jenis_komoditi_id);
            if ($q) $this->db->like('tbl_sub_komoditi.sub_komoditi_nama', $q);
            $data = $this->db->get('tbl_sub_komoditi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function tipe_bantuan_id($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('tbl_tipe_bantuan.id as id, concat(tbl_tipe_bantuan.tipe_bantuan) as text');
            $data = $this->db->where('tbl_tipe_bantuan.id', $id)->get('tbl_tipe_bantuan')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_tipe_bantuan.id as id, concat(tbl_tipe_bantuan.tipe_bantuan) as text');
            $this->db->limit(50);
            $this->db->where('tbl_tipe_bantuan.app_id', '2');
            $this->db->order_by('tbl_tipe_bantuan.id', 'asc');
            if ($q) $this->db->like('tbl_tipe_bantuan.tipe_bantuan', $q);
            $data = $this->db->get('tbl_tipe_bantuan')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function champ_pasar_id($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('champ_pasar.id as id, concat(champ_pasar.nama) as text');
            $data = $this->db->where('champ_pasar.id', $id)->get('champ_pasar')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('champ_pasar.id as id, concat(champ_pasar.nama) as text');
            $this->db->limit(50);
            $this->db->order_by('champ_pasar.id', 'asc');
            if ($q) $this->db->like('champ_pasar.nama', $q);
            $data = $this->db->get('champ_pasar')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function champ_bawang_merah($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('champ.id as id, concat(champ.nama) as text');
            $data = $this->db->where('champ.id', $id)->get('champ')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('champ.id as id, concat(champ.nama) as text');
            $this->db->where('champ.jenis_komoditi_id','68');
            $this->db->limit(50);
            $this->db->order_by('champ.id', 'asc');
            if ($q) $this->db->like('champ.nama', $q);
            $data = $this->db->get('champ')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function champ_cabai($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('champ.id as id, concat(champ.nama) as text');
            $data = $this->db->where('champ.id', $id)->get('champ')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('champ.id as id, concat(champ.nama) as text');
            $this->db->where('champ.jenis_komoditi_id !=','68');
            $this->db->limit(50);
            $this->db->order_by('champ.id', 'asc');
            if ($q) $this->db->like('champ.nama', $q);
            $data = $this->db->get('champ')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function jenis_bantuan_id($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('tbl_jenis_bantuan.id as id, concat(tbl_jenis_bantuan.nama_bantuan, " - ", tbl_jenis_bantuan.satuan) as text');
            $data = $this->db->where('tbl_jenis_bantuan.id', $id)->get('tbl_jenis_bantuan')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_jenis_bantuan.id as id, concat(tbl_jenis_bantuan.nama_bantuan, " - ", tbl_jenis_bantuan.satuan) as text');
            $this->db->limit(50);
            $this->db->where('tbl_jenis_bantuan.app_id', '2');
            $this->db->where('tbl_jenis_bantuan.harga_satuan >', 0);
            $this->db->order_by('tbl_jenis_bantuan.id', 'asc');
            if ($q) $this->db->like('tbl_jenis_bantuan.nama_bantuan', $q);
            $data = $this->db->get('tbl_jenis_bantuan')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function kelompok_tani_id($id = null)
    {
        $q = $this->input->get('q');
        $id_permohonan = $this->input->get('id_permohonan');
        if ($id) {
            $this->db->select('tbl_kelompok_tani.id as id, concat(tbl_kelompok_tani.nama_kelompok) as text');
            $data = $this->db->where('tbl_kelompok_tani.id', $id)->get('tbl_kelompok_tani')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_kelompok_tani.id as id, concat(tbl_kelompok_tani.nama_kelompok) as text');
            $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id');
            $this->db->limit(50);
            if ($id_permohonan) $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
            $this->db->order_by('tbl_kelompok_tani.id', 'asc');
            if ($q) $this->db->like('tbl_kelompok_tani.nama_kelompok', $q);
            $data = $this->db->get('tbl_kelompok_tani')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function registrasi_p2l_id($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('
                tbl_registrasi_p2l.id as id, concat("<strong>",tbl_registrasi_p2l.nomor_registrasi_p2l, "</strong>", "<br/>", tbl_registrasi_p2l.nama_kelompok,
                " (", tbl_registrasi_p2l.nama_ketua, ")<br/>", tbl_provinsi.nama, ", ", tbl_kabupaten.nama, ", ", tbl_kecamatan.nama) as text
            ');
            $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
            $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
            $this->db->join('tbl_kecamatan', 'tbl_registrasi_p2l.kecamatan_kode = tbl_kecamatan.kode', 'left');
            $data = $this->db->where('tbl_registrasi_p2l.id', $id)->get('tbl_registrasi_p2l')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('
                tbl_registrasi_p2l.id as id, concat("<strong>",tbl_registrasi_p2l.nomor_registrasi_p2l, "</strong>", "<br/>", tbl_registrasi_p2l.nama_kelompok,
                " (", tbl_registrasi_p2l.nama_ketua, ")<br/>", tbl_provinsi.nama, ", ", tbl_kabupaten.nama, ", ", tbl_kecamatan.nama) as text
            ');
            $this->db->limit(10);
            $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
            $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
            $this->db->join('tbl_kecamatan', 'tbl_registrasi_p2l.kecamatan_kode = tbl_kecamatan.kode', 'left');
            if($this->session->userdata('session_user_level_id') == 11 || $this->session->userdata('session_user_level_id') == 10) {
                $this->db->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
            }
            $this->db->order_by('tbl_registrasi_p2l.id', 'asc');
            if ($q) $this->db->like('tbl_registrasi_p2l.nomor_registrasi_p2l', $q);
            $data = $this->db->get('tbl_registrasi_p2l')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function anggota_tani_id($id = null)
    {
        $q = $this->input->get('q');
        $id_kelompok_tani = $this->input->get('id_kelompok_tani');
        if ($id) {
            $this->db->select('tbl_anggota_tani.id as id, concat(tbl_anggota_tani.nama_anggota) as text');
            $data = $this->db->where('tbl_anggota_tani.id', $id)->get('tbl_anggota_tani')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_anggota_tani.id as id, concat(tbl_anggota_tani.nama_anggota) as text');
            $this->db->join('tbl_permohonan', 'tbl_anggota_tani.id_permohonan = tbl_permohonan.id');
            $this->db->limit(50);
            if ($id_kelompok_tani) $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
            $this->db->order_by('tbl_anggota_tani.id', 'asc');
            if ($q) $this->db->like('tbl_anggota_tani.nama_anggota', $q);
            $data = $this->db->get('tbl_anggota_tani')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    public function nomor_registrasi($id = null)
    {
        $q = $this->input->get('q');
        $provinsi_kode = $this->input->get('provinsi_kode');
        $kabupaten_kode = $this->input->get('kabupaten_kode');
        $kecamatan_kode = $this->input->get('kecamatan_kode');
        $desa_kode = $this->input->get('desa_kode');
        $kelompok_komoditi_kode = $this->input->get('kelompok_komoditi_kode');
        $jenis_komoditi_kode = $this->input->get('jenis_komoditi_kode');
        $sub_komoditi_kode = $this->input->get('sub_komoditi_kode');

        if ($id) {
            $this->db->select('tbl_permohonan.id as id, concat(tbl_permohonan.tanggal_permohonan, " - ", tbl_permohonan.nomor_registrasi) as text');
            $data = $this->db->where('tbl_permohonan.id', $id)->get('tbl_permohonan')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_permohonan.id as id, concat(tbl_permohonan.tanggal_permohonan, " - ", tbl_permohonan.nomor_registrasi) as text');
            $this->db->limit(50);
            $this->db->where('tbl_permohonan.app_id', '2');
            $this->db->order_by('tbl_permohonan.id', 'asc');
            if ($provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $provinsi_kode);
            if ($kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $kabupaten_kode);
            if ($kecamatan_kode) $this->db->where('tbl_permohonan.kecamatan_kode', $kecamatan_kode);
            if ($desa_kode) $this->db->where('tbl_permohonan.desa_kode', $desa_kode);
            if ($kelompok_komoditi_kode) $this->db->where('tbl_permohonan.kelompok_komoditi_kode', $kelompok_komoditi_kode);
            if ($jenis_komoditi_kode) $this->db->where('tbl_permohonan.jenis_komoditi_kode', $jenis_komoditi_kode);
            if ($sub_komoditi_kode) $this->db->where('tbl_permohonan.sub_komoditi_kode', $sub_komoditi_kode);
            if ($q) $this->db->like('tbl_permohonan.nomor_registrasi', $q);
            $data = $this->db->get('tbl_permohonan')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function no_reg_horti($id = null)
    {
        $q = $this->input->get('q');
        $provinsi_kode = $this->input->get('provinsi_kode');
        $kabupaten_kode = $this->input->get('kabupaten_kode');
        $kecamatan_kode = $this->input->get('kecamatan_kode');
        $desa_kode = $this->input->get('desa_kode');
        $kelompok_komoditi_kode = $this->input->get('kelompok_komoditi_kode');
        $jenis_komoditi_kode = $this->input->get('jenis_komoditi_kode');
        $sub_komoditi_kode = $this->input->get('sub_komoditi_kode');

        if ($id) {
            $this->db->select('tbl_permohonan.id as id, concat(tbl_permohonan.no_reg_horti) as text');
            $data = $this->db->where('tbl_permohonan.id', $id)->get('tbl_permohonan')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_permohonan.id as id, concat(tbl_permohonan.no_reg_horti) as text');
            $this->db->limit(50);
            $this->db->order_by('tbl_permohonan.id', 'asc');
            $this->db->where('tbl_permohonan.app_id', '2');
            if ($provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $provinsi_kode);
            if ($kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $kabupaten_kode);
            if ($kecamatan_kode) $this->db->where('tbl_permohonan.kecamatan_kode', $kecamatan_kode);
            if ($desa_kode) $this->db->where('tbl_permohonan.desa_kode', $desa_kode);
            if ($kelompok_komoditi_kode) $this->db->where('tbl_permohonan.kelompok_komoditi_kode', $kelompok_komoditi_kode);
            if ($jenis_komoditi_kode) $this->db->where('tbl_permohonan.jenis_komoditi_kode', $jenis_komoditi_kode);
            if ($sub_komoditi_kode) $this->db->where('tbl_permohonan.sub_komoditi_kode', $sub_komoditi_kode);
            $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
            if ($q) $this->db->like('tbl_permohonan.no_reg_horti', $q);
            $data = $this->db->get('tbl_permohonan')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }



    public function user_level_id($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('tbl_user_level.id as id, concat(tbl_user_level.nama) as text');
            $data = $this->db->where('tbl_user_level.id', $id)->get('tbl_user_level')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_user_level.id as id, concat(tbl_user_level.nama) as text');
            $this->db->where('app_id','2');
            $this->db->limit(50);
            $this->db->order_by('tbl_user_level.id', 'asc');
            if ($term) $this->db->like('tbl_user_level.nama', $term);

            $data = $this->db->get('tbl_user_level')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    public function nomor_registrasi_only($id = null)
    {
        $q = $this->input->get('q');

        if ($id) {
            $this->db->select('tbl_permohonan.id as id, concat(tbl_permohonan.nomor_registrasi) as text');
            $data = $this->db->where('tbl_permohonan.id', $id)->get('tbl_permohonan')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_permohonan.id as id, concat(tbl_permohonan.nomor_registrasi) as text');
            $this->db->limit(50);
            $this->db->where('tbl_permohonan.app_id', '2');
            $this->db->order_by('tbl_permohonan.id', 'asc');
            if ($q) $this->db->like('tbl_permohonan.nomor_registrasi', $q);
            $data = $this->db->get('tbl_permohonan')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function kelompok_tani_by_permohonan($id = null)
    {
        $q = $this->input->get('q');
        $id_permohonan = $this->input->get('id_permohonan');

        if ($id) {
            $this->db->select('tbl_kelompok_tani.id as id, concat(tbl_kelompok_tani.nama_kelompok) as text');
            $data = $this->db->where('tbl_kelompok_tani.id', $id)->get('tbl_kelompok_tani')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_kelompok_tani.id as id, concat(tbl_kelompok_tani.nama_kelompok) as text');
            $this->db->limit(50);
            if ($id_permohonan) $this->db->where('id_permohonan', $id_permohonan);
            if ($q) $this->db->like('tbl_kelompok_tani.nama_kelompok', $q);
            $this->db->order_by('tbl_kelompok_tani.id', 'asc');
            $data = $this->db->get('tbl_kelompok_tani')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    public function provinsi_select($id = null)
    {
        $q = $this->input->get('q');
        if ($id) {
            $this->db->select('tbl_provinsi.kode as id, concat(tbl_provinsi.kode, " - ", tbl_provinsi.nama) as text');
            $data = $this->db->where('tbl_provinsi.kode', $id)->get('tbl_provinsi')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_provinsi.kode as id, concat(tbl_provinsi.kode, " - ", tbl_provinsi.nama) as text');
            $this->db->order_by('tbl_provinsi.kode', 'asc');
            if ($q) $this->db->like('tbl_provinsi.nama', $q);
            $data = $this->db->get('tbl_provinsi')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function kabupaten_select($id = null)
    {
        $q = $this->input->get('q');
        $provinsi_kode = $this->input->get('provinsi_kode');
        if ($id) {
            $this->db->select('tbl_kabupaten.kode as id, concat(tbl_kabupaten.kode, " - ", tbl_kabupaten.nama) as text');
            $data = $this->db->where('tbl_kabupaten.kode', $id)->get('tbl_kabupaten')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_kabupaten.kode as id, concat(tbl_kabupaten.kode, " - ", tbl_kabupaten.nama) as text');
            $this->db->order_by('tbl_kabupaten.kode', 'asc');
            $this->db->limit(50);
            if ($provinsi_kode) {
                $this->db->where('tbl_kabupaten.provinsiKode', $provinsi_kode);
            }
            if ($q) $this->db->like('tbl_kabupaten.nama', $q);
            $data = $this->db->get('tbl_kabupaten')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function kecamatan_select($id = null)
    {
        $term = $this->input->get('q');
        $kabupaten_kode = $this->input->get('kabupaten_kode');
        if ($id) {
            $this->db->select('tbl_kecamatan.kode as id, concat(tbl_kecamatan.kode, " - ", tbl_kecamatan.nama) as text');
            $data = $this->db->where('tbl_kecamatan.kode', $id)->get('tbl_kecamatan')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_kecamatan.kode as id, concat(tbl_kecamatan.kode, " - ", tbl_kecamatan.nama) as text');
            $this->db->limit(50);
            if ($kabupaten_kode) {
                $this->db->like('tbl_kecamatan.kabupatenKode', $kabupaten_kode);
            }
            if ($term) {
                $this->db->like('tbl_kecamatan.nama', $term);
            }

            $data = $this->db->get('tbl_kecamatan')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function desa_select($id = null)
    {
        $term = $this->input->get('q');
        $kecamatan_kode = $this->input->get('kecamatan_kode');
        if ($id) {
            $this->db->select('tbl_desa.kode as id, concat(tbl_desa.nama) as text');
            $data = $this->db->where('tbl_desa.kode', $id)->get('tbl_desa')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_desa.kode as id, concat(tbl_desa.nama) as text');
            $this->db->limit(50);
            if ($kecamatan_kode) {
                $this->db->like('tbl_desa.kecamatanKode', $kecamatan_kode);
            }
            if ($term) {
                $this->db->like('tbl_desa.nama', $term);
            }

            $data = $this->db->get('tbl_desa')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    public function sipedas_provinsi() {
        $this->load->library('curl');
        $result = $this->curl->simple_get('https://sipedas.pertanian.go.id/api/wilayah/list_wilayah?thn=2022&lvl=10&lv2=11');
        $result = json_decode($result, True);
        // var_dump($result);

        $data = array();
        foreach($result as $key => $val) {
            $item['id'] = $key;
            $item['text'] = $val;
            $data[] = $item;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    
    public function sipedas_provinsi_datatable() {
        $this->load->library('curl');
        $result = $this->curl->simple_get($this->config->item('url_api_wilayah') . 'list_wilayah?thn=2022&lvl=10&lv2=11');
        $result = json_decode($result, True);

        $data = array();
        foreach($result as $key => $val) {
            $item['id'] = $key;
            $item['text'] = $val;
            $item['url'] = site_url('kabupaten/list_kab?thn=2022&lvl=11&pro=32');
            $data['data'][] = $item;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
    }

    public function sipedas_kabupaten($prov) {
        $this->load->library('curl');
        $result = $this->curl->simple_get('https://sipedas.pertanian.go.id/api/wilayah/list_wilayah?thn=2022&lvl=11&pro='.$prov.'&lv2=12');
        $result = json_decode($result, True);
        // var_dump($result);

        $term = $this->input->get('term');
        $data = array();
        foreach($result as $key => $val) {
            if($term) {
                if(str_contains($val, $term)) {
                    $item['id'] = $key;
                    $item['text'] = $val;
                }
            } else {
                $item['id'] = $key;
                $item['text'] = $val;
            }
            $data[] = $item;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    
    public function sipedas_kecamatan($kab=null) {
        $this->load->library('curl');
        $result = $this->curl->simple_get('https://sipedas.pertanian.go.id/api/wilayah/list_wilayah?thn=2022&lvl=12&pro='.substr($kab, 0, 2).'&kab='.substr($kab, 2).'&lv2=13');
        $result = json_decode($result, True);
        // var_dump($result);

        $term = $this->input->get('term');
        $data = array();
        foreach($result as $key => $val) {
            if($term) {
                if(str_contains($val, $term)) {
                    $item['id'] = $key;
                    $item['text'] = $val;
                }
            } else {
                $item['id'] = $key;
                $item['text'] = $val;
            }
            $data[] = $item;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    
    public function sipedas_desa($kec=null) {
        $this->load->library('curl');
        $result = $this->curl->simple_get('https://sipedas.pertanian.go.id/api/wilayah/list_wilayah?thn=2022&lvl=13&pro='.substr($kec, 0,2).'&kab='.substr($kec, 2,2).'&kec='.substr($kec, 4).'&lv2=14');
        $result = json_decode($result, True);
        // var_dump($result);

        $term = $this->input->get('term');
        $data = array();
        foreach($result as $key => $val) {
            if($term) {
                if(str_contains($val, $term)) {
                    $item['id'] = $key;
                    $item['text'] = $val;
                }
            } else {
                $item['id'] = $key;
                $item['text'] = $val;
            }
            $data[] = $item;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}
