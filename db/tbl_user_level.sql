/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 27/06/2023 13:05:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_user_level
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_level`;
CREATE TABLE `tbl_user_level`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `app_id` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_user_level
-- ----------------------------
INSERT INTO `tbl_user_level` VALUES (1, 'ADMIN DEVELOPER', 2);
INSERT INTO `tbl_user_level` VALUES (2, 'ADMIN PROVINSI STO', 2);
INSERT INTO `tbl_user_level` VALUES (3, 'ADMIN KABUPATEN STO', 2);
INSERT INTO `tbl_user_level` VALUES (5, 'ADMIN PUSAT STO', 2);
INSERT INTO `tbl_user_level` VALUES (6, 'ADMIN PUSAT BUFLO', 3);
INSERT INTO `tbl_user_level` VALUES (9, 'ADMIN P2L STO', 2);

SET FOREIGN_KEY_CHECKS = 1;
