<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kampung_model extends CI_Model
{

    public function get_jumlah($id_permohonan)
    {
        return $this->db->where('id', $id_permohonan)->get('tbl_permohonan')->num_rows();
    }

    public function get_permohonan($jenis_komoditi_id, $provinsi_kode, $kabupaten_kode) {
        $this->db->select('
            tbl_permohonan.id,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_id,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.sub_komoditi_nama,
            tbl_kelompok_tani.luas as luas_lahan_kelompok_tani,
            tbl_kelompok_tani.nama_kelompok,
            tbl_kelompok_tani.nama_ketua,
            tbl_kelompok_tani.nik_ketua as nik,
            tbl_kelompok_tani.no_hp,
        ');
        $this->db->join('tbl_kelompok_tani', 'tbl_permohonan.id = tbl_kelompok_tani.id_permohonan', 'left');
        $this->db->order_by('tbl_permohonan.jenis_komoditi_nama ASC');
        $this->db->where('tbl_permohonan.jenis_komoditi_id', $jenis_komoditi_id);
        $this->db->where('tbl_permohonan.provinsi_kode', $provinsi_kode);
        $this->db->where('tbl_permohonan.kabupaten_kode', $kabupaten_kode);
        $this->db->where('tbl_permohonan.jenis_komoditi_nama !=', '');
        $this->db->where('tbl_permohonan.app_id', '2');
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        if($this->input->get('jenis_komoditi_id')) $this->db->where('tbl_permohonan.jenis_komoditi_id', $this->input->get('jenis_komoditi_id'));
        $this->db->from('tbl_permohonan');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_luas_kampung() {
        $this->db->select('
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.jenis_komoditi_id,
            tbl_permohonan.satuan_luas_kebun,
            sum(tbl_permohonan.luas_kebun) as total_luas_kebun,
        ');
        $this->db->group_by('tbl_permohonan.jenis_komoditi_id');
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        if($this->input->get('jenis_komoditi_id')) $this->db->where('tbl_permohonan.jenis_komoditi_id', $this->input->get('jenis_komoditi_id'));
        $this->db->where('tbl_permohonan.jenis_komoditi_nama !=', '');
        $this->db->where('tbl_permohonan.app_id', '2');
        $this->db->from('tbl_permohonan');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_luas_kampung_prov($jenis_komoditi_id) {
        $this->db->select('
            tbl_permohonan.satuan_luas_kebun,
            sum(tbl_permohonan.luas_kebun) as total_luas_kebun,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.provinsi_kode,
        ');
        $this->db->group_by('tbl_permohonan.provinsi_kode');
        $this->db->where('tbl_permohonan.jenis_komoditi_id', $jenis_komoditi_id);
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        if($this->input->get('jenis_komoditi_id')) $this->db->where('tbl_permohonan.jenis_komoditi_id', $this->input->get('jenis_komoditi_id'));
        $this->db->where('tbl_permohonan.jenis_komoditi_nama !=', '');
        $this->db->where('tbl_permohonan.app_id', '2');
        $this->db->from('tbl_permohonan');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_luas_kampung_kab($jenis_komoditi_id, $provinsi_kode) {
        $this->db->select('
            tbl_permohonan.satuan_luas_kebun,
            sum(tbl_permohonan.luas_kebun) as total_luas_kebun,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kabupaten_kode,
        ');
        $this->db->group_by('tbl_permohonan.kabupaten_kode');
        $this->db->where('tbl_permohonan.jenis_komoditi_id', $jenis_komoditi_id);
        $this->db->where('tbl_permohonan.provinsi_kode', $provinsi_kode);
        $this->db->where('tbl_permohonan.jenis_komoditi_nama !=', '');
        $this->db->where('tbl_permohonan.app_id', '2');
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        if($this->input->get('jenis_komoditi_id')) $this->db->where('tbl_permohonan.jenis_komoditi_id', $this->input->get('jenis_komoditi_id'));
        $this->db->from('tbl_permohonan');
        $res = $this->db->get();
        return $res->result_array();
    }


    public function get_laporan()
    {
        // $this->db->select('
        //     id_pemohon,
        //     komoditi_jenis_m.komoditi_jenis_kode,
        //     komoditi_jenis_nama,
        //     desa_nama,
        //     nomor_registrasi,
        //     provinsi_nama,
        //     kabupaten_nama,
        //     kecamatan_nama,
        //     luas as volume,
        //     nama_kampung,
        //     nama_kelompok,
        //     nama_ketua,
        //     nik,
        //     no_hp,
        //     varitas_m.nama as varietas,
        // ');
        // $this->db->join('permohonan_m', 'id_pemohon = id_permohonan');
        // $this->db->join('kecamatan_m', 'permohonan_m.kecamatan_kode = kecamatan_m.kecamatan_kode', 'left');
        // $this->db->join('desa_m', 'permohonan_m.desa_kode = desa_m.desa_kode', 'left');
        // $this->db->join('provinsi_m', 'permohonan_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        // $this->db->join('kabupaten_m', 'permohonan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        // $this->db->join('komoditi_jenis_m', 'permohonan_m.id_jenis_komoditi = komoditi_jenis_m.komoditi_jenis_kode');
        // $this->db->join('varitas_m', 'permohonan_m.id_varietas = varitas_m.kode', 'left');
        // $this->db->where('permohonan_m.jenis_registrasi', 'Kampung');
        // $this->db->where('permohonan_m.id_jenis_komoditi !=', '');
        // $this->db->where('permohonan_m.id_komoditi !=', '');


        // if ($this->input->get('tanggal_permohonan_awal')) {
        //     $this->db->where('permohonan_m.tanggal_permohonan >= ', $this->input->get('tanggal_permohonan_awal'));
        // }
        // if ($this->input->get('tanggal_permohonan_akhir')) {
        //     $this->db->where('permohonan_m.tanggal_permohonan <= ', $this->input->get('tanggal_permohonan_akhir'));
        // }

        // if ($this->input->get('perkiraan_panen_awal')) {
        //     $this->db->where('permohonan_m.perkiraan_panen >= ', $this->input->get('perkiraan_panen_awal'));
        // }
        // if ($this->input->get('perkiraan_panen_akhir')) {
        //     $this->db->where('permohonan_m.perkiraan_panen <= ', $this->input->get('perkiraan_panen_akhir'));
        // }

        // if ($this->input->get('id_provinsi')) {
        //     $this->db->where('permohonan_m.provinsi_kode', $this->input->get('id_provinsi'));
        // }

        // if ($this->input->get('id_kabupaten')) {
        //     $this->db->where('permohonan_m.kabupaten_kode', $this->input->get('id_kabupaten'));
        // }

        // if ($this->input->get('id_komoditi')) {
        //     $this->db->where('permohonan_m.id_komoditi', $this->input->get('id_komoditi'));
        // }

        // if ($this->input->get('id_jenis_komoditi')) {
        //     $this->db->where('permohonan_m.id_jenis_komoditi', $this->input->get('id_jenis_komoditi'));
        // }
        // if ($this->input->get('id_varietas')) {
        //     $this->db->where('permohonan_m.id_varietas', $this->input->get('id_varietas'));
        // }

        // $this->db->order_by('komoditi_jenis_nama', 'ASC');
        // $this->db->order_by('provinsi_nama', 'ASC');
        // $this->db->order_by('kabupaten_nama', 'ASC');
        // $this->db->order_by('nomor_registrasi', 'ASC');

        // $res = $this->db->get('detail_kelompok_tani');
        // echo $this->db->last_query();
        $this->db->select('
            tbl_permohonan.id as id_pemohon,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.luas_kebun as volume,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.jenis_komoditi_kode as komoditi_jenis_kode,
            tbl_kelompok_komoditi.kelompok_komoditi_nama as nama_kelompok,
            tbl_jenis_komoditi.jenis_komoditi_nama as komoditi_jenis_nama,
            tbl_sub_komoditi.sub_komoditi_nama,
            tbl_kelompok_tani.nama_ketua,
            tbl_kelompok_tani.nik_ketua as nik,
            tbl_kelompok_tani.no_hp,
        ');
        $this->db->join('tbl_kelompok_komoditi', 'tbl_permohonan.kelompok_komoditi_id = tbl_kelompok_komoditi.id', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->join('tbl_sub_komoditi', 'tbl_permohonan.sub_komoditi_id = tbl_sub_komoditi.id', 'left');
        $this->db->join('tbl_kelompok_tani', 'tbl_permohonan.id = tbl_kelompok_tani.id_permohonan', 'left');
        $this->db->where('tbl_permohonan.app_id', '2');
        $this->db->from('tbl_permohonan');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function get_jenis_komoditi()
    {
        $this->db->select('
            id_jenis_komoditi, 
            komoditi_jenis_nama, 
            id_pemohon, 
            nama_kampung,
            (select count(*) from detail_kelompok_tani where detail_kelompok_tani.id_permohonan = permohonan_m.id_pemohon) as jumlah
        ');
        $this->db->join('komoditi_jenis_m', 'permohonan_m.id_jenis_komoditi = komoditi_jenis_m.komoditi_jenis_kode');
        $this->db->where('jenis_registrasi', 'Kampung');
        $this->db->group_by('id_jenis_komoditi');
        $res = $this->db->get('permohonan_m');
        return $res->result_array();
    }

    public function get_provinsi($id_jenis_komoditi)
    {
        $this->db->select('permohonan_m.provinsi_kode, provinsi_nama, kabupaten_nama');
        $this->db->join('provinsi_m', 'permohonan_m.provinsi_kode = provinsi_m.provinsi_kode');
        $this->db->join('kabupaten_m', 'permohonan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        $this->db->where('id_jenis_komoditi', $id_jenis_komoditi);
        $this->db->order_by('id_jenis_komoditi', 'ASC');
        // $this->db->group_by('provinsi_kode');
        $res = $this->db->get('permohonan_m');
        return $res->result_array();
    }

    public function get_detail_kelompok($id_permohonan)
    {
        $this->db->select('*');
        $this->db->where('id_permohonan', $id_permohonan);
        $res = $this->db->get('detail_kelompok_tani');
        return $res->result_array();
    }

    public function data_laporan()
    {
        $this->db->select('
            permohonan_m.nomor_registrasi,
            varitas_m.nama as varietas,
            permohonan_m.id_pemohon,
            permohonan_m.satuan_luas,
            provinsi_m.provinsi_nama,
            provinsi_m.provinsi_kode,
            kabupaten_m.kabupaten_nama,
            permohonan_m.kabupaten_kode,
            permohonan_m.luas_kebun,
            permohonan_m.nama_kampung,
            kecamatan_m.kecamatan_nama,
            desa_m.desa_nama,
            permohonan_m.nama_lengkap as nama_ketua,
            registrasi_m.nomor_identitas as nik,
            permohonan_m.telepon,
            komoditi_jenis_m.komoditi_jenis_nama,
            permohonan_m.id_jenis_komoditi,
            permohonan_m.luas_kebun as volume,
            (select count(*) from detail_kelompok_tani where detail_kelompok_tani.id_permohonan = permohonan_m.id_pemohon) as jumlah
        ');

        $this->db->join('provinsi_m', 'permohonan_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        $this->db->join('kabupaten_m', 'permohonan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        $this->db->join('kecamatan_m', 'permohonan_m.kecamatan_kode = kecamatan_m.kecamatan_kode', 'left');
        $this->db->join('desa_m', 'permohonan_m.desa_kode = desa_m.desa_kode', 'left');
        $this->db->join('registrasi_m', 'permohonan_m.id_registrasi = registrasi_m.registrasi_id', 'left');
        $this->db->join('komoditi_jenis_m', 'permohonan_m.id_jenis_komoditi = komoditi_jenis_m.komoditi_jenis_kode', 'left');
        $this->db->join('varitas_m', 'permohonan_m.id_varietas = varitas_m.kode', 'left');
        $this->db->join('view_status_sertifikat', 'permohonan_m.id_pemohon = view_status_sertifikat.id_pemohon', 'left');
        $this->db->where('permohonan_m.jenis_registrasi', 'Kampung');
        $this->db->where('komoditi_jenis_m.komoditi_jenis_nama !=', '');

        if ($this->input->get('id_provinsi')) {
            $this->db->where('permohonan_m.provinsi_kode', $this->input->get('id_provinsi'));
        }

        if ($this->input->get('id_kabupaten')) {
            $this->db->where('permohonan_m.kabupaten_kode', $this->input->get('id_kabupaten'));
        }

        if ($this->input->get('id_komoditi')) {
            $this->db->where('permohonan_m.id_komoditi', $this->input->get('id_komoditi'));
        }

        if ($this->input->get('id_jenis_komoditi')) {
            $this->db->where('permohonan_m.id_jenis_komoditi', $this->input->get('jenis_komoditi'));
        }
        if ($this->input->get('id_varietas')) {
            $this->db->where('permohonan_m.id_varietas', $this->input->get('id_varietas'));
        }

        // if ( $this->input->get('jenis_registrasi') ) {
        //     $this->db->where('permohonan_m.jenis_registrasi', $this->input->get('jenis_registrasi'));
        // }

        // if ( $this->input->get('status') ) {
        //     $status = $this->input->get('status');
        //     $whereStatus = 'blm_proses';
        //     if($status == '2') {
        //         $whereStatus = 'terima';
        //     } elseif($status == '3') {
        //         $whereStatus = 'tolak';
        //     }
        //     $this->db->where('permohonan_m.status_provinsi', $whereStatus);
        // }



        $this->db->order_by('komoditi_jenis_m.komoditi_jenis_nama', 'ASC');
        $this->db->order_by('provinsi_m.provinsi_nama', 'ASC');
        $this->db->order_by('kabupaten_m.kabupaten_nama', 'ASC');
        $get = $this->db->get('permohonan_m');
        return $get->result_array();
    }


    public function countKampung()
    {
        $this->db->select('permohonan_m.id_pemohon');

        $this->db->join('provinsi_m', 'permohonan_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        $this->db->join('kabupaten_m', 'permohonan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        $this->db->join('kecamatan_m', 'permohonan_m.kecamatan_kode = kecamatan_m.kecamatan_kode', 'left');
        $this->db->join('desa_m', 'permohonan_m.desa_kode = desa_m.desa_kode', 'left');
        $this->db->join('registrasi_m', 'permohonan_m.id_registrasi = registrasi_m.registrasi_id', 'left');
        $this->db->join('komoditi_jenis_m', 'permohonan_m.id_jenis_komoditi = komoditi_jenis_m.komoditi_jenis_kode', 'left');
        $this->db->join('view_status_sertifikat', 'permohonan_m.id_pemohon = view_status_sertifikat.id_pemohon', 'left');
        $this->db->where('permohonan_m.jenis_registrasi', 'Kampung');
        $this->db->where('komoditi_jenis_m.komoditi_jenis_nama !=', '');

        if ($this->input->get('id_provinsi')) {
            $this->db->where('permohonan_m.provinsi_kode', $this->input->get('id_provinsi'));
        }

        if ($this->input->get('id_kabupaten')) {
            $this->db->where('permohonan_m.kabupaten_kode', $this->input->get('id_kabupaten'));
        }

        if ($this->input->get('id_komoditi')) {
            $this->db->where('permohonan_m.id_komoditi', $this->input->get('id_komoditi'));
        }

        if ($this->input->get('id_jenis_komoditi')) {
            $this->db->where('permohonan_m.id_jenis_komoditi', $this->input->get('jenis_komoditi'));
        }



        $this->db->order_by('komoditi_jenis_m.komoditi_jenis_nama', 'ASC');
        $this->db->order_by('provinsi_m.provinsi_nama', 'ASC');
        $this->db->order_by('kabupaten_m.kabupaten_nama', 'ASC');
        $get = $this->db->get('permohonan_m');
        return $get;
    }
}

/* End of file Lahan_model.php */