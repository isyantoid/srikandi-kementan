<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> {title} </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--noborder">
                                <div class="kt-portlet__head-label">
                                    <span class="kt-portlet__head-icon kt-hidden">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="kt-portlet__head-title"> PROVINSI JAWA BARAT </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div id="chartdiv" style="height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--noborder">
                                <div class="kt-portlet__head-label">
                                    <span class="kt-portlet__head-icon kt-hidden">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="kt-portlet__head-title"> PROVINSI JAWA TENGAH </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div id="chartdiv2" style="height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
am4core.ready(function() {

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.XYChart);

    // Add data
    chart.data = [{
        "country": "Januari",
        "research": 501.9,
        "marketing": 250,
        "sales": 199
    }, {
        "country": "Febuari",
        "research": 301.9,
        "marketing": 222,
        "sales": 251
    }, {
        "country": "Maret",
        "research": 201.1,
        "marketing": 170,
        "sales": 199
    }, {
        "country": "April",
        "research": 165.8,
        "marketing": 122,
        "sales": 90
    }, {
        "country": "Australia",
        "research": 139.9,
        "marketing": 99,
        "sales": 252
    }, {
        "country": "Austria",
        "research": 128.3,
        "marketing": 85,
        "sales": 84
    }, {
        "country": "UK",
        "research": 99,
        "marketing": 93,
        "sales": 142
    }, {
        "country": "Belgium",
        "research": 60,
        "marketing": 50,
        "sales": 55
    }, {
        "country": "The Netherlands",
        "research": 50,
        "marketing": 42,
        "sales": 25
    }];

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "country";
    categoryAxis.title.text = "Local country offices";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Expenditure (M)";

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "research";
    series.dataFields.categoryX = "country";
    series.name = "Research";
    series.tooltipText = "{name}: [bold]{valueY}[/]";
    // This has no effect
    // series.stacked = true;

    var series2 = chart.series.push(new am4charts.ColumnSeries());
    series2.dataFields.valueY = "marketing";
    series2.dataFields.categoryX = "country";
    series2.name = "Marketing";
    series2.tooltipText = "{name}: [bold]{valueY}[/]";
    // Do not try to stack on top of previous series
    // series2.stacked = true;

    var series3 = chart.series.push(new am4charts.ColumnSeries());
    series3.dataFields.valueY = "sales";
    series3.dataFields.categoryX = "country";
    series3.name = "Sales";
    series3.tooltipText = "{name}: [bold]{valueY}[/]";
    series3.stacked = true;

    // Add cursor
    chart.cursor = new am4charts.XYCursor();

    // Add legend
    chart.legend = new am4charts.Legend();

    // Add legend events
    chart.legend.itemContainers.template.events.on("hit", function(ev) {
        alert("Clicked on " + ev.target.dataItem.dataContext.name);
    });
    // end am4core.ready()

    var chart2 = am4core.create("chartdiv2", am4charts.PieChart);
    chart2.data = [{
            "sector": "Bawang Merah",
            "size": 3.6
        },
        {
            "sector": "Bawang Daun",
            "size": 7.6
        },
        {
            "sector": "Bawang Putih",
            "size": 2.2
        },
        {
            "sector": "Jahe",
            "size": 5.2
        },
        {
            "sector": "Brokoli",
            "size": 4.5
        },
    ];
    // Add label
    chart2.innerRadius = 0;
    var label = chart2.seriesContainer.createChild(am4core.Label);
    // Add and configure Series
    var pieSeries = chart2.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "size";
    pieSeries.dataFields.category = "sector";

});
</script>