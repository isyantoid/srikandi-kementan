<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Pengaturan extends CI_Controller
{

    private $title = 'Pengaturan';
    private $view = 'Pengaturan';

    public function __construct()
    {
        parent::__construct();

        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['list_pengaturan'] = $this->db->where('aktif','1')->where('html','0')->get('tbl_pengaturan')->result_array();
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function save() {
        if(akses_user('update')) {
            $post = $this->input->post();
            foreach($post as $key => $val) {
                $this->db->set('setting_desc', $val);
                $this->db->where('setting_key', $key);
                if($this->db->update('tbl_pengaturan')) {
                    return jsonOutputSuccess('data berhasil disimpan');
                }
            }
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }

    
}