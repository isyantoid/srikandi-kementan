<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_dokumen_model extends CI_Model
{
    private $tableName = 'tbl_jenis_dokumen';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $params = array(
                    'jenis_dokumen' => $this->input->post('jenis_dokumen', TRUE),
                );
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName, $params);

                if (!empty($_FILES['icon'])) :
                    $nama_image = $this->upload_image($id);
                    $this->db->where($this->primaryKey, $id);
                    $this->db->update($this->tableName, array('icon' => $nama_image));
                endif;

                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {
                $params = array(
                    'jenis_dokumen' => $this->input->post('jenis_dokumen', TRUE),
                );
                $insert = $this->db->insert($this->tableName, $params);
                $id = $this->db->insert_id();

                if (!empty($_FILES['icon'])) :
                    $nama_image = $this->upload_image($id);
                    $this->db->where($this->primaryKey, $id);
                    $this->db->update($this->tableName, array('icon' => $nama_image));
                endif;

                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }

    //delete image and folder
    function delete_all_file($id)
    {
        $dir = './uploads/form_artikel/' . $id;
        if (!is_dir($dir)) {
            $file = "direktori folder kosong";
        } else {
            $data = scandir($dir);

            $num = count($data) - 1;
            if ($num > 1) {
                foreach (scandir($dir) as $item) {
                    if ($item == '.' || $item == '..') {
                        continue;
                    }

                    unlink($dir . '/' . $item);
                }
                rmdir($dir);
            } else {
                $file = "file kosong";
            }
        }
    }

    public function upload_image($id)
    {

        $path = './uploads/jenis_dokumen/' . $id;
        if (!is_dir($path)) {
            //Directory does not exist, so lets create it.
            mkdir($path, 0755, true);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|gif|png|jpeg';
        $config['max_size'] = 0;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('icon')) {
            $error = array('error' => $this->upload->display_errors());
            $nama_file = '';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $nama_file =  $data['upload_data']['file_name'];
        }

        return $nama_file;
    }
}