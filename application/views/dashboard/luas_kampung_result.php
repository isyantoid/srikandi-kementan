<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> Kampung</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Nomor</th>
                                <th>Kelompok Komoditi</th>
                                <th>Jenis Komoditi</th>
                                <th>Tanggal</th>
                                <th>Luas</th>
                            </thead>
                            <tbody>
                                <?php $jenis_komoditi_nama = '' ?>
                                <?php if($this->model->total_jumlah_kampung()) : ?>
                                    <?php $totalLuas=0; $no=1; foreach($this->model->total_jumlah_kampung() as $row): ?>
                                        
                                        <tr>
                                            <td><?php echo $no ?></td>
                                            <td><?php echo $row['nomor_registrasi'] ?></td>
                                            <td><?php echo $row['kelompok_komoditi_nama'] ?></td>
                                            <td><?php echo $row['jenis_komoditi_nama'] ?></td>
                                            <td><?php echo formatTanggal($row['tanggal_permohonan']) ?></td>
                                            <td><?php echo number_format($row['luas_kebun']) ?>Ha</td>
                                        </tr>

                                        <?php $totalLuas += $row['luas_kebun'] ?>

                                    <?php $no++ ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                            <tfoot>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Total Luas</th>
                                <th><?php echo number_format($totalLuas) ?>Ha</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>