var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6.2,
        center: {
            lat: -2.4833826,
            lng: 117.8902853
        },
        disableDefaultUI: true
    });

    // Muat GeoJSON menggunakan fetch API
    fetch(baseUrl + 'laporan_rekap_p2l/json_nasional').then(response => response.json()).then(geojson => { // Buat lapisan GeoJSON
        var geojsonLayer = new google.maps.Data({
            map: map,
            style: {
                fillColor: 'green',
                strokeWeight: 1
            }
        });

        // Tambahkan GeoJSON ke lapisan
        geojsonLayer.addGeoJson(geojson);

        // Muat skrip InfoBox
        var script = document.createElement('script');
        script.src = 'https://cdn.jsdelivr.net/npm/js-info-bubble@0.8.0/src/infobubble-compiled.min.js';
        script.onload = function () { // Iterasi melalui fitur dan tambahkan InfoBox dengan gaya latar belakang kustom
            geojsonLayer.forEach(function (feature) {
                var name = feature.getProperty('Propinsi'); // Ambil properti NAME_1
                var kode = feature.getProperty('kode');
                var valueJson = feature.getProperty('value');
                var geometry = feature.getGeometry();

                var bounds = new google.maps.LatLngBounds();
                geometry.forEachLatLng(function (latlng) {
                    bounds.extend(latlng);
                });

                var value = $('#kodeprov-' + kode).text()
                var bgColor = 'rgba(255,255,255, 0.5)';
                var contentString = '<div class="custom-infobox">' + name + '<br>0</div>'; // Tambahkan garis baru dengan <br>
                if (value) {
                    bgColor = 'rgba(255,0,0, 0.7)';
                    contentString = '<div class="custom-infobox-value">' + name + '<br>' + valueJson + '</div>';
                }

                var infobox = new InfoBubble({
                    content: contentString,
                    position: bounds.getCenter(),
                    backgroundColor: bgColor,
                    borderRadius: 0,
                    padding: 0,
                    borderWidth: 0,
                    disableAutoPan: true,
                    closeBox: false,
                    optimized: false
                });

                infobox.open(map);
            });
        };
        document.head.appendChild(script);
    });

    $("#saveButton").click(function () {
        // $(this).attr('hidden', true)
        btnSpinnerShow('#saveButton')
        saveMapImage();
    });
}

function saveMapImage() {
    const mapCanvas = map.getDiv();

    html2canvas(mapCanvas, {
        useCORS: true, // Aktifkan CORS jika Anda mengalami masalah dengan izin
    }).then(function (canvas) {
        const dataUrl = canvas.toDataURL("image/png");

        // Kirim dataUrl ke server menggunakan metode POST
        $.ajax({
            type: "POST",
            url: baseUrl + "laporan_rekap_p2l/create_capture_map", // Ganti dengan URL skrip PHP Anda
            data: { image: dataUrl },
            success: function (response) {
                if (response === "success") {
                    // alert("Gambar berhasil disimpan di server.");
                    window.location.href = baseUrl + 'laporan_rekap_p2l/export_rekap_nasional';
                } else {
                    alert("Gagal menyimpan gambar di server.");
                }
            },
            error: function (xhr, status, error) {
                console.error(error); // Tampilkan pesan kesalahan (opsional)
                alert("Terjadi kesalahan saat mengirim permintaan ke server.");
            },
        });


        // const a = document.createElement("a");
        // a.href = dataUrl;
        // a.download = "map_image.png"; // Nama file yang diunduh
        // a.click();
    });
}
