<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}" class="btn btn-danger btn-elevate btn-icon-sm">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-borderless table-hover table-checkable w-50">
                <tbody>
                    <tr>
                        <td>Nomor Registrasi P2L</td>
                        <td>:</td>
                        <td class="font-weight-bold">{nomor_registrasi_p2l}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Registrasi</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_registrasi_p2l}</td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>:</td>
                        <td class="font-weight-bold">{provinsi_nama}</td>
                    </tr>
                    <tr>
                        <td>Kabupaten</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kabupaten_nama}</td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>:</td>
                        <td class="font-weight-bold">{kecamatan_nama}</td>
                    </tr>
                    <tr>
                        <td>Desa</td>
                        <td>:</td>
                        <td class="font-weight-bold">{desa_nama}</td>
                    </tr>
                    <tr>
                        <td>Garis Lintang</td>
                        <td>:</td>
                        <td class="font-weight-bold">{lat}</td>
                    </tr>
                    <tr>
                        <td>Garis Bujur</td>
                        <td>:</td>
                        <td class="font-weight-bold">{lng}</td>
                    </tr>
                    <tr>
                        <td>Nama Kelompok Tani</td>
                        <td>:</td>
                        <td class="font-weight-bold">{nama_kelompok}</td>
                    </tr>
                    <tr>
                        <td>Nama Ketua</td>
                        <td>:</td>
                        <td class="font-weight-bold">{nama_ketua}</td>
                    </tr>
                    <tr>
                        <td>Terdaftar Simluhtan</td>
                        <td>:</td>
                        <td class="font-weight-bold"><?= ($terdaftar_simluhtan == '1') ? 'Ya' : 'Tidak' ?></td>
                    </tr>
                    <tr>
                        <td>Jumlah Penerima Manfaat</td>
                        <td>:</td>
                        <td class="font-weight-bold">{jumlah_penerima_manfaat}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Penerima Bantuan</td>
                        <td>:</td>
                        <td class="font-weight-bold">{tanggal_penerima_bantuan}</td>
                    </tr>
                    <tr>
                        <td>Realisasi pemanfaatan anggaran</td>
                        <td>:</td>
                        <td class="font-weight-bold"><?= number_format($realisasi_pemanfaatan_anggaran,2 ) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end:: Content -->

<script>
var table;

table = $('#kt_table_1').DataTable({
    ajax: {
        url: '{segment1}datatable'
    },
    responsive: true,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 10,
    order: [
        [0, 'desc']
    ],
    columns: [{
            data: 'id',
            visible: false
        },
        {
            data: 'id',
            width: 25,
            className: 'text-center'
        },
        { data: 'nomor_registrasi' },
        { data: 'tanggal_permohonan' },
        { data: 'kelompok_komoditi_nama' },
        { data: 'jenis_komoditi_nama' },
        { data: 'sub_komoditi_nama' },
        { 
            data: 'luas_kebun',
            render: function(data, type, row) {
                return data + ' ' + row.satuan_luas_kebun;
            }
        },
        {
            targets: -1,
            width: 100,
            title: 'Actions',
            className: 'text-center',
            orderable: false,
            render: function(data, type, row, meta) {
                aksi = '<span class="dropdown">';
                aksi +=
                    '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">';
                aksi += '<i class="la la-ellipsis-h"></i>';
                aksi += '</a>';
                aksi += '<div class="dropdown-menu dropdown-menu-right">';
                aksi += '<a class="dropdown-item" href="{segment1}detail/' + row.id + '"><i class="la la-eye"></i> Detail</a>';
                aksi += '<a class="dropdown-item" href="javascript:deleteData(' + row.id + ')"><i class="la la-trash"></i> Delete</a>';
                aksi += '</div>';
                aksi += '</span>';
                return aksi;
            },
        },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        var index = iDisplayIndex + 1;
        $('td:eq(0)', nRow).html(index);
        return nRow;
    }
});

function deleteData(id) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data yang telah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus data!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '{segment1}delete/' + id
            });
            table.ajax.reload();
            Swal.fire('Sukses!', 'Data berhasil dihapus.', 'success');
        }
    });
}
</script>