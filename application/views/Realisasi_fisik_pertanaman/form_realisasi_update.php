<div id="<?= $uniqid ?>">
    <input type="hidden" name="detail_id[]" value="<?= $uniqid ?>">
    <div class="form-group row">
        <div class="col-md-2">
            <label>Nama Realisasi:</label>
            <input type="text" class="form-control nama_realisasi" name="nama_realisasi[]" value="{nama_realisasi}"
                required placeholder="Masukan nama" data-id="<?= $uniqid ?>">
        </div>
        <div class="col-md-2">
            <label>Jumlah:</label>
            <input type="number" class="form-control jumlah" name="jumlah[]" value="{jumlah}" required
                placeholder="Masukan jumlah" data-id="<?= $uniqid ?>">
        </div>
        <div class="col-md-2">
            <label>Nilai (Rp):</label>
            <input type="number" class="form-control nilai" name="nilai[]" value="{nilai}" required
                placeholder="Masukan nilai" data-id="<?= $uniqid ?>">
        </div>
        <div class="col-md-2">
            <label>Foto:</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="foto_<?= $uniqid ?>" id="foto"
                    data-id="<?= $uniqid ?>">
                <label class="custom-file-label text-left" for="foto">Pilih foto</label>
                <span class="form-text text-muted">File yang diizinkan .jpg, .jpeg, .png maksimal ukuran file (2mb)</span>
            </div>
            <div class="mt-3">
                <img src="{base_url}uploads/realisasi_bantuan/{foto}" width="60">
            </div>
        </div>
        <?php if($btnDelete == '1'): ?>
        <div class="col-md-2">
            <label>&nbsp;</label>
            <div>
                <button type="button" class="btn btn-sm btn-danger btn-circle btn-elevate btn-icon btn-delete"
                    data-id="<?= $uniqid ?>"><i class="la la-trash"></i></button>
            </div>
        </div>
        <?php endif ?>
    </div>
</div>