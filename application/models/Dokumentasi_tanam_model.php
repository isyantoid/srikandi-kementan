<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dokumentasi_tanam_model extends CI_Model {

    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $id = $this->uri->segment(3);
            $permohonan = getRowArray('tbl_permohonan', ['id' => $this->input->post('permohonan_id')]);

            if($id) {
                $this->db->set('permohonan_id', $this->input->post('permohonan_id'));
                $this->db->set('tanggal_tanam', $this->input->post('tanggal_tanam'));
                if($this->input->post('tanggal_perkiraan_panen')) {
                    $arr_perkiraan_panen = explode(',',$this->input->post('tanggal_perkiraan_panen'));
                    $this->db->set('tanggal_perkiraan_panen', json_encode($arr_perkiraan_panen));
                }

                
                $this->db->set('provinsi_kode', $permohonan['provinsi_kode']);
                $this->db->set('kabupaten_kode', $permohonan['kabupaten_kode']);
                $this->db->set('kecamatan_kode', $permohonan['kecamatan_kode']);
                $this->db->set('desa_kode', $permohonan['desa_kode']);
                $this->db->set('kelompok_komoditi_id', $permohonan['kelompok_komoditi_id']);
                $this->db->set('jenis_komoditi_id', $permohonan['jenis_komoditi_id']);
                $this->db->set('sub_komoditi_id', $permohonan['sub_komoditi_id']);
                $this->db->set('kelompok_tani_id', $this->input->post('kelompok_tani_id'));
                $this->db->set('luas_tanam', $this->input->post('luas_tanam'));
                $this->db->set('jumlah_hasil_panen', $this->input->post('jumlah_hasil_panen'));
                $this->db->set('tipe', '1');
                
                $config = $this->config_file_upload();                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if ( $this->upload->do_upload('foto_tanam')){
                    $data_upload = $this->upload->data();
                    $this->db->set('foto_tanam', $data_upload['file_name']);
                }
    
                $this->db->where('tbl_dokumentasi.id', $id);
                $this->db->update('tbl_dokumentasi');
                return jsonOutputSuccess();
            } else {
                $this->db->set('permohonan_id', $this->input->post('permohonan_id'));
                $this->db->set('tanggal_tanam', $this->input->post('tanggal_tanam'));
                if($this->input->post('tanggal_perkiraan_panen')) {
                    $arr_perkiraan_panen = explode(',',$this->input->post('tanggal_perkiraan_panen'));
                    $this->db->set('tanggal_perkiraan_panen', json_encode($arr_perkiraan_panen));
                }

                $this->db->set('tanggal_kirim', date('Y-m-d'));
                $this->db->set('provinsi_kode', $permohonan['provinsi_kode']);
                $this->db->set('kabupaten_kode', $permohonan['kabupaten_kode']);
                $this->db->set('kecamatan_kode', $permohonan['kecamatan_kode']);
                $this->db->set('desa_kode', $permohonan['desa_kode']);
                $this->db->set('kelompok_komoditi_id', $permohonan['kelompok_komoditi_id']);
                $this->db->set('jenis_komoditi_id', $permohonan['jenis_komoditi_id']);
                $this->db->set('sub_komoditi_id', $permohonan['sub_komoditi_id']);
                $this->db->set('kelompok_tani_id', $this->input->post('kelompok_tani_id'));
                $this->db->set('luas_tanam', $this->input->post('luas_tanam'));
                $this->db->set('jumlah_hasil_panen', $this->input->post('jumlah_hasil_panen'));
                $this->db->set('tipe', '1');
                
                $config = $this->config_file_upload();                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if ( $this->upload->do_upload('foto_tanam')){
                    $data_upload = $this->upload->data();
                    $this->db->set('foto_tanam', $data_upload['file_name']);
                }
    
                $this->db->insert('tbl_dokumentasi');
                return jsonOutputSuccess();
            }
            

        }

        
    }

    public function config_file_upload() {
        $config['upload_path'] = './dist/images/dokumentasi/';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        return $this->db->delete('tbl_dokumentasi');
    }

    public function detail_dokumentasi($id) {
        $this->db->select('
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.sub_komoditi_nama,
            tbl_permohonan.satuan_luas_kebun,
            tbl_dokumentasi.luas_tanam,
            tbl_dokumentasi.tanggal_tanam,
            tbl_dokumentasi.tanggal_perkiraan_panen,
            tbl_dokumentasi.foto_tanam,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->db->join('tbl_permohonan','tbl_dokumentasi.permohonan_id = tbl_permohonan.id', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_dokumentasi.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_dokumentasi.id', $id);
        $this->db->from('tbl_dokumentasi');
        $get = $this->db->get();
        return $get->row_array();
    }

    public function get_kelompok_tani($id_permohonan) {
        $this->db->select(' tbl_kelompok_tani.*');
        $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function get_anggota_tani($id_kelompok_tani) {
        $this->db->select(' tbl_anggota_tani.*');
        $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
        $this->db->from('tbl_anggota_tani');
        $get = $this->db->get();
        return $get->result_array();
    }

    public function exportExcel() {
        $this->db->select('
            tbl_permohonan.no_reg_horti,
            tbl_dokumentasi.permohonan_id,
            tbl_dokumentasi.tanggal_tanam,
            tbl_kelompok_tani.nama_kelompok,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_dokumentasi.luas_tanam,
            tbl_permohonan.satuan_luas_kebun,
            tbl_dokumentasi.foto_tanam as foto
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_dokumentasi.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_dokumentasi.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_dokumentasi.permohonan_id = tbl_permohonan.id', 'left');
        $this->db->join('tbl_kelompok_tani', 'tbl_dokumentasi.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_dokumentasi.tipe', '1');
        $this->db->order_by('tbl_permohonan.jenis_komoditi_nama ASC');
        $this->db->from('tbl_dokumentasi');
        $res = $this->db->get();
        return $res->result_array();
    }
    
}
