<style>
    .dataTables_wrapper .dataTable .selected th, .dataTables_wrapper .dataTable .selected td {
        background-color: #2786fb;
        color: #ffffff;
    }
</style>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi Horti:</label>
                        <div class="input-group">
                            <input type="hidden" name="nomor_registrasi" class="nomor_registrasi form-control" value="" required readonly>
                            <input type="text" name="no_reg_horti" class="no_reg_horti form-control" value="" required readonly>
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-modal-table" data-toggle="modal" data-target="#modal-table-permohonan" type="button">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="form_penilaian"></div>

                <div class="modal-footer">
                    <a href="{segment1}" class="btn btn-secondary">Close</a>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>

        </form>
    </div>
</div>



<div class="modal fade" id="modal-table-permohonan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrasi Kampung</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-light alert-elevate" role="alert">
                    <div class="alert-icon"><i class="flaticon-info kt-font-info"></i></div>
                    <div class="alert-text">Klik baris pada table di bawah ini untuk memilih nomor registrasi horti! Gunakan fitur pencarian untuk memudahkan menemukan data.</div>
                </div>

                <table class="table table-sm table-bordered" id="table-permohonan">
                    <thead>
                        <th>#</th>
                        <th>#</th>
                        <th>Nomor Registrasi STO</th>
                        <th>Nomor Registrasi Horti</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kelompok Komoditas</th>
                        <th>Jenis Komoditas</th>
                        <th>Luas</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>

    var table;

    datatable()

    function datatable() {
        table = $('#table-permohonan').DataTable({
            ajax: {
                url: '{segment1}datatable_permohonan',
                method: 'post',
            },
            select: 'single',
            stateSave: true,
            responsive: true,
            serverSide: true,
            lengthMenu: [5, 10, 25, 50],
            pageLength: 25,
            destroy: true,
            order: [
                [0, 'desc']
            ],
            columns: [{
                    data: 'id',
                    visible: false,
                },
                {
                    data: 'id',
                    width: 25,
                    className: 'text-center'
                },
                { data: 'nomor_registrasi', className: 'text-left' },
                { data: 'no_reg_horti', className: 'text-left' },
                { data: 'provinsi_nama', className: 'text-left' },
                { data: 'kabupaten_nama', className: 'text-left' },
                { data: 'kelompok_komoditi_nama' },
                { data: 'jenis_komoditi_nama' },
                { 
                    data: 'luas_kebun', className: 'text-center',
                    render: function(data, type, row) {
                        return data + row.satuan_luas_kebun;
                    }
                },
            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var index = iDisplayIndex + 1;
                $('td:eq(0)', nRow).html(index);
                return nRow;
            }
        });
        
    }

    $('#form_penilaian').load('{segment1}form_penilaian', function() {});


    table.on( 'select', function ( e, dt, type, indexes ) {
        $('.nomor_registrasi').val('')
        $('.no_reg_horti').val('')
        
        var rowData = table.rows( indexes ).data().toArray();
        var id = rowData[0].id;
        $('.nomor_registrasi').val(id)
        $('.no_reg_horti').val(rowData[0].no_reg_horti)

        $('#modal-table-permohonan').modal('hide')
    });

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
                btnSpinnerHide();
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>