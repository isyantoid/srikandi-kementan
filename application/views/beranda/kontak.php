<div class="kt-portlet kt-margin-top-30">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                PESAN ANDA
            </h3>
        </div>
    </div>

    <!--begin::Form-->
    <form class="kt-form" action="javascript:save()" id="form1" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>NAMA</label>
                        <input type="text" class="form-control" aria-describedby="nameHelp" name="nama"
                            placeholder="Masukan nama anda">
                    </div>
                    <div class="form-group">
                        <label>EMAIL</label>
                        <input type="email" class="form-control" aria-describedby="emailHelp" name="email"
                            placeholder="Masukan email anda">
                    </div>
                    <div class="form-group">
                        <label>LAMPIRAN</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="lampiran" name="lampiran">
                            <label class="custom-file-label" for="lampiran">Pilih lampiran</label>
                            <span class="form-text text-muted">Silahkan memilih berkas jpg, jpeg, png dan pdf .</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">PESAN ANDA</label>
                        <textarea class="form-control" id="keterangan" rows="5" name="pesan"></textarea>
                        <span class="form-text text-muted">Masukan pesan anda disini</span>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary form-control">KIRIM PESAN</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </form>

    <!--end::Form-->
</div>

<script src="{dist_path}assets/js/custom.js" type="text/javascript"></script>
<script>
function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>