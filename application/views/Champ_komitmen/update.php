<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Tahun</label>
                            <input class="form-control tahun" type="text" value="{tahun}" disabled name="tahun" required>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <th class="h3">Bulan</th>
                        <th class="h3">Komitmen Stok (Ton)</th>
                    </thead>
                    <tbody>
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                            <?php $month = date("M", mktime(0, 0, 0, $i, 1)); ?>
                            <tr>
                                <td class="align-middle h5 font-weight-bold"><?= $month ?></td>
                                <td class="align-midle">
                                    <input class="form-control" type="number" value="<?= $data[strtolower($month)] ?>" min="0" name="<?= strtolower($month) ?>" required>
                                </td>
                            </tr>
                        <?php endfor ?>
                    </tbody>
                </table>
            </div>
            
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>
ajaxSelect({
    id: '.kabupaten_kode',
    url: '{site_url}ajax_selectbox/kabupaten_select',
    selected: '{kabupaten_kode}',
});
function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save/{id}',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>