<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>


                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Nomor Registrasi Horti:</label>
                        <select class="form-control nomor_registrasi" name="nomor_registrasi" style="width:100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Kelompok Tani:</label>
                        <select class="form-control id_kelompok_tani" name="id_kelompok_tani" style="width:100%;" required></select>
                    </div>
                </div>

                <div class="from-group row">
                    <div class="col-md-6">
                        <div id="form_detail_permohonan" hidden>
                            <table class="table">
                                <tr>
                                    <td>Jenis Komoditi</td>
                                    <td>:</td>
                                    <td class="font-weight-bold kelompok_komoditi_nama"></td>
                                </tr>
                                <tr>
                                    <td>Komoditi</td>
                                    <td>:</td>
                                    <td class="font-weight-bold jenis_komoditi_nama"></td>
                                </tr>
                                <tr>
                                    <td>Luas Kebun</td>
                                    <td>:</td>
                                    <td class="font-weight-bold luas_kebun"></td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>:</td>
                                    <td class="font-weight-bold provinsi_nama"></td>
                                </tr>
                                <tr>
                                    <td>Kabupaten</td>
                                    <td>:</td>
                                    <td class="font-weight-bold kabupaten_nama"></td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>:</td>
                                    <td class="font-weight-bold kecamatan_nama"></td>
                                </tr>
                                <tr>
                                    <td>Desa</td>
                                    <td>:</td>
                                    <td class="font-weight-bold desa_nama"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Tanggal kendala panen:</label>
                        <input type="text" class="form-control tanggal_kendala_panen" autocomplete="off" name="tanggal_kendala_panen" value="" required placeholder="Tanggal kendala panen">
                    </div>
                    <div class="col-md-3">
                        <label>Jumlah produksi terkendala:</label>
                        <input type="text" class="form-control jumlah_produksi_terkendala" autocomplete="off" name="jumlah_produksi_terkendala" value="" required placeholder="Jumlah produksi terkendala">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Keterangan kendala panen:</label>
                        <textarea name="keterangan" id="keterangan" class="form-control keterangan" required></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Dokumentasi kendala panen:</label>
                        <div class="dropzone dropzone-default dropzone-brand" id="dropzone">
                            <div class="dropzone-msg dz-message needsclick">
                                <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                <span class="dropzone-msg-desc">Upload up to 10 files</span>
                            </div>
                        </div>
                        <span class="form-text text-muted">dokumen dalam bentuk file gambar/pdf</span>
                    </div>
                </div>
                <div class="form-group row" hidden>
                    <div class="col-md-6">
                        <label>Dokumentasi kendala panen:</label>
                        <textarea name="file_txt" id="file_txt" class="form-control file_txt"></textarea>
                    </div>
                </div>



                


                <div class="modal-footer">
                    <div class="text-left">
                        <a href="{segment1}" class="btn btn-secondary">Close</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script>

    file_txt = [];
    id = '#dropzone';
    var myDropzone = new Dropzone('#dropzone', {
        url: "{site_url}kendala_panen/do_upload_document",
        acceptedFiles: 'image/jpeg,image/png,application/pdf',
        paramName: "file",
        maxFiles: 10,
        addRemoveLinks: true,
        uploadMultiple: false,
        accept: function(file, done) {
            if ((file.type).toLowerCase() != "image/jpg" &&
                    (file.type).toLowerCase() != "image/gif" &&
                    (file.type).toLowerCase() != "image/jpeg" &&
                    (file.type).toLowerCase() != "image/png" &&
                    (file.type).toLowerCase() != "application/pdf"
                ) {
                done("Invalid file");
            } else {
                done()
            }
        }
    })

    myDropzone.on('complete', function(file) {
        dataFile = JSON.parse(file.xhr.responseText);
        arrayPush = {
            name: dataFile.file_name,
        }
        file_txt.push(arrayPush)

        $('.file_txt').val(JSON.stringify(file_txt))
    })

    myDropzone.on('sending', function(file, xhr, formData){
        var data = $('#form1').serializeArray();
        $.each(data, function(key, val){
            formData.append(val.name, val.value);
        })
    })

    myDropzone.on("removedfile", function(file){
        respon = JSON.parse(file.xhr.responseText);
        $.post('{site_url}kendala_panen/remove_file/', {file_name: respon.file_name}, function(res){
            file_json = JSON.parse($('.file_txt').val())

            var removeByAttr = function(arr, attr, value){
                var i = arr.length;
                while(i--){
                    if( arr[i] && arr[i].hasOwnProperty(attr) && (arguments.length > 2 && arr[i][attr] === value ) ){
                        arr.splice(i,1);
                    }
                }
                return arr;
            }
            removeByAttr(file_json, 'name', respon.file_name)
            file_txt = file_json;
            $('.file_txt').val(JSON.stringify(file_txt))
        })
    });

    
    $(document).ready(function() {
        
        $('.tanggal_kendala_panen').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });
        $('.tanggal_perkiraan_panen').datepicker({
            multidate: true,
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: false,
            orientation: "bottom left",
            autoclose: false
        });

        $('.nomor_registrasi').change(function() {
            var val = $(this).val();
            ajaxSelect({
                id: '.kelompok_tani_id',
                url: '{site_url}ajax_selectbox/kelompok_tani_id',
                optionalSearch: {
                    id_permohonan: val
                }
            });
        });

        $('.id_kelompok_tani').select2({
            placeholder: 'Pilih kelompok tani',
        })


        ajaxSelect({
            placeholder: 'Pilih nomor registrasi',
            allowClear: true,
            id: '.nomor_registrasi',
            url: '{site_url}ajax_selectbox/no_reg_horti',
        });



        $('.nomor_registrasi').change(function() {
            var val = $(this).val();

            $.ajax({
                url: '{segment1}get_permohonan/' + val,
                dataType: 'json',
                method: 'get',
                success: function(data) {
                    console.log(data)
                    ajaxSelect({
                        placeholder: 'Pilih kelompok tani',
                        allowClear: true,
                        id: '.id_kelompok_tani',
                        url: '{site_url}ajax_selectbox/kelompok_tani_by_permohonan',
                        optionalSearch: {
                            id_permohonan: val,
                        }
                    });

                    $('.kelompok_komoditi_nama').text(data.kelompok_komoditi_nama)
                    $('.jenis_komoditi_nama').text(data.jenis_komoditi_nama)
                    $('.luas_kebun').text(data.luas_kebun + ' ' + data.satuan_luas_kebun)
                    $('.provinsi_nama').text(data.provinsi_nama)
                    $('.kabupaten_nama').text(data.kabupaten_nama)
                    $('.kecamatan_nama').text(data.kecamatan_nama)
                    $('.desa_nama').text(data.desa_nama)
                }
            })

        });

        $('.id_kelompok_tani').change(function(){
            $('#form_detail_permohonan').attr('hidden', false);
        })
        
    });

    function get_nomor_registrasi() {
        $('#form_penilaian').html('');

        var provinsi_kode = $('.provinsi_kode').val();
        var kabupaten_kode = $('.kabupaten_kode').val();
        var kecamatan_kode = $('.kecamatan_kode').val();
        var desa_kode = $('.desa_kode').val();
        var kelompok_komoditi_kode = $('.kelompok_komoditi_kode').val();
        var jenis_komoditi_kode = $('.jenis_komoditi_kode').val();
        var sub_komoditi_kode = $('.sub_komoditi_kode').val();

        $('.nomor_registrasi').empty();
        ajaxSelect({
            allowClear: true,
            id: '.nomor_registrasi',
            url: '{site_url}ajax_selectbox/no_reg_horti',
        });
    }



    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        // myDropzone.processQueue()

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
                btnSpinnerHide();
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>