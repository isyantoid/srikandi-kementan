<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Privacy_police extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Privacy Police';
        $data['content'] = 'Privacy_police';
        $data = array_merge($data, path_info());
        $this->load->view('Privacy_police', $data);
    }
}