-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2023 at 01:51 PM
-- Server version: 10.5.21-MariaDB
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcdrcpia_srikandi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `tipe` char(1) DEFAULT NULL,
  `subId` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `nomor_urut` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `nama`, `tipe`, `subId`, `icon`, `url`, `nomor_urut`, `app_id`) VALUES
(1, 'Dashboard', '1', NULL, 'flaticon2-protection', 'dashboard', 1, NULL),
(2, 'Menu Master', '2', NULL, 'flaticon2-list-3', NULL, 2, NULL),
(3, 'Tipe Bantuan', '1', 2, NULL, 'tipe_bantuan', NULL, NULL),
(4, 'Jenis Bantuan', '1', 2, NULL, 'jenis_bantuan', NULL, NULL),
(5, 'Kelompok Komoditas', '1', 2, NULL, 'kelompok_komoditi', NULL, NULL),
(6, 'Jenis Komoditas', '1', 2, NULL, 'jenis_komoditi', NULL, NULL),
(7, 'Sub Komoditas', '1', 2, NULL, 'sub_komoditi', NULL, NULL),
(8, 'Jenis Dokumen', '1', 2, NULL, 'jenis_dokumen', NULL, NULL),
(9, 'Lihat Pesan Masuk', '1', 2, NULL, 'lihat_pesan_masuk', NULL, NULL),
(10, 'Desa', '1', 2, NULL, 'desa', NULL, NULL),
(11, 'Kecamatan', '1', 2, NULL, 'kecamatan', NULL, NULL),
(12, 'Kabupaten', '1', 2, NULL, 'kabupaten', NULL, NULL),
(13, 'Provinsi', '1', 2, NULL, 'provinsi', NULL, NULL),
(14, 'Input Data Kampung', '2', NULL, 'flaticon2-hourglass-1', NULL, 3, NULL),
(16, 'Form Artikel', '1', 14, NULL, 'form_artikel', NULL, NULL),
(17, 'Buku Digital', '1', 14, NULL, 'buku_digital', NULL, NULL),
(18, 'Registrasi Kampung', '1', 14, NULL, 'registrasi_kampung', NULL, NULL),
(19, 'Bantuan', '1', 14, NULL, 'bantuan', NULL, NULL),
(20, 'Penilaian Kampung', '1', 14, NULL, 'penilaian_kampung', NULL, NULL),
(21, 'Dokumentasi Tanam', '1', 14, NULL, 'dokumentasi_tanam', NULL, NULL),
(22, 'Dokumentasi Panen', '1', 14, NULL, 'dokumentasi_panen', NULL, NULL),
(23, 'Kendala Tanam', '1', 14, NULL, 'kendala_tanam', NULL, NULL),
(24, 'Kendala Panen', '1', 14, NULL, 'kendala_panen', NULL, NULL),
(25, 'Pemantauan Tanam', '1', 14, NULL, 'pemantauan_tanam', NULL, NULL),
(26, 'Laporan', '2', NULL, 'flaticon2-medical-records-1', NULL, 5, NULL),
(27, 'Tabel Kampung', '1', 26, NULL, 'kampung', NULL, NULL),
(29, 'Laporan Rekapitulasi Kampung', '1', 26, NULL, 'laporan_rekapitulasi_kampung', NULL, NULL),
(30, 'Pengaturan', '2', NULL, 'flaticon2-gear', NULL, 6, NULL),
(31, 'User', '1', 30, NULL, 'user', NULL, NULL),
(32, 'Keluar', '1', NULL, 'fa fa-power-off', 'beranda/logout', 7, NULL),
(33, 'Laporan Registrasi Kampung', '1', 26, NULL, 'laporan_registrasi_kampung', NULL, NULL),
(34, 'Laporan Kelompok Tani', '1', 26, NULL, 'laporan_kelompok_tani', NULL, NULL),
(35, 'Target Pencapaian', '1', 2, NULL, 'target_pencapaian', NULL, NULL),
(36, 'Registrasi P2L', '1', 40, NULL, 'registrasi_p2l', NULL, 4),
(37, 'Pengaturan Umum', '1', 30, NULL, 'pengaturan', NULL, NULL),
(38, 'Bantuan P2L', '1', 40, NULL, 'bantuan_p2l', NULL, 4),
(39, 'Realisasi Bantuan Fisik Sarana Pembenihan', '1', 40, NULL, 'realisasi_fisik_sarana_pembenihan', NULL, NULL),
(40, 'Input Data P2L', '2', NULL, 'flaticon2-hourglass-1', NULL, 4, 4),
(41, 'Realisasi Fisik Demplot', '1', 40, NULL, 'realisasi_fisik_demplot', NULL, 4),
(42, 'Realisasi Fisik Pertanaman', '1', 40, NULL, 'realisasi_fisik_pertanaman', NULL, 4),
(43, 'Realisasi Fisik Saran Pasca Panen', '1', 40, NULL, 'realisasi_fisik_sarana_pasca_panen', NULL, 4),
(44, 'Capaian Kinerja Produksi Benih', '1', 40, NULL, 'capaian_kinerja_produksi_benih', NULL, 4),
(45, 'Capaian Kinerja Hasil Panen Demplot', '1', 40, NULL, 'capaian_kinerja_hasil_panen_demplot', NULL, 4),
(46, 'Capaian Kinerja Hasil Panen Pertanaman', '1', 40, NULL, 'capaian_kinerja_hasil_panen_pertanaman', NULL, 4),
(47, 'Capaian Kinerja Hasil Penjualan', '1', 40, NULL, 'capaian_kinerja_hasil_penjualan', NULL, 4),
(48, 'Capaian Kinerja Hasil Panen yang dikonsumsi anggota', '1', 40, NULL, 'capaian_kinerja_hasil_panen_konsumsi_anggota', NULL, 4),
(49, 'User Level', '1', 30, NULL, 'user_level', NULL, NULL),
(50, 'User Akses', '1', 30, NULL, 'user_akses', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
