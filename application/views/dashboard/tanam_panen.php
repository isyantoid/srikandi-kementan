<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet" data-ktportlet="true" id="collapse">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-list-3"></i>
                        </span>
                        <h3 class="kt-portlet__head-title"> Filter {title}</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form kt-form--fit kt-form--label-right" action="javascript:filter()" id="form1">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Tanggal Awal:</label>
                                <div class="col-lg-3">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control tanggal_awal" name="tanggal_awal" value="{tanggal_awal}" placeholder="Tanggal Awal">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
                                    </div>
                                </div>
                                <label class="col-lg-2 col-form-label">Tanggal Akhir:</label>
                                <div class="col-lg-3">
                                    <div class="kt-input-icon">
                                        <input type="text" class="form-control tanggal_akhir" name="tanggal_akhir" value="{tanggal_akhir}" placeholder="Tanggal Akhir">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot kt-portlet__foot--fit-x">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-success">Tampilkan Hasil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="result"></div>
</div>

<script>
    $(document).ready(function(){
        filter();

        $('.tanggal_awal, .tanggal_akhir').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });
    });

    function filter() {
        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            $('.result').load('{site_url}dashboard/tanam_panen_result', {tanggal_awal: $('.tanggal_awal').val(), tanggal_akhir: $('.tanggal_akhir').val()}, function() {
                unblockPage();
            });
        }, 1000);
    }
</script>