<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1" enctype="multipart/form-data">
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Nama Ebook</label>
                    <div class="col-10">
                        <input class="form-control nama_ebook" type="text" value="" name="nama_ebook" required>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Jenis Dokumen</label>
                    <div class="col-10">
                        <select name="id_jenis_dokumen" id="id_jenis_dokumen" class="form-control" required>
                            <option value="">-- Pilih Jenis Dokumen--</option>
                            <?php foreach ($show_jenis_dokumen as $sjd) : ?>
                            <option value="<?php echo $sjd->id; ?>"><?php echo $sjd->jenis_dokumen; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Keterangan</label>
                    <div class="col-10">
                        <textarea class="form-control Keterangan" name="keterangan" id="keterangan"></textarea>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">File Upload</label>
                    <div class="col-10">
                        <input class="form-control file_upload" type="file" name="file_upload">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script src="{dist_path}/assets/plugins/tinymce/tinymce.js"></script>

<script>
tinymce.init({
    selector: '#keterangan',
    height: 500,
    theme: 'silver',
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
    ],
    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
    image_advtab: true,
    templates: [{
            title: 'Test template 1',
            content: 'Test 1'
        },
        {
            title: 'Test template 2',
            content: 'Test 2'
        }
    ],
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
    ]
});
</script>

<script>
function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>