
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title"><?= $title ?></h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}export_excel?tahun={tahun}&jenis_komoditi_id={jenis_komoditi_id}" class="btn btn-brand btn-elevate btn-icon-sm">Export Excel</a>
                        <a href="{segment1}export_pdf?tahun={tahun}&jenis_komoditi_id={jenis_komoditi_id}" class="btn btn-brand btn-elevate btn-icon-sm">Export Pdf</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <form action="{segment1}?">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Pilih Tahun</label>
                            <input class="form-control tahun" type="text" value="{tahun}" readonly name="tahun" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-10">
                        <button type="submit" class="btn btn-success">Filter</button>
                    </div>
                </div>
            </form>
            <?php if($tahun && $jenis_komoditi_id): ?>
                <div class="table-responsive mt-5">
                    <table class="table table-sm table-bordered">
                        <thead class="bg-light">
                            <th class="text-center">No</th>
                            <th>Nama Champion</th>
                            <th>Kecamatan</th>
                            <th>Kabupaten</th>
                            <th>Stok</th>
                            <th>Jan</th>
                            <th>Feb</th>
                            <th>Mar</th>
                            <th>Apr</th>
                            <th>May</th>
                            <th>Jun</th>
                            <th>Jul</th>
                            <th>Aug</th>
                            <th>Sep</th>
                            <th>Oct</th>
                            <th>Nov</th>
                            <th>Dec</th>
                            <th>Total Kumulatif</th>
                            <th>Kontak Dinas</th>
                        </thead>
                        <tbody>
                            <?php if($data): ?>
                                <?php $no=1; foreach($data as $row): ?>
                                    <tr>
                                        <td class="align-middle text-center"><?= $no ?></td>
                                        <td class="align-middle"><?= $row['champion_nama'] ?></td>
                                        <td class="align-middle"><?= $row['kecamatan_nama'] ?></td>
                                        <td class="align-middle"><?= $row['kabupaten_nama'] ?></td>
                                        <td class="align-middle"><?= $row['stok'] ?></td>
                                        <td class="align-middle"><?= $row['jan'] ?></td>
                                        <td class="align-middle"><?= $row['feb'] ?></td>
                                        <td class="align-middle"><?= $row['mar'] ?></td>
                                        <td class="align-middle"><?= $row['apr'] ?></td>
                                        <td class="align-middle"><?= $row['may'] ?></td>
                                        <td class="align-middle"><?= $row['jun'] ?></td>
                                        <td class="align-middle"><?= $row['jul'] ?></td>
                                        <td class="align-middle"><?= $row['aug'] ?></td>
                                        <td class="align-middle"><?= $row['sep'] ?></td>
                                        <td class="align-middle"><?= $row['oct'] ?></td>
                                        <td class="align-middle"><?= $row['nov'] ?></td>
                                        <td class="align-middle"><?= $row['dec'] ?></td>
                                        <td class="align-middle"><?= $row['stok'] ?></td>
                                        <td class="align-middle"><?= $row['kontak_dinas_nama'] ?> <br><?= $row['kontak_dinas_nomor_hp'] ?> </td>
                                    </tr>
                                <?php $no++; endforeach ?>
                            <?php else: ?>
                                <tr><td colspan="19">Tidak ada data</td></tr>
                            <?php endif ?>
                        </tbody>
                        
                    </table>
                </div>
            <?php endif ?>

        </div>
    </div>
</div>

<script>
    var baseUrl = '{base_url}';
    $(".tahun").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
    });
    ajaxSelect({
        id: '.jenis_komoditi_id',
        url: '{site_url}ajax_selectbox/jenis_komoditi_id',
        selected: '{jenis_komoditi_id}',
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chroma-js/2.1.1/chroma.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap" async defer></script>
<!-- <script src="{base_url}dist/json/script.js"></script> -->
<script src="{base_url}dist/json/map.js"></script>

<script>


</script>