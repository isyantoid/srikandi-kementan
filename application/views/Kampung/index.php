<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <form class="kt-form" action="" id="form1">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">Pencarian</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Provinsi</label>
                        <select class="form-control kt-select2 provinsi_kode" name="provinsi_kode"
                            style="width:100%;"></select>
                    </div>
                    <div class="col-md-6">
                        <label>Kabupaten</label>
                        <select class="form-control kt-select2 kabupaten_kode" name="kabupaten_kode"
                            style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Jenis Komoditi / Nama Kampung</label>
                        <select class="form-control kt-select2 jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary submit">Submit</button>
                    <a href="{site_url}kampung" class="btn btn-secondary">Clear</a>
                </div>
            </div>
        </form>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">Data Lahan</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <th>Nomor Registrasi STO</th>
                        <th>Tanggal Registrasi STO</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Nama Kampung</th>
                        <th>Kelompok Tani</th>
                        <th>Luas</th>
                    </thead>
                    <tbody>
                        <?php $luas_kampung = $this->model->get_luas_kampung(); ?>
                        <?php if($luas_kampung): ?>
                            <?php $id = null; ?>
                            <?php $no=1; foreach($luas_kampung as $row): ?>

                                <?php $luas_kampung_prov = $this->model->get_luas_kampung_prov($row['jenis_komoditi_id']); ?>
                                <?php if($luas_kampung_prov): ?>
                                    <?php foreach($luas_kampung_prov as $prov): ?>

                                        <?php $luas_kampung_kab = $this->model->get_luas_kampung_kab($row['jenis_komoditi_id'], $prov['provinsi_kode']); ?>
                                        <?php if($luas_kampung_kab): ?>
                                            <?php foreach($luas_kampung_kab as $kab): ?>


                                                <?php $permohonan = $this->model->get_permohonan($row['jenis_komoditi_id'], $prov['provinsi_kode'], $kab['kabupaten_kode']); ?>
                                                <?php if($permohonan): ?>
                                                    <?php foreach($permohonan as $p): ?>
                                                        <tr>
                                                            <td><?= $p['nomor_registrasi'] ?></td>
                                                            <td><?= $p['tanggal_permohonan'] ?></td>
                                                            <td><?= $p['provinsi_nama'] ?></td>
                                                            <td><?= $p['kabupaten_nama'] ?></td>
                                                            <td><?= $p['jenis_komoditi_nama'] ?></td>
                                                            <td><?= $p['nama_kelompok'] ?></td>
                                                            <td><?= $p['luas_lahan_kelompok_tani'] . $p['satuan_luas_kebun'] ?></td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                                
                                                
                                                <tr class="bg-warning text-dark">
                                                    <td colspan="6">
                                                        Total Luas Kampung <?= $row['jenis_komoditi_nama'] ?> di <?= $kab['kabupaten_nama'] ?>
                                                    </td>
                                                    <td><?= $kab['total_luas_kebun'] ." ". $kab['satuan_luas_kebun'] ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif ?>                                        
                                        
                                        <?php if(!$this->input->get('kabupaten_kode')): ?>
                                            <tr class="bg-primary text-white">
                                                <td colspan="6">
                                                    Total Luas Kampung <?= $row['jenis_komoditi_nama'] ?> di Provinsi <?= $prov['provinsi_nama'] ?>
                                                </td>
                                                <td><?= $prov['total_luas_kebun'] ." ". $prov['satuan_luas_kebun'] ?></td>
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                <?php endif ?>
                                

                                <?php if(!$this->input->get('provinsi_kode')): ?>
                                    <tr class="bg-dark text-white">
                                        <td colspan="6">
                                            Total Luas Kampung <?= $row['jenis_komoditi_nama'] ?> di Indonesia
                                        </td>
                                        <td><?= $row['total_luas_kebun'] ." ". $row['satuan_luas_kebun'] ?></td>
                                    </tr>
                                <?php endif ?>
                                

                                <?php $no++ ?>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

    $('.kabupaten_kode, .jenis_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');

    ajaxSelect({
        allowClear: true,
        id: '.provinsi_kode',
        url: '{site_url}ajax_selectbox/provinsi_select',
        selected: '<?= $this->input->get('provinsi_kode') ?>'
    });

    $('.provinsi_kode').change(function() {
        var val = $(this).val();
        if(val) {
            $('.kabupaten_kode').empty();
            ajaxSelect({
                allowClear: true,
                id: '.kabupaten_kode',
                url: '{site_url}ajax_selectbox/kabupaten_select',
                optionalSearch: {
                    provinsi_kode: val
                },
                selected: '<?= $this->input->get('kabupaten_kode') ?>'
            });
        }
    });

    $('.provinsi_kode').on("select2:unselecting", function (e) {
        $('.kabupaten_kode').empty();
        $('.kabupaten_kode, .jenis_komoditi_id').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');
    });

    ajaxSelect({
        id: '.jenis_komoditi_id',
        url: '{site_url}ajax_selectbox/jenis_komoditi_id',
        selected: '<?= $this->input->get('jenis_komoditi_id') ?>'
    });
    
</script>