<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text');
    }

    public function index()
    {
        redirect('beranda/home');
    }

    public function home()
    {
        $this->db->where('status_aktif', 'aktif');
        $data['query_artikel_aktif'] = $this->db->get('tbl_form_artikel')->result();

        $data['query_artikel'] = $this->db->get('tbl_form_artikel')->result();
        $data['title'] = 'Home';
        $data['content'] = 'beranda/home';
        $data['style_css'] = $this->parser->parse('beranda/style', $data, TRUE);
        $data = array_merge($data, path_info());
        $this->parser->parse('front_template/main', $data);
    }

    public function ebook($id = '')
    {
        $data['style_css'] = '';

        $data['title'] = 'Ebook';
        if (!empty($id)) :
            $this->db->where('tbl_buku_digital.id_jenis_dokumen', $id);
            $this->db->select('tbl_buku_digital.id, tbl_buku_digital.nama_ebook, tbl_jenis_dokumen.id as id_jenis_dokumen, tbl_jenis_dokumen.icon, tbl_buku_digital.keterangan, tbl_buku_digital.file_upload');
            $this->db->join('tbl_jenis_dokumen', 'tbl_buku_digital.id_jenis_dokumen = tbl_jenis_dokumen.id', 'left');
            $this->db->from('tbl_buku_digital');
            $data['show_buku_digital'] = $this->db->get()->result();
            $data['content'] = 'beranda/ebook';
        else :
            $data['show_jenis_dokumen'] = $this->db->get('tbl_jenis_dokumen')->result();
            $data['content'] = 'beranda/jenis_dokumen';
        endif;
        $data = array_merge($data, path_info());
        $this->parser->parse('front_template/main', $data);
    }

    public function kontak()
    {
        $data['style_css'] = '';
        $data['title'] = 'Kontak';
        $data['content'] = 'beranda/kontak';
        $data = array_merge($data, path_info());
        $this->parser->parse('front_template/main', $data);
    }

    public function save()
    {
        $this->load->model('Kontak_model', 'model');
        $this->model->save();
    }

    public function login()
    {
        if (is_user()) redirect('dashboard');
        $data['style_css'] = '';
        $data['title'] = 'Login';
        $data = array_merge($data, path_info());
        $this->parser->parse('beranda/login', $data);
    }

    public function logout()
    {

        $this->session->sess_destroy();
        redirect('beranda');
    }

    public function do_login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($username && $password) {
            $this->db->where('username', $username);
            $this->db->where('password', md5($password));
            $this->db->where('app_id', '2');
            $this->db->where_in('user_level_id', ['1', '2', '3', '5']);
            $user = $this->db->get('tbl_user');

            if ($user->num_rows() > 0) {
                $row = $user->row_array();

                $array = array(
                    'session_user_level_id' => $row['user_level_id'],
                    'session_user_id' => $row['id'],
                    'session_provinsi_kode' => $row['provinsi_kode'],
                    'session_kabupaten_kode' => $row['kabupaten_kode'],
                    'session_is_user' => true,
                );

                $this->session->set_userdata($array);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error_login', 'Username atau password salah');
                $this->login();
            }
        } else {
            $this->session->set_flashdata('error_login', 'Username atau password salah');
            $this->login();
        }
    }


    public function login_p2l()
    {
        if (is_user()) redirect('dashboard');
        $data['style_css'] = '';
        $data['title'] = 'Login';
        $data = array_merge($data, path_info());
        $this->parser->parse('beranda/login_p2l', $data);
    }

    public function do_login_p2l()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($username && $password) {
            $this->db->where('username', $username);
            $this->db->where('password', md5($password));
            $this->db->where('app_id', '2');
            $this->db->where_in('user_level_id', ['1', '9', '10', '11', '12']);
            $user = $this->db->get('tbl_user');


            if ($user->num_rows() > 0) {
                $row = $user->row_array();

                $array = array(
                    'session_user_level_id' => $row['user_level_id'],
                    'session_user_id' => $row['id'],
                    'session_provinsi_kode' => $row['provinsi_kode'],
                    'session_kabupaten_kode' => $row['kabupaten_kode'],
                    'session_is_user' => true,
                );

                $this->session->set_userdata($array);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error_login', 'Username atau password salah');
                $this->login_p2l();
            }
        } else {
            $this->session->set_flashdata('error_login', 'Username atau password salah');
            $this->login_p2l();
        }
    }

    public function login_champ()
    {
        if (is_user()) redirect('dashboard');
        $data['style_css'] = '';
        $data['title'] = 'Login';
        $data = array_merge($data, path_info());
        $this->parser->parse('beranda/login_champ', $data);
    }

    public function do_login_champ()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($username && $password) {
            $this->db->where('username', $username);
            $this->db->where('password', md5($password));
            $this->db->where('app_id', '2');
            $this->db->where_in('user_level_id', [1, 13, 14, 15, 16, 17, 18, 19]);
            $user = $this->db->get('tbl_user');


            if ($user->num_rows() > 0) {
                $row = $user->row_array();

                $array = array(
                    'session_user_level_id' => $row['user_level_id'],
                    'session_user_id' => $row['id'],
                    'session_provinsi_kode' => $row['provinsi_kode'],
                    'session_kabupaten_kode' => $row['kabupaten_kode'],
                    'session_is_user' => true,
                );

                $this->session->set_userdata($array);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error_login', 'Username atau password salah');
                $this->login_champ();
            }
        } else {
            $this->session->set_flashdata('error_login', 'Username atau password salah');
            $this->login_champ();
        }
    }


    public function login_nurseri()
    {
        if (is_user()) redirect('dashboard');
        $data['style_css'] = '';
        $data['title'] = 'Login';
        $data = array_merge($data, path_info());
        $this->parser->parse('beranda/login_nurseri', $data);
    }

    public function do_login_nurseri()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($username && $password) {
            $this->db->where('username', $username);
            $this->db->where('password', md5($password));
            $this->db->where('app_id', '2');
            $this->db->where_in('user_level_id', [20]);
            $user = $this->db->get('tbl_user');


            if ($user->num_rows() > 0) {
                $row = $user->row_array();

                $array = array(
                    'session_user_level_id' => $row['user_level_id'],
                    'session_user_id' => $row['id'],
                    'session_provinsi_kode' => $row['provinsi_kode'],
                    'session_kabupaten_kode' => $row['kabupaten_kode'],
                    'session_is_user' => true,
                );

                $this->session->set_userdata($array);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error_login', 'Username atau password salah');
                $this->login_nurseri();
            }
        } else {
            $this->session->set_flashdata('error_login', 'Username atau password salah');
            $this->login_nurseri();
        }
    }

    public function baca_artikel($id = null)
    {

        if ($id) {
            $data = getRowArray('tbl_form_artikel', array('id' => $id));
            $this->db->limit(10, 0);
            $data['style_css'] = '';
            $data['show_artikel'] = $this->db->get('tbl_form_artikel')->result();
            $data['title'] = 'Baca Artikel ';
            $data['id'] = $id;
            $data['content'] = 'beranda/baca_artikel';
            $data = array_merge($data, path_info());
            $this->parser->parse('front_template/main', $data);
        } else {
            show_404();
        }
    }
}
