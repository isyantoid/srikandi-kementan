<style>
.pac-container {
    z-index: 1050;
    position: fixed !important;
    top: 15% !important;
}
</style>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Nomor Registrasi P2L:</label>
                        <select class="form-control registrasi_p2l_id" name="registrasi_p2l_id" style="width:100%;"
                            required></select>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit">
                </div>

                <h4 class="kt-section__title">Realisasi Fisik Sarana Perbenihan</h4>
                <table class="table" id="realisasi_fisik_sarana_pembenihan">
                    <tbody></tbody>
                </table>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>
$(document).ready(function() {

    generate_form_realisasi('#realisasi_fisik_sarana_pembenihan', 0);
    generate_form_realisasi('#realisasi_fisik_demplot', 0);
    generate_form_realisasi('#realisasi_fisik_pertanaman', 0);

    function generate_form_realisasi(elementID, btnDelete) {
        $.ajax({
            url: '{segment1}generate_form?btnDelete=' + btnDelete + '&tableID=' + elementID ,
            method: 'get',
            success: function(html) {
                $(elementID + ' > tbody').append(html)
            }
        });
    }

    $(document).on('click', '#realisasi_fisik_sarana_pembenihan #btn-add', function(){
        generate_form_realisasi('#realisasi_fisik_sarana_pembenihan', 1);
    })

    

    // console.log(uuidv4())

    // function uuidv4() {
    //     return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    //         (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    //     );
    // }

    $('.tanggal_registrasi_p2l, .tanggal_penerima_bantuan')
        .datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });

    $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');

    $('.jenis_realisasi').select2({
        placeholder: 'Pilih Opsi',
        data: [{
                id: '1',
                text: 'Realisasi Fisik Sarana Perbenihan'
            },
            {
                id: '2',
                text: 'Realisasi Fisik Demplot'
            },
            {
                id: '3',
                text: 'Realisasi Fisik Pertanaman'
            },
        ],
    }).val('').trigger('change');


    ajaxSelect({
        id: '.registrasi_p2l_id',
        url: '{site_url}ajax_selectbox/registrasi_p2l_id',
    });
    $('.kelompok_tani_id').change(function() {
        var val = $(this).val();
        if (val) {
            $.ajax({
                url: '{segment1}get_nama_ketua_kelompok_tani/' + val,
                method: 'post',
                dataType: 'json',
                success: function(res) {
                    $('.nama_ketua').val(res.nama_ketua);
                }
            });
        }
    });
    ajaxSelect({
        id: '.kelompok_komoditi_id',
        url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
    });

    $('.kelompok_komoditi_id').change(function() {
        var val = $(this).val();
        $('.jenis_komoditi_id').empty();
        ajaxSelect({
            id: '.jenis_komoditi_id',
            url: '{site_url}ajax_selectbox/jenis_komoditi_id',
            optionalSearch: {
                kelompok_komoditi_id: val
            }
        });
    });

    $('.jenis_komoditi_id').change(function() {
        var val = $(this).val();
        $('.sub_komoditi_id').empty();
        ajaxSelect({
            id: '.sub_komoditi_id',
            url: '{site_url}ajax_selectbox/sub_komoditi_id',
            optionalSearch: {
                jenis_komoditi_id: val
            }
        });
    });

    // open map1 
    $('.btn_modal_map1').click(function() {
        $('#modal_map1').modal('show');
    })





    $(document).on('click', '.btn_modal_map2', function() {
        var index = $(this).data('index');
        $('.latMap2nomorheader').val(index);

        $('#modal_map2').modal('show');
    });

    $(document).on('click', '.btn_modal_map3', function() {
        var index = $(this).data('index');
        console.log(index)
        $('.latMap3nomorheader').val(index);

        $('#modal_map3').modal('show');
    });
})


function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
            btnSpinnerHide();
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>