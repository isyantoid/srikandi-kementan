/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 27/06/2023 13:05:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_user_akses
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_akses`;
CREATE TABLE `tbl_user_akses`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_level_id` int NULL DEFAULT NULL,
  `menu_id` int NULL DEFAULT NULL,
  `show` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `read` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `create` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `update` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_user_akses
-- ----------------------------
INSERT INTO `tbl_user_akses` VALUES (1, 1, 1, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (2, 2, 1, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (3, 1, 3, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (4, 1, 4, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (5, 1, 6, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (6, 1, 7, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (7, 1, 9, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (8, 1, 8, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (9, 1, 12, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (10, 1, 13, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (11, 1, 10, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (12, 1, 5, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (13, 1, 18, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (14, 1, 17, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (15, 1, 21, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (16, 1, 16, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (17, 1, 22, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (18, 1, 19, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (19, 1, 20, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (20, 1, 11, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (21, 1, 23, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (22, 1, 24, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (23, 1, 25, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (24, 1, 27, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (25, 1, 29, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (26, 1, 32, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (27, 1, 34, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (28, 1, 33, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (29, 1, 36, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (30, 1, 37, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (31, 1, 31, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (32, 1, 35, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (33, 1, 41, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (34, 1, 42, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (35, 1, 43, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (36, 1, 38, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (37, 1, 39, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (38, 1, 46, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (39, 1, 45, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (40, 1, 44, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (41, 1, 50, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (42, 1, 49, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (43, 1, 47, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (44, 1, 48, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (45, 1, 2, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (46, 1, 14, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (47, 1, 26, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (48, 1, 40, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (49, 1, 30, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (50, 2, 4, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (51, 2, 5, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (52, 2, 2, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (53, 2, 8, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (54, 2, 6, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (55, 2, 10, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (56, 2, 9, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (57, 2, 7, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (58, 2, 12, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (59, 2, 14, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (60, 2, 13, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (61, 2, 3, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (62, 2, 16, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (63, 2, 17, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (64, 2, 18, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (65, 2, 20, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (66, 2, 35, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (67, 2, 19, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (68, 2, 11, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (69, 2, 21, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (70, 2, 22, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (71, 2, 23, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (72, 2, 25, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (73, 2, 26, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (74, 2, 24, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (75, 2, 29, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (76, 2, 27, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (77, 2, 34, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (78, 2, 33, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (79, 2, 30, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (80, 2, 49, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (81, 2, 37, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (82, 2, 50, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (83, 2, 40, '0', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (84, 2, 31, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (85, 2, 36, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (86, 2, 41, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (87, 2, 38, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (88, 2, 43, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (89, 2, 42, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (90, 2, 32, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (91, 2, 45, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (92, 2, 46, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (93, 2, 48, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (94, 2, 44, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (95, 2, 39, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (96, 2, 47, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (97, 5, 2, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (98, 5, 1, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (99, 5, 4, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (100, 5, 8, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (101, 5, 6, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (102, 5, 5, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (103, 5, 9, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (104, 5, 7, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (105, 5, 10, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (106, 5, 3, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (107, 5, 13, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (108, 5, 12, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (109, 5, 35, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (110, 5, 11, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (111, 5, 16, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (112, 5, 17, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (113, 5, 18, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (114, 5, 14, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (115, 5, 21, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (116, 5, 23, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (117, 5, 19, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (118, 5, 20, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (119, 5, 26, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (120, 5, 27, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (121, 5, 22, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (122, 5, 29, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (123, 5, 24, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (124, 5, 25, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (125, 5, 30, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (126, 5, 34, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (127, 5, 33, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (128, 5, 37, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (129, 5, 50, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (130, 5, 32, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (131, 5, 49, '0', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (132, 5, 31, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (133, 5, 36, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (134, 5, 40, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (135, 5, 39, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (136, 5, 41, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (137, 5, 43, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (138, 5, 42, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (139, 5, 38, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (140, 5, 44, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (141, 5, 46, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (142, 5, 45, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (143, 5, 47, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (144, 5, 48, '1', '1', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (145, 9, 40, '1', '0', '0', '0', '0');
INSERT INTO `tbl_user_akses` VALUES (146, 9, 36, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (147, 9, 38, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (148, 9, 39, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (149, 9, 41, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (150, 9, 42, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (151, 9, 44, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (152, 9, 43, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (153, 9, 45, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (154, 9, 47, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (155, 9, 46, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (156, 9, 48, '1', '1', '1', '1', '1');
INSERT INTO `tbl_user_akses` VALUES (157, 9, 1, '0', '1', '0', '0', '0');

SET FOREIGN_KEY_CHECKS = 1;
