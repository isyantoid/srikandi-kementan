<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body" style="padding-bottom:0px !important">
                
                <div class="form-group row">
                    <label class="col-2 col-form-label">Provinsi</label>
                    <div class="col-10">
                        <select class="form-control provinsiKode" name="provinsiKode" style="width:100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Kabupaten</label>
                    <div class="col-10">
                        <select class="form-control kabupatenKode" name="kabupatenKode" style="width:100%;" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Kecamatan</label>
                    <div class="col-10">
                        <select class="form-control kecamatanKode" name="kecamatanKode" style="width:100%;" required></select>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-2 col-form-label">Kode</label>
                    <div class="col-10">
                        <input class="form-control kode" type="text" value="" name="kode" required>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label class="col-2 col-form-label">Nama</label>
                    <div class="col-10">
                        <input class="form-control nama" type="text" value="" name="nama" required>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>

$('.kabupatenKode, .kecamatanKode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id').select2({
    placeholder: 'Pilih Opsi'
}).val('').trigger('change');


ajaxSelect({
    allowClear: true,
    id: '.provinsiKode',
    url: '{site_url}ajax_selectbox/provinsi_select',
});

$('.provinsiKode').change(function() {
    var val = $(this).val();
    $('.kabupatenKode, .kecamatanKode, .desa_kode').empty();
    ajaxSelect({
        id: '.kabupatenKode',
        url: '{site_url}ajax_selectbox/kabupaten_select',
        optionalSearch: {
            provinsi_kode: val
        }
    });
});

$('.kabupatenKode').change(function() {
    var val = $(this).val();
    $('.kecamatanKode, .desa_kode').empty();
    ajaxSelect({
        id: '.kecamatanKode',
        url: '{site_url}ajax_selectbox/kecamatan_select',
        optionalSearch: {
            kabupaten_kode: val
        }
    });
});

$('.kecamatanKode').change(function() {
    var val = $(this).val();
    $('.desa_kode').empty();
    ajaxSelect({
        id: '.desa_kode',
        url: '{site_url}ajax_selectbox/desa_select',
        optionalSearch: {
            kecamatan_kode: val
        }
    });
});
function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>