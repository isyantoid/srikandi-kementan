<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Data_master extends Api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('','');
    }


    public function tipe_bantuan_get() {
        $q = $this->get('q');
        $start = $this->get('start');
        $limit = $this->get('limit') ? $this->get('limit') : 10;

        $this->db->select('
            tbl_tipe_bantuan.id,
            tbl_tipe_bantuan.tipe_bantuan as nama,
        ');
        if($q) $this->db->like('tbl_tipe_bantuan.tipe_bantuan', $q, 'match');
        $this->db->order_by('tbl_tipe_bantuan.tipe_bantuan', 'ASC');
        $respon = $this->db->get('tbl_tipe_bantuan', $limit, $start)->result_array();
        $this->set_response($respon, REST_Controller::HTTP_OK);
    }
    
    public function jenis_bantuan_get() {
        $q = $this->get('q');
        $start = $this->get('start');
        $limit = $this->get('limit') ? $this->get('limit') : 10;

        $this->db->select('
            tbl_jenis_bantuan.id,
            concat(tbl_jenis_bantuan.kode_jenis, " - ", tbl_jenis_bantuan.nama_bantuan, " (", tbl_jenis_bantuan.satuan, ")") as nama,
        ');
        if($q) $this->db->like('tbl_jenis_bantuan.nama_bantuan', $q, 'match');
        $this->db->order_by('tbl_jenis_bantuan.nama_bantuan', 'ASC');
        $respon = $this->db->get('tbl_jenis_bantuan', $limit, $start)->result_array();
        $this->set_response($respon, REST_Controller::HTTP_OK);
    }
    
    public function jenis_komoditi_get() {
        $q = $this->get('q');
        $start = $this->get('start');
        $limit = $this->get('limit') ? $this->get('limit') : 10;

        $this->db->select('
            tbl_jenis_komoditi.id,
            concat("(",tbl_jenis_komoditi.jenis_komoditi_kode, ") ", tbl_jenis_komoditi.jenis_komoditi_nama) as nama,
        ');
        if($q) $this->db->like('tbl_jenis_komoditi.jenis_komoditi_nama', $q, 'match');
        $this->db->order_by('tbl_jenis_komoditi.jenis_komoditi_nama', 'ASC');
        $respon = $this->db->get('tbl_jenis_komoditi', $limit, $start)->result_array();
        $this->set_response($respon, REST_Controller::HTTP_OK);
    }
    
    public function sub_komoditi_get() {
        $idHead = $this->get('idHead');

        if($idHead) {
            $q = $this->get('q');
            $start = $this->get('start');
            $limit = $this->get('limit') ? $this->get('limit') : 10;
    
            $this->db->select('
                tbl_sub_komoditi.id,
                concat("(",tbl_sub_komoditi.sub_komoditi_kode, ") ", tbl_sub_komoditi.sub_komoditi_nama) as nama,
            ');
            if($q) $this->db->like('tbl_sub_komoditi.sub_komoditi_nama', $q, 'match');
            $this->db->where('jenis_komoditi_id', $idHead);
            $this->db->order_by('tbl_sub_komoditi.sub_komoditi_nama', 'ASC');
            $respon = $this->db->get('tbl_sub_komoditi', $limit, $start)->result_array();
            $this->set_response($respon, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
    public function provinsi_get() {

        $q = $this->get('q');
        $start = $this->get('start');
        $limit = $this->get('limit') ? $this->get('limit') : 10;

        $this->db->select('
            tbl_provinsi.kode as id,
            concat("(",tbl_provinsi.kode, ") ", tbl_provinsi.nama) as nama,
        ');
        if($q) $this->db->like('tbl_provinsi.nama', $q, 'match');
        $this->db->order_by('tbl_provinsi.id', 'ASC');
        $respon = $this->db->get('tbl_provinsi', $limit, $start)->result_array();
        $this->set_response($respon, REST_Controller::HTTP_OK);
    }

    public function kabupaten_get() {
        $idHead = $this->get('idHead');

        if($idHead) {
            $q = $this->get('q');
            $start = $this->get('start');
            $limit = $this->get('limit') ? $this->get('limit') : 10;
    
            $this->db->select('
                tbl_kabupaten.kode as id,
                concat("(",tbl_kabupaten.kode, ") ", tbl_kabupaten.nama) as nama,
            ');
            if($q) $this->db->like('tbl_kabupaten.nama', $q, 'match');
            $this->db->where('provinsiKode', $idHead);
            $this->db->order_by('tbl_kabupaten.id', 'ASC');
            $respon = $this->db->get('tbl_kabupaten', $limit, $start)->result_array();
            $this->set_response($respon, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
    public function kecamatan_get() {
        $idHead = $this->get('idHead');

        if($idHead) {
            $q = $this->get('q');
            $start = $this->get('start');
            $limit = $this->get('limit') ? $this->get('limit') : 10;
    
            $this->db->select('
                tbl_kecamatan.kode as id,
                concat("(",tbl_kecamatan.kode, ") ", tbl_kecamatan.nama) as nama,
            ');
            if($q) $this->db->like('tbl_kecamatan.nama', $q, 'match');
            $this->db->where('kabupatenKode', $idHead);
            $this->db->order_by('tbl_kecamatan.id', 'ASC');
            $respon = $this->db->get('tbl_kecamatan', $limit, $start)->result_array();
            $this->set_response($respon, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
    public function desa_get() {
        $idHead = $this->get('idHead');

        if($idHead) {
            $q = $this->get('q');
            $start = $this->get('start');
            $limit = $this->get('limit') ? $this->get('limit') : 10;
    
            $this->db->select('
                tbl_desa.kode as id,
                concat("(",tbl_desa.kode, ") ", tbl_desa.nama) as nama,
            ');
            if($q) $this->db->like('tbl_desa.nama', $q, 'match');
            $this->db->where('kecamatanKode', $idHead);
            $this->db->order_by('tbl_desa.id', 'ASC');
            $respon = $this->db->get('tbl_desa', $limit, $start)->result_array();
            $this->set_response($respon, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    
    public function kelompok_tani_get() {
        $id_permohonan = $this->get('id_permohonan');

        if($id_permohonan) {
            $q = $this->get('q');
            $start = $this->get('start');
            $limit = $this->get('limit') ? $this->get('limit') : 10;
    
            $this->db->select('
                tbl_kelompok_tani.id,
                tbl_kelompok_tani.nama_kelompok as nama,
            ');
            if($q) $this->db->like('tbl_kelompok_tani.nama_kelompok', $q, 'match');
            $this->db->where('id_permohonan', $id_permohonan);
            $this->db->order_by('tbl_kelompok_tani.nama_kelompok', 'ASC');
            $respon = $this->db->get('tbl_kelompok_tani', $limit, $start)->result_array();
            $this->set_response($respon, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    public function nomor_registrasi_get() {

        $q = $this->get('q');
        $start = $this->get('start');
        $limit = $this->get('limit') ? $this->get('limit') : 10;

        $this->db->select('
            tbl_permohonan.id,
            tbl_permohonan.nomor_registrasi as nama,
        ');
        if($q) $this->db->like('tbl_permohonan.nomor_registrasi', $q, 'match');
        $this->db->order_by('tbl_permohonan.id', 'ASC');
        $respon = $this->db->get('tbl_permohonan', $limit, $start)->result_array();
        $this->set_response($respon, REST_Controller::HTTP_OK);
    }
}
