<style>
    .pac-container {
        z-index: 1050;
        position: fixed !important;
        top: 15% !important;
    }
</style>
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body" style="padding-bottom:0px !important">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Nama Nurseri</label>
                    <div class="col-10">
                        <input class="form-control nama" type="text" value="" name="nama" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Nomor HP</label>
                    <div class="col-10">
                        <input class="form-control nomor_hp" type="text" value="" name="nomor_hp" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Alamat</label>
                    <div class="col-10">
                        <input class="form-control alamat" type="text" value="" name="alamat" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Komoditi</label>
                    <div class="col-10">
                        <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Luas (ha)</label>
                    <div class="col-10">
                        <input class="form-control luas" type="number" value="" name="luas" required step="any">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Provinsi</label>
                    <div class="col-10">
                        <select class="form-control provinsi_kode" name="provinsi_kode" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Kabupaten</label>
                    <div class="col-10">
                        <select class="form-control kabupaten_kode" name="kabupaten_kode" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Kecamatan</label>
                    <div class="col-10">
                        <select class="form-control kecamatan_kode" name="kecamatan_kode" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Desa</label>
                    <div class="col-10">
                        <select class="form-control desa_kode" name="desa_kode" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Lat & Lng</label>
                    <div class="col-4">
                        <input class="form-control lat" type="text" value="" name="lat" required autocomplete="off">
                    </div>
                    <div class="col-4">
                        <input class="form-control lng" type="text" value="" name="lng" required autocomplete="off">
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-success btn-block btn_modal_map1">Cari</button>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Username Login</label>
                    <div class="col-10">
                        <input class="form-control username" type="text" value="" name="username" required autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Password Login</label>
                    <div class="col-10">
                        <input class="form-control password" type="password" value="" name="password" required autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Photo</label>
                    <div class="col-10">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="foto" id="customFile">
                            <label class="custom-file-label text-left" for="customFile">Foto</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<div class="modal fade" id="modal_map1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map1()" id="formMap1">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-margin-b-20" id="pac-card" style="">
                        <div class="input-group" id="pac-container">
                            <input type="text" class="form-control" id="pac-input" placeholder="address...">
                        </div>
                    </div>
                    <div style="height:350px;" id="map1"></div>

                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai latitude dan longitude</p>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Garis lintang:</label>
                            <input type="text" class="form-control latMap1" name="latMap1" id="latMap1" required placeholder="Garis lintang">
                        </div>
                        <div class="col-md-6">
                            <label>Garis bujur:</label>
                            <input type="text" class="form-control lngMap1" name="lngMap1" id="lngMap1" required placeholder="Garis bujur">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&libraries=places,geometry,drawing"></script>

<script>
    function save_lat_lng_map1() {
        var form = $('#formMap1')[0];
        var formData = new FormData(form);

        if (!formData.get('latMap1') && !formData.get('lngMap1')) {
            alert('Latitude dan longitude wajib diisi')
            return false;
        } else {
            $('.lat').val(formData.get('latMap1'));
            $('.lng').val(formData.get('lngMap1'));
            $('#modal_map1').modal('hide');
        }
    }

    $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');

    ajaxSelect({
        id: '.jenis_komoditi_id',
        url: '{site_url}ajax_selectbox/bamer_id',
        selected: '{jenis_komoditi_id}'
    });
    ajaxSelect({
        allowClear: true,
        id: '.provinsi_kode',
        url: '{site_url}ajax_selectbox/provinsi_select',
    });

    $('.provinsi_kode').change(function() {
        var val = $(this).val();
        $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
        ajaxSelect({
            id: '.kabupaten_kode',
            url: '{site_url}ajax_selectbox/kabupaten_select',
            optionalSearch: {
                provinsi_kode: val
            }
        });
    });

    $('.kabupaten_kode').change(function() {
        var val = $(this).val();
        $('.kecamatan_kode, .desa_kode').empty();
        ajaxSelect({
            id: '.kecamatan_kode',
            url: '{site_url}ajax_selectbox/kecamatan_select',
            optionalSearch: {
                kabupaten_kode: val
            }
        });
    });

    $('.kecamatan_kode').change(function() {
        var val = $(this).val();
        $('.desa_kode').empty();
        ajaxSelect({
            id: '.desa_kode',
            url: '{site_url}ajax_selectbox/desa_select',
            optionalSearch: {
                kecamatan_kode: val
            }
        });
    });

    $('.btn_modal_map1').click(function() {
        $('#modal_map1').modal('show');
    })

    var map;
    var geocoder;
    var polygonString = [];
    var polygonArray = [];

    function initialize() {
        var mapOptions = {
            zoom: 5.2,
            center: new google.maps.LatLng(-2.0423904, 102.8983498),
            // mapTypeId: google.maps.MapTypeId.ROADMAP
            mapTypeControl: false,
        };

        map = new google.maps.Map(document.getElementById('map1'),
            mapOptions
        );


        var card = document.getElementById("pac-card");
        var input = document.getElementById("pac-input");
        const options = {
            fields: ["formatted_address", "geometry", "name"],
            strictBounds: false,
            componentRestrictions: {
                country: "id"
            },
            types: ["geocode"],
            draggable: false,
            disableDefaultUI: true,
        };

        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo("bounds", map);

        const infowindow = new google.maps.InfoWindow();
        const infowindowContent = document.getElementById("infowindow-content");

        infowindow.setContent(infowindowContent);

        var marker = new google.maps.Marker({
            map,
            anchorPoint: new google.maps.Point(0, -29),
        });

        autocomplete.addListener("place_changed", () => {
            infowindow.close();
            marker.setVisible(false);

            const place = autocomplete.getPlace();

            if (!place.geometry || !place.geometry.location) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            document.getElementById('latMap1').value = place.geometry.location.lat();
            document.getElementById('lngMap1').value = place.geometry.location.lng();
            console.log(event);

            if (marker != null) {
                marker.setMap(null);
            }
            addMarker(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()), map);


            infowindowContent.children["place-name"].textContent = place.name;
            infowindowContent.children["place-address"].textContent =
                place.formatted_address;
            infowindow.open(map, marker);
        });

        google.maps.event.addListener(map, 'click', function(event) {
            document.getElementById('latMap1').value = event.latLng.lat();
            document.getElementById('lngMap1').value = event.latLng.lng();
            console.log(event);

            if (marker != null) {
                marker.setMap(null);
            }
            addMarker(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()), map);
        });

        function addMarker(latLng, map) {
            // if marker exist remove it show only one marker
            if (marker != null) {
                marker.setMap(null);
            }

            marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }


    }
    google.maps.event.addDomListener(window, 'load', initialize);

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>