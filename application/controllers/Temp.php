<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Temp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function insert_kelompok_tani() {
        $permohonan = $this->db->select('id, nomor_registrasi')->order_by('id')->get('tbl_permohonan')->result_array();
        foreach($permohonan as $row) {
            $data_import = $this->db->select('nama_kelompok_tani, luas')->where('nomor_registrasi', $row['nomor_registrasi'])->get('data_import')->result_array();
            $luas = 0;
            foreach($data_import as $di) {
                $data['id_permohonan'] = $row['id'];
                $data['nama_kelompok'] = $di['nama_kelompok_tani'];
                $data['luas'] = $di['luas'];
                $this->db->insert('tbl_kelompok_tani', $data);

                $luas = $luas + $di['luas'];
            }
            $this->db->set('luas_kebun', $luas);
            $this->db->where('id', $row['id']);
            $this->db->update('tbl_permohonan');
        }
    }
}