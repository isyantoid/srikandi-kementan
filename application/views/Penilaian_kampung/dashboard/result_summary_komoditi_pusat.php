<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> {title} </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row">
                    <?php $arr = array(NULL, 'Bawang Merah', 'Bawang Putih', 'Bawang Daun', 'Jahe Gajah'); ?>
                    <?php for ($i = 1; $i <= 4; $i++) : ?>
                        <div class="col-md-3">
                            <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head kt-portlet__head--noborder">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-2">
                                        <div class="kt-widget__head">
                                            <div class="kt-widget__media">
                                                <img class="kt-widget__img kt-hidden-" src="{dist_path}images/komoditi/<?php echo $i ?>.jpg" alt="image">
                                            </div>
                                            <div class="kt-widget__info">
                                                <a href="#" class="kt-widget__username result_summary_provinsi" data-komoditi="<?php echo $arr[$i] ?>" data-image="<?php echo $i ?>.jpg"> <?php echo $arr[$i] ?> </a>
                                                <span class="kt-widget__desc">
                                                    Tanaman Sayuran
                                                </span>
                                            </div>
                                        </div>
                                        <div class="kt-widget__body">
                                            <div class="kt-widget__item mt-5">
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Total Kampung:</span>
                                                    <a href="#" class="kt-widget__data">10</a>
                                                </div>
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Total Luas Kampung:</span>
                                                    <a href="#" class="kt-widget__data">10</a>
                                                </div>
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Total Kelompok Tani:</span>
                                                    <span class="kt-widget__data">5</span>
                                                </div>
                                                <div class="kt-widget__contact">
                                                    <span class="kt-widget__label">Total Anggota Tani:</span>
                                                    <span class="kt-widget__data">12</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor ?>
                </div>
            </div>
        </div>
    </div>
</div>

