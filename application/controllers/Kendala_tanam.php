<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kendala_tanam extends CI_Controller
{

    private $title = 'Kendala Tanam';
    private $view = 'Kendala_tanam';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        $this->load->helper('download');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_kendala_tanam.id,
            tbl_kendala_tanam.tanggal_kendala_tanam,
            tbl_kendala_tanam.luas_lahan,
            tbl_kendala_tanam.keterangan,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.sub_komoditi_nama,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->datatables_server_side->join('tbl_permohonan','tbl_kendala_tanam.id_permohonan = tbl_permohonan.id', 'left');
        $this->datatables_server_side->join('tbl_kelompok_tani','tbl_kendala_tanam.id_kelompok_tani = tbl_kelompok_tani.id', 'left');
        $this->datatables_server_side->where('tbl_permohonan.app_id', '2');

        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        
        $this->datatables_server_side->from('tbl_kendala_tanam');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    
    public function form_kelompok()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['nomor'] = $this->input->post('nomor');
        $data = array_merge($data, path_info());
        $this->parser->parse($this->view . '/form_kelompok', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('tbl_kendala_tanam', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }
    
    public function detail($id = null)
    {
        akses_user('read');
        if ($id) {
            $data = $this->model->detail_kendala_tanam($id);
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Detail Kendala Tanam';
                $data['content'] = $this->view . '/detail';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function get_permohonan($id = null) {
        if($this->input->is_ajax_request()) {
            if($id) {
                $permohonan = getRowArray('tbl_permohonan', array('id' => $id));
                if($permohonan) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($permohonan));
                }
            }
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }

    public function get_nomor_urut_registrasi() {
        if($this->input->is_ajax_request()) {
            $search = $this->input->post('search');
            $this->db->like('nomor_registrasi', $search);
            $count = $this->db->get('tbl_permohonan')->num_rows();
            $data['nomor_urut'] = str_pad($count + 1, 4, "0", STR_PAD_LEFT);
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
        
    }


    public function do_upload_document() {
        if (!file_exists($_FILES['file']['tmp_name']) || !is_uploaded_file($_FILES['file']['tmp_name'])) {

        } else {
            $config['upload_path'] = './uploads/kendala_tanam/';
            $config['allowed_types'] = 'jpg|png|pdf';
            $config['encrypt_name'] = True;

            $this->load->library('upload', $config);
            if($this->upload->do_upload('file')) {
                $data = $this->upload->data();
                $this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
            } else {
                $error = $this->upload->display_errors();
                $this->output->set_content_type('application/json')->set_output(json_encode($error, JSON_PRETTY_PRINT));
            }
        }
    }


    public function remove_file() {
        $filename =  $this->input->post('file_name');
        if(file_exists('./uploads/kendala_tanam/'.$filename)) {
            unlink('./uploads/kendala_tanam/'.$filename);
            $this->output->set_content_type('application/json')->set_output(json_encode(['status' => 'success']));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(['status' => 'error']));
        }
    }

    public function download_file() {
        $url = $this->input->get('url');
        force_download($url, NULL);
    }


    public function export_data()
    {
        akses_user('read');
        $fileName = 'kendala_tanam' . date('d_m_Y') . '.xlsx';
        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $styleForTitle = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 16,
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForHeader = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 10,
            ],
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'DDDDDD'],
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForBody = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
                'name' => 'Cambria',
            ],
            'borders' => [
                'allBorders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => ['rgb' => '444444']
                ]
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->mergeCells('A1:H1')->getStyle('A1')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A2:H2')->getStyle('A2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A3:H3')->getStyle('A1:A3')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'LAPORAN KENDALA TANAM');

        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Nomor Registrasi');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Tanggal Terkendala');
        $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Nama Kelompok');
        $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Jenis Komoditi');
        $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Luas Terkendala');
        $objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Satuan');
        $objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Keterangan');
        // $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Foto');
        foreach (range('A', 'H') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A4:H4')->applyFromArray($styleForHeader);

        $regKampung = $this->model->exportExcel();
        if($regKampung) {
            $rowCount = 5;
            $no=1;
            foreach($regKampung as $row) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no)->getStyle('A' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['nomor_registrasi'])->getStyle('B' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('c' . $rowCount, $row['tanggal_kendala_tanam'])->getStyle('c' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['nama_kelompok'])->getStyle('D' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['jenis_komoditi_nama'])->getStyle('E' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['luas_lahan'])->getStyle('F' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['satuan_luas_kebun'])->getStyle('G' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['keterangan'])->getStyle('H' . $rowCount)->applyFromArray($styleForBody);
                $rowCount++;
                $no++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}