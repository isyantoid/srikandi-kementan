<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Wilayah extends CI_Controller
{

    private $title = 'Wilayah';
    private $view = 'Wilayah';

    public function __construct()
    {
        parent::__construct();
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index_provinsi()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'List Provinsi';
        $data['content'] = $this->view . '/index_provinsi';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    
    public function index_kabupaten($prov=null)
    {
        if($prov) {
            $data['title'] = $this->title;
            $data['subTitle'] = 'List Kabupaten';
            $data['prov'] = $prov;
            $data['content'] = $this->view . '/index_kabupaten';
            $data = array_merge($data, path_info());
            $this->parser->parse('admin_template/main', $data);
        } else {
            show_404();
        }
    }
    
    public function index_kecamatan($prov=null, $kab = null)
    {
        if($prov && $kab) {
            $data['title'] = $this->title;
            $data['subTitle'] = 'List Kecamatan';
            $data['prov'] = $prov;
            $data['kab'] = $kab;
            $data['content'] = $this->view . '/index_kecamatan';
            $data = array_merge($data, path_info());
            $this->parser->parse('admin_template/main', $data);
        } else {
            show_404();
        }
    }

    public function index_desa($prov=null, $kab = null, $kec = null)
    {
        if($prov && $kab && $kec) {
            $data['title'] = $this->title;
            $data['subTitle'] = 'List Desa';
            $data['prov'] = $prov;
            $data['kab'] = $kab;
            $data['kec'] = $kec;
            $data['content'] = $this->view . '/index_desa';
            $data = array_merge($data, path_info());
            $this->parser->parse('admin_template/main', $data);
        } else {
            show_404();
        }
    }
}