<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Registrasi_kampung extends Api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('','');
    }


    public function all_get() {
        $q = $this->get('q');
        $start = $this->get('start');
        $limit = $this->get('limit') ? $this->get('limit') : 10;

        $this->db->select('
            permohonan_view.id,
            permohonan_view.nomor_registrasi as nomorRegistrasi,
            permohonan_view.tanggal_permohonan as tanggalPermohonan,
            permohonan_view.provinsi,
            permohonan_view.kabupaten,
            permohonan_view.jenis_komoditi_nama as jenisKomoditi,
            permohonan_view.luas_kebun as luasKebun,
            permohonan_view.satuan_luas_kebun as satuanLuasKebun,
            permohonan_view.jenis_komoditi_icon as jenisKomoditiIcon,
            concat("'.base_url('dist/images/jenis_komoditi/').'", permohonan_view.jenis_komoditi_icon) as jenisKomoditiIconUrl,
            date_format(permohonan_view.tanggal_tanam, "%d %M %Y") as tanggalTanam,
            date_format(permohonan_view.tanggal_perkiraan_panen, "%d %M %Y") as tanggalPerkiraanPanen,
        ');
        if($q) {
            $this->db->like('permohonan_view.nomor_registrasi', $q, 'match');
            $this->db->or_like('permohonan_view.jenis_komoditi_nama', $q, 'match');
            $limit = 5;
        }
        $this->db->order_by('permohonan_view.id', 'DESC');
        $this->db->order_by('permohonan_view.jenis_komoditi_nama', 'ASC');
        $respon = $this->db->get('permohonan_view', $limit, $start)->result_array();
        $this->set_response($respon, REST_Controller::HTTP_OK);
    }

    public function save_registrasi_kampung_post() {
        // $respon = $this->input->post();
        // $this->set_response($respon, REST_Controller::HTTP_OK);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nomorRegistrasi', 'nomorRegistrasi', 'trim|required');
        $this->form_validation->set_rules('jenisKomoditiId', 'jenisKomoditiId', 'trim|required');
        $this->form_validation->set_rules('subKomoditiId', 'subKomoditiId', 'trim|required');
        $this->form_validation->set_rules('provinsiId', 'provinsiId', 'trim|required');
        $this->form_validation->set_rules('kabupatenId', 'kabupatenId', 'trim|required');
        $this->form_validation->set_rules('kecamatanId', 'kecamatanId', 'trim|required');
        $this->form_validation->set_rules('desaId', 'desaId', 'trim|required');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required');
        $this->form_validation->set_rules('luasKebun', 'luasKebun', 'trim|required');
        $this->form_validation->set_rules('tanggalTanam', 'tanggalTanam', 'trim|required');
        $this->form_validation->set_rules('tanggalPerkiraanPanen', 'tanggalPerkiraanPanen', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->set_response(['message' => validation_errors()], REST_Controller::HTTP_BAD_REQUEST);
        } else {
            $jenisKomoditi = getRowArray('tbl_jenis_komoditi', ['id' => $this->input->post('jenisKomoditiId')]);

            $this->db->set('nomor_registrasi', $this->input->post('nomorRegistrasi'));
            $this->db->set('tanggal_permohonan', date('Y-m-d'));
            $this->db->set('provinsi_kode', $this->input->post('provinsiId'));
            $this->db->set('kabupaten_kode', $this->input->post('kabupatenId'));
            $this->db->set('kecamatan_kode', $this->input->post('kecamatanId'));
            $this->db->set('desa_kode', $this->input->post('desaId'));
            $this->db->set('lat', $this->input->post('latitude'));
            $this->db->set('lng', $this->input->post('longitude'));
            $this->db->set('kelompok_komoditi_id', $jenisKomoditi['kelompok_komoditi_id']);
            $this->db->set('jenis_komoditi_id', $this->input->post('jenisKomoditiId'));
            $this->db->set('sub_komoditi_id', $this->input->post('subKomoditiId'));
            $this->db->set('luas_kebun', $this->input->post('luasKebun'));
            $this->db->set('satuan_luas_kebun', 'Hektar');
            $this->db->set('tanggal_tanam', $this->input->post('tanggalTanam'));
            $this->db->set('tanggal_perkiraan_panen', $this->input->post('tanggalPerkiraanPanen'));
            if($this->db->insert('tbl_permohonan')) {
                $this->set_response(['message' => 'data has '], REST_Controller::HTTP_OK);
            } else {
                $this->set_response(['message' => 'internal server error'], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }


    public function detail_get() {
        $id = $this->get('id');

        if($id) {
            $this->db->select('
                tbl_permohonan.id,
                tbl_permohonan.nomor_registrasi as nomorRegistrasi,
                tbl_permohonan.tanggal_permohonan as tanggalPermohonan,
                tbl_permohonan.luas_kebun as luasKebun,
                tbl_permohonan.satuan_luas_kebun as satuanLuasKebun,
                tbl_permohonan.tanggal_tanam as tanggalTanam,
                tbl_permohonan.tanggal_perkiraan_panen as tanggalPerkiraanPanen,
                tbl_jenis_komoditi.jenis_komoditi_nama as jenisKomoditi,
                tbl_provinsi.nama as provinsi,
                tbl_kabupaten.nama as kabupaten,
                tbl_kecamatan.nama as kecamatan,
                tbl_desa.nama as desa,
            ');
            $this->db->where('tbl_permohonan.id', $id);
            $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
            $this->db->join('tbl_provinsi', 'tbl_permohonan.provinsi_kode = tbl_provinsi.kode', 'left');
            $this->db->join('tbl_kabupaten', 'tbl_permohonan.kabupaten_kode = tbl_kabupaten.kode', 'left');
            $this->db->join('tbl_kecamatan', 'tbl_permohonan.kecamatan_kode = tbl_kecamatan.kode', 'left');
            $this->db->join('tbl_desa', 'tbl_permohonan.desa_kode = tbl_desa.kode', 'left');
            $sql = $this->db->get('tbl_permohonan');
            $respon = $sql->result_array();
            $this->set_response($respon, REST_Controller::HTTP_OK);
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
