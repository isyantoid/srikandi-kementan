<style>
.pac-container {
    z-index: 1050;
    position: fixed !important;
    top: 15% !important;
}
</style>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Nomor Registrasi P2L:</label>
                        <select class="form-control registrasi_p2l_id" name="registrasi_p2l_id" style="width:100%;"
                            required></select>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Tanggal penerima bantuan:</label>
                        <input type="text" class="form-control tanggal_penerima_bantuan" name="tanggal_penerima_bantuan"
                            value="{tanggal_penerima_bantuan}" required placeholder="Masukan Tanggal penerima bantuan">
                    </div>
                    <div class="col-md-3">
                        <label>Realisasi pemanfaatan anggaran (Rp):</label>
                        <input type="number" class="form-control realisasi_pemanfaatan_anggaran"
                            name="realisasi_pemanfaatan_anggaran" value="{realisasi_pemanfaatan_anggaran}" required
                            placeholder="Masukan realisasi pemanfaatan anggaran">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<!--begin::Modal-->
<div class="modal fade" id="modal_map1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map1()" id="formMap1">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-margin-b-20" id="pac-card" style="">
                        <div class="input-group" id="pac-container">
                            <input type="text" class="form-control" id="pac-input" placeholder="address...">
                        </div>
                    </div>
                    <div style="height:350px;" id="map1"></div>

                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai
                        latitude dan longitude</p>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Garis lintang:</label>
                            <input type="text" class="form-control latMap1" name="latMap1" id="latMap1" required
                                placeholder="Garis lintang">
                        </div>
                        <div class="col-md-6">
                            <label>Garis bujur:</label>
                            <input type="text" class="form-control lngMap1" name="lngMap1" id="lngMap1" required
                                placeholder="Garis bujur">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="modal_map2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map2()" id="formMap2">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP 2</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div style="height:350px;" id="map2"></div>
                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai
                        latitude dan longitude</p>

                    <!-- <input type='file' accept=".kml,.kmz" onchange="fileChanged()"> -->

                    <input type="hidden" class="latMap2nomorheader" name="latMap2nomorheader" value="">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Garis lintang:</label>
                            <input type="text" class="form-control latMap2" name="latMap2" id="latMap2" required
                                placeholder="Garis lintang">
                        </div>
                        <div class="col-md-6">
                            <label>Garis Bujur:</label>
                            <input type="text" class="form-control lngMap2" name="lngMap2" id="lngMap2" required
                                placeholder="Garis bujur">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="modal_map3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map3()" id="formMap3">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP Polygon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div style="height:350px;" id="map3"></div>
                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai
                        latitude dan longitude</p>

                    <!-- <input type='file' accept=".kml,.kmz" onchange="fileChanged()"> -->

                    <input type="hidden" class="latMap3nomorheader" name="latMap3nomorheader" value="">
                    <input type="hidden" id="polygon_array" class="polygon_array" name="polygon_array" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<script src="{base_url}dist/assets/js/togeojson.js"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&libraries=places,geometry,drawing">
</script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu67fpz02di0x7OTI-ICN4tLDWxCoJzIk&callback=initMap&libraries=geometry,drawing&ext=.js"></script> -->

<script>
$(document).ready(function() {

    $('.tanggal_registrasi_p2l, .tanggal_penerima_bantuan')
        .datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });

    $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');


    ajaxSelect({
        id: '.registrasi_p2l_id',
        url: '{site_url}ajax_selectbox/registrasi_p2l_id',
        selected: '{registrasi_p2l_id}'
    });


    ajaxSelect({
        allowClear: true,
        id: '.provinsi_kode',
        url: '{site_url}ajax_selectbox/provinsi_select',
        selected: '{provinsi_kode}'
    });

    $('.provinsi_kode').change(function() {
        var val = $(this).val();
        $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
        ajaxSelect({
            id: '.kabupaten_kode',
            url: '{site_url}ajax_selectbox/kabupaten_select',
            selected: '{kabupaten_kode}',
            optionalSearch: {
                provinsi_kode: val
            }
        });
    });

    $('.kabupaten_kode').change(function() {
        var val = $(this).val();
        $('.kecamatan_kode, .desa_kode').empty();
        ajaxSelect({
            id: '.kecamatan_kode',
            url: '{site_url}ajax_selectbox/kecamatan_select',
            selected: '{kecamatan_kode}',
            optionalSearch: {
                kabupaten_kode: val
            }
        });
    });

    $('.kecamatan_kode').change(function() {
        var val = $(this).val();
        $('.desa_kode').empty();
        ajaxSelect({
            id: '.desa_kode',
            url: '{site_url}ajax_selectbox/desa_select',
            selected: '{desa_kode}',
            optionalSearch: {
                kecamatan_kode: val
            }
        });
    });


    ajaxSelect({
        id: '.kelompok_tani_id',
        url: '{site_url}ajax_selectbox/kelompok_tani_id',
        selected: '{kelompok_tani_id}'
    });
    ajaxSelect({
        id: '.kelompok_komoditi_id',
        url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
    });

    $('.kelompok_komoditi_id').change(function() {
        var val = $(this).val();
        $('.jenis_komoditi_id').empty();
        ajaxSelect({
            id: '.jenis_komoditi_id',
            url: '{site_url}ajax_selectbox/jenis_komoditi_id',
            optionalSearch: {
                kelompok_komoditi_id: val
            }
        });
    });

    $('.jenis_komoditi_id').change(function() {
        var val = $(this).val();
        $('.sub_komoditi_id').empty();
        ajaxSelect({
            id: '.sub_komoditi_id',
            url: '{site_url}ajax_selectbox/sub_komoditi_id',
            optionalSearch: {
                jenis_komoditi_id: val
            }
        });
    });

    // open map1 
    $('.btn_modal_map1').click(function() {
        $('#modal_map1').modal('show');
    })





    $(document).on('click', '.btn_modal_map2', function() {
        var index = $(this).data('index');
        $('.latMap2nomorheader').val(index);

        $('#modal_map2').modal('show');
    });

    $(document).on('click', '.btn_modal_map3', function() {
        var index = $(this).data('index');
        console.log(index)
        $('.latMap3nomorheader').val(index);

        $('#modal_map3').modal('show');
    });
})



function save_lat_lng_map1() {
    var form = $('#formMap1')[0];
    var formData = new FormData(form);

    if (!formData.get('latMap1') && !formData.get('lngMap1')) {
        alert('Latitude dan longitude wajib diisi')
        return false;
    } else {
        $('.lat').val(formData.get('latMap1'));
        $('.lng').val(formData.get('lngMap1'));
        $('#modal_map1').modal('hide');
    }
}

function save_lat_lng_map2() {
    var form = $('#formMap2')[0];
    var formData = new FormData(form);

    if (!formData.get('latMap2') && !formData.get('lngMap2')) {
        alert('Latitude dan longitude wajib diisi')
        return false;
    } else {
        tableKelompokTani.find('tr.' + formData.get('latMap2nomorheader')).find('.latitude_anggota').val(
            formData.get(
                'latMap2'));
        tableKelompokTani.find('tr.' + formData.get('latMap2nomorheader')).find('.longitude_anggota').val(
            formData.get(
                'lngMap2'));
        $('#modal_map2').modal('hide');
    }
}

function save_lat_lng_map3() {
    var form = $('#formMap3')[0];
    var formData = new FormData(form);

    if (!formData.get('polygon_array')) {
        alert('koordinate polygon wajib diisi')
        return false;
    } else {
        tableKelompokTani.find('tr.' + formData.get('latMap3nomorheader')).find('.polygon').val(formData
            .get(
                'polygon_array'));
        $('#modal_map3').modal('hide');
    }
}

function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save/{id}',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
            btnSpinnerHide();
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}

function generate_nomor_registrasi() {
    var static = 'STO.01.';
    var kabupaten_kode = $('.kabupaten_kode').val();
    var kelompok_komoditi_id = romawiKelompokKomoditi($('.kelompok_komoditi_id').val());
    var jenis_komoditi_id = ($('.jenis_komoditi_id').val()) ? $('.jenis_komoditi_id').val() : '';
    var year = new Date().getFullYear().toString().substr(-2);
    var nomor_registrasi = static + kabupaten_kode + '.' + kelompok_komoditi_id + '.' + jenis_komoditi_id +
        '.' + year;

    $.ajax({
        url: '{segment1}get_nomor_urut_registrasi',
        dataType: 'json',
        method: 'post',
        data: {
            search: nomor_registrasi
        },
        success: function(res) {
            nomor_registrasi += '.' + res.nomor_urut;
            $('.nomor_registrasi').val(nomor_registrasi);
        }
    })

}

function romawiKelompokKomoditi(kode) {
    switch (kode) {
        case '01':
            return 'I';
        case '02':
            return 'II';
        case '03':
            return 'III';
        case '04':
            return 'IV';
        default:
            return '';
    }
}






// MAP1 
var map;
var geocoder;
var polygonString = [];
var polygonArray = [];

function initialize() {
    var mapOptions = {
        zoom: 5.2,
        center: new google.maps.LatLng(-2.0423904, 102.8983498),
        // mapTypeId: google.maps.MapTypeId.ROADMAP
        mapTypeControl: false,
    };

    map = new google.maps.Map(document.getElementById('map1'),
        mapOptions
    );


    var card = document.getElementById("pac-card");
    var input = document.getElementById("pac-input");
    const options = {
        fields: ["formatted_address", "geometry", "name"],
        strictBounds: false,
        componentRestrictions: {
            country: "id"
        },
        types: ["geocode"],
        draggable: false,
        disableDefaultUI: true,
    };

    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo("bounds", map);

    const infowindow = new google.maps.InfoWindow();
    const infowindowContent = document.getElementById("infowindow-content");

    infowindow.setContent(infowindowContent);

    var marker = new google.maps.Marker({
        map,
        anchorPoint: new google.maps.Point(0, -29),
    });

    autocomplete.addListener("place_changed", () => {
        infowindow.close();
        marker.setVisible(false);

        const place = autocomplete.getPlace();

        if (!place.geometry || !place.geometry.location) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        infowindowContent.children["place-name"].textContent = place.name;
        infowindowContent.children["place-address"].textContent =
            place.formatted_address;
        infowindow.open(map, marker);
    });

    google.maps.event.addListener(map, 'click', function(event) {
        document.getElementById('latMap1').value = event.latLng.lat();
        document.getElementById('lngMap1').value = event.latLng.lng();
        console.log(event);

        if (marker != null) {
            marker.setMap(null);
        }
        addMarker(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()), map);
        // marker.setPosition(event.location);
        // marker.setVisible(true);
    });

    function addMarker(latLng, map) {
        // if marker exist remove it show only one marker
        if (marker != null) {
            marker.setMap(null);
        }

        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
    }

    const map2 = new google.maps.Map(document.getElementById("map2"), {
        zoom: 5.2,
        center: {
            lat: -2.0423904,
            lng: 102.8983498
        },
    });

    google.maps.event.addListener(map2, 'click', function(event) {
        document.getElementById('latMap2').value = event.latLng.lat();
        document.getElementById('lngMap2').value = event.latLng.lng();
        console.log(event);
    });



    const map3 = new google.maps.Map(document.getElementById("map3"), {
        zoom: 5.2,
        center: {
            lat: -2.0423904,
            lng: 102.8983498
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_CENTER,
            drawingModes: [
                google.maps.drawing.OverlayType.POLYGON,
            ]
        },
        markerOptions: {
            icon: 'images/car-icon.png'
        },
        circleOptions: {
            fillColor: '#ffff00',
            fillOpacity: 1,
            strokeWeight: 5,
            clickable: false,
            editable: true,
            zIndex: 1
        },
        polygonOptions: {
            fillColor: '#BCDCF9',
            fillOpacity: 0.5,
            strokeWeight: 2,
            strokeColor: '#57ACF9',
            clickable: false,
            editable: false,
            zIndex: 1
        }
    });

    drawingManager.setMap(map3);

    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
        var string = [];
        arrLatLng = [];
        for (var i = 0; i < polygon.getPath().getLength(); i++) {
            latLng = {
                lat: polygon.getPath().getAt(i).lat(),
                lng: polygon.getPath().getAt(i).lng()
            };
            arrLatLng.push(latLng);
        }
        polygonString.push(string);
        polygonArray.push(polygon);
        document.getElementById('polygon_array').value = JSON.stringify(arrLatLng);
    });


}
google.maps.event.addDomListener(window, 'load', initialize);
</script>