<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_rekapitulasi_kampung_model extends CI_Model
{


    public function query_laporan() {        
        $classProcedure = $this->db->query("call rekapitulasi_kampung(2)");
        $resProcedure = $classProcedure->row_array();
        $classProcedure->next_result();
        $classProcedure->free_result();
        $query = $this->db->query($resProcedure['generate_query'])->result_array();
        return $query;
    }

    public function provinsi_nama() {
        $query = $this->db->query("select distinct provinsi_nama from tbl_permohonan");
        $res = $query->result_array();
        return $res;
    }
}