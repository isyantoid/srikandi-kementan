<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet" data-ktportlet="true" id="collapse">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-list-3"></i>
                        </span>
                        <h3 class="kt-portlet__head-title"> {title} </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle"
                                class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md"><i
                                    class="la la-angle-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form kt-form--fit kt-form--label-right" action="javascript:filter()" id="form1">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Tahun:</label>
                                <div class="col-lg-3">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="">--Pilih Tahun--</option>
                                        <?php for ($i = 2010; $i <= date('Y'); $i++) : ?>
                                        <option value="<?php echo $i; ?>" <?php echo ($i == date('Y')) ? 'selected' : '' ?> ><?php echo $i; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Provinsi:</label>
                                <div class="col-lg-3">
                                    <select class="form-control provinsi_kode" name="provinsi_kode"></select>
                                </div>
                                <label class="col-lg-2 col-form-label">Kabupaten:</label>
                                <div class="col-lg-3">
                                    <select class="form-control kabupaten_kode" name="kabupaten_kode"></select>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot kt-portlet__foot--fit-x">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-success">Tampilkan Hasil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="result"></div>

</div>

<!--begin::Page Vendors(used by this page) -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<!--end::Page Vendors -->


<script>
$('.kabupaten_kode, .jenis_komoditi_id').select2({
    placeholder: 'Pilih Opsi'
}).val('').trigger('change');
    
ajaxSelect({
    allowClear: true,
    id: '.provinsi_kode',
    url: '{site_url}ajax_selectbox/provinsi_select',
});

$('.provinsi_kode').change(function() {
    var val = $(this).val();
    $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
    ajaxSelect({
        id: '.kabupaten_kode',
        url: '{site_url}ajax_selectbox/kabupaten_select',
        optionalSearch: {
            provinsi_kode: val
        }
    });
});


var portlet = new KTPortlet('collapse');

$('.id_komoditi').select2({
    placeholder: 'Pilih komoditi'
});
$('.id_jenis_komoditi').select2({
    placeholder: 'Pilih jenis komoditi'
});


$('.tanggal_awal, .tanggal_akhir').datepicker({
    todayHighlight: true,
    orientation: "bottom left",
});

function filter() {

    blockPage();
    $("#form1").ajaxSubmit({
        url: '{site_url}dashboard/result_chart_pusat',
        type: 'post',
        success: function(data) {
            // portlet.collapse('hide');
            unblockPage();
            $('.result').html(data);
        }

    })
    // setTimeout(function() {
    //     portlet.collapse('hide');
    //     $('.result').load('{site_url}dashboard/result_chart_pusat', function() {
    //         unblockPage();
    //     });
    // }, 1000);
}
</script>