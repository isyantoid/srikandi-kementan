<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Realisasi_fisik_sarana_pembenihan extends CI_Controller
{

    private $title = 'Realisasi Fisik Sarana Pembenihan';
    private $view = 'Realisasi_fisik_sarana_pembenihan';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_bantuan_p2l_realisasi.id,
            tbl_bantuan_p2l_realisasi.jumlah,
            tbl_bantuan_p2l_realisasi.nama_realisasi,
            tbl_bantuan_p2l_realisasi.nilai,
            tbl_bantuan_p2l_realisasi.foto,
            tbl_registrasi_p2l.nomor_registrasi_p2l,
            tbl_registrasi_p2l.tanggal_registrasi_p2l,
            tbl_registrasi_p2l.jumlah_penerima_manfaat,
            tbl_registrasi_p2l.terdaftar_simluhtan,
            tbl_bantuan_p2l.tanggal_penerima_bantuan,
            tbl_bantuan_p2l.realisasi_pemanfaatan_anggaran,
            tbl_kelompok_tani.nama_kelompok,
            tbl_registrasi_p2l.nama_ketua,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->join('tbl_bantuan_p2l', 'tbl_bantuan_p2l_realisasi.bantuan_p2l_id = tbl_bantuan_p2l.id');
        $this->datatables_server_side->join('tbl_registrasi_p2l', 'tbl_bantuan_p2l.registrasi_p2l_id = tbl_registrasi_p2l.id');
        $this->datatables_server_side->join('tbl_kelompok_tani', 'tbl_registrasi_p2l.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->datatables_server_side->where('tbl_bantuan_p2l_realisasi.jenis','1');
        $this->datatables_server_side->from('tbl_bantuan_p2l_realisasi');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['provinsi_kode'] = $this->session->userdata('provinsi_kode');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    

    public function detail_registrasi($id) {
        if($id) {
            $registrasi = $this->db
            ->select('tbl_registrasi_p2l.*')
            ->where('tbl_registrasi_p2l.id', $id)
            ->get('tbl_registrasi_p2l')->row_array();
            if($registrasi) {
                return $this->output->set_content_type('application/json')->set_output(json_encode($registrasi));
            } else {
                return jsonOutputError('not found');
            }
        }
    }

    public function list_bantuan($id = null)
    {
        $q = $this->input->get('q');
        $registrasiId = $this->input->get('registrasi_id');
        if ($id) {
            $this->db->select('tbl_bantuan_p2l.id as id, concat(tbl_registrasi_p2l.nomor_registrasi_p2l, " - ",tbl_bantuan_p2l.tanggal_penerima_bantuan, " | ", format(tbl_bantuan_p2l.realisasi_pemanfaatan_anggaran,0)) as text');
            $data = $this->db->where('tbl_bantuan_p2l.id', $id)->get('tbl_bantuan_p2l')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('tbl_bantuan_p2l.id as id, concat(tbl_registrasi_p2l.nomor_registrasi_p2l, " - ",tbl_bantuan_p2l.tanggal_penerima_bantuan, " | ", format(tbl_bantuan_p2l.realisasi_pemanfaatan_anggaran,0)) as text');
            $this->db->limit(50);
            $this->db->order_by('tbl_bantuan_p2l.id', 'asc');
            $this->db->join('tbl_registrasi_p2l', 'tbl_registrasi_p2l.id = tbl_bantuan_p2l.registrasi_p2l_id');
            if ($q) $this->db->like('tbl_bantuan_p2l.tanggal_penerima_bantuan', $q);
            if ($registrasiId) $this->db->where('tbl_bantuan_p2l.registrasi_p2l_id', $registrasiId);
            $data = $this->db->get('tbl_bantuan_p2l')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    private function _upload_files($files) {
        
        $config = array(
            'upload_path'   => './uploads/realisasi_bantuan/',
            'allowed_types' => 'jpg|png|jpeg|gif',
            'overwrite'     => true,
            'encrypt_name'  => true,
            'max_size'      => 1024 * 2,
        );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($files)) {
            $data['status'] = true;
            $data['data'] = $this->upload->data();
            return $data;
        } else {
            $data['status'] = false;
            $data['data'] = $this->upload->display_errors("","");
            return $data;
        }
    }

    public function save($id = null)
    {
        $postDetail = $this->input->post('detail_id');

        if($id) {
            foreach($postDetail as $key => $post) {
                
                if(!empty($_FILES['foto_' . $postDetail[$key]]['name'])) {
                    $upload = $this->_upload_files('foto_' . $postDetail[$key]);
                    if(!$upload['status']) {
                        return jsonOutputError($upload['data']);
                    } else {
                        $data['foto'] = $upload['data']['file_name'];
                    }
                }
    
                $data['nama_realisasi'] = $this->input->post('nama_realisasi')[$key];
                $data['jumlah'] = $this->input->post('jumlah')[$key];
                $data['nilai'] = $this->input->post('nilai')[$key];
                $data['jenis'] = '1';
    
                $this->db->where('id', $id);
                $this->db->update('tbl_bantuan_p2l_realisasi', $data);
            }
        } else {
            foreach($postDetail as $key => $post) {
                
                if(!empty($_FILES['foto_' . $postDetail[$key]]['name'])) {
                    $upload = $this->_upload_files('foto_' . $postDetail[$key]);
                    if(!$upload['status']) {
                        return jsonOutputError($upload['data']);
                    } else {
                        $data['foto'] = $upload['data']['file_name'];
                    }
                } else {
                    $data['foto'] = null;
                }

                
                $data['bantuan_p2l_id'] = $this->input->post('bantuan_p2l_id');
                $data['nama_realisasi'] = $this->input->post('nama_realisasi')[$key];
                $data['jumlah'] = $this->input->post('jumlah')[$key];
                $data['nilai'] = $this->input->post('nilai')[$key];
                $data['jenis'] = '1';
    
                $this->db->insert('tbl_bantuan_p2l_realisasi', $data);
            }
        }
        

        return jsonOutputSuccess();
        
    }

    public function generate_form($id = null)
    {
        if($id) {
            $data = $this->db
            ->select('tbl_bantuan_p2l_realisasi.*')
            ->where('tbl_bantuan_p2l_realisasi.id', $id)
            ->get('tbl_bantuan_p2l_realisasi')->row_array();
            $data['btnDelete'] = $this->input->get('btnDelete');
            $data['tableID'] = $this->input->get('tableID');
            $data['uniqid'] = uniqid();
            $data = array_merge($data, path_info());
            $this->parser->parse($this->view . '/form_realisasi_update', $data);
        } else {
            $data['btnDelete'] = $this->input->get('btnDelete');
            $data['tableID'] = $this->input->get('tableID');
            $data['uniqid'] = uniqid();
            $data = array_merge($data, path_info());
            $this->parser->parse($this->view . '/form_realisasi', $data);
        }
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = $this->db
            ->select('tbl_bantuan_p2l_realisasi.*, tbl_bantuan_p2l.registrasi_p2l_id')
            ->where('tbl_bantuan_p2l_realisasi.id', $id)
            ->join('tbl_bantuan_p2l','tbl_bantuan_p2l_realisasi.bantuan_p2l_id = tbl_bantuan_p2l.id')
            ->get('tbl_bantuan_p2l_realisasi')->row_array();
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}