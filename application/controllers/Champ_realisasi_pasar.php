<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_realisasi_pasar extends CI_Controller
{

    private $title = 'Realisasi Stok Champion Bamer';
    private $view = 'Champ_realisasi_pasar';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('champ_realisasi_pasar.*, tbl_user.nama as user_nama, champ_pasar.nama as pasar_nama');
        $this->datatables->join('tbl_user','champ_realisasi_pasar.user_id = tbl_user.id');
        $this->datatables->join('champ_pasar','champ_realisasi_pasar.champ_pasar_id = champ_pasar.id');
        if($this->session->userdata('session_user_level_id') == 13) {
            $this->datatables->where('tbl_user.kabupaten_kode',$this->session->userdata('session_kabupaten_kode'));
        } else {
            $this->datatables->where('champ_realisasi_pasar.user_id',$this->session->userdata('session_user_id'));
        }
        $this->datatables->from('champ_realisasi_pasar');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('champ_realisasi_pasar', array('id' => $id));
            if ($data) {
                $data['data'] = $data;
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit ' . $this->title;
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}