<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_realisasi_pasar_model extends CI_Model
{

    private $tableName = 'champ_realisasi_pasar';
    private $primaryKey = 'id';

    public function get_pasar() {
        $get = $this->db->get('champ_pasar');
        return $get->result_array();
    }
    
    public function get_persentase($jenis_komoditi_id) {
        $this->db->where('jenis_komoditi_id', $jenis_komoditi_id);
        $get = $this->db->get('champ_carry_over');
        return $get->row_array();
    }
    
    public function get_luas_jenis_komoditi($tahun, $jenis_komoditi_id, $champ_id = null) {
        $this->db->select_sum('champ.luas', 'luas');
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->join('champ', 'champ_komitmen.user_id = champ.user_id', 'left');
        $this->db->where('champ.jenis_komoditi_id', $jenis_komoditi_id);
        if($champ_id) {
            $this->db->where('champ.id', $champ_id);
        }
        $this->db->group_by('champ_komitmen.tahun');
        $this->db->group_by('champ.jenis_komoditi_id');
        $get = $this->db->get('champ_komitmen');
        return $get->row_array()['luas'];
    }

    public function get_stok_awal($tahun, $jenis_komoditi_id, $field, $champ_id = null) {
        $this->db->select_sum($field, $field);
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->join('champ', 'champ_komitmen.user_id = champ.user_id', 'left');
        $this->db->where('champ.jenis_komoditi_id', $jenis_komoditi_id);
        if($champ_id) {
            $this->db->where('champ.id', $champ_id);
        }
        $this->db->group_by('champ_komitmen.tahun');
        $this->db->group_by('champ.jenis_komoditi_id');
        $get = $this->db->get('champ_komitmen');
        return $get->row_array()[$field];
    }
    
    public function get_realisasi_pasar($tahun, $bulan, $jenis_komoditi_id, $pasar_id, $champ_id) {
        $this->db->select_sum('jumlah', 'jumlah');
        $this->db->join('champ', 'champ_realisasi_pasar.user_id = champ.user_id', 'left');
        $this->db->group_by('champ_realisasi_pasar.bulan');
        $this->db->where('champ_realisasi_pasar.champ_pasar_id', $pasar_id);
        $this->db->where('champ_realisasi_pasar.bulan', $bulan);
        $this->db->where('champ_realisasi_pasar.tahun', $tahun);
        $this->db->where('champ.jenis_komoditi_id', $jenis_komoditi_id);
        if($champ_id) {
            $this->db->where('champ.id', $champ_id);
        }
        $get = $this->db->get('champ_realisasi_pasar');
        return $get->row_array();
    }

    

    public function laporan_komitmen($tahun = '', $jenis_komoditi_id = '')
    {
        $id = $this->uri->segment(3);
        $this->db->select('
            champ_komitmen.*,
            champ.nama as champion_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            champ_kontak_dinas.nama as kontak_dinas_nama,
            champ_kontak_dinas.nomor_hp as kontak_dinas_nomor_hp,
        ');
        $this->db->join('champ', 'champ_komitmen.user_id = champ.user_id', 'left');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('champ_kontak_dinas', 'champ_kontak_dinas.kabupaten_kode = champ.kabupaten_kode', 'left');
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->where('champ.jenis_komoditi_id', $jenis_komoditi_id);
        $get = $this->db->get('champ_komitmen');
        return $get->result_array();
    }
}