<div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
<table class="table table-sm">
    <thead>
        <th>#</th>
        <th>Indikator</th>
        <th>Ya/Tidak</th>
    </thead>
    <tbody>
        <?php $indikator = getResultArray('tbl_indikator_penilaian') ?>
        <?php $no = 1;
        foreach ($indikator as $row) : ?>
            <tr>
                <td><?php echo $no ?></td>
                <td class="font-weight-bold" colspan="2"><?php echo $row['nama_indikator'] ?></td>
            </tr>
            <?php $kriteria = getResultArray('tbl_kriteria_penilaian', array('indikator_id' => $row['id'])) ?>
            <?php foreach ($kriteria as $k) : ?>
                <input type="hidden" name="kriteria_id[]" value="<?php echo $k['id'] ?>">
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo $k['nama_kriteria'] ?></td>
                    <td>
                        <div class="kt-radio-inline">
                            <label class="kt-radio kt-radio--bold kt-radio--success">
                                <input type="radio" class="nilai" name="nilai[<?php echo $k['id'] ?>]" value="1" required> Ya
                                <span></span>
                            </label>
                            <label class="kt-radio kt-radio--bold kt-radio--danger">
                                <input type="radio" class="nilai" name="nilai[<?php echo $k['id'] ?>]" value="0" required> Tidak
                                <span></span>
                            </label>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
            <?php $no++ ?>
        <?php endforeach ?>
    </tbody>
</table>