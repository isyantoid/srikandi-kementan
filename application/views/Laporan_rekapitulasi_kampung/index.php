<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead class="bg-dark text-white">
                        <tr>
                            <th class="text-center align-middle" rowspan="2">Jenis Komoditas</th>
                            <th class="text-center align-middle" style="min-width:200px;" colspan="2">Total Indonesia</th>
                            <?php foreach($provinsi_nama as $prov): ?>
                                <th style="min-width:240px;" colspan="2" class="text-center"><?= $prov['provinsi_nama'] ?></th>
                            <?php endforeach ?>
                        </tr>
                        <tr>
                            <th style="min-width:100px;" class="text-center">Luas Kebun <sup>(ha)</sup></th>
                            <th style="min-width:100px;" class="text-center">Total Kebun</th>
                            <?php foreach($provinsi_nama as $prov): ?>
                                <th style="min-width:120px;" class="text-center">Luas Kebun <sup>(ha)</sup></th>
                                <th style="min-width:120px;" class="text-center">Total Kebun</th>
                            <?php endforeach ?>
                        </tr>
                    </thead>

                    <?php if($laporan): ?>
                        <?php foreach($laporan as $row): ?>
                            <tr>
                                <td style="min-width:200px;" class="text-center"><?= $row['jenis_komoditi_nama'] ?></td>
                                <td style="min-width:110px;" class="text-center"><?= $row['total_luas'] ?> Hektar</td>
                                <td style="min-width:110px;" class="text-center"><?= $row['total_kebun'] ?></td>
                                <?php foreach($provinsi_nama as $prov): ?>
                                    <td style="min-width:120px;" class="text-center"><?= $row['luaskebun_'.$prov['provinsi_nama']] ?> Hektar</td>
                                    <td style="min-width:120px;" class="text-center"><?= $row['totalkebun_'.$prov['provinsi_nama']] ?></td>
                                <?php endforeach ?>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </table>
            </div>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->