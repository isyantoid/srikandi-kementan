<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    private $title = 'User';
    private $view = 'User';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('tbl_user.*, tbl_user_level.nama as nama_level');
        $this->datatables->join('tbl_user_level', 'tbl_user.user_level_id = tbl_user_level.id', 'left');
        $this->datatables->where('tbl_user.app_id', '2');
        $this->datatables->from('tbl_user');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('tbl_user', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            return $this->model->delete();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }

    public function change_password()
    {
        $id = $this->session->userdata('session_user_id');
        if ($id) {
            $data = getRowArray('tbl_user', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/change_password';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save_change_password()
    {
        $id = $this->session->userdata('session_user_id');
        $user = getRowArray('tbl_user', ['id' => $id]);
        $password = $this->input->post('password');
        if(md5($password) == $user['password']) {
            $this->db->set('password', md5($this->input->post('new_password')));
            $this->db->where('id', $id);
            $this->db->update('tbl_user');
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Password saat ini salah');
        }
    }



}