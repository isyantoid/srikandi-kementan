<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_carry_over_model extends CI_Model
{

    private $tableName = 'champ_carry_over';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['jenis_komoditi_id'] != $this->input->post('jenis_komoditi_id')) {
                    if (isDuplicate($this->tableName, 'jenis_komoditi_id', $this->input->post('jenis_komoditi_id'))) {
                        return jsonOutputError('Jenis komoditi sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {
                
                if (isDuplicate('champ_carry_over', 'jenis_komoditi_id', $this->input->post('jenis_komoditi_id'))) {
                    return jsonOutputError('Jenis komoditi sudah ada sebelumnya.');
                }

                $this->db->set('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'));
                $this->db->set('persen', $this->input->post('persen'));
                $insertUser = $this->db->insert('champ_carry_over');
                if($insertUser) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }

            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }

    public function config_file_upload() {
        $config['upload_path'] = './uploads/users/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }
}