<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Master_data extends Api
{

    public function __construct()
    {
        parent::__construct();
    }

    public function komoditi_datatable_post() {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            komoditi_m.komoditi_kode,
            komoditi_m.komoditi_nama,
        ');
        $this->datatables_server_side->from('komoditi_m');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function jenis_komoditi_datatable_post() {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            komoditi_jenis_m.komoditi_jenis_id,
            komoditi_jenis_m.komoditi_jenis_kode,
            komoditi_jenis_m.komoditi_jenis_nama,
            komoditi_jenis_m.komoditi_kode,
            komoditi_m.komoditi_nama,
        ');
        $this->datatables_server_side->join('komoditi_m', 'komoditi_jenis_m.komoditi_kode = komoditi_m.komoditi_kode', 'left');
        $this->datatables_server_side->from('komoditi_jenis_m');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function provinsi_datatable_post() {
        $this->load->library('Datatables');
        $this->datatables->select('provinsi_m.*, wilayah_m.wilayah_nama');
        $this->datatables->join('wilayah_m', 'provinsi_m.wilayah_kode = wilayah_m.wilayah_kode', 'left');
        $this->datatables->from('provinsi_m');
        return print_r($this->datatables->generate());
    }
    
    public function kabupaten_datatable_post() {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_kabupaten.id,
            tbl_kabupaten.provinsiKode,
            tbl_kabupaten.kode,
            tbl_kabupaten.nama,
            tbl_provinsi.nama as provinsiNama,
        ');
        $this->datatables_server_side->join('tbl_provinsi', 'tbl_kabupaten.provinsiKode = tbl_provinsi.kode', 'left');
        $this->datatables_server_side->from('tbl_kabupaten');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function kecamatan_datatable_post() {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            kecamatan_m.kecamatan_id,
            kecamatan_m.kecamatan_kode,
            kecamatan_m.kecamatan_nama,
            kecamatan_m.kecamatan_telepon,
            kecamatan_m.kecamatan_hp,
            kecamatan_m.kecamatan_email,
            kabupaten_m.kabupaten_kode,
            kabupaten_m.kabupaten_nama,
            provinsi_m.provinsi_kode,
            provinsi_m.provinsi_nama
        ');
        $this->datatables_server_side->join('kabupaten_m', 'kecamatan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        $this->datatables_server_side->join('provinsi_m', 'kecamatan_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        $this->datatables_server_side->from('kecamatan_m');
        return print_r($this->datatables_server_side->generate());
    }
    
    public function desa_datatable_post() {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            desa_m.desa_id,
            desa_m.desa_kode,
            desa_m.desa_nama,
            desa_m.desa_telepon,
            desa_m.desa_hp,
            desa_m.desa_email,
            kecamatan_m.kecamatan_kode,
            kecamatan_m.kecamatan_nama,
            kabupaten_m.kabupaten_kode,
            kabupaten_m.kabupaten_nama,
            provinsi_m.provinsi_kode,
            provinsi_m.provinsi_nama,
        ');
        $this->datatables_server_side->join('kecamatan_m','desa_m.kecamatan_kode = kecamatan_m.kecamatan_kode','left');
        $this->datatables_server_side->join('kabupaten_m', 'desa_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        $this->datatables_server_side->join('provinsi_m', 'desa_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        $this->datatables_server_side->from('desa_m');
        return print_r($this->datatables_server_side->generate());
    }


    public function komoditi_select_get($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('komoditi_m.komoditi_kode as id, concat(komoditi_m.komoditi_kode, " ", komoditi_m.komoditi_nama) as text');
            $data = $this->db->where('komoditi_m.komoditi_kode', $id)->get('komoditi_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('komoditi_m.komoditi_kode as id, concat(komoditi_m.komoditi_kode, " ", komoditi_m.komoditi_nama) as text');
            $this->db->limit(50);
            if ($term) {
                $this->db->like('komoditi_m.komoditi_nama', $term);
            }

            $data = $this->db->get('komoditi_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function komoditi_jenis_select_get($id = null)
    {
        $term = $this->input->get('q');
        $komoditi_kode = $this->input->get('komoditi_kode');
        if ($id) {
            $this->db->select('komoditi_jenis_m.komoditi_jenis_kode as id, concat(komoditi_jenis_m.komoditi_jenis_kode, " ", komoditi_jenis_m.komoditi_jenis_nama) as text');
            $data = $this->db->where('komoditi_jenis_m.komoditi_jenis_kode', $id)->get('komoditi_jenis_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('komoditi_jenis_m.komoditi_jenis_kode as id, concat(komoditi_jenis_m.komoditi_jenis_kode, " ", komoditi_jenis_m.komoditi_jenis_nama) as text');
            $this->db->limit(50);
            if ($komoditi_kode) {
                $this->db->like('komoditi_jenis_m.komoditi_kode', $komoditi_kode);
            }
            if ($term) {
                $this->db->like('komoditi_jenis_m.komoditi_jenis_nama', $term);
            }

            $data = $this->db->get('komoditi_jenis_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    public function provinsi_select_get($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('provinsi_m.provinsi_kode as id, concat(provinsi_m.provinsi_nama) as text');
            $data = $this->db->where('provinsi_m.provinsi_kode', $id)->get('provinsi_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('provinsi_m.provinsi_kode as id, concat(provinsi_m.provinsi_nama) as text');
            $this->db->limit(50);
            if ($term) {
                $this->db->like('provinsi_m.provinsi_nama', $term);
            }

            $data = $this->db->get('provinsi_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function kabupaten_select_get($id = null)
    {
        $term = $this->input->get('q');
        $provinsi_kode = $this->input->get('provinsi_kode');
        if ($id) {
            $this->db->select('kabupaten_m.kabupaten_kode as id, concat(kabupaten_m.kabupaten_nama) as text');
            $data = $this->db->where('kabupaten_m.kabupaten_kode', $id)->get('kabupaten_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('kabupaten_m.kabupaten_kode as id, concat(kabupaten_m.kabupaten_nama) as text');
            $this->db->limit(50);
            if ($provinsi_kode) {
                $this->db->like('kabupaten_m.provinsi_kode', $provinsi_kode);
            }
            if ($term) {
                $this->db->like('kabupaten_m.kabupaten_nama', $term);
            }

            $data = $this->db->get('kabupaten_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function kecamatan_select_get($id = null)
    {
        $term = $this->input->get('q');
        $kabupaten_kode = $this->input->get('kabupaten_kode');
        if ($id) {
            $this->db->select('kecamatan_m.kecamatan_kode as id, concat(kecamatan_m.kecamatan_nama) as text');
            $data = $this->db->where('kecamatan_m.kecamatan_kode', $id)->get('kecamatan_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('kecamatan_m.kecamatan_kode as id, concat(kecamatan_m.kecamatan_nama) as text');
            $this->db->limit(50);
            if ($kabupaten_kode) {
                $this->db->like('kecamatan_m.kabupaten_kode', $kabupaten_kode);
            }
            if ($term) {
                $this->db->like('kecamatan_m.kecamatan_nama', $term);
            }

            $data = $this->db->get('kecamatan_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
    
    public function desa_select_get($id = null)
    {
        $term = $this->input->get('q');
        $kecamatan_kode = $this->input->get('kecamatan_kode');
        if ($id) {
            $this->db->select('desa_m.desa_kode as id, concat(desa_m.desa_nama) as text');
            $data = $this->db->where('desa_m.desa_kode', $id)->get('desa_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('desa_m.desa_kode as id, concat(desa_m.desa_nama) as text');
            $this->db->limit(50);
            if ($kecamatan_kode) {
                $this->db->like('desa_m.kecamatan_kode', $kecamatan_kode);
            }
            if ($term) {
                $this->db->like('desa_m.desa_nama', $term);
            }

            $data = $this->db->get('desa_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}