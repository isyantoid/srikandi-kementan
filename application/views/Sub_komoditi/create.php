<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-3 col-form-label">Kelompok Komoditas</label>
                    <div class="col-8">
                        <select class="form-control kelompok_komoditi_id" name="kelompok_komoditi_id" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Jenis Komoditas</label>
                    <div class="col-8">
                        <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Sub Komoditas Kode</label>
                    <div class="col-8">
                        <input class="form-control sub_komoditi_kode" type="text" value="" name="sub_komoditi_kode" required placeholder="Sub Komoditas Kode">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Sub Komoditas Nama</label>
                    <div class="col-8">
                        <input class="form-control sub_komoditi_nama" type="text" value="" name="sub_komoditi_nama" required placeholder="Sub Komoditas Nama">
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-3"> </div>
                        <div class="col-8">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>

    $('.jenis_komoditi_id').select2({placeholder: 'Pilih Jenis Komoditas'});
    
    ajaxSelect({
        placeholder: 'Pilih Kelompok Komoditas',
        id: '.kelompok_komoditi_id',
        url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
    });

    $('.kelompok_komoditi_id').change(function(){
        var val = $(this).val();
        ajaxSelect({
            placeholder: 'Pilih Jenis Komoditas',
            id: '.jenis_komoditi_id',
            url: '{site_url}ajax_selectbox/jenis_komoditi_id',
            optionalSearch: {
                kelompok_komoditi_id: val
            }
        });
    });
    

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>