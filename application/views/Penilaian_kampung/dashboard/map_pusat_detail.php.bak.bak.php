<div class="kt-portlet" data-ktportlet="true" id="reload">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title"></h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <a href="javascript:(0)" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md" onclick="filter()"><i class="la la-undo"></i></a>
                <a href="#" data-ktportlet-tool="reload" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md reload"><i class="la la-refresh"></i></a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-8">
                <div id="kt_gmap_1" style="height:580px;"></div>
            </div>
            <div class="col-md-4">
                <table class="table table-borderless">
                    <tr>
                        <td>Provinsi</td>
                        <td><?php echo $provinsi_nama ?></td>
                    </tr>
                    <tr>
                        <td>Kabupaten</td>
                        <td><?php echo $kabupaten_nama ?></td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td><?php echo $kecamatan_nama ?></td>
                    </tr>
                    <tr>
                        <td>Desa</td>
                        <td><?php echo $desa_nama ?></td>
                    </tr>
                    <tr>
                        <td>Total Luas</td>
                        <td><?php echo $luas_kebun ?>Ha</td>
                    </tr>
                    <tr>
                        <td>Total kelompok tani</td>
                        <td>{total_kelompok_tani}</td>
                    </tr>
                    <tr>
                        <td>Total Anggota Tani</td>
                        <td><?php echo $total_anggota_tani ?></td>
                    </tr>
                </table>
                <hr>
                <p>Penerima Manfaat APBN Tahun :</p>
                <div class="row">
                    <?php for ($i = 1; $i <= 4; $i++) : ?>
                        <div class="col-md-3">
                            <a href="#" data-toggle="modal" data-target="#modal_image_bantuan_<?php echo $i ?>">
                                <img src="{dist_path}images/bantuan.jpeg" alt="" width="100">
                            </a>
                        </div>
                        <div class="modal fade" id="modal_image_bantuan_<?php echo $i ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog text-center" role="document">
                                <img src="{dist_path}images/bantuan.jpeg" width="600px">
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
                <hr>
                <p>Foto Kampung Durian Sukowangi :</p>
                <div class="row">
                    <?php for ($i = 1; $i <= 4; $i++) : ?>
                        <div class="col-md-3">
                            <a href="#" data-toggle="modal" data-target="#modal_image_foto_kampung_<?php echo $i ?>">
                                <img src="{dist_path}images/kampung.jpg" alt="" width="100">
                            </a>
                        </div>
                        <div class="modal fade" id="modal_image_foto_kampung_<?php echo $i ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog text-center" role="document">
                                <img src="{dist_path}images/kampung.jpg" width="600px">
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCu67fpz02di0x7OTI-ICN4tLDWxCoJzIk&libraries=places&callback=initMap"></script>
<script>
    var reload = new KTPortlet('reload');
    reload.on('reload', function(portlet) {
        map_pusat_detail('{id}');
    });

    var map;
    var lat = parseFloat('{lat}');
    var lng = parseFloat('{lng}');

    var arrayPolygon = '<?php echo json_encode($array_polygon) ?>';

    console.log(JSON.parse(arrayPolygon).length);

    function initMap() {
        map = new google.maps.Map(document.getElementById('kt_gmap_1'), {
            center: new google.maps.LatLng(lat, lng),
            zoom: 9,
        });


        var points = [];
        for (var i = 0; i < arrayPolygon.length; i++) {
            console.log(arrayPolygon[0])
            points.push({
                lat: arrayPolygon[i][0],
                lng: arrayPolygon[i][1]
            });
        }
        // Construct the polygon.
        var bermudaTriangle = new google.maps.Polygon({
            paths: points,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        bermudaTriangle.setMap(map);
    }

    // function initMap() {
    //     const map = new google.maps.Map(document.getElementById("kt_gmap_1"), {
    //         zoom: 5.25,
    //         center: {
    //             lat: -2.0423904,
    //             lng: 122.8983498
    //         },
    //     });

    //     const triangleCoords = [{
    //             lat: -6.378124038876082,
    //             lng: 107.18216340154149
    //         },
    //         {
    //             lat: -6.918288391034365,
    //             lng: 106.89377231168342
    //         },
    //         {
    //             lat: -7.357060135211541,
    //             lng: 108.26431663396131
    //         },
    //         {
    //             lat: -6.353557215154645,
    //             lng: 108.53897481477854
    //         },
    //     ];


    //     const bermudaTriangle = new google.maps.Polygon({
    //         paths: triangleCoords,
    //         strokeColor: "#FF0000",
    //         strokeOpacity: 0.8,
    //         strokeWeight: 2,
    //         fillColor: "#FF0000",
    //         fillOpacity: 0.35,
    //     });

    //     bermudaTriangle.setMap(map);
    // }

    // var map = new GMaps({
    //     div: '#kt_gmap_1',
    //     lat: -6.8678439,
    //     lng: 107.605137,
    //     zoom: 8.5,
    // });

    // var path = [
    //     [-6.378124038876082, 107.18216340154149],
    //     [-6.918288391034365, 106.89377231168342],
    //     [-7.357060135211541, 108.26431663396131],
    //     [-6.749210167595442, 108.53897481477854],
    //     [-6.353557215154645, 108.31650168831659],
    // ];

    // var polygon = map.drawPolygon({
    //     paths: path,
    //     strokeColor: '#000',
    //     strokeOpacity: 0.8,
    //     strokeWeight: 1,
    //     fillColor: '#f44336',
    //     fillOpacity: 0.6
    // });


    // var content = `
    //     <table class="table table-borderless">
    //         <tr>
    //             <td>Kampung</td>
    //             <td>Durian Sukowangi</td>
    //         </tr>
    //         <tr>
    //             <td>Provinsi</td>
    //             <td>Jawa Barat</td>
    //         </tr>
    //         <tr>
    //             <td>Kabupaten</td>
    //             <td>Kunignan</td>
    //         </tr>
    //         <tr>
    //             <td>Kecamatan</td>
    //             <td>Kadugede</td>
    //         </tr>
    //         <tr>
    //             <td>Desa</td>
    //             <td>Babatan</td>
    //         </tr>
    //         <tr>
    //             <td>Total Luas</td>
    //             <td>1 Hektar</td>
    //         </tr>
    //         <tr>
    //             <td>Total kelompok tani</td>
    //             <td>23</td>
    //         </tr>
    //         <tr>
    //             <td>Total Anggota Tani</td>
    //             <td>45</td>
    //         </tr>
    //     </table>
    //     <div class="mt-3">
    //         <a href="javascript:map_pusat_detail()" class="btn btn-link">Lihat Detail</a>
    //     </div>
    // `;

    // map.addMarker({
    //     lat: -6.378124038876082,
    //     lng: 107.18216340154149,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.918288391034365,
    //     lng: 106.89377231168342,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -7.357060135211541,
    //     lng: 108.26431663396131,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.749210167595442,
    //     lng: 108.53897481477854,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.353557215154645,
    //     lng: 108.31650168831659,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.5637016707619615,
    //     lng: 107.75070583583313,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
</script>