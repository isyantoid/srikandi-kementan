function blockPage() {
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'success',
        size: 'lg'
    });
}

function unblockPage() {
    KTApp.unblockPage();
}

function redirect(url) {
    window.location.href = url;
}

function btnSpinnerShow(buttonID = 'button[type="submit"]') {
    $(buttonID).addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
    $(buttonID).prop('disabled', true);
    setTimeout(() => { }, 1000);
}

function btnSpinnerHide(buttonID = 'button[type="submit"]') {
    console.log('wait ... !');
    $(buttonID).removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
    $(buttonID).prop('disabled', false);
}

function templateResult(item, container) {
    // replace the placeholder with the break-tag and put it into an jquery object
    return $('<span>' + item.text
        .replace('<strong>', '<strong>')
        .replace('</strong>', '</strong>')
        .replace('[br]', '<br/>')
        .replace('[br]', '<br/>') + '</span>');
}
function templateSelection(item, container) {
    // replace your placeholder with nothing, so your select shows the whole option text
    // console.log(item.text.match(/<strong>(.*?)<\/strong>/g))
    // var nr = /<strong>(.*?)<\/strong>/g.exec(item.text);
    return item.text
        .replace('<strong>', '')
        .replace('</strong>', '')
        .replace('<br/>', ', ')
        .replace('<br/>', ', ')
        .replace('<br/>', ', ')
        .replace('#', '');
}

$(document).on('change', '.custom-file', function () {
    var file = $('input[type=file]', this)[0].files[0].name;
    $('.custom-file-label',this).text(file);
});

function ajaxSelect(data) {
    if (data) {
        $(data.id).select2({
            multiple: data.multiple ? data.multiple : false,
            templateResult: data.templateResult ? data.templateResult : templateResult,
            templateSelection: data.templateSelection ? data.templateSelection : templateSelection,
            minimumInputLength: data.minimumInputLength ? data.minimumInputLength : null,
            allowClear: data.allowClear ? data.allowClear : null,
            readonly: data.readonly ? data.readonly : null,
            disabled: data.disabled ? data.disabled : null,
            placeholder: data.placeholder ? data.placeholder : 'Pilih Opsi',
            ajax: {
                url: data.url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        q: params.term,
                        type: 'public',
                    }

                    let mergedParams = { ...data.optionalSearch, ...query };
                    return mergedParams;
                },
                processResults: function (data) {
                    return {
                        results: data
                    }
                },
                cache: false,
            },
        })

        var selected = data.selected
        if (selected) {
            var dataoption = $(data.id)
            $.ajax({
                url: data.url + '/' + selected,
                dataType: 'json',
                success: function (data) {
                    var option = new Option(data.text, data.id, true, true);
                    dataoption.append(option).trigger('change');
                    dataoption.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    })
                }
            })
        }
    }
}


function addScore(score, $domElement) {
    $("<span class='stars-container'>")
        .addClass("stars-" + score.toString())
        .text("★★★★★")
        .appendTo($domElement);
}

function descriptionScore(score) {
    if (score > 80) {
        return 'Sangat Baik';
    } else if (score <= 80 && score > 60) {
        return 'Baik';
    } else if (score <= 60 && score > 40) {
        return 'Sedang';
    } else if (score <= 40 && score > 20) {
        return 'Cukup';
    } else if (score <= 20) {
        return 'Kurang';
    }
}