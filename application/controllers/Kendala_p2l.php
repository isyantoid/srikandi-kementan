<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kendala_p2l extends CI_Controller
{

    private $title = 'Permasalahan / Kendala';
    private $view = 'Kendala_p2l';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_kendala_p2l.id,
            tbl_kendala_p2l.tanggal_kejadian,
            tbl_kendala_p2l.komponen,
            tbl_kendala_p2l.uraian,
            tbl_kendala_p2l.uraian_foto,
            tbl_kendala_p2l.upaya_tindak_lanjut,
            tbl_kendala_p2l.upaya_foto,
            tbl_registrasi_p2l.nomor_registrasi_p2l,
            tbl_registrasi_p2l.tanggal_registrasi_p2l,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->join('tbl_registrasi_p2l', 'tbl_kendala_p2l.registrasi_p2l_id = tbl_registrasi_p2l.id');
        $this->datatables_server_side->from('tbl_kendala_p2l');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['provinsi_kode'] = $this->session->userdata('provinsi_kode');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    

    public function detail_registrasi($id) {
        if($id) {
            $registrasi = $this->db
            ->select('tbl_registrasi_p2l.*')
            ->where('tbl_registrasi_p2l.id', $id)
            ->get('tbl_registrasi_p2l')->row_array();
            if($registrasi) {
                return $this->output->set_content_type('application/json')->set_output(json_encode($registrasi));
            } else {
                return jsonOutputError('not found');
            }
        }
    }

    public function save($id = null)
    {

        $this->db->set('registrasi_p2l_id', $this->input->post('registrasi_p2l_id'));
        $this->db->set('nama_kelompok_tani', $this->input->post('nama_kelompok_tani'));
        $this->db->set('komponen', $this->input->post('komponen'));
        $this->db->set('tanggal_kejadian', $this->input->post('tanggal_kejadian'));
        $this->db->set('uraian', $this->input->post('uraian'));
        if(!empty($_FILES['uraian_foto'])) {
            $upload = $this->_upload_files('uraian_foto');
            if(!$upload['status']) {
                return jsonOutputError($upload['data']);
            } else {
                $this->db->set('uraian_foto', $upload['data']['file_name']);
            }
        }
        $this->db->set('upaya_tindak_lanjut', $this->input->post('upaya_tindak_lanjut'));
        if(!empty($_FILES['upaya_foto'])) {
            $upload = $this->_upload_files('upaya_foto');
            if(!$upload['status']) {
                return jsonOutputError($upload['data']);
            } else {
                $this->db->set('upaya_foto', $upload['data']['file_name']);
            }
        }

        if($id) {

        } else {
            
        }
        
        $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
        $insert = $this->db->insert('tbl_kendala_p2l');
        if($insert) {
            return jsonOutputSuccess();
        } else {
            echo 'error';
        }

        
    }

    private function _upload_files($files) {
        
        $config = array(
            'upload_path'   => './uploads/kendala_p2l/',
            'allowed_types' => 'jpg|jpg|png',
            'overwrite'     => true,
            'max_size'      => 1024 * 2,
            'encrypt_name'     => true,
        );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($files)) {
            $data['status'] = true;
            $data['data'] = $this->upload->data();
            return $data;
        } else {
            $data['status'] = false;
            $data['data'] = $this->upload->display_errors("","");
            return $data;
        }
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = $this->db
            ->select('tbl_kendala_p2l.*')
            ->where('tbl_kendala_p2l.id', $id)
            ->get('tbl_kendala_p2l')->row_array();
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}