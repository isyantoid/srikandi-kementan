<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <style>
        <?= $css ?>
    </style>
    <link rel="stylesheet" href="<?= base_url('dist/assets/css/pdf_style.css') ?>">
</head>
<body>
    <h1><?= $title ?></h1>
    <img src="<?= $capture_map ?>" width="100%"/>
    <br><br>
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th class="text-left">Provinsi</th>
                <th class="text-left">Kabupaten</th>
                <th>Alokasi (Kel)</th>
            </tr>
        </thead>
        <tbody>
            <?php if($get_laporan): ?>
            <?php $no=1; foreach($get_laporan as $row): ?>
            <tr>
                <td class="text-center"><?= $no ?></td>
                <td class="text-left"><?= $row['provinsi_nama'] ?></td>
                <td class="text-left"><?= $row['kabupaten_nama'] ?></td>
                <td class="text-center"><?= $row['alokasi'] ?></td>
            </tr>
            <?php $no++; endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</body>
</html>