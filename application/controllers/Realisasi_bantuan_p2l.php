<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH . '/libraries/PHPExcel.php');

class Realisasi_bantuan_p2l extends CI_Controller
{

    private $title = 'Realisasi Bantuan P2L';
    private $view = 'Realisasi_bantuan_p2l';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_bantuan_p2l.id,
            tbl_registrasi_p2l.nomor_registrasi_p2l,
            tbl_registrasi_p2l.tanggal_registrasi_p2l,
            tbl_registrasi_p2l.jumlah_penerima_manfaat,
            tbl_registrasi_p2l.terdaftar_simluhtan,
            tbl_bantuan_p2l.tanggal_penerima_bantuan,
            tbl_bantuan_p2l.realisasi_pemanfaatan_anggaran,
            tbl_kelompok_tani.nama_kelompok,
            tbl_registrasi_p2l.nama_ketua,
            tbl_provinsi.nama as provinsi_nama,
            tbl_kabupaten.nama as kabupaten_nama,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->join('tbl_registrasi_p2l', 'tbl_bantuan_p2l.registrasi_p2l_id = tbl_registrasi_p2l.id', 'left');
        $this->datatables_server_side->join('tbl_kelompok_tani', 'tbl_registrasi_p2l.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->datatables_server_side->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->datatables_server_side->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->datatables_server_side->from('tbl_bantuan_p2l');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        $data['provinsi_kode'] = $this->session->userdata('provinsi_kode');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    

    public function get_nama_ketua_kelompok_tani($id) {
        if($id) {
            $this->db->where('id', $id);
            $kelompok_tani = $this->db->get('tbl_kelompok_tani')->row_array();
            if($kelompok_tani) {
                return $this->output->set_content_type('application/json')->set_output(json_encode($kelompok_tani));
            } else {
                return jsonOutputError('not found');
            }
        }
    }

    public function save($id = null)
    {
        return $this->model->save($id);
    }

    public function generate_form()
    {
        $data['btnDelete'] = $this->input->get('btnDelete');
        $data['tableID'] = $this->input->get('tableID');
        $data['uniqid'] = uniqid();
        $data = array_merge($data, path_info());
        $this->parser->parse($this->view . '/form_realisasi', $data);
    }

    public function update($id = null)
    {
        if ($id) {
            $data = $this->model->detail_registrasi($id);
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function detail($id = null)
    {
        if ($id) {
            $data = $this->model->detail_registrasi($id);
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Detail Registrasi P2L';
                $data['content'] = $this->view . '/detail';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        $this->model->delete();
    }
}
