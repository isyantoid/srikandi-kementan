<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once(APPPATH . '/libraries/PHPExcel.php');

class Reg_kampung extends CI_Controller
{

    private $title = 'Registrasi Kampung';
    private $view = 'Reg_kampung';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_permohonan.id,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_permohonan.status_reg_horti,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.sub_komoditi_nama,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->where('tbl_permohonan.app_id', '2');
        $this->datatables_server_side->from('tbl_permohonan');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['provinsi_kode'] = $this->session->userdata('provinsi_kode');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function form_kelompok()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['nomor'] = $this->input->post('nomor');
        $data = array_merge($data, path_info());
        $this->parser->parse($this->view . '/form_kelompok', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('tbl_permohonan', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function detail($id = null)
    {
        if ($id) {
            $data = $this->model->detail_permohonan($id);
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Detail Registrasi';
                $data['content'] = $this->view . '/detail';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $id = $this->uri->segment(3);

        // $nomor_registrasi_kampung = 'STO.01.73.03.III.016.22.0056';
        $nomor_registrasi_kampung = $this->input->post('nomor_registrasi');
        $cekProvKode = getRowArray('tbl_provinsi', ['kode' => substr($nomor_registrasi_kampung, 7, 2)]);
        $cekKabKode = getRowArray('tbl_kabupaten', ['kode' => substr($nomor_registrasi_kampung, 7, 5)]);
        $kelompokKomoditi = substr($nomor_registrasi_kampung, 13, strpos(substr($nomor_registrasi_kampung, 13), '.'));
        $cekKelompokKomoditi = getRowArray('tbl_kelompok_komoditi', ['romawi' => $kelompokKomoditi]);
        $jenisKomoditi = substr($nomor_registrasi_kampung, strpos($nomor_registrasi_kampung, '.', 13)+1, 3);
        $cekJenisKomoditi = getRowArray('tbl_jenis_komoditi', ['jenis_komoditi_kode' => $jenisKomoditi]);

        $tahun = substr($nomor_registrasi_kampung, -7, 2);
        $nourut = substr($nomor_registrasi_kampung, -4);

        if($id) {
            $permohonan = getRowArray('tbl_permohonan', ['id' => $id]);
            if($permohonan['nomor_registrasi'] != $nomor_registrasi_kampung) {
                $check = isDuplicate('tbl_permohonan', 'nomor_registrasi', $nomor_registrasi_kampung);
                if($check) {
                    return jsonOutputError('nomor registrasi kampung sudah ada sebelumnya!');
                }
            }
        } else {
            $checkDuplicate = isDuplicate('tbl_permohonan', 'nomor_registrasi', $nomor_registrasi_kampung);
            if($checkDuplicate) {
                return jsonOutputError('nomor registrasi kampung sudah ada sebelumnya!');
            }
        }
        

        if(strlen($nomor_registrasi_kampung) < 26 || strlen($nomor_registrasi_kampung) > 28 ) {
            jsonOutputError('panjang karakter nomor registrasi kampung setidaknya memiliki 26-28 karakter');
        } elseif(substr($nomor_registrasi_kampung, 0, 6) != 'STO.01') {            
            jsonOutputError('karakter awal nomor registrasi kampung salah! harus diawali STO.01');
        } elseif(substr($nomor_registrasi_kampung, 6, 1) != '.') {
            jsonOutputError('karakter pemisah nomor registrasi kampung hanya diizinkan titik');
        } elseif(!$cekProvKode) {
            jsonOutputError('karakter urutan provinsi nomor registrasi kampung salah');
        } elseif(!$cekKabKode) {
            jsonOutputError('karakter urutan kabupaten nomor registrasi kampung salah');
        } elseif(substr($nomor_registrasi_kampung, 12, 1) != '.') {
            jsonOutputError('karakter pemisah nomor registrasi kampung hanya diizinkan titik');
        } elseif(!$cekKelompokKomoditi) {
            jsonOutputError('karakter kelompok komoditi nomor registrasi kampung salah');
        } elseif(substr($nomor_registrasi_kampung, strpos($nomor_registrasi_kampung, '.', 13), 1) != '.') {
            jsonOutputError('karakter pemisah nomor registrasi kampung hanya diizinkan titik');
        } elseif(!$cekJenisKomoditi) {
            jsonOutputError('karakter jenis komoditi nomor registrasi kampung salah');
        } elseif(substr($nomor_registrasi_kampung, -8, 1) != '.') {
            jsonOutputError('karakter pemisah nomor registrasi kampung hanya diizinkan titik');
        } elseif(!is_numeric($tahun)) {
            jsonOutputError('karakter tahun nomor registrasi kampung salah');
        } elseif(substr($nomor_registrasi_kampung, -5, 1) != '.') {
            jsonOutputError('karakter pemisah nomor registrasi kampung hanya diizinkan titik');
        } elseif(!is_numeric($nourut)) {
            jsonOutputError('karakter nomor urut registrasi kampung salah');
        } else {
            // $nomor_reg_horti = 'RK.01.73.03.III.016.22.0056';
            $nomor_reg_horti = $this->input->post('no_reg_horti');
            $cekProvKode = getRowArray('tbl_provinsi', ['kode' => substr($nomor_reg_horti, 6, 2)]);
            $cekKabKode = getRowArray('tbl_kabupaten', ['kode' => substr($nomor_reg_horti, 6, 5)]);
            $kelompokKomoditi = substr($nomor_reg_horti, 12, strpos(substr($nomor_reg_horti, 12), '.'));
            $cekKelompokKomoditi = getRowArray('tbl_kelompok_komoditi', ['romawi' => $kelompokKomoditi]);
            $jenisKomoditi = substr($nomor_reg_horti, strpos($nomor_reg_horti, '.', 12)+1, 3);
            $cekJenisKomoditi = getRowArray('tbl_jenis_komoditi', ['jenis_komoditi_kode' => $jenisKomoditi]);
            $tahun = substr($nomor_reg_horti, -7, 2);
            $nourut = substr($nomor_reg_horti, -4);


            if($id) {
                $permohonan = getRowArray('tbl_permohonan', ['id' => $id]);
                if($permohonan['nomor_registrasi'] != $nomor_reg_horti) {
                    $check = isDuplicate('tbl_permohonan', 'nomor_registrasi', $nomor_reg_horti);
                    if($check) {
                        return jsonOutputError('nomor registrasi horti sudah ada sebelumnya!');
                    }
                }
            } else {
                $checkDuplicate = isDuplicate('tbl_permohonan', 'nomor_registrasi', $nomor_reg_horti);
                if($checkDuplicate) {
                    return jsonOutputError('nomor registrasi horti sudah ada sebelumnya!');
                }
            }
    
            if(strlen($nomor_reg_horti) < 25 || strlen($nomor_reg_horti) > 27 ) {
                jsonOutputError('panjang karakter nomor registrasi horti setidaknya memiliki 26-27 karakter');
            } elseif(substr($nomor_reg_horti, 0, 5) != 'RK.01') {            
                jsonOutputError('karakter awal nomor registrasi horti salah! harus diawali RK.01');
            } elseif(substr($nomor_reg_horti, 5, 1) != '.') {
                jsonOutputError('karakter pemisah nomor registrasi horti hanya diizinkan titik');
            } elseif(!$cekProvKode) {
                jsonOutputError('karakter urutan provinsi nomor registrasi horti salah');
            } elseif(!$cekKabKode) {
                jsonOutputError('karakter urutan kabupaten nomor registrasi horti salah');
            } elseif(substr($nomor_reg_horti, 11, 1) != '.') {
                jsonOutputError('karakter pemisah nomor registrasi horti hanya diizinkan titik');
            } elseif(!$cekKelompokKomoditi) {
                jsonOutputError('karakter kelompok komoditi nomor registrasi horti salah');
            } elseif(substr($nomor_reg_horti, strpos($nomor_reg_horti, '.', 12), 1) != '.') {
                jsonOutputError('karakter pemisah nomor registrasi horti hanya diizinkan titik');
            } elseif(!$cekJenisKomoditi) {
                jsonOutputError('karakter jenis komoditi nomor registrasi horti salah');
            } elseif(substr($nomor_reg_horti, -8, 1) != '.') {
                jsonOutputError('karakter pemisah nomor registrasi horti hanya diizinkan titik');
            } elseif(!is_numeric($tahun)) {
                jsonOutputError('karakter tahun nomor registrasi horti salah');
            } elseif(substr($nomor_reg_horti, -5, 1) != '.') {
                jsonOutputError('karakter pemisah nomor registrasi horti hanya diizinkan titik');
            } elseif(!is_numeric($nourut)) {
                jsonOutputError('karakter nomor urut registrasi horti salah');
            } else {
                $this->model->save();
            }
        }
    }

    function validate_nomor_registrasi() {
        
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }

    public function get_nomor_urut_registrasi()
    {
        if ($this->input->is_ajax_request()) {
            $search = $this->input->post('search');
            $this->db->like('nomor_registrasi', $search);
            $count = $this->db->get('tbl_permohonan')->num_rows();
            $data['nomor_urut'] = str_pad($count + 1, 4, "0", STR_PAD_LEFT);
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }


    public function test_import_excel()
    {
        $this->load->library('excel');
        $file_tmp = './uploads/document_registrasi_kampung/import_registrasi_kampung.xls';
        $object = PHPExcel_IOFactory::load($file_tmp);
        foreach ($object->getWorksheetIterator() as $worksheet) {
            $highestRow = $worksheet->getHighestRow();
            for ($row = 2; $row <= $highestRow; $row++) {
                $data['nomor_registrasi'] = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                $tanggal_permohonan = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $conv_tanggal_permohonan = ($tanggal_permohonan) ? gmdate("Y-m-d", ($tanggal_permohonan - 25569) * 86400) : '';
                $data['tanggal_permohonan'] = $conv_tanggal_permohonan;
            }
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function export_data()
    {
        $fileName = 'registrasi_kampung_' . date('d_m_Y') . '.xlsx';
        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $styleForTitle = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 16,
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForHeader = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 10,
            ],
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'DDDDDD'],
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForBody = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
                'name' => 'Cambria',
            ],
            'borders' => [
                'allBorders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => ['rgb' => '444444']
                ]
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->mergeCells('A1:P1')->getStyle('A1')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A2:P2')->getStyle('A2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A3:P3')->getStyle('A1:A3')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'LAPORAN REGISTRASI KAMPUNG');

        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Nomor Registrasi STO');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Tanggal Registrasi STO');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Tanggal Registrasi Horti');
        $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Tanggal Pengesahan Horti');
        $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Kelompok Tani');
        $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Jenis Komoditi');
        $objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Kelompok Komoditi');
        $objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Luas');
        $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Satuan Luas');
        $objPHPExcel->getActiveSheet()->SetCellValue('J4', 'Terdaftar Simluhtan');
        $objPHPExcel->getActiveSheet()->SetCellValue('K4', 'Provinsi');
        $objPHPExcel->getActiveSheet()->SetCellValue('L4', 'Kabupaten');
        $objPHPExcel->getActiveSheet()->SetCellValue('M4', 'Kecamatan');
        $objPHPExcel->getActiveSheet()->SetCellValue('N4', 'Desa');
        $objPHPExcel->getActiveSheet()->SetCellValue('O4', 'Garis Lintang');
        $objPHPExcel->getActiveSheet()->SetCellValue('P4', 'Garis Bujur');
        foreach (range('A', 'P') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A4:P4')->applyFromArray($styleForHeader);

        $regKampung = $this->model->getRegistrasiKampung();
        if($regKampung) {
            $rowCount = 5;
            foreach($regKampung as $row) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $row['nomor_registrasi'])->getStyle('A' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['tanggal_permohonan'])->getStyle('B' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['no_reg_horti'])->getStyle('C' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['tanggal_reg_horti'])->getStyle('D' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['nama_kelompok'])->getStyle('E' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['jenis_komoditi_nama'])->getStyle('F' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['kelompok_komoditi_nama'])->getStyle('G' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['luas_kebun'])->getStyle('H' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['satuan_luas_kebun'])->getStyle('I' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, ($row['terdaftar_simluhtan'] == '1') ? 'Ya' : 'Tidak')->getStyle('J' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row['provinsi_nama'])->getStyle('K' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $row['kabupaten_nama'])->getStyle('L' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $row['kecamatan_nama'])->getStyle('M' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $row['desa_nama'])->getStyle('N' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $row['lat'])->getStyle('O' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $row['lng'])->getStyle('P' . $rowCount)->applyFromArray($styleForBody);
                $rowCount++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }


    public function direction($id = null)
    {
        if ($id) {
            $data = $this->model->detail_permohonan($id);
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Arah Menuju Kampung';
                $data['content'] = $this->view . '/direction';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }
}
