<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Dokumentasi kendala tanam:</label>
                            <div class="dropzone dropzone-default dropzone-brand" id="dropzone">
                                <div class="dropzone-msg dz-message needsclick">
                                    <h3 class="dropzone-msg-title">Drop files here or click to upload.</h3>
                                    <span class="dropzone-msg-desc">Upload up to 10 files</span>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="form-group row" hidden>
                    <div class="col-md-6">
                        <label>Dokumentasi kendala tanam:</label>
                        <textarea name="file_txt" id="file_txt" class="form-control file_txt"></textarea>
                    </div>
                </div>



                


                <div class="modal-footer">
                    <div class="text-left">
                        <a href="{segment1}" class="btn btn-secondary">Close</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script>
let dropzone = new Dropzone('#dropzone', {
    url: "{site_url}kendala_tanam/do_upload_document",
    acceptedFiles: 'image/jpeg,image/png,application/pdf',
    paramName: "file",
    maxFiles: 10,
    addRemoveLinks: true,
})

dropzone.on('addedfile', function(file) {
    console.log(file)
})
</script>