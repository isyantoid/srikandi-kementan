<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body" style="padding-bottom:0px !important">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input class="form-control tanggal" type="text" value="<?= date('Y-m-d') ?>" readonly name="tanggal" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>Tahun</label>
                            <input class="form-control tahun" type="text" value="<?= date('Y') ?>" readonly name="tahun" required>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label>Bulan</label>
                            <input class="form-control bulan" type="text" value="<?= date('m') ?>" readonly name="bulan" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Komoditi</label>
                            <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Pasar</label>
                            <select class="form-control champ_pasar_id" name="champ_pasar_id" style="width:100%;"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Nama Toko / Bandar</label>
                            <input class="form-control nama_toko" type="text" value="" name="nama_toko" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Telepon Toko / Bandar</label>
                            <input class="form-control telepon_toko" type="text" value="" name="telepon_toko" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Jumlah (Ton)</label>
                            <input class="form-control jumlah" type="number" value="0" name="jumlah" required step="any">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Jumlah Rupiah/Kg</label>
                            <input class="form-control harga" type="number" value="0" name="harga" required step="any">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Total Rupiah</label>
                            <input class="form-control harga_total" type="number" value="0" name="harga_total" required step="any">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Foto Pengiriman (Surat Jalan)</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="foto" id="customFile">
                                <label class="custom-file-label text-left" for="customFile">Foto Pengiriman</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Foto Loading Barang (Kwitansi)</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="foto_kwitansi" id="customFile">
                                <label class="custom-file-label text-left" for="customFile">Foto Kwitansi</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Foto Serah Terima</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="foto_serah_terima" id="customFile">
                                <label class="custom-file-label text-left" for="customFile">Foto Serah Terima</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Foto Dokumen</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="foto_dokumen" id="customFile">
                                <label class="custom-file-label text-left" for="customFile">Foto Dokumen</label>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>
$(".tanggal").datepicker({
    format: "yyyy-mm-dd",
}).on('changeDate', function(e) {
    var selectedDate = new Date(e.date);
    var month = (selectedDate.getMonth() + 1).toString().padStart(2, '0');
    $('.tahun').val(selectedDate.getFullYear());
    $('.bulan').val(month);
});


$(document).on('keyup', '.jumlah, .harga', function(){
    var jumlah = parseFloat($('.jumlah').val()) || 0;
    var harga = parseFloat($('.harga').val()) || 0;

    var totalHarga = (jumlah * harga) * 1000 ;

    $('.harga_total').val(totalHarga);
});

ajaxSelect({
    id: '.champ_pasar_id',
    url: '{site_url}ajax_selectbox/champ_pasar_id',
});

ajaxSelect({
    id: '.jenis_komoditi_id',
    url: '{site_url}ajax_selectbox/cabe_id',
    selected: '{jenis_komoditi_id}',
});

function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>