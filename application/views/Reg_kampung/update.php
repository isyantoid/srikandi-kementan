<style>
    .pac-container {
        z-index: 1050;
        position: fixed !important;
        top: 15% !important;
    }
</style>

<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="alert alert-secondary" role="alert">
                    <div class="alert-icon"><i class="flaticon-info kt-font-brand"></i></div>
                    <div class="alert-text font-weight-bold">A. Data Pemohon</div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Nomor Registrasi STO:</label>
                        <input type="text" class="form-control nomor_registrasi" name="nomor_registrasi" value="{nomor_registrasi}" required placeholder="Masukan Nomor Registrasi">
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal registrasi STO:</label>
                        <input type="text" class="form-control tanggal_permohonan" name="tanggal_permohonan" value="{tanggal_permohonan}" required placeholder="Masukan Tanggal registrasi">
                    </div>
                    <div class="col-md-3">
                        <label>Nomor Registrasi HORTI:</label>
                        <input type="text" class="form-control no_reg_horti" name="no_reg_horti" value="{no_reg_horti}" placeholder="Masukan Nomor Registrasi Horti">
                    </div>
                    <div class="col-md-3">
                        <label>Tanggal Pengesahan Horti:</label>
                        <input type="text" class="form-control tanggal_reg_horti" name="tanggal_reg_horti" value="{tanggal_reg_horti}" placeholder="Tanggal Pengesahan Horti">
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Kelompok Komoditas:</label>
                        <select class="form-control kelompok_komoditi_id" name="kelompok_komoditi_id" style="width:100%;"></select>
                    </div>
                    <div class="col-md-4">
                        <label>Jenis Komoditas:</label>
                        <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                    </div>
                    <div class="col-md-4">
                        <label>Sub Komoditas:</label>
                        <select class="form-control sub_komoditi_id" name="sub_komoditi_id" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Tanggal tanam:</label>
                        <input type="text" class="form-control tanggal_tanam" name="tanggal_tanam" value="{tanggal_tanam}" required placeholder="Masukan Tanggal tanam">
                    </div>
                    <div class="col-md-4">
                        <label>Tanggal perkiraan panen:</label>
                        <input type="text" class="form-control tanggal_perkiraan_panen" name="tanggal_perkiraan_panen" value="{tanggal_perkiraan_panen}" required placeholder="Masukan Tanggal perkiraan panen">
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>


                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Provinsi:</label>
                        <select class="form-control provinsi_kode" name="provinsi_kode" style="width:100%;"></select>
                        <input type="hidden" name="provinsi_nama" class="provinsi_nama">
                    </div>
                    <div class="col-md-3">
                        <label>Kabupaten:</label>
                        <select class="form-control kabupaten_kode" name="kabupaten_kode" style="width:100%;"></select>
                        <input type="hidden" name="kabupaten_nama" class="kabupaten_nama">
                    </div>
                    <div class="col-md-3">
                        <label>Kecamatan:</label>
                        <select class="form-control kecamatan_kode" name="kecamatan_kode" style="width:100%;"></select>
                        <input type="hidden" name="kecamatan_nama" class="kecamatan_nama">
                    </div>
                    <div class="col-md-3">
                        <label>Desa:</label>
                        <select class="form-control desa_kode" name="desa_kode" style="width:100%;"></select>
                        <input type="hidden" name="desa_nama" class="desa_nama">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-2">
                        <label>Garis Lintang:</label>
                        <input type="text" class="form-control lat" name="lat" value="{lat}" required placeholder="Garis Lintang">
                    </div>
                    <div class="col-md-2">
                        <label>Garis Bujur:</label>
                        <input type="text" class="form-control lng" name="lng" value="{lng}" required placeholder="Garis Bujur">
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <button type="button" class="btn btn-sm btn-info form-control btn_modal_map1"><i class="fa fa-globe"></i> Buka peta</button>
                    </div>
                </div>

                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>


                

                <div class="form-group row">
                    <div class="col-md-3">
                        <label>Luas kebun:</label>
                        <input type="text" class="form-control luas_kebun" name="luas_kebun" value="{luas_kebun}" required placeholder="0" readonly>
                    </div>
                    <div class="col-md-3">
                        <label>Total kebun:</label>
                        <input type="text" class="form-control total_kebun" name="total_kebun" value="{total_kebun}" required placeholder="0" readonly>
                    </div>
                </div>


                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless table_kelompok">
                                <tbody>
                                    <?php $kelompok_tani = getResultArray('tbl_kelompok_tani', array('id_permohonan' => $id)) ?>
                                    <?php if($kelompok_tani): ?>
                                        <?php $no=1; foreach($kelompok_tani as $row) : ?>
                                            <tr class="header bg-light" data-header="<?php echo $no ?>">
                                                <input type="hidden" required class="form-control" name="kelompok_tani_header[]" value="<?php echo $no ?>">
                                                <td class="text-center">
                                                    <div class="btn-group btn-group-sm button_aksi">
                                                        <button type="button" class="btn btn-danger btn_delete_kelompok" title="delete kelompok tani"><i class="fa fa-trash"></i></button>
                                                        <button type="button" class="btn btn-success btn_add_anggota_kelompok_tani" title="add anggota kelompok tani"><i class="fa fa-users"></i></button>
                                                    </div>
                                                </td>
                                                <td> <input type="text" required class="form-control" placeholder="Nama Kelompok" value="<?php echo $row['nama_kelompok'] ?>" name="kelompok_tani_nama_kelompok[]"> </td>
                                                <td> <input type="text" required class="form-control" placeholder="Nama Ketua" value="<?php echo $row['nama_ketua'] ?>" name="kelompok_tani_nama_ketua[]"> </td>
                                                <td> <input type="text" required class="form-control" placeholder="NIK Ketua" value="<?php echo $row['nik_ketua'] ?>" name="kelompok_tani_nik[]" pattern="[0-9]+"> </td>
                                                <td> <input type="text" required class="form-control" placeholder="No. HP" value="<?php echo $row['no_hp'] ?>" name="kelompok_tani_no_hp[]" pattern="[0-9]+"> </td>
                                                <td> <input type="text" required class="form-control kelompok_tani_luas" placeholder="Luas Hektar" value="<?php echo $row['luas'] ?>" name="kelompok_tani_luas[]" pattern="[0-9.]+"> </td>
                                                <td>
                                                    <div class="kt-checkbox-inline"> <label class="kt-checkbox"> <input type="checkbox" value="1" <?php echo ($row['terdaftar_simluhtan'] == '1') ? 'checked' : '' ?> name="kelompok_tani_terdaftar_simluhtan_1"> Terdaftar Simluhtan <span></span> </label> </div>
                                                </td>
                                                <td>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input kml_kelompok">
                                                        <label class="custom-file-label text-left small">Pilih file kml</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php $anggota_tani = getResultArray('tbl_anggota_tani', array('id_kelompok_tani' => $row['id'])) ?>
                                            <?php if($anggota_tani): ?>
                                                <?php $noAnggota=1; foreach($anggota_tani as $det) : ?>
                                                    <tr class="<?php echo $noAnggota ?>" data-header="<?php echo $no ?>" data-sub="y">
                                                        <td  class="text-center"> <a href="#" class=" btn_delete_anggota" title="delete anggota tani"><i class="fa fa-trash text-danger"></i></a> </td>
                                                        <td > <input type="text" required class="form-control nama_anggota" placeholder="Nama Anggota" value="<?php echo $det['nama_anggota'] ?>" name="<?php echo $no ?>_nama_anggota[]"> </td>
                                                        <td > <input type="text" required class="form-control nik_anggota" placeholder="NIK Anggota" value="<?php echo $det['nik_anggota'] ?>" name="<?php echo $no ?>_nik_anggota[]" pattern="[0-9]+"> </td>
                                                        <td > <input type="text" required class="form-control hp_anggota" placeholder="No. HP Anggota" value="<?php echo $det['no_hp'] ?>" name="<?php echo $no ?>_hp_anggota[]" pattern="[0-9]+"> </td>
                                                        <td>&nbsp;</td>
                                                        <td > <input type="text" required class="form-control luas_anggota" placeholder="Luas Hektar" value="<?php echo $det['luas'] ?>" name="<?php echo $no ?>_luas_anggota[]" pattern="[0-9.]+"> </td>                
                                                        </tr><tr class="<?php echo $noAnggota ?>" data-header="<?php echo $no ?>">
                                                        <td>&nbsp;</td>
                                                        <td > <input type="text" required class="form-control tanggal_tanam" placeholder="Tanggal tanam" value="<?php echo $det['tanggal_tanam'] ?>" name="<?php echo $no ?>_tanggal_tanam[]" readonly> </td>
                                                        <td > <input type="text" required class="form-control tanggal_panen" placeholder="Tanggal panen" value="<?php echo $det['tanggal_panen'] ?>" name="<?php echo $no ?>_tanggal_panen[]" readonly> </td>
                                                        </tr>
                                                        <tr class="<?php echo $noAnggota ?>" data-header="<?php echo $no ?>">
                                                        <td>&nbsp;</td>
                                                        <td > <input type="text" required class="form-control latitude_anggota" placeholder="Garis Lintang" value="<?php echo $det['latitude'] ?>" name="<?php echo $no ?>_latitude[]" readonly> </td>
                                                        <td > <input type="text" required class="form-control longitude_anggota" placeholder="Garis Bujur" value="<?php echo $det['longitude'] ?>" name="<?php echo $no ?>_longitude[]" readonly> </td>
                                                        <td > <button type="button" class="btn btn-sm btn-secondary form-control btn_modal_map2" data-index="<?php echo $noAnggota ?>"><i class="fa fa-globe"></i> Buka peta</button> </td>
                                                        </tr><tr class="<?php echo $noAnggota ?>" data-header="<?php echo $no ?>">
                                                        <td>&nbsp;</td>
                                                        <td > <textarea type="text" required class="form-control polygon" placeholder="Polygon" value="" name="<?php echo $no ?>_polygon[]" readonly><?php echo $det['polygon'] ?></textarea> </td>
                                                        <td > <button type="button" class="btn btn-sm btn-secondary form-control btn_modal_map3" data-index="<?php echo $noAnggota ?>"><i class="fa fa-globe"></i> Polygon peta</button> </td>
                                                        <td>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input '<?php echo $noAnggota ?>'_kml_anggota kml_anggota">
                                                                <label class="custom-file-label text-left small">Pilih file kml</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php $noAnggota++; endforeach ?>
                                            <?php endif ?>
                                        <?php $no++; endforeach ?>
                                    <?php endif ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info btn_add_kelompok">Tambah Kelompok Tani</button>
                    </div>
                </div>






            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{site_url}registrasi_kampung" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<!--begin::Modal-->
<div class="modal fade" id="modal_map1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map1()" id="formMap1">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP 1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-margin-b-20" id="pac-card">
                        <div class="input-group" id="pac-container">
                            <input type="text" class="form-control" id="pac-input" placeholder="address...">
                        </div>
                    </div>
                    <div style="height:350px;" id="map1"></div>
                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai latitude dan longitude</p>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Garis lintang:</label>
                            <input type="text" class="form-control latMap1" name="latMap1" id="latMap1" required placeholder="Garis lintang">
                        </div>
                        <div class="col-md-6">
                            <label>Garis bujur:</label>
                            <input type="text" class="form-control lngMap1" name="lngMap1" id="lngMap1" required placeholder="Garis bujur">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="modal_map2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map2()" id="formMap2">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP 2</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-margin-b-20" id="pac-card2">
                        <div class="input-group" id="pac-container">
                            <input type="text" class="form-control" id="pac-input2" placeholder="address...">
                        </div>
                    </div>
                    <div style="height:350px;" id="map2"></div>
                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai latitude dan longitude</p>

                    <!-- <input type='file' accept=".kml,.kmz" onchange="fileChanged()"> -->

                    <input type="hidden" class="latMap2nomorheader" name="latMap2nomorheader" value="{latMap2nomorheader}">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Garis lintang:</label>
                            <input type="text" class="form-control latMap2" name="latMap2" id="latMap2" required placeholder="Garis lintang">
                        </div>
                        <div class="col-md-6">
                            <label>Garis Bujur:</label>
                            <input type="text" class="form-control lngMap2" name="lngMap2" id="lngMap2" required placeholder="Garis bujur">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="modal_map3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="kt-form kt-form--label-right" action="javascript:save_lat_lng_map3()" id="formMap3">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">MAP Polygon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="kt-margin-b-20" id="pac-card3">
                        <div class="input-group" id="pac-container">
                            <input type="text" class="form-control" id="pac-input3" placeholder="address...">
                        </div>
                    </div>
                    <div style="height:350px;" id="map3"></div>
                    <p class="mt-5 text-info"><i class="fa fa-info-circle"></i> Klik pada map untuk mendapatkan nilai latitude dan longitude</p>

                    <!-- <input type='file' accept=".kml,.kmz" onchange="fileChanged()"> -->

                    <input type="hidden" class="latMap3nomorheader" name="latMap3nomorheader" value="{latMap3nomorheader}">
                    <input type="hidden" id="polygon_array" class="polygon_array" name="polygon_array" value="{polygon_array}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<script src="{base_url}dist/assets/js/togeojson.js"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&libraries=places,geometry,drawing"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap&libraries=geometry,drawing&ext=.js"></script> -->

<script>
    function fileChanged(e) {
        this.file = e.target.files[0]
        // // this.parseDocument(this.file);
        console.log(this.file);
    }

    var tableKelompokTani = $('.table_kelompok');
    tableKelompokTani.on('change', '.kml_kelompok', function() {
        var row = $(this).closest('tr');
        var index = row.index();
        var nomorHeader = row.data('header');

        var fileName = $(this)[0];
        
        if (fileName.files && fileName.files[0]) {
            readFile(fileName.files[0], function(e) {
                var xml = jQuery.parseXML(e.srcElement.result);
                var json = toGeoJSON.kml(xml);
                console.log(json)

                var lengthJson = json.features.length;
                for (var i = 0; i < lengthJson; i++) {
                    var properties = json.features[i].properties;
                    var geometry = json.features[i].geometry;
                    var type = geometry.type;
                    
                    
                    if (type == 'Polygon') {
                        myAync(properties, nomorHeader, i, index, geometry);
                    }
                }

            });

        }
    });


    async function myAync(properties, nomorHeader, subID, index, geometry) {
        setTimeout(function() {
            var polygon = [];
            var coordinatesLength = geometry.coordinates[0].length;                        
            for (var c = 0; c < coordinatesLength; c++) {
                var coordinates = geometry.coordinates[0][c];
                polygon.push({
                    lat: coordinates[1],
                    lng: coordinates[0]
                });
            }

            var html = `<tr class="` + subID + `" data-header="` + nomorHeader + `" data-sub="y">
            <td style="" class="text-center"> <a href="#" class=" btn_delete_anggota" title="delete anggota tani"><i class="fa fa-trash text-danger"></i></a> </td>
            <td style=""> <input type="text" required class="form-control nama_anggota" value="` + properties.name + `" placeholder="Nama Anggota" name="` + nomorHeader + `_nama_anggota[]"> </td>
            <td style=""> <input type="text" required class="form-control nik_anggota" placeholder="NIK Anggota" name="` + nomorHeader + `_nik_anggota[]" pattern="[0-9]+"> </td>
            <td style=""> <input type="text" required class="form-control hp_anggota" placeholder="No. HP Anggota" name="` + nomorHeader + `_hp_anggota[]" pattern="[0-9]+"> </td>
            <td>&nbsp;</td>
            <td style=""> <input type="text" required class="form-control luas_anggota" placeholder="Luas Hektar" name="` + nomorHeader + `_luas_anggota[]" pattern="[0-9.]+"> </td>                
            </tr><tr class="` + subID + `" data-header="` + nomorHeader + `">
            <td>&nbsp;</td>
            <td style=""> <input type="text" required class="form-control tanggal_tanam" placeholder="Tanggal tanam" name="` + nomorHeader + `_tanggal_tanam[]" readonly> </td>
            <td style=""> <input type="text" required class="form-control tanggal_panen" placeholder="Tanggal panen" name="` + nomorHeader + `_tanggal_panen[]" readonly> </td>
            </tr>
            <tr class="` + subID + `" data-header="` + nomorHeader + `">
            <td>&nbsp;</td>
            <td style=""> <input type="text" required class="form-control latitude_anggota" placeholder="Garis Lintang" name="` + nomorHeader + `_latitude[]" readonly> </td>
            <td style=""> <input type="text" required class="form-control longitude_anggota" placeholder="Garis Bujur" name="` + nomorHeader + `_longitude[]" readonly> </td>
            <td style=""> <button type="button" class="btn btn-sm btn-secondary form-control btn_modal_map2" data-index="` + subID + `"><i class="fa fa-globe"></i> Buka peta</button> </td>
            </tr><tr class="` + subID + `" data-header="` + nomorHeader + `">
            <td>&nbsp;</td>
            <td style=""> <textarea rows="2" type="text" required class="form-control polygon" placeholder="Polygon" name="` + nomorHeader + `_polygon[]" readonly>`+JSON.stringify(polygon)+`</textarea> </td>
            </tr>`;
            tableKelompokTani.find('tbody tr').eq(index).after(html);
            kalkulasiTotalKebun();
            $('.tanggal_tanam, .tanggal_panen').datepicker({
                format: 'yyyy-mm-dd',
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true
            });
        }, 500);
    }



    function readURL(fileName) {
        if (fileName.files && fileName.files[0]) {

            readFile(fileName.files[0], function(e) {
                var xml = jQuery.parseXML(e.srcElement.result);
                var json = toGeoJSON.kml(xml);


                var lengthJson = json.features.length;
                for (var i = 0; i < lengthJson; i++) {
                    var properties = json.features[i].properties;
                    var geometry = json.features[i].geometry;
                    var type = geometry.type;
                    if (type == 'Polygon') {
                        var polygon = [];
                        var coordinatesLength = geometry.coordinates[0].length;
                        for (var c = 0; c < coordinatesLength; c++) {
                            var coordinates = geometry.coordinates[0][c];
                            polygon.push({
                                lat: coordinates[0],
                                lng: coordinates[1]
                            });
                        }
                    }
                }
            });
        }
    }

    function readFile(file, onLoadCallback) {
        var reader = new FileReader();
        reader.onload = onLoadCallback;
        reader.readAsText(file);
    }

    $(document).ready(function() {

        $('.tanggal_permohonan, .tanggal_tanam, .tanggal_reg_horti, .tanggal_perkiraan_panen, .tanggal_tanam_pendamping, .tanggal_perkiraan_panen_pendamping, .tanggal_panen').datepicker({
            format: 'yyyy-mm-dd',
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            autoclose: true
        });

        $('.kabupaten_kode, .kecamatan_kode, .desa_kode, .jenis_komoditi_id, .sub_komoditi_id').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');

        ajaxSelect({
            allowClear: true,
            id: '.provinsi_kode',
            url: '{site_url}ajax_selectbox/provinsi_select',
            selected: '{provinsi_kode}'
        });

        $('.provinsi_kode').change(function() {
            var val = $(this).val();
            $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
            ajaxSelect({
                id: '.kabupaten_kode',
                url: '{site_url}ajax_selectbox/kabupaten_select',
                selected: '{kabupaten_kode}',
                optionalSearch: {
                    provinsi_kode: val
                }
            });
        });
        
        $('.kabupaten_kode').change(function() {
            var val = $(this).val();
            $('.kecamatan_kode, .desa_kode').empty();
            ajaxSelect({
                id: '.kecamatan_kode',
                url: '{site_url}ajax_selectbox/kecamatan_select',
                selected: '{kecamatan_kode}',
                optionalSearch: {
                    kabupaten_kode: val
                }
            });
        });
        
        $('.kecamatan_kode').change(function() {
            var val = $(this).val();
            $('.desa_kode').empty();
            ajaxSelect({
                id: '.desa_kode',
                url: '{site_url}ajax_selectbox/desa_select',
                selected: '{desa_kode}',
                optionalSearch: {
                    kecamatan_kode: val
                }
            });
        });


        ajaxSelect({
            id: '.kelompok_komoditi_id',
            selected: '{kelompok_komoditi_id}',
            url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
        });

        $('.kelompok_komoditi_id').change(function() {
            var val = $(this).val();
            $('.jenis_komoditi_id').empty();
            ajaxSelect({
                id: '.jenis_komoditi_id',
                url: '{site_url}ajax_selectbox/jenis_komoditi_id',
                selected: '{jenis_komoditi_id}',
                optionalSearch: {
                    kelompok_komoditi_id: val
                }
            });
        });
        
        $('.jenis_komoditi_id').change(function() {
            var val = $(this).val();
            $('.sub_komoditi_id').empty();
            ajaxSelect({
                id: '.sub_komoditi_id',
                selected: '{sub_komoditi_id}',
                url: '{site_url}ajax_selectbox/sub_komoditi_id',
                optionalSearch: {
                    jenis_komoditi_id: val
                }
            });
        });

        // open map1 
        $('.btn_modal_map1').click(function() {
            $('#modal_map1').modal('show');
        })

        

        var tableKelompokTani = $('.table_kelompok');
        $('.btn_add_kelompok').click(function() {

            var nomorHeader = new Date().getTime();

            var html = `<tr class="header bg-light" data-header="` + nomorHeader + `">
                <input type="hidden" required class="form-control" name="kelompok_tani_header[]" value="` + nomorHeader + `">
                <td class="text-center">
                    <div class="btn-group btn-group-sm button_aksi">
                        <button type="button" class="btn btn-danger btn_delete_kelompok" title="delete kelompok tani"><i class="fa fa-trash"></i></button>
                        <button type="button" class="btn btn-success btn_add_anggota_kelompok_tani" title="add anggota kelompok tani"><i class="fa fa-users"></i></button>
                    </div>
                </td>
                <td> <input type="text" required class="form-control" placeholder="Nama Kelompok" name="kelompok_tani_nama_kelompok[]"> </td>
                <td> <input type="text" required class="form-control" placeholder="Nama Ketua" name="kelompok_tani_nama_ketua[]"> </td>
                <td> <input type="text" required class="form-control" placeholder="NIK Ketua" name="kelompok_tani_nik[]" pattern="[0-9]+"> </td>
                <td> <input type="text" required class="form-control" placeholder="No. HP" name="kelompok_tani_no_hp[]" pattern="[0-9]+"> </td>
                <td> <input type="text" required class="form-control kelompok_tani_luas" placeholder="Luas Hektar" name="kelompok_tani_luas[]" pattern="[0-9.]+"> </td>
                <td> <div class="kt-checkbox-inline"> <label class="kt-checkbox"> <input type="checkbox" value="1" name="kelompok_tani_terdaftar_simluhtan_` + nomorHeader + `"> Terdaftar Simluhtan <span></span> </label> </div> </td>
                <td><div class="custom-file"><input type="file" class="custom-file-input kml_kelompok">
                <label class="custom-file-label text-left small">Pilih file kml</label>
                </div></td>
            </tr>`;

            tableKelompokTani.find('tbody').append(html);
        });


        tableKelompokTani.on('click', '.btn_add_anggota_kelompok_tani', function() {
            var row = $(this).closest('tr');
            var index = row.index();
            var nomorHeader = row.data('header');
            var subID = new Date().getTime();

            var html = `<tr class="` + subID + `" data-header="` + nomorHeader + `" data-sub="y">
                <td  class="text-center"> <a href="#" class=" btn_delete_anggota" title="delete anggota tani"><i class="fa fa-trash text-danger"></i></a> </td>
                <td > <input type="text" required class="form-control nama_anggota" placeholder="Nama Anggota" name="` + nomorHeader + `_nama_anggota[]"> </td>
                <td > <input type="text" required class="form-control nik_anggota" placeholder="NIK Anggota" name="` + nomorHeader + `_nik_anggota[]" pattern="[0-9]+"> </td>
                <td > <input type="text" required class="form-control hp_anggota" placeholder="No. HP Anggota" name="` + nomorHeader + `_hp_anggota[]" pattern="[0-9]+"> </td>
                <td>&nbsp;</td>
                <td > <input type="text" required class="form-control luas_anggota" placeholder="Luas Hektar" name="` + nomorHeader + `_luas_anggota[]" pattern="[0-9.]+"> </td>                
                </tr><tr class="` + subID + `" data-header="` + nomorHeader + `">
                <td>&nbsp;</td>
                <td > <input type="text" required class="form-control tanggal_tanam" placeholder="Tanggal tanam" name="` + nomorHeader + `_tanggal_tanam[]" readonly> </td>
                <td > <input type="text" required class="form-control tanggal_panen" placeholder="Tanggal panen" name="` + nomorHeader + `_tanggal_panen[]" readonly> </td>
                </tr>
                <tr class="` + subID + `" data-header="` + nomorHeader + `">
                <td>&nbsp;</td>
                <td > <input type="text" required class="form-control latitude_anggota" placeholder="Garis Lintang" name="` + nomorHeader + `_latitude[]" readonly> </td>
                <td > <input type="text" required class="form-control longitude_anggota" placeholder="Garis Bujur" name="` + nomorHeader + `_longitude[]" readonly> </td>
                <td > <button type="button" class="btn btn-sm btn-secondary form-control btn_modal_map2" data-index="` + subID + `"><i class="fa fa-globe"></i> Buka peta</button> </td>
                </tr><tr class="` + subID + `" data-header="` + nomorHeader + `">
                <td>&nbsp;</td>
                <td > <input type="text" required class="form-control polygon" placeholder="Polygon" name="` + nomorHeader + `_polygon[]" readonly> </td>
                <td > <button type="button" class="btn btn-sm btn-secondary form-control btn_modal_map3" data-index="` + subID + `"><i class="fa fa-globe"></i> Polygon peta</button> </td>
            </tr>`;

            
            
            tableKelompokTani.find('tbody tr').eq(index).after(html);
            kalkulasiTotalKebun();
            $('.tanggal_tanam, .tanggal_panen').datepicker({
                format: 'yyyy-mm-dd',
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                autoclose: true
            });
        });
    });

    var tableKelompokTani = $('.table_kelompok');

    $(document).on('change', '.kml_anggota', function() {
        var row = $(this).closest('tr');
        var index = row.index();
        var nomorHeader = row.data('header');
        var subID = row.attr('class');
        

        var fileName = $(this)[0];
        if (fileName.files && fileName.files[0]) {
            readFile(fileName.files[0], function(e) {
                var xml = jQuery.parseXML(e.srcElement.result);
                var json = toGeoJSON.kml(xml);
                console.log(json)

                var lengthJson = json.features.length;
                for (var i = 0; i < lengthJson; i++) {
                    var properties = json.features[i].properties;
                    var geometry = json.features[i].geometry;
                    var type = geometry.type;
                    
                    
                    if (type == 'Polygon') {
                        // myAync2(properties, nomorHeader, i, index, geometry);
                        setTimeout(function() {
                            var polygon = [];
                            var coordinatesLength = geometry.coordinates[0].length;                        
                            for (var c = 0; c < coordinatesLength; c++) {
                                var coordinates = geometry.coordinates[0][c];
                                polygon.push({
                                    lat: coordinates[1],
                                    lng: coordinates[0]
                                });
                            }
                            row.find('.polygon').val(JSON.stringify(polygon));
                        }, 500)
                    }
                }

            });

        }
        console.log(fileName)
    });

    tableKelompokTani.on('click', '.btn_delete_kelompok', function() {
        var row = $(this).closest('tr');
        var nomorHeader = row.data('header');
        tableKelompokTani.find('tbody tr[data-header=' + nomorHeader + ']').each(function() {
            $(this).remove();
        });
        kalkulasiTotalKebun();
    });

    tableKelompokTani.on('click', '.btn_delete_anggota', function(e) {
        e.preventDefault();
        var row = $(this).closest('tr');
        var className = row.attr('class');
        tableKelompokTani.find('tbody tr.' + className).each(function() {
            $(this).remove();
        });
        kalkulasiTotalKebun();
    });

    tableKelompokTani.on('keyup', '.kelompok_tani_luas', function() {
        kalkulasiLuasKebun();
    });

    $(document).on('click', '.btn_modal_map2', function() {
        var index = $(this).data('index');
        $('.latMap2nomorheader').val(index);

        $('#modal_map2').modal('show');
    });
    
    $(document).on('click', '.btn_modal_map3', function() {
        var index = $(this).data('index');
        console.log(index)
        $('.latMap3nomorheader').val(index);

        $('#modal_map3').modal('show');
    });


    function kalkulasiLuasKebun() {
        var sum = 0;
        tableKelompokTani.find('tbody tr.header').filter(function() {
            var luas = parseFloat($(this).closest('tr').find('input.kelompok_tani_luas').val());
            luas = isNaN(luas) ? 0 : luas;
            sum += luas;
        });
        $('.luas_kebun').val(sum);
    }

    function kalkulasiTotalKebun() {
        var sum = 0;
        tableKelompokTani.find('tbody tr[data-sub=y]').filter(function() {
            var total = parseInt(1);
            total = isNaN(total) ? 0 : total;
            sum += total;
        });
        $('.total_kebun').val(sum);
    }

    function save_lat_lng_map1() {
        var form = $('#formMap1')[0];
        var formData = new FormData(form);

        if (!formData.get('latMap1') && !formData.get('lngMap1')) {
            alert('Latitude dan longitude wajib diisi')
            return false;
        } else {
            $('.lat').val(formData.get('latMap1'));
            $('.lng').val(formData.get('lngMap1'));
            $('#modal_map1').modal('hide');
        }
    }

    function save_lat_lng_map2() {
        var form = $('#formMap2')[0];
        var formData = new FormData(form);

        if (!formData.get('latMap2') && !formData.get('lngMap2')) {
            alert('Latitude dan longitude wajib diisi')
            return false;
        } else {
            tableKelompokTani.find('tr.' + formData.get('latMap2nomorheader')).find('.latitude_anggota').val(formData.get('latMap2'));
            tableKelompokTani.find('tr.' + formData.get('latMap2nomorheader')).find('.longitude_anggota').val(formData.get('lngMap2'));
            $('#modal_map2').modal('hide');
        }
    }

    function save_lat_lng_map3() {
        var form = $('#formMap3')[0];
        var formData = new FormData(form);
        
        if (!formData.get('polygon_array')) {
            alert('koordinate polygon wajib diisi')
            return false;
        } else {
            tableKelompokTani.find('tr.' + formData.get('latMap3nomorheader')).find('.polygon').val(formData.get('polygon_array'));
            $('#modal_map3').modal('hide');
        }
    }

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save/{id}',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
                btnSpinnerHide();
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }

    function generate_nomor_registrasi() {
        var static = 'STO.01.';
        var kabupaten_kode = $('.kabupaten_kode').val();
        var kelompok_komoditi_id = romawiKelompokKomoditi($('.kelompok_komoditi_id').val());
        var jenis_komoditi_id = ($('.jenis_komoditi_id').val()) ? $('.jenis_komoditi_id').val() : '';
        var year = new Date().getFullYear().toString().substr(-2);
        var nomor_registrasi = static + kabupaten_kode + '.' + kelompok_komoditi_id + '.' + jenis_komoditi_id + '.' + year;

        $.ajax({
            url: '{segment1}get_nomor_urut_registrasi',
            dataType: 'json',
            method: 'post',
            data: {
                search: nomor_registrasi
            },
            success: function(res) {
                nomor_registrasi += '.' + res.nomor_urut;
                $('.nomor_registrasi').val(nomor_registrasi);
            }
        })

    }

    function romawiKelompokKomoditi(kode) {
        switch (kode) {
            case '01':
                return 'I';
            case '02':
                return 'II';
            case '03':
                return 'III';
            case '04':
                return 'IV';
            default:
                return '';
        }
    }

    




    // MAP1 
    var map;
    var geocoder;
    var polygonString = [];
    var polygonArray = [];

    function initialize() {
        var mapOptions = {
            zoom: 5.2,
            center: new google.maps.LatLng(-2.0423904, 102.8983498),
            // mapTypeId: google.maps.MapTypeId.ROADMAP
            mapTypeControl: false,
        };

        map = new google.maps.Map(document.getElementById('map1'),
            mapOptions
        );
        

        var card = document.getElementById("pac-card");
        var input = document.getElementById("pac-input");
        const options = {
            fields: ["formatted_address", "geometry", "name"],
            strictBounds: false,
            componentRestrictions: {country: "id"},
            types: ["geocode"],
            draggable: false,
            disableDefaultUI: true,
        };

        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo("bounds", map);

        const infowindow = new google.maps.InfoWindow();
        const infowindowContent = document.getElementById("infowindow-content");

        infowindow.setContent(infowindowContent);

        var marker = new google.maps.Marker({
            map,
            anchorPoint: new google.maps.Point(0, -29),
        });

        autocomplete.addListener("place_changed", () => {
            infowindow.close();
            marker.setVisible(false);

            const place = autocomplete.getPlace();

            if (!place.geometry || !place.geometry.location) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            document.getElementById('latMap1').value = place.geometry.location.lat();
            document.getElementById('lngMap1').value = place.geometry.location.lng();
            addMarker(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()), map);

            infowindowContent.children["place-name"].textContent = place.name;
            infowindowContent.children["place-address"].textContent =
            place.formatted_address;
            infowindow.open(map, marker);
        });

        google.maps.event.addListener(map, 'click', function(event) {
            document.getElementById('latMap1').value = event.latLng.lat();
            document.getElementById('lngMap1').value = event.latLng.lng();
            console.log(event);

            if (marker != null) {
                marker.setMap(null);
            }
            addMarker(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()), map);
            // marker.setPosition(event.location);
            // marker.setVisible(true);
        });

        function addMarker(latLng, map) {
            // if marker exist remove it show only one marker
            if (marker != null) {
                marker.setMap(null);
            }

            marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }




        // map 2 

        const map2 = new google.maps.Map(document.getElementById("map2"), {
            zoom: 5.2,
            center: {
                lat: -2.0423904,
                lng: 102.8983498
            },
        });

        var card2 = document.getElementById("pac-card2");
        var input2 = document.getElementById("pac-input2");
        const options2 = {
            fields: ["formatted_address", "geometry", "name"],
            strictBounds: false,
            componentRestrictions: {country: "id"},
            types: ["geocode"],
            draggable: false,
            disableDefaultUI: true,
        };

        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

        var autocomplete2 = new google.maps.places.Autocomplete(input2, options2);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete2.bindTo("bounds", map2);

        const infowindow2 = new google.maps.InfoWindow();
        const infowindowContent2 = document.getElementById("infowindow-content");

        infowindow2.setContent(infowindowContent2);

        var marker2 = new google.maps.Marker({
            map2,
            anchorPoint: new google.maps.Point(0, -29),
        });

        autocomplete2.addListener("place_changed", () => {
            infowindow2.close();
            marker2.setVisible(false);

            const place2 = autocomplete2.getPlace();

            if (!place2.geometry || !place2.geometry.location) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place2.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place2.geometry.viewport) {
                map2.fitBounds(place2.geometry.viewport);
            } else {
                map2.setCenter(place2.geometry.location);
                map2.setZoom(17);
            }

            marker2.setPosition(place2.geometry.location);
            marker2.setVisible(true);

            addMarker(new google.maps.LatLng(place2.geometry.location.lat(), place2.geometry.location.lng()), map2);
            document.getElementById('latMap2').value = place2.geometry.location.lat();
            document.getElementById('lngMap2').value = place2.geometry.location.lng();

            infowindowContent2.children["place-name"].textContent = place2.name;
            infowindowContent2.children["place-address"].textContent =
            place2.formatted_address;
            infowindow2.open(map2, marker2);
        });

        function addMarker(latLng, map) {
            // if marker exist remove it show only one marker
            if (marker2 != null) {
                marker2.setMap(null);
            }

            marker2 = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }

        google.maps.event.addListener(map2, 'click', function(event) {
            document.getElementById('latMap2').value = event.latLng.lat();
            document.getElementById('lngMap2').value = event.latLng.lng();
            if (marker2 != null) {
                marker2.setMap(null);
            }
            addMarker(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()), map2);
        });


        // map 3



        const map3 = new google.maps.Map(document.getElementById("map3"), {
            zoom: 5.2,
            center: {
                lat: -2.0423904,
                lng: 102.8983498
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                    google.maps.drawing.OverlayType.POLYGON,
                ]
            },
            markerOptions: {
                icon: 'images/car-icon.png'
            },
            circleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 1,
                strokeWeight: 5,
                clickable: false,
                editable: true,
                zIndex: 1
            },
            polygonOptions: {
                fillColor: '#BCDCF9',
                fillOpacity: 0.5,
                strokeWeight: 2,
                strokeColor: '#57ACF9',
                clickable: false,
                editable: false,
                zIndex: 1
            }
        });

        drawingManager.setMap(map3);


        var card3 = document.getElementById("pac-card3");
        var input3 = document.getElementById("pac-input3");
        const options3 = {
            fields: ["formatted_address", "geometry", "name"],
            strictBounds: false,
            componentRestrictions: {country: "id"},
            types: ["geocode"],
            draggable: false,
            disableDefaultUI: true,
        };

        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(card);

        var autocomplete3 = new google.maps.places.Autocomplete(input3, options3);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete3.bindTo("bounds", map3);

        const infowindow3 = new google.maps.InfoWindow();
        const infowindowContent3 = document.getElementById("infowindow-content");

        infowindow3.setContent(infowindowContent3);

        var marker3 = new google.maps.Marker({
            map3,
            anchorPoint: new google.maps.Point(0, -29),
        });

        autocomplete3.addListener("place_changed", () => {
            infowindow3.close();
            marker3.setVisible(false);

            const place2 = autocomplete3.getPlace();

            if (!place2.geometry || !place2.geometry.location) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place2.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place2.geometry.viewport) {
                map3.fitBounds(place2.geometry.viewport);
            } else {
                map3.setCenter(place2.geometry.location);
                map3.setZoom(17);
            }

            marker3.setPosition(place2.geometry.location);
            marker3.setVisible(true);

            addMarker(new google.maps.LatLng(place2.geometry.location.lat(), place2.geometry.location.lng()), map3);

            infowindowContent3.children["place-name"].textContent = place2.name;
            infowindowContent3.children["place-address"].textContent =
            place2.formatted_address;
            infowindow3.open(map3, marker3);
        });

        function addMarker(latLng, map) {
            // if marker exist remove it show only one marker
            if (marker3 != null) {
                marker3.setMap(null);
            }

            marker3 = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }

        google.maps.event.addListener(map3, 'click', function(event) {
            document.getElementById('latMap3').value = event.latLng.lat();
            document.getElementById('lngMap3').value = event.latLng.lng();
            if (marker3 != null) {
                marker3.setMap(null);
            }
            addMarker(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()), map3);
        });
        // ops 

        google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
            var string = [];
            arrLatLng = [];
            for (var i = 0; i < polygon.getPath().getLength(); i++) {
                latLng = {
                    lat: polygon.getPath().getAt(i).lat(),
                    lng: polygon.getPath().getAt(i).lng()
                };
                arrLatLng.push(latLng);
            }
            polygonString.push(string);
            polygonArray.push(polygon);
            document.getElementById('polygon_array').value = JSON.stringify(arrLatLng);
        });


    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>