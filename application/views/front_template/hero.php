<div class="kt-portlet kt-portlet-no-margin kt-sc-2">

    <!-- NAV BAR  -->
    <div class="myhero" style="background-image: url('{dist_path}images/frontend_bg.jpg')">
        <div style="margin-left:27%; position:relative; padding-top:6%;">
            <div class="img" style="position: absolute; left: 17%; top: -80%;">
                <img alt="Logo" src="{dist_path}images/logo.png" class="logo-default">
            </div>
            <div class="title" style="position: absolute; left: 4%; top: 100%;">
                <center>
                    <b style="font-size:x-large;">DIREKTORAT JENDERAL HORTIKULTURA</b>
                </center>
            </div>
        </div>
    </div>
    <div class=" kt-sc__bottom" style="background-color:#ff9f43; padding-bottom:1%">
        <div style="width: max-content;margin: 0 auto;">
            <ul class="nav nav-tabs  nav-tabs-line" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->uri->segment(2) == 'home') ? 'active' : '' ?>" href="{site_url}beranda/home" style="color:white !important">BERANDA</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->uri->segment(2) == 'ebook') ? 'active' : '' ?>" href="{site_url}beranda/ebook" style="color:white !important">E-BOOK</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($this->uri->segment(2) == 'kontak') ? 'active' : '' ?>" href="{site_url}beranda/kontak" style="color:white !important">KONTAK</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{site_url}beranda/login" style="color:white !important">LOGIN SRIKANDI</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{site_url}beranda/login_p2l" style="color:white !important">LOGIN P2L</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{site_url}beranda/login_champ" style="color:white !important">LOGIN
                        CHAMPION</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{site_url}beranda/login_nurseri" style="color:white !important">LOGIN
                        NURSERI</a>
                </li>
            </ul>
        </div>
    </div>
</div>