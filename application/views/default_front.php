<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
    <base href="../../../">
    <meta charset="utf-8" />
    <title>{title}</title>
    <meta name="description" content="Support center home example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{dist_path}assets/css/pages/support-center/home-2.css" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{dist_path}assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{dist_path}assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{dist_path}assets/media/logos/favicon.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

    <!-- begin:: Page -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <?php $this->load->view($content) ?>

        

        <!-- begin:: Section -->
        <div class="row">
            <div class="col-lg-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <div class="kt-infobox">
                            <div class="kt-infobox__header">
                                <h2 class="kt-infobox__title">AirPlus SAAS License</h2>
                                <div class="kt-infobox__badge">Blog</div>
                            </div>
                            <div class="kt-infobox__body">
                                <div class="kt-infobox__section">
                                    <div class="kt-infobox__content">
                                        Windows 10 automatically downloads and installs updates to make sure your device is secure and up to date. This means you receive the latest fixes and security updates, helping your device run efficiently and stay protected. In most cases, restarting your device completes the update. Make sure your device is plugged in when you know updates will be installed.
                                        <br />
                                        <br />
                                        <a href="#" class="kt-link">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__body">
                        <div class="kt-infobox kt-infobox--success">
                            <div class="kt-infobox__header">
                                <h2 class="kt-infobox__title">AirPlus SAAS License</h2>
                                <div class="kt-infobox__badge">Blog</div>
                            </div>
                            <div class="kt-infobox__body">
                                <div class="kt-infobox__section">
                                    <div class="kt-infobox__content">
                                        Windows 10 automatically downloads and installs updates to make sure your device is secure and up to date. This means you receive the latest fixes and security updates, helping your device run efficiently and stay protected. In most cases, restarting your device completes the update. Make sure your device is plugged in when you know updates will be installed.
                                        <br />
                                        <br />
                                        <a href="#" class="kt-link">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Section -->

        <!-- begin:: Section -->
        <div class="row">
            <div class="col-lg-6">
                <div class="kt-portlet kt-callout">
                    <div class="kt-portlet__body">
                        <div class="kt-callout__body">
                            <div class="kt-callout__content">
                                <h3 class="kt-callout__title">Get in Touch</h3>
                                <p class="kt-callout__desc">
                                    Windows 10 automatically installs updates to make for sure
                                </p>
                            </div>
                            <div class="kt-callout__action">
                                <a href="custom/apps/support-center/feedback.html" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-primary">Submit a Request</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="kt-portlet kt-callout">
                    <div class="kt-portlet__body">
                        <div class="kt-callout__body">
                            <div class="kt-callout__content">
                                <h3 class="kt-callout__title">Live Chat</h3>
                                <p class="kt-callout__desc">
                                    Windows 10 automatically installs updates to make for sure
                                </p>
                            </div>
                            <div class="kt-callout__action">
                                <a href="#" data-toggle="modal" data-target="#kt_chat_modal" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-success">Start Chat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- end::  content -->
    </div>

    <!-- end:: Content -->


    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    <!-- end::Scrolltop -->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#22b9ff",
                    "light": "#ffffff",
                    "dark": "#282a3c",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{dist_path}assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="{dist_path}assets/js/scripts.bundle.js" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->
</body>

<!-- end::Body -->

</html>