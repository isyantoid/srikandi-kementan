// Inisialisasi peta

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6.2,
        // Zoom level
        // zoom: 5.6, // Zoom level
        center: {
            lat: -2.4833826,
            lng: 117.8902853
        }, // Koordinat tengah peta
        disableDefaultUI: true,
    });

    // Muat GeoJSON menggunakan fetch API
    fetch(baseUrl + '/dist/json/indonesia-prov.geojson').then(response => response.json()).then(geojson => { // Buat lapisan GeoJSON
        var geojsonLayer = new google.maps.Data({
            map: map,
            style: {
                fillColor: 'green',
                strokeWeight: 1
            }
        });

        // Tambahkan GeoJSON ke lapisan
        geojsonLayer.addGeoJson(geojson);

        // Muat skrip InfoBox
        var script = document.createElement('script');
        script.src = 'https://cdn.jsdelivr.net/npm/js-info-bubble@0.8.0/src/infobubble-compiled.min.js';
        script.onload = function () { // Iterasi melalui fitur dan tambahkan InfoBox dengan gaya latar belakang kustom
            geojsonLayer.forEach(function (feature) {
                var name = feature.getProperty('Propinsi'); // Ambil properti NAME_1
                var kode = feature.getProperty('kode');
                var geometry = feature.getGeometry();

                var bounds = new google.maps.LatLngBounds();
                geometry.forEachLatLng(function (latlng) {
                    bounds.extend(latlng);
                });

                var value = $('#kodeprov-' + kode).text()
                var bgColor = 'rgba(255,255,255, 0.5)';
                var contentString = '<div class="custom-infobox">' + name + '<br>0</div>'; // Tambahkan garis baru dengan <br>
                if (value) {
                    bgColor = 'rgba(255,0,0, 0.7)';
                    contentString = '<div class="custom-infobox-value">' + name + '<br>' + value + '</div>';
                }

                var infobox = new InfoBubble({
                    content: contentString,
                    position: bounds.getCenter(),
                    backgroundColor: bgColor,
                    borderRadius: 0,
                    padding: 0,
                    borderWidth: 0,
                    disableAutoPan: true,
                    closeBox: false,
                    optimized: false,
                });

                infobox.open(map);
            });
        };
        document.head.appendChild(script);
    });

    $(function () {
        $("#divclick").click(function () {
            html2canvas($("#totalimage"), {
                useCORS: true,
                onrendered: function (canvas) {
                    const dataUrl = canvas.toDataURL("image/png");
                    console.log(dataUrl);
                    $('#show_img').val(canvas.toDataURL("image/png"))
                    $("#show_img").append(canvas);
                }
            });
        });
    });
}

function saveMapImage(map) {
    const mapCanvas = map.getDiv();
    const mapImage = new Image();

    map.addListener("tilesloaded", function () {
        const canvas = document.createElement("canvas");
        canvas.width = mapCanvas.offsetWidth;
        canvas.height = mapCanvas.offsetHeight;
        const context = canvas.getContext("2d");
        context.drawImage(mapCanvas, 0, 0, canvas.width, canvas.height);

        mapImage.src = canvas.toDataURL("image/png");

        console.log(mapImage.src)

        // Simpan mapImage.src, misalnya dengan mengirim ke server atau menampilkan di elemen gambar lain.
        // Di sini, Anda dapat menambahkan kode untuk mengirim mapImage.src ke server atau menampilkan gambar di elemen lain.
        // Misalnya:
        // const imageElement = $("<img>").attr("src", mapImage.src);
        // $("body").append(imageElement);
    });
}


// Definisikan custom overlay untuk label
function LabelOverlay(bounds, label, map) {
    this.bounds_ = bounds;
    this.label_ = label;
    this.map_ = map;

    this.div_ = null;

    this.setMap(map);
}

LabelOverlay.prototype.onAdd = function () {
    var div = document.createElement('div');
    div.style.borderStyle = 'none';
    div.style.borderWidth = '0px';
    div.style.position = 'absolute';
    div.style.transform = 'translate(-50%, -100%)'; // Atur posisi label di atas marker
    div.innerHTML = this.label_;

    this.div_ = div;

    var panes = this.getPanes();
    panes.overlayLayer.appendChild(div);
};

LabelOverlay.prototype.draw = function () {
    var overlayProjection = this.getProjection();
    var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
    var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

    var div = this.div_;
    div.style.left = sw.x + 'px';
    div.style.top = ne.y + 'px';
};

LabelOverlay.prototype.onRemove = function () {
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
};
