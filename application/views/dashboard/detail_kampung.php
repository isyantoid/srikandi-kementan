<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet" data-ktportlet="true" id="collapse">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <a href="{segment1}" class="btn btn-sm btn-icon btn-primary btn-pill btn-icon-md"><i class="la la-arrow-left"></i></a>
                        </span>
                        <h3 class="kt-portlet__head-title"> {title}</h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Tanggal Permohonan</th>
                                <th>Nomor Horti</th>
                                <th>Jenis Komoditi</th>
                                <th>Luas</th>
                                <th>Provinsi</th>
                                <th>Kabupaten</th>
                            </thead>
                            <tbody>
                                <?php if($detail_kampung): ?>
                                    <?php $no=1; foreach($detail_kampung as $row): ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $row['tanggal_permohonan'] ?></td>
                                            <td><?= $row['no_reg_horti'] ?></td>
                                            <td><?= $row['jenis_komoditi_nama'] ?></td>
                                            <td><?= $row['luas_kebun'] . '' . $row['satuan_luas_kebun'] ?></td>
                                            <td><?= $row['provinsi_nama'] ?></td>
                                            <td><?= $row['kabupaten_nama'] ?></td>
                                        </tr>
                                    <?php $no++; endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>