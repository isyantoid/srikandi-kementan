<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mockup extends CI_Controller
{

	public function index()
	{
		redirect('mockup/home');
	}

	public function home()
	{
		$this->db->where('status_aktif', 'aktif');
		$data['query_artikel'] = $this->db->get('tbl_form_artikel')->result();
		$data['title'] = 'Home';
		$data['content'] = 'mockup/home';
		$data = array_merge($data, path_info());
		$this->parser->parse('front_template/main', $data);
	}

	public function ebook()
	{
		$data['title'] = 'Ebook';
		$data['content'] = 'mockup/ebook';
		$data = array_merge($data, path_info());
		$this->parser->parse('front_template/main', $data);
	}

	public function kontak()
	{
		$data['title'] = 'Kontak';
		$data['content'] = 'mockup/kontak';
		$data = array_merge($data, path_info());
		$this->parser->parse('front_template/main', $data);
	}

	public function login()
	{
		$data['title'] = 'Login';
		$data = array_merge($data, path_info());
		$this->parser->parse('mockup/login', $data);
	}

	public function baca_artikel($id = null)
	{
		if ($id) {
			$data['title'] = 'Baca Artikel ' . $id;
			$data['id'] = $id;
			$data['content'] = 'mockup/baca_artikel';
			$data = array_merge($data, path_info());
			$this->parser->parse('front_template/main', $data);
		} else {
			show_404();
		}
	}
}