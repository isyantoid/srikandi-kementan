<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_realisasi_pasar_cabai_model extends CI_Model
{

    private $tableName = 'champ_realisasi_pasar';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {

                $config = $this->config_file_upload();                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $data_upload = [];
                if ( $this->upload->do_upload('foto')){
                    $data_upload = $this->upload->data();
                    $this->db->set('foto', $data_upload['file_name']);
                }
                
                $data_upload2 = [];
                if ( $this->upload->do_upload('foto_kwitansi')){
                    $data_upload2 = $this->upload->data();
                    $this->db->set('foto_kwitansi', $data_upload2['file_name']);
                }
                
                $data_upload3 = [];
                if ( $this->upload->do_upload('foto_serah_terima')){
                    $data_upload3 = $this->upload->data();
                    $this->db->set('foto_serah_terima', $data_upload3['file_name']);
                }
                
                $data_upload4 = [];
                if ( $this->upload->do_upload('foto_dokumen')){
                    $data_upload4 = $this->upload->data();
                    $this->db->set('foto_dokumen', $data_upload4['file_name']);
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                $config = $this->config_file_upload();                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $data_upload = [];
                if ( $this->upload->do_upload('foto')){
                    $data_upload = $this->upload->data();
                    $this->db->set('foto', $data_upload['file_name']);
                } else {
                    return jsonOutputError($this->upload->display_errors('',''));
                }
                
                $data_upload2 = [];
                if ( $this->upload->do_upload('foto_kwitansi')){
                    $data_upload2 = $this->upload->data();
                    $this->db->set('foto_kwitansi', $data_upload2['file_name']);
                } else {
                    return jsonOutputError($this->upload->display_errors('',''));
                }
                
                $data_upload3 = [];
                if ( $this->upload->do_upload('foto_serah_terima')){
                    $data_upload3 = $this->upload->data();
                    $this->db->set('foto_serah_terima', $data_upload3['file_name']);
                } else {
                    return jsonOutputError($this->upload->display_errors('',''));
                }
                
                $data_upload4 = [];
                if ( $this->upload->do_upload('foto_dokumen')){
                    $data_upload4 = $this->upload->data();
                    $this->db->set('foto_dokumen', $data_upload4['file_name']);
                } else {
                    return jsonOutputError($this->upload->display_errors('',''));
                }


                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                
                $this->db->set('user_id', $this->session->userdata('session_user_id'));
                $this->db->set('created_by', $this->session->userdata('session_user_id'));
                $this->db->set('created_at', date('Y-m-d H:i:s'));
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }

    public function config_file_upload() {
        $config['upload_path'] = './uploads/champ/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }
}