<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-2 col-form-label">Cari Titik Awal</label>
                <div class="col-6">
                    <div class="input-group">
                        <input id="pac-input" type="text" class="form-control" placeholder="Cari Titik Awal" />
                        <div class="input-group-append">
                            <button class="btn btn-secondary clear" type="button">Clear</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Jarak (Km)</label>
                <div class="col-3">
                    <p id="destination" class="form-control-plaintext text-muted"></p>
                </div>
            </div>
            <div id="map1" style="height:500px;"></div>
        </div>
    </div>
</div>


<script>
    var map, myLoc, directionsService, directionsRenderer, origin, destination;
    // var destination = new google.maps.LatLng('{lat}', '{lng}');

    function initMap() {
        const input = document.getElementById("pac-input");

        directionsService = new google.maps.DirectionsService();
        directionsRenderer = new google.maps.DirectionsRenderer();
        map = new google.maps.Map(document.getElementById("map1"), {
            zoom: 5.25,
            center: {
                lat: -2.0423904,
                lng: 122.8983498
            },
            disableDefaultUI: true,
        });

        const options = {
            fields: ["formatted_address", "geometry", "name"],
            strictBounds: false,
            types: ["establishment"],
            componentRestrictions: {
                country: 'id'
            }
        };
        const autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.addListener("place_changed", () => {
            const place = autocomplete.getPlace();
            myLoc = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            origin = place.geometry.location.lat() + ',' + place.geometry.location.lng();
            calculateAndDisplayRoute(directionsService, directionsRenderer);
        })

        directionsRenderer.setMap(map);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }

    function showPosition(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        myLoc = new google.maps.LatLng(lat, lng);
        origin = lat + ',' + lng;
        calculateAndDisplayRoute(directionsService, directionsRenderer);
    }

    function calculateAndDisplayRoute(directionsService, directionsRenderer) {
        destination = new google.maps.LatLng('{lat}', '{lng}');        
        // set distance 
        var distance = getDistance(myLoc, destination);
        $('#destination').text((distance/1000).toFixed(2) + ' Km' );
        
        destination = destination.lat() + ',' + destination.lng();
        directionsService
            .route({
                origin: {
                    query: origin,
                },
                destination: {
                    query: destination,
                },
                travelMode: google.maps.TravelMode.DRIVING,
            })
            .then((response) => {
                directionsRenderer.setDirections(response);
            })
            .catch((e) => window.alert("Directions request failed due to " + status));
    }


    var rad = function(x) {
        return x * Math.PI / 180;
    };

    var getDistance = function(p1, p2) {
        var R = 6378137;
        var dLat = rad(p2.lat() - p1.lat());
        var dLong = rad(p2.lng() - p1.lng());
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };

    window.initMap = initMap;


    $(document).on('click', '.clear', function(){
        $('#pac-input').val(null);
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&libraries=places&callback=initMap"></script>