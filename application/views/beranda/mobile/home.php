

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="la la-puzzle-piece"></i>
                </span>
                <h3 class="kt-portlet__head-title">Berita Kampung</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-pricing-1 kt-pricing-1--fixed">
                <div class="kt-pricing-1__items row">
                    <?php foreach ($query_artikel as $qa) : ?>
                        <div class="kt-pricing-1__item col-lg-4">
                            <img src="<?php echo base_url('/uploads/form_artikel/' . $qa->id . '/' . $qa->image); ?>" class="img-fluid">
                            <div class="kt-pricing-1__visual">
                                <div class="w-75 ">
                                </div>
                            </div>
                            <h2 class="kt-pricing-1__subtitle"><?php echo $qa->nama_artikel; ?></h2>
                            <span class="kt-pricing-1__description">
                                <span><?php echo word_limiter($qa->keterangan, 20); ?></span>
                            </span>
                            <div class="kt-pricing-1__btn">
                                <a href="<?php echo base_url('beranda/baca_artikel/' . $qa->id); ?>" class="btn btn-brand btn-wide btn-bold btn-upper">Selengkapnya</a>
                            </div>
                        </div>
                    <?php endforeach ?>
                    <div class="kt-pricing-1__item col-lg-4">
                        <div class="kt-pricing-1__visual">
                            <div class="kt-pricing-1__hexagon1"></div>
                            <div class="kt-pricing-1__hexagon2"></div>
                            <span class="kt-pricing-1__icon kt-font-brand"><i class="fa flaticon-piggy-bank"></i></span>
                        </div>
                        <span class="kt-pricing-1__price">69<span class="kt-pricing-1__label">$</span></span>
                        <h2 class="kt-pricing-1__subtitle">Business License</h2>
                        <span class="kt-pricing-1__description">
                            <span>Lorem ipsum dolor sit amet edipiscing elit</span>
                            <span>sed do eiusmod elpors labore et dolore</span>
                            <span>magna siad enim aliqua</span>
                        </span>
                        <div class="kt-pricing-1__btn">
                            <button type="button" class="btn btn-brand btn-wide btn-bold btn-upper">Purchase</button>
                        </div>
                    </div>
                    <div class="kt-pricing-1__item col-lg-4">
                        <div class="kt-pricing-1__visual">
                            <div class="kt-pricing-1__hexagon1"></div>
                            <div class="kt-pricing-1__hexagon2"></div>
                            <span class="kt-pricing-1__icon kt-font-brand"><i class="fa flaticon-gift"></i></span>
                        </div>
                        <span class="kt-pricing-1__price">889<span class="kt-pricing-1__label">$</span></span>
                        <h2 class="kt-pricing-1__subtitle">Enterprice License</h2>
                        <span class="kt-pricing-1__description">
                            <span>Lorem ipsum dolor sit amet edipiscing elit</span>
                            <span>sed do eiusmod elpors labore et dolore</span>
                            <span>magna siad enim aliqua</span>
                        </span>
                        <div class="kt-pricing-1__btn">
                            <button type="button" class="btn btn-brand btn-wide btn-bold btn-upper">Purchase</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>