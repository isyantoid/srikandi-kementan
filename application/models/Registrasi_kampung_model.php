<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Registrasi_kampung_model extends CI_Model {

    private $tableName = 'user';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $id = $this->uri->segment(3);
            if($id) {
                $permohonan = getRowArray('tbl_permohonan', ['id' => $id]);
                if($permohonan['nomor_registrasi'] != $this->input->post('nomor_registrasi')) {
                    $check = isDuplicate('tbl_permohonan', 'nomor_registrasi', $this->input->post('nomor_registrasi'));
                    if($check) {
                        return jsonOutputError('Nomor registrasi sudah ada!!');
                    }
                }

                if($this->input->post('no_reg_horti')) {
                    $this->db->set('no_reg_horti', $this->input->post('no_reg_horti'));
                    $this->db->set('tanggal_reg_horti', $this->input->post('tanggal_reg_horti'));
                    $this->db->set('status_reg_horti', '1');
                } else {
                    $this->db->set('no_reg_horti', Null);
                    $this->db->set('tanggal_reg_horti', Null);
                    $this->db->set('status_reg_horti', Null);
                }
                
                $this->db->set('nomor_registrasi', $this->input->post('nomor_registrasi'));
                $this->db->set('tanggal_permohonan', $this->input->post('tanggal_permohonan'));
                $this->db->set('provinsi_kode', $this->input->post('provinsi_kode'));
                $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                $this->db->set('kecamatan_kode', $this->input->post('kecamatan_kode'));
                $this->db->set('desa_kode', $this->input->post('desa_kode'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('kelompok_komoditi_id', $this->input->post('kelompok_komoditi_id'));
                $this->db->set('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'));
                $this->db->set('sub_komoditi_id', $this->input->post('sub_komoditi_id'));
                $this->db->set('tanggal_tanam', $this->input->post('tanggal_tanam'));
                $this->db->set('tanggal_perkiraan_panen', $this->input->post('tanggal_perkiraan_panen'));
                $this->db->set('luas_kebun', $this->input->post('luas_kebun'));
                $this->db->set('total_kebun', $this->input->post('total_kebun'));
                $this->db->set('tanggal_kirim_registrasi', date('Y-m-d H:i:s'));
                $this->db->where('id', $id);
                $this->db->update('tbl_permohonan');
                $id_permohonan = $id;
    
                $postKelompok = $this->input->post('kelompok_tani_nama_kelompok');
                $countKelompok = count($postKelompok);
                if($countKelompok > 0) {
                    
                    $this->db->delete('tbl_kelompok_tani', array('id_permohonan' => $id));
                    $this->db->delete('tbl_anggota_tani', array('id_permohonan' => $id));

                    for($i=0; $i<$countKelompok; $i++) {
                        $nomorHeader = $this->input->post('kelompok_tani_header')[$i];
    
                        $this->db->set('nama_kelompok',$this->input->post('kelompok_tani_nama_kelompok')[$i]);
                        $this->db->set('nama_ketua',$this->input->post('kelompok_tani_nama_ketua')[$i]);
                        $this->db->set('nik_ketua',$this->input->post('kelompok_tani_nik')[$i]);
                        $this->db->set('no_hp',$this->input->post('kelompok_tani_no_hp')[$i]);
                        $this->db->set('luas',$this->input->post('kelompok_tani_luas')[$i]);
                        if($this->input->post('kelompok_tani_terdaftar_simluhtan_'.$nomorHeader)) $this->db->set('terdaftar_simluhtan','1');
                        else $this->db->set('terdaftar_simluhtan','0');
                        $this->db->set('id_permohonan',$id_permohonan);
                        $this->db->insert('tbl_kelompok_tani');
                        $id_kelompok_tani = $this->db->insert_id();
    
                        
                        $postAnggota = $this->input->post($nomorHeader.'_nama_anggota');
                        if($postAnggota) {
                            $countAnggota = count($postAnggota);
                            if($countAnggota > 0) {
                                for($j=0; $j<$countAnggota; $j++) {
                                    $this->db->set('nama_anggota',$this->input->post($nomorHeader.'_nama_anggota')[$j]);
                                    $this->db->set('nik_anggota',$this->input->post($nomorHeader.'_nik_anggota')[$j]);
                                    $this->db->set('no_hp',$this->input->post($nomorHeader.'_hp_anggota')[$j]);
                                    $this->db->set('luas',$this->input->post($nomorHeader.'_luas_anggota')[$j]);
                                    $this->db->set('tanggal_tanam',$this->input->post($nomorHeader.'_tanggal_tanam')[$j]);
                                    $this->db->set('tanggal_panen',$this->input->post($nomorHeader.'_tanggal_panen')[$j]);
                                    $this->db->set('latitude',$this->input->post($nomorHeader.'_latitude')[$j]);
                                    $this->db->set('longitude',$this->input->post($nomorHeader.'_longitude')[$j]);
                                    $this->db->set('polygon',$this->input->post($nomorHeader.'_polygon')[$j]);
                                    $this->db->set('id_permohonan',$id_permohonan);
                                    $this->db->set('id_kelompok_tani',$id_kelompok_tani);
                                    $this->db->insert('tbl_anggota_tani');
                                }
                            }
                        }
    
                    }
                }
                return jsonOutputSuccess();
            } else {

                $check = isDuplicate('tbl_permohonan', 'nomor_registrasi', $this->input->post('nomor_registrasi'));
                if($check) {
                    return jsonOutputError('Nomor registrasi sudah ada!!');
                }

                if($this->input->post('no_reg_horti')) {
                    $this->db->set('no_reg_horti', $this->input->post('no_reg_horti'));
                    $this->db->set('tanggal_reg_horti', $this->input->post('tanggal_reg_horti'));
                    $this->db->set('status_reg_horti', '1');
                }
                
                $this->db->set('nomor_registrasi', $this->input->post('nomor_registrasi'));
                $this->db->set('tanggal_permohonan', $this->input->post('tanggal_permohonan'));
                $this->db->set('provinsi_kode', $this->input->post('provinsi_kode'));
                $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                $this->db->set('kecamatan_kode', $this->input->post('kecamatan_kode'));
                $this->db->set('desa_kode', $this->input->post('desa_kode'));
                $this->db->set('lat', $this->input->post('lat'));
                $this->db->set('lng', $this->input->post('lng'));
                $this->db->set('kelompok_komoditi_id', $this->input->post('kelompok_komoditi_id'));
                $this->db->set('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'));
                $this->db->set('sub_komoditi_id', $this->input->post('sub_komoditi_id'));
                $this->db->set('tanggal_tanam', $this->input->post('tanggal_tanam'));
                $this->db->set('tanggal_perkiraan_panen', $this->input->post('tanggal_perkiraan_panen'));
                $this->db->set('luas_kebun', $this->input->post('luas_kebun'));
                $this->db->set('total_kebun', $this->input->post('total_kebun'));
                $this->db->set('tanggal_kirim_registrasi', date('Y-m-d H:i:s'));
                $this->db->insert('tbl_permohonan');
                $id_permohonan = $this->db->insert_id();
    
                $postKelompok = $this->input->post('kelompok_tani_nama_kelompok');
                $countKelompok = count($postKelompok);
                if($countKelompok > 0) {
                    for($i=0; $i<$countKelompok; $i++) {
                        $nomorHeader = $this->input->post('kelompok_tani_header')[$i];
    
                        $this->db->set('nama_kelompok',$this->input->post('kelompok_tani_nama_kelompok')[$i]);
                        $this->db->set('nama_ketua',$this->input->post('kelompok_tani_nama_ketua')[$i]);
                        $this->db->set('nik_ketua',$this->input->post('kelompok_tani_nik')[$i]);
                        $this->db->set('no_hp',$this->input->post('kelompok_tani_no_hp')[$i]);
                        $this->db->set('luas',$this->input->post('kelompok_tani_luas')[$i]);
                        if($this->input->post('kelompok_tani_terdaftar_simluhtan_'.$nomorHeader)) $this->db->set('terdaftar_simluhtan','1');
                        else $this->db->set('terdaftar_simluhtan','0');
                        $this->db->set('id_permohonan',$id_permohonan);
                        $this->db->insert('tbl_kelompok_tani');
                        $id_kelompok_tani = $this->db->insert_id();
    
                        
                        $postAnggota = $this->input->post($nomorHeader.'_nama_anggota');
                        if($postAnggota) {
                            $countAnggota = count($postAnggota);
                            if($countAnggota > 0) {
                                for($j=0; $j<$countAnggota; $j++) {
                                    $this->db->set('nama_anggota',$this->input->post($nomorHeader.'_nama_anggota')[$j]);
                                    $this->db->set('nik_anggota',$this->input->post($nomorHeader.'_nik_anggota')[$j]);
                                    $this->db->set('no_hp',$this->input->post($nomorHeader.'_hp_anggota')[$j]);
                                    $this->db->set('luas',$this->input->post($nomorHeader.'_luas_anggota')[$j]);
                                    $this->db->set('tanggal_tanam',$this->input->post($nomorHeader.'_tanggal_tanam')[$j]);
                                    $this->db->set('tanggal_panen',$this->input->post($nomorHeader.'_tanggal_panen')[$j]);
                                    $this->db->set('latitude',$this->input->post($nomorHeader.'_latitude')[$j]);
                                    $this->db->set('longitude',$this->input->post($nomorHeader.'_longitude')[$j]);
                                    $this->db->set('polygon',$this->input->post($nomorHeader.'_polygon')[$j]);
                                    $this->db->set('id_permohonan',$id_permohonan);
                                    $this->db->set('id_kelompok_tani',$id_kelompok_tani);
                                    $this->db->insert('tbl_anggota_tani');
                                }
                            }
                        }
    
                    }
                }
                return jsonOutputSuccess();
            }
            
        }


        
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('tbl_permohonan');

        $this->db->where('id_permohonan', $id);
        $this->db->delete('tbl_kelompok_tani');

        $this->db->where('id_permohonan', $id);
        $this->db->delete('tbl_anggota_tani');
    }

    public function detail_permohonan($id_permohonan) {
        $this->db->select('
            tbl_permohonan.*,
        ');
        $this->db->where('tbl_permohonan.id', $id_permohonan);
        $this->db->from('tbl_permohonan');
        $get = $this->db->get();
        return $get->row_array();
    }

    public function get_kelompok_tani($id_permohonan) {
        $this->db->select(' tbl_kelompok_tani.*');
        $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function get_anggota_tani($id_kelompok_tani) {
        $this->db->select(' tbl_anggota_tani.*');
        $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
        $this->db->from('tbl_anggota_tani');
        $get = $this->db->get();
        return $get->result_array();
    }

    public function getRegistrasiKampung() {
        $this->db->select('
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_kelompok_tani.nama_kelompok,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_kelompok_tani.terdaftar_simluhtan,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->order_by('tbl_permohonan.nomor_registrasi ASC', 'tbl_jenis_komoditi.jenis_komoditi_nama ASC', 'tbl_permohonan.tanggal_permohonan DESC');
        $this->db->from('tbl_kelompok_tani');
        $res = $this->db->get();
        return $res->result_array();
    }
}
