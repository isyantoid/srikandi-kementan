<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
            
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Nama</label>
                    <div class="col-10">
                        <input class="form-control nama" type="text" value="" name="nama" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Username</label>
                    <div class="col-10">
                        <input class="form-control username" type="text" value="" name="username" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Email</label>
                    <div class="col-10">
                        <input class="form-control email" type="text" value="" name="email" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Password</label>
                    <div class="col-10">
                        <input class="form-control password" type="text" value="" name="password">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Level</label>
                    <div class="col-10">
                        <select class="form-control user_level_id" name="user_level_id" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row select_provinsi" hidden>
                    <label class="col-2 col-form-label">Provinsi</label>
                    <div class="col-10">
                        <select class="form-control provinsi_kode" name="provinsi_kode" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row select_kabupaten" hidden>
                    <label class="col-2 col-form-label">Kabupaten</label>
                    <div class="col-10">
                        <select class="form-control kabupaten_kode" name="kabupaten_kode" style="width:100%;"></select>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>
    ajaxSelect({
        id: '.user_level_id',
        url: '{site_url}ajax_selectbox/user_level_id',
    });

    $('.user_level_id').change(function() {
        
        var val = $(this).val();
        if (val == '1' || val == '5' || val == '6' || val == '9') {
            $('.select_provinsi').attr('hidden', true);
            $('.select_kabupaten').attr('hidden', true);
            $('.provinsi_kode').attr('disabled', true);
            $('.kabupaten_kode').attr('disabled', true);
        } else if (val == '2' || val == '10') {
            $('.select_provinsi').attr('hidden', false);
            $('.select_kabupaten').attr('hidden', true);
            ajaxSelect({
                id: '.provinsi_kode',
                url: '{site_url}ajax_selectbox/provinsi_select',
            });
            $('.provinsi_kode').attr('disabled', false);
        } else if (val == '3' || val == '11' || val == '13' || val == '14') {
            $('.select_provinsi').attr('hidden', false);
            $('.select_kabupaten').attr('hidden', false);
            $('.provinsi_kode').select2().val('').trigger('change');
            ajaxSelect({
                id: '.provinsi_kode',
                url: '{site_url}ajax_selectbox/provinsi_select',
            });
            $('.kabupaten_kode').select2({
                placeholder: 'Pilih opsi'
            });
            $('.provinsi_kode').attr('disabled', false);
            $('.kabupaten_kode').attr('disabled', false);
        }
    });

    $('.provinsi_kode').change(function() {
        var val = $(this).val();
        $('.kabupaten_kode').empty();
        ajaxSelect({
            id: '.kabupaten_kode',
            url: '{site_url}ajax_selectbox/kabupaten_select',
            optionalSearch: {
                provinsi_kode: val
            }
        });
        $('.provinsi_nama').val($(this).text());
    });

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>