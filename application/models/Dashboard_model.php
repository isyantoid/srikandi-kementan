<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Dashboard_model extends CI_Model {
    
    
    public function __construct()
    {
        parent::__construct();
        $this->provinsi_kode = $this->input->post('provinsi_kode') ? $this->input->post('provinsi_kode') : $this->session->userdata('session_provinsi_kode');
        $this->kabupaten_kode = $this->input->post('kabupaten_kode') ? $this->input->post('kabupaten_kode') : $this->session->userdata('session_kabupaten_kode');

    }


    public function get_champion_ori($tahun = '') {
        // $this->db->where('champ.provinsi_kode', $provinsiKode);
        $this->db->select('
            champ.*,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            champ_komitmen.stok,
            (select sum(jumlah) from champ_realisasi_pasar where user_id = champ_komitmen.user_id and tahun = "'.$tahun.'") as realisasi_stok,
        ');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('tbl_jenis_komoditi', 'champ.jenis_komoditi_id = tbl_jenis_komoditi.id');
        $this->db->join('champ_komitmen', 'champ.user_id = champ_komitmen.user_id', 'left');

        if(champ_user() == 'bamer') {
			$this->db->where('champ.jenis_komoditi_id', '68');
		} else if(champ_user() == 'cabai') {
			$this->db->where('champ.jenis_komoditi_id !=', '68');
		}
        
        if($tahun) $this->db->where('champ_komitmen.tahun', $tahun);
        $res = $this->db->get('champ');
        return $res->result_array();
    }
    
    public function get_champion_komitmen_stok($tahun, $champId) {
        $this->db->select('
        COALESCE(champ_komitmen.stok,0) as stok,
        ');
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->where('champ_komitmen.champ_id', $champId);
        $res = $this->db->get('champ_komitmen');
        return $res->row_array();
    }
    
    public function get_champion_komitmen_stok_cabai($tahun, $champId, $jenis_komoditi_id) {
        $this->db->select('
        COALESCE(champ_komitmen.stok,0) as stok,
        ');
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->where('champ_komitmen.champ_id', $champId);
        $this->db->where('champ_komitmen.jenis_komoditi_id', $jenis_komoditi_id);
        $res = $this->db->get('champ_komitmen');
        return $res->row_array();
    }
    
    public function get_champion_saldo_stok($tahun, $champId) {
        $this->db->select_sum('champ_realisasi_pasar.jumlah', 'jumlah');
        
        if($tahun) $this->db->where('champ_realisasi_pasar.tahun', $tahun);
        if($tahun) $this->db->where('champ_realisasi_pasar.champ_id', $champId);
        $res = $this->db->get('champ_realisasi_pasar');
        return $res->row_array();
    }
    
    public function get_champion_saldo_stok_cabai($tahun, $champId, $jenis_komoditi_id) {
        $this->db->select_sum('champ_realisasi_pasar.jumlah', 'jumlah');
        
        if($tahun) $this->db->where('champ_realisasi_pasar.tahun', $tahun);
        if($tahun) $this->db->where('champ_realisasi_pasar.champ_id', $champId);
        if($tahun) $this->db->where('champ_realisasi_pasar.jenis_komoditi_id', $jenis_komoditi_id);
        $res = $this->db->get('champ_realisasi_pasar');
        return $res->row_array();
    }
    
    public function get_champion($tahun = '') {
        // $this->db->where('champ.provinsi_kode', $provinsiKode);
        $this->db->select('
            champ.*,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            0 as stok,
            0 as realisasi_stok,
        ');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('tbl_jenis_komoditi', 'champ.jenis_komoditi_id = tbl_jenis_komoditi.id');
        $this->db->join('champ_komitmen', 'champ.user_id = champ_komitmen.user_id', 'left');

        // if(champ_user() == 'bamer') {
		// 	$this->db->where('champ.jenis_komoditi_id', '68');
		// } else if(champ_user() == 'cabai') {
        //     $this->db->where('champ.jenis_komoditi_id !=', '68');
		// }
        $this->db->where('champ.jenis_komoditi_id', '68');
        
        // if($tahun) $this->db->where('champ_komitmen.tahun', $tahun);
        // $this->db->group_by('champ.id');
        // $this->db->group_by('champ_komitmen.jenis_komoditi_id');
        $res = $this->db->get('champ');
        return $res->result_array();
    }
    
    public function get_champion_cabai($tahun = '') {
        $this->db->select('
            champ.*,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi2.jenis_komoditi_nama as jenis_komoditi_nama2,
            0 as stok,
            0 as realisasi_stok,
        ');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('tbl_jenis_komoditi', 'champ.jenis_komoditi_id = tbl_jenis_komoditi.id');
        $this->db->join('tbl_jenis_komoditi as tbl_jenis_komoditi2', 'champ.jenis_komoditi_kedua_id = tbl_jenis_komoditi2.id', 'left');
        $this->db->join('champ_komitmen', 'champ.user_id = champ_komitmen.user_id', 'left');

        $this->db->where('champ.jenis_komoditi_id !=', '68');
        $this->db->group_by('champ.id');
        $res = $this->db->get('champ');
        return $res->result_array();
    }
    
    
    
    public function get_count_alokasi($provinsiKode) {
        $this->db->where('tbl_registrasi_p2l.provinsi_kode', $provinsiKode);
        $this->db->from('tbl_registrasi_p2l');
        $res = $this->db->get()->num_rows();
        return $res;
    }
    


    public function chart_1() {
        $this->db->select('
            tbl_bantuan.jenis_komoditi_kode,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            sum(tbl_bantuan.total_nilai_bantuan) as total_nilai_bantuan
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_bantuan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        if($this->input->get('tahun')) $this->db->where('YEAR(`tbl_bantuan`.`tanggal_kirim`)', $this->input->get('tahun'));
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_bantuan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_bantuan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        $this->db->group_by('tbl_bantuan.jenis_komoditi_kode');
        $get = $this->db->get('tbl_bantuan');
        return $get->result_array();
    }
    
    public function chart_2() {
        $this->db->select('
        tbl_jenis_bantuan.nama_bantuan,
        sum(tbl_bantuan_detail.total) as total_nilai_bantuan
        ');
        $this->db->join('tbl_jenis_bantuan','tbl_bantuan_detail.jenis_bantuan_id = tbl_jenis_bantuan.id', 'left');
        $this->db->join('tbl_bantuan','tbl_bantuan_detail.bantuan_id = tbl_bantuan.id', 'left');
        if($this->input->get('tahun')) $this->db->where('YEAR(`tbl_bantuan`.`tanggal_kirim`)', $this->input->get('tahun'));
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_bantuan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_bantuan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        $this->db->group_by('tbl_bantuan_detail.jenis_bantuan_id');
        $get = $this->db->get('tbl_bantuan_detail');
        return $get->result_array();
    }
    
    public function chart_3() {
        $this->db->select('
            tbl_bantuan.kabupaten_nama,
            sum(tbl_bantuan.total_nilai_bantuan) as total_nilai_bantuan
        ');
        if($this->input->get('tahun')) $this->db->where('YEAR(`tbl_bantuan`.`tanggal_kirim`)', $this->input->get('tahun'));
        if($this->input->get('provinsi_kode')) $this->db->where('tbl_bantuan.provinsi_kode', $this->input->get('provinsi_kode'));
        if($this->input->get('kabupaten_kode')) $this->db->where('tbl_bantuan.kabupaten_kode', $this->input->get('kabupaten_kode'));
        $this->db->group_by('tbl_bantuan.kabupaten_kode');
        $get = $this->db->get('tbl_bantuan');
        return $get->result_array();
    }

    public function list_index_komoditi_dashboard($tahun) {

        $totalLuas = "(select sum(luas_kebun) from tbl_permohonan ";
        $totalLuas .= "where tbl_permohonan.app_id = 2 and tbl_permohonan.jenis_komoditi_id = tbl_head.jenis_komoditi_id ";
        if($tahun) {
            $totalLuas .= "and year(tbl_permohonan.tanggal_permohonan) = " . $tahun . " ";
        }
        if($this->provinsi_kode) {
            $totalLuas .= "and tbl_permohonan.provinsi_kode = " . $this->provinsi_kode . " ";
        }
        if($this->kabupaten_kode) {
            $totalLuas .= "and tbl_permohonan.kabupaten_kode = " . $this->kabupaten_kode . " ";
        }
        $totalLuas .= "and tbl_permohonan.jenis_komoditi_id != ''";
        $totalLuas .= " ) as total_luas,";

        $totalKampung = "(select count(*) from tbl_permohonan where tbl_permohonan.app_id = 2 and tbl_permohonan.jenis_komoditi_id = tbl_head.jenis_komoditi_id ";
        if($tahun) {
            $totalKampung .= "and year(tbl_permohonan.tanggal_permohonan) = " . $tahun . " ";
        }
        if($this->provinsi_kode) {
            $totalKampung .= "and tbl_permohonan.provinsi_kode = " . $this->provinsi_kode . " ";
        }
        if($this->kabupaten_kode) {
            $totalKampung .= "and tbl_permohonan.kabupaten_kode = " . $this->kabupaten_kode . " ";
        }
        $totalKampung .= "and tbl_permohonan.jenis_komoditi_id != ''";
        $totalKampung .= " ) as total_kampung,";
        
        
        $totalKelompok = "(select id from tbl_permohonan where tbl_permohonan.app_id = 2 and tbl_permohonan.jenis_komoditi_id = tbl_head.jenis_komoditi_id ";
        if($tahun) {
            $totalKelompok .= "and year(tbl_permohonan.tanggal_permohonan) = " . $tahun . " ";
        }
        if($this->provinsi_kode) {
            $totalKelompok .= "and tbl_permohonan.provinsi_kode = " . $this->provinsi_kode . " ";
        }
        if($this->kabupaten_kode) {
            $totalKelompok .= "and tbl_permohonan.kabupaten_kode = " . $this->kabupaten_kode . " ";
        }
        $totalKelompok .= "and tbl_permohonan.jenis_komoditi_id != ''";
        $totalKelompok .= " )";

        $this->db->select('
        tbl_head.kelompok_komoditi_nama,
        tbl_head.jenis_komoditi_kode,
        tbl_head.jenis_komoditi_nama,
        tbl_jenis_komoditi.jenis_komoditi_icon,
        tbl_jenis_komoditi.id as jenis_komoditi_id,
        '.$totalKampung.'
        '.$totalLuas.'
        (select count(*) from tbl_kelompok_tani where id_permohonan in '.$totalKelompok.') as total_kelompok,
        (select count(*) from tbl_anggota_tani where id_permohonan = tbl_head.id ) as total_anggota
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_head.jenis_komoditi_id = tbl_jenis_komoditi.id');
        if($this->provinsi_kode) $this->db->where('tbl_head.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_head.kabupaten_kode', $this->kabupaten_kode);
        $this->db->where('year(tbl_head.tanggal_permohonan)', $tahun, FALSE);
        $this->db->where('tbl_head.app_id', '2');
        $this->db->group_by('tbl_head.jenis_komoditi_id');
        $get = $this->db->get('tbl_permohonan tbl_head');
        return $get->result_array();
    }

    public function total_jumlah_kampung() {
        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);
        if($this->input->post('tanggal_awal')) $this->db->where('tbl_permohonan.tanggal_permohonan >=', $this->input->post('tanggal_awal'));
        if($this->input->post('tanggal_akhir')) $this->db->where('tbl_permohonan.tanggal_permohonan <=', $this->input->post('tanggal_akhir'));
        $this->db->where('tbl_permohonan.app_id', '2');
        return $this->db->get('tbl_permohonan')->result_array();
    }

    public function total_kampung_indo($tahun) {
        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);
        $this->db->where('year(tbl_permohonan.tanggal_permohonan)', $tahun, FALSE);
        $this->db->where('tbl_permohonan.jenis_komoditi_id !=', null);
        $this->db->where('tbl_permohonan.app_id', '2');
        return $this->db->get('tbl_permohonan')->num_rows();
    }


    public function total_kelompok_tani_detail() {
        $this->db->select('tbl_kelompok_tani.*, tbl_permohonan.nomor_registrasi');
        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);
        if($this->input->post('tanggal_awal')) $this->db->where('tbl_permohonan.tanggal_permohonan >=', $this->input->post('tanggal_awal'));
        if($this->input->post('tanggal_akhir')) $this->db->where('tbl_permohonan.tanggal_permohonan <=', $this->input->post('tanggal_akhir'));
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->where('tbl_permohonan.app_id', '2');
        return $this->db->get('tbl_kelompok_tani')->result_array();
    }
    
    public function total_kelompok_tani_indo($tahun) {
        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->where('tbl_permohonan.jenis_komoditi_id !=', null);
        $this->db->where('year(tbl_permohonan.tanggal_permohonan)', $tahun, FALSE);
        $this->db->where('tbl_permohonan.app_id', '2');
        return $this->db->get('tbl_kelompok_tani')->num_rows();
    }
    
    public function total_luas_indo($tahun) {
        $this->db->select_sum('luas', 'total_luas');
        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);
        $this->db->join('tbl_permohonan', 'tbl_kelompok_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->where('tbl_permohonan.jenis_komoditi_id !=', null);
        $this->db->where('year(tbl_permohonan.tanggal_permohonan)', $tahun, FALSE);
        $this->db->where('tbl_permohonan.app_id', '2');
        $get = $this->db->get('tbl_kelompok_tani');
        return $get->row()->total_luas;
    }

    public function get_lat_lng_permohonan() {
        $this->db->select('
            tbl_permohonan.id,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
            tbl_permohonan.desa_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_pin,
            (select COALESCE(sum(total_nilai_bantuan),0) from tbl_bantuan where permohonan_id = tbl_permohonan.id) total_nilai_bantuan,
            (select count(*) from tbl_kelompok_tani where id_permohonan = tbl_permohonan.id) total_kelompok_tani,
            (select count(*) from tbl_anggota_tani where id_permohonan = tbl_permohonan.id) total_anggota_tani,
            coalesce((select nilai_rating_persen from tbl_penilaian where permohonan_id = tbl_permohonan.id), 0) as nilai_rating_persen,
        ');
        $this->db->where('lat !=', null);
        $this->db->or_where('lat !=', 0);
        $this->db->where('lng !=', null);
        $this->db->or_where('lng !=', 0);

        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->where('tbl_permohonan.app_id', '2');
        $get = $this->db->get('tbl_permohonan');
        return $get->result_array();
    }
    
    public function get_lat_lng_anggota($id_permohonan) {
        $this->db->select('
            tbl_anggota_tani.id,
            tbl_anggota_tani.nama_anggota,
            tbl_anggota_tani.nik_anggota,
            tbl_anggota_tani.no_hp,
            tbl_anggota_tani.luas,
            tbl_anggota_tani.latitude,
            tbl_anggota_tani.longitude,
            tbl_anggota_tani.polygon,
            tbl_anggota_tani.tanggal_tanam,
            tbl_anggota_tani.tanggal_panen,
            tbl_anggota_tani.id_permohonan,
            tbl_anggota_tani.id_kelompok_tani,
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');
        $this->db->join('tbl_permohonan','tbl_anggota_tani.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->where('id_permohonan', $id_permohonan);
        $this->db->where('tbl_permohonan.app_id', '2');
        $get = $this->db->get('tbl_anggota_tani');
        return $get->result_array();
    }


    public function detail_permohonan($id_permohonan) {
        $this->db->select('
            tbl_permohonan.*,
            tbl_kelompok_komoditi.kelompok_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_sub_komoditi.sub_komoditi_nama,
            (select COALESCE(sum(total_nilai_bantuan),0) from tbl_bantuan where permohonan_id = tbl_permohonan.id) total_nilai_bantuan,
            (select count(*) from tbl_kelompok_tani where id_permohonan = tbl_permohonan.id) total_kelompok_tani,
            (select count(*) from tbl_anggota_tani where id_permohonan = tbl_permohonan.id) total_anggota_tani
        ');
        $this->db->join('tbl_kelompok_komoditi','tbl_permohonan.kelompok_komoditi_id = tbl_kelompok_komoditi.id', 'left');
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->join('tbl_sub_komoditi','tbl_permohonan.sub_komoditi_id = tbl_sub_komoditi.id', 'left');
        $this->db->where('tbl_permohonan.id', $id_permohonan);
        $this->db->where('tbl_permohonan.app_id', '2');
        $this->db->from('tbl_permohonan');
        $get = $this->db->get();
        return $get->row_array();
    }
    
    public function list_bantuan($permohonan_id) {
        $this->db->select('
            tbl_bantuan.*,
            tbl_tipe_bantuan.tipe_bantuan,
            tbl_kelompok_tani.nama_kelompok
        ');
        $this->db->join('tbl_tipe_bantuan','tbl_bantuan.tipe_bantuan_id = tbl_tipe_bantuan.id', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_bantuan.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_bantuan.permohonan_id', $permohonan_id);
        $this->db->from('tbl_bantuan');
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function list_dokumentasi($permohonan_id, $tipe) {
        $this->db->select('
            tbl_dokumentasi.*,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->db->join('tbl_kelompok_tani','tbl_dokumentasi.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_dokumentasi.permohonan_id', $permohonan_id);
        $this->db->where('tbl_dokumentasi.tipe', $tipe);
        $this->db->from('tbl_dokumentasi');
        $get = $this->db->get();
        return $get->result_array();
    }


    public function get_penilaian_kampung() {

    }

    public function total_nilai_bantuan($id_permohonan) {
        $str_query = "select
        sum(total_nilai_bantuan) as total_nilai_bantuan
        from tbl_bantuan
        where kelompok_tani_id in (select id from tbl_kelompok_tani where id_permohonan = " . $id_permohonan . ")";
        return $this->db->query($str_query)->row_array();
    }


    public function view_dokumentasi_tanam() {
        $this->db->select('
            tbl_dokumentasi.provinsi_nama,
            tbl_dokumentasi.kabupaten_nama,
            tbl_dokumentasi.kecamatan_nama,
            tbl_dokumentasi.desa_nama,
            tbl_dokumentasi.jenis_komoditi_kode,
            tbl_dokumentasi.tanggal_tanam,
            tbl_dokumentasi.luas_tanam,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_dokumentasi.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_dokumentasi.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_dokumentasi.tipe','1');
        if($this->input->post('tanggal_awal')) $this->db->where('tbl_dokumentasi.tanggal_tanam >=', $this->input->post('tanggal_awal'));
        if($this->input->post('tanggal_akhir')) $this->db->where('tbl_dokumentasi.tanggal_tanam <=', $this->input->post('tanggal_akhir'));
        $this->db->order_by('tbl_dokumentasi.jenis_komoditi_kode','desc');
        $get = $this->db->get('tbl_dokumentasi');
        return $get->result_array();
    }


    public function view_dokumentasi_perkiraan_panen() {
        $this->db->select('
            tbl_dokumentasi.provinsi_nama,
            tbl_dokumentasi.kabupaten_nama,
            tbl_dokumentasi.kecamatan_nama,
            tbl_dokumentasi.desa_nama,
            tbl_dokumentasi.jenis_komoditi_kode,
            tbl_dokumentasi.tanggal_tanam,
            tbl_dokumentasi.luas_tanam,
            tbl_dokumentasi.tanggal_perkiraan_panen,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_dokumentasi.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_dokumentasi.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_dokumentasi.tipe','1');
        if($this->input->post('tanggal_awal')) $this->db->where('tbl_dokumentasi.tanggal_tanam >=', $this->input->post('tanggal_awal'));
        if($this->input->post('tanggal_akhir')) $this->db->where('tbl_dokumentasi.tanggal_tanam <=', $this->input->post('tanggal_akhir'));
        $this->db->order_by('tbl_dokumentasi.jenis_komoditi_kode','desc');
        $get = $this->db->get('tbl_dokumentasi');
        $resulst = $get->result_array();
        if($resulst) {
            foreach($resulst as $row) {
                $item = array();
                $arr_tanggal_perkiraan_panen = json_decode($row['tanggal_perkiraan_panen']);
                $count_arr_perkiraan_panen = count($arr_tanggal_perkiraan_panen);
                if($count_arr_perkiraan_panen > 0) {
                    for($i=0; $i<$count_arr_perkiraan_panen; $i++) {
                        $item['jenis_komoditi_nama'] = $row['jenis_komoditi_nama'];
                        $item['tanggal_perkiraan_panen'] = $arr_tanggal_perkiraan_panen[$i];
                        $item['provinsi_nama'] = $row['provinsi_nama'];
                        $item['kabupaten_nama'] = $row['kabupaten_nama'];
                        $item['kecamatan_nama'] = $row['kecamatan_nama'];
                        $item['desa_nama'] = $row['desa_nama'];
                        $item['nama_kelompok'] = $row['nama_kelompok'];
                        $data[] = $item;
                    }
                }
            }
            return $data;
        }
    }
    
    public function view_dokumentasi_panen() {
        $this->db->select('
            tbl_dokumentasi.provinsi_nama,
            tbl_dokumentasi.kabupaten_nama,
            tbl_dokumentasi.kecamatan_nama,
            tbl_dokumentasi.desa_nama,
            tbl_dokumentasi.jenis_komoditi_kode,
            tbl_dokumentasi.tanggal_tanam,
            tbl_dokumentasi.tanggal_panen,
            tbl_dokumentasi.luas_tanam,
            tbl_dokumentasi.luas_panen,
            tbl_dokumentasi.jumlah_hasil_panen,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_dokumentasi.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_dokumentasi.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_dokumentasi.tipe','2');
        if($this->input->post('tanggal_awal')) $this->db->where('tbl_dokumentasi.tanggal_panen >=', $this->input->post('tanggal_awal'));
        if($this->input->post('tanggal_akhir')) $this->db->where('tbl_dokumentasi.tanggal_panen <=', $this->input->post('tanggal_akhir'));
        $this->db->order_by('tbl_dokumentasi.jenis_komoditi_kode','desc');
        $get = $this->db->get('tbl_dokumentasi');
        return $get->result_array();
    }

    public function detail_kampung($tahun, $jenis_komoditi_id) {
        $this->db->select('
            tbl_permohonan.*,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_provinsi.nama as provinsi_nama,
            tbl_kabupaten.nama as kabupaten_nama,
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->join('tbl_provinsi','tbl_permohonan.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten','tbl_permohonan.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->where('year(tbl_permohonan.tanggal_permohonan)', $tahun);
        $this->db->where('tbl_permohonan.jenis_komoditi_id', $jenis_komoditi_id);
        $this->db->where('tbl_permohonan.app_id', '2');
        $this->db->from('tbl_permohonan');
        $get = $this->db->get();
        return $get->result_array();
    }

}

/* End of file Dashboard_model.php */
