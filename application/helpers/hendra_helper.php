<?php
defined('BASEPATH') or exit('No direct script access allowed');

function count_kampung($key1, $key2)
{
    $ci = &get_instance();

    $ci->db->where(
        array(
            'tbl_jenis_komoditi.jenis_komoditi_nama' => $key1,
            'tbl_permohonan.provinsi_nama' => $key2
        )
    );
    $ci->db->order_by('tbl_permohonan.provinsi_nama', 'asc');
    $ci->db->join('tbl_kelompok_komoditi', 'tbl_permohonan.kelompok_komoditi_kode = tbl_kelompok_komoditi.kelompok_komoditi_kode', 'left');
    $ci->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
    $ci->db->join('tbl_sub_komoditi', 'tbl_permohonan.sub_komoditi_kode = tbl_sub_komoditi.sub_komoditi_kode', 'left');
    $ci->db->from('tbl_permohonan');
    return $ci->db->get()->num_rows();
}

function count_luas($key1, $key2)
{
    $ci = &get_instance();

    $ci->db->select('sum(tbl_permohonan.luas_kebun) as luas_kebun');

    $ci->db->where(
        array(
            'tbl_jenis_komoditi.jenis_komoditi_nama' => $key1,
            'tbl_permohonan.provinsi_nama' => $key2
        )
    );
    $ci->db->order_by('tbl_permohonan.provinsi_nama', 'asc');
    $ci->db->join('tbl_kelompok_komoditi', 'tbl_permohonan.kelompok_komoditi_kode = tbl_kelompok_komoditi.kelompok_komoditi_kode', 'left');
    $ci->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
    $ci->db->join('tbl_sub_komoditi', 'tbl_permohonan.sub_komoditi_kode = tbl_sub_komoditi.sub_komoditi_kode', 'left');
    $ci->db->from('tbl_permohonan');

    $query = $ci->db->get()->row();

    if (!empty($query)) :
        return $query->luas_kebun;
    else :
        return 0;
    endif;
}