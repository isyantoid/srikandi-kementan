/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 01/06/2023 20:53:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_target_pencapaian
-- ----------------------------
DROP TABLE IF EXISTS `tbl_target_pencapaian`;
CREATE TABLE `tbl_target_pencapaian`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `jenis_komoditi_id` int NULL DEFAULT NULL,
  `jenis_komoditi_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_target_pencapaian
-- ----------------------------
INSERT INTO `tbl_target_pencapaian` VALUES (1, 68, 'Bawang Merah', '2023', 20);
INSERT INTO `tbl_target_pencapaian` VALUES (2, 70, 'Bawang Putih', '2023', 50);

-- ----------------------------
-- Triggers structure for table tbl_target_pencapaian
-- ----------------------------
DROP TRIGGER IF EXISTS `bi_target_pencapaian`;
delimiter ;;
CREATE TRIGGER `bi_target_pencapaian` BEFORE INSERT ON `tbl_target_pencapaian` FOR EACH ROW begin
	set new.jenis_komoditi_nama = (select jenis_komoditi_nama from tbl_jenis_komoditi where id = new.jenis_komoditi_id);
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table tbl_target_pencapaian
-- ----------------------------
DROP TRIGGER IF EXISTS `bu_target_pencapaian`;
delimiter ;;
CREATE TRIGGER `bu_target_pencapaian` BEFORE UPDATE ON `tbl_target_pencapaian` FOR EACH ROW begin
	set new.jenis_komoditi_nama = (select jenis_komoditi_nama from tbl_jenis_komoditi where id = new.jenis_komoditi_id);
end
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
