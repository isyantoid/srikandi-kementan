<div class="kt-portlet" data-ktportlet="true" id="reload">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title"></h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <a href="javascript:(0)" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md" onclick="filter()"><i class="la la-undo"></i></a>
                <a href="#" data-ktportlet-tool="reload" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md reload"><i class="la la-refresh"></i></a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-7">
                <?php if($array_polygon): ?>
                    <div id="kt_gmap_1" style="height:580px;"></div>
                <?php else: ?>
                    <p class="text-danger">Tidak ada data poligon yang dapat ditampilkan.</p>
                <?php endif ?>
            </div>
            <div class="col-md-5">
                <div class="alert alert-warning">
                    <div class="alert-text">
                        <p class="m-0 small">Tanggal registrasi : <?php echo formatTanggal($tanggal_permohonan) ?></p>
                        <p class="m-0 small">Nomor registrasi : {nomor_registrasi}</p>
                        <p class="m-0 small">Provinsi : {provinsi_nama}</p>
                        <p class="m-0 small">Kabupaten : {kabupaten_nama}</p>
                        <p class="m-0 small">Kecamatan : {kecamatan_nama}</p>
                        <p class="m-0 small">Desa : {desa_nama}</p>
                        <p class="m-0 small">Total luas : {luas_kebun} Ha</p>
                        <p class="m-0 small">Total kelompok : {total_kelompok_tani} kelompok</p>
                        <p class="m-0 small">Total petani : {total_anggota_tani} petani</p>
                        <p class="m-0 small">Total bantuan yang diterima : Rp. <?php echo number_format($total_nilai_bantuan) ?></p>
                    </div>
                </div>
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md m-1"></div>
                <p class="font-weight-bold">PENERIMA MANFAAT APBN TAHUN:</p>
                <?php $bantuan = $this->model->list_bantuan($id) ?>
                
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped small">
                        <thead class="bg-success text-white">
                            <th class="p-1 text-left">Tanggal</th>
                            <th class="p-1 text-left">Nama Kelompok</th>
                            <th class="p-1 text-right">Nilai Bantuan</th>
                        </thead>
                        <tbody>
                            <?php if($bantuan): ?>
                                <?php $no=1; foreach($bantuan as $b) : ?>
                                    <tr>
                                        <td class="p-1 text-left"><?php echo formatTanggal($b['tanggal_kirim']) ?></td>
                                        <td class="p-1 text-left"><?php echo $b['nama_kelompok'] ?></td>
                                        <td class="p-1 text-right">Rp. <?php echo number_format($b['total_nilai_bantuan']) ?></td>
                                    </tr>
                                    <?php $no++ ?>
                                <?php endforeach ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="3" class="p-1 text-center text-danger">Tidak ada bantuan</td>
                                </tr>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
                
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md m-1"></div>
                <p class="font-weight-bold">DOKUMENTASI TANAM</p>
                <?php $dokumentasi_tanam = $this->model->list_dokumentasi($id, '1') ?>
                
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped small">
                        <thead class="bg-success text-white">
                            <th class="p-1 text-center">Foto</th>
                            <th class="p-1 text-left">Tgl. Tanam</th>
                            <th class="p-1 text-left">Nama kelompok</th>
                            <th class="p-1 text-right">Luas tanam</th>
                        </thead>
                        <tbody>
                            <?php if($dokumentasi_tanam): ?>
                                <?php $no=1; foreach($dokumentasi_tanam as $b) : ?>
                                    <tr>
                                        <td class="p-1 text-center"><img src="<?php echo getImage('dokumentasi/'.$b['foto_tanam']) ?>" height="32"></td>
                                        <td class="p-1 text-left"><?php echo formatTanggal($b['tanggal_tanam']) ?></td>
                                        <td class="p-1 text-left"><?php echo $b['nama_kelompok'] ?></td>
                                        <td class="p-1 text-right"><?php echo $b['luas_tanam'] ?>Ha</td>
                                    </tr>
                                    <?php $no++ ?>
                                <?php endforeach ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="5" class="p-1 text-center text-danger">Tidak ada dokumentasi tanam</td>
                                </tr>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
                
                <div class="kt-separator kt-separator--border-dashed kt-separator--space-md m-1"></div>
                <p class="font-weight-bold">DOKUMENTASI PANEN</p>
                <?php $dokumentasi_tanam = $this->model->list_dokumentasi($id, '2') ?>
                
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped small">
                        <thead class="bg-success text-white">
                            <th class="p-1 text-center">Foto</th>
                            <th class="p-1 text-left">Tgl. Panen</th>
                            <th class="p-1 text-left">Nama kelompok</th>
                            <th class="p-1 text-right">Luas panen</th>
                            <th class="p-1 text-right">Jumlah hasil panen</th>
                        </thead>
                        <tbody>
                            <?php if($dokumentasi_tanam): ?>
                                <?php $no=1; foreach($dokumentasi_tanam as $b) : ?>
                                    <tr>
                                        <td class="p-1 text-center"><img src="<?php echo getImage('dokumentasi/'.$b['foto_panen']) ?>" height="32"></td>
                                        <td class="p-1 text-left"><?php echo formatTanggal($b['tanggal_panen']) ?></td>
                                        <td class="p-1 text-left"><?php echo $b['nama_kelompok'] ?></td>
                                        <td class="p-1 text-right"><?php echo $b['luas_panen'] ?>Ha</td>
                                        <td class="p-1 text-right"><?php echo $b['jumlah_hasil_panen'] ?></td>
                                    </tr>
                                    <?php $no++ ?>
                                <?php endforeach ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="6" class="p-1 text-center text-danger">Tidak ada dokumentasi panen</td>
                                </tr>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap&v=weekly&channel=2"></script>
<script>
    var reload = new KTPortlet('reload');
    reload.on('reload', function(portlet) {
        map_pusat_detail('{id}');
    });

    var map;
    var src = 'https://srikandi.saestusemesta.com/file3.kml';

    function initMap() {
        
        const image = {
            url: "http://maps.google.com/mapfiles/ms/icons/blue.png",
            size: new google.maps.Size(32, 32),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 32),
        };

        map = new google.maps.Map(document.getElementById('kt_gmap_1'), {
            center: new google.maps.LatLng('{lat}', '{lng}'),
            zoom: 12,
        });

        $.ajax({
            url: '{site_url}dashboard/map_pusat_detail_polygon/{id}',
            dataType: 'json',
            success: function(data) {
                lengthData = data.length;


                pathCoordinates = [];
                var infowindow = new google.maps.InfoWindow();
                for(i=0; i<lengthData; i++) {
                    pathCoordinates.push(data[i].coordinates);

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(data[i].latitude, data[i].longitude),
                        map: map,
                        icon: image
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            var row = data[i];
                            const contentString = `
                            <div class="bg-info text-white">
                            <p class="m-0 small">Nama anggota: `+row.nama_anggota+`</p>
                            <p class="m-0 small">Luas lahan: `+row.luas+`Ha</p>
                            <p class="m-0 small">Komoditas: `+row.jenis_komoditi_nama+`</p>
                            <p class="m-0 small">Tanggal tanam: `+row.tanggal_tanam+`</p>
                            <p class="m-0 small">Tanggal panen: `+row.tanggal_panen+`</p>
                            </div>`;
                            infowindow.setContent(contentString);
                            infowindow.setOptions({maxWidth: 300});
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }

                const bermudaTriangle = new google.maps.Polygon({
                    paths: pathCoordinates,
                    strokeColor: "#000000",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#FF0000",
                    fillOpacity: 0.35,
                });
                bermudaTriangle.setMap(map);


                google.maps.event.addListener(bermudaTriangle, 'click', function (event) {
                    console.log(this.getPath())
                }); 
                
            }
        });

        // var kmlLayer = new google.maps.KmlLayer(src, {
        //     suppressInfoWindows: true,
        //     preserveViewport: false,
        //     map: map
        // });

        

        // var infowindow = new google.maps.InfoWindow();

        // kmlLayer.addListener('click', function(event) {
        //     var content = event.featureData.infoWindowHtml;
        //     infowindow.setContent(content);
        //     infowindow.setPosition(event.latLng);
        //     infowindow.open(map);
        // });

        // google.maps.event.addListener(polygon, 'click', function (event) {
        //     alert(this.indexID);
        // }); 
    }

    // function initMap() {
    //     const map = new google.maps.Map(document.getElementById("kt_gmap_1"), {
    //         zoom: 5.25,
    //         center: {
    //             lat: -2.0423904,
    //             lng: 122.8983498
    //         },
    //     });

    //     const triangleCoords = [{
    //             lat: -6.378124038876082,
    //             lng: 107.18216340154149
    //         },
    //         {
    //             lat: -6.918288391034365,
    //             lng: 106.89377231168342
    //         },
    //         {
    //             lat: -7.357060135211541,
    //             lng: 108.26431663396131
    //         },
    //         {
    //             lat: -6.353557215154645,
    //             lng: 108.53897481477854
    //         },
    //     ];


    //     const bermudaTriangle = new google.maps.Polygon({
    //         paths: triangleCoords,
    //         strokeColor: "#FF0000",
    //         strokeOpacity: 0.8,
    //         strokeWeight: 2,
    //         fillColor: "#FF0000",
    //         fillOpacity: 0.35,
    //     });

    //     bermudaTriangle.setMap(map);
    // }

    // var map = new GMaps({
    //     div: '#kt_gmap_1',
    //     lat: -6.8678439,
    //     lng: 107.605137,
    //     zoom: 8.5,
    // });

    // var path = [
    //     [-6.378124038876082, 107.18216340154149],
    //     [-6.918288391034365, 106.89377231168342],
    //     [-7.357060135211541, 108.26431663396131],
    //     [-6.749210167595442, 108.53897481477854],
    //     [-6.353557215154645, 108.31650168831659],
    // ];

    // var polygon = map.drawPolygon({
    //     paths: path,
    //     strokeColor: '#000',
    //     strokeOpacity: 0.8,
    //     strokeWeight: 1,
    //     fillColor: '#f44336',
    //     fillOpacity: 0.6
    // });


    // var content = `
    //     <table class="table table-borderless">
    //         <tr>
    //             <td>Kampung</td>
    //             <td>Durian Sukowangi</td>
    //         </tr>
    //         <tr>
    //             <td>Provinsi</td>
    //             <td>Jawa Barat</td>
    //         </tr>
    //         <tr>
    //             <td>Kabupaten</td>
    //             <td>Kunignan</td>
    //         </tr>
    //         <tr>
    //             <td>Kecamatan</td>
    //             <td>Kadugede</td>
    //         </tr>
    //         <tr>
    //             <td>Desa</td>
    //             <td>Babatan</td>
    //         </tr>
    //         <tr>
    //             <td>Total Luas</td>
    //             <td>1 Hektar</td>
    //         </tr>
    //         <tr>
    //             <td>Total kelompok tani</td>
    //             <td>23</td>
    //         </tr>
    //         <tr>
    //             <td>Total Anggota Tani</td>
    //             <td>45</td>
    //         </tr>
    //     </table>
    //     <div class="mt-3">
    //         <a href="javascript:map_pusat_detail()" class="btn btn-link">Lihat Detail</a>
    //     </div>
    // `;

    // map.addMarker({
    //     lat: -6.378124038876082,
    //     lng: 107.18216340154149,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.918288391034365,
    //     lng: 106.89377231168342,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -7.357060135211541,
    //     lng: 108.26431663396131,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.749210167595442,
    //     lng: 108.53897481477854,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.353557215154645,
    //     lng: 108.31650168831659,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
    // map.addMarker({
    //     lat: -6.5637016707619615,
    //     lng: 107.75070583583313,
    //     infoWindow: {
    //         content: content
    //     },
    //     icon: {
    //         url: '{dist_path}images/pin/3.png',
    //         scaledSize: new google.maps.Size(25, 25),
    //     },
    // });
</script>