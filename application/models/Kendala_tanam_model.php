<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kendala_tanam_model extends CI_Model {

    
    public function save()
    {
        if ($this->input->is_ajax_request()) {

            $id = $this->uri->segment(3);
            if($id) {
                $this->db->set('id_permohonan', $this->input->post('nomor_registrasi'));
                $this->db->set('id_kelompok_tani', $this->input->post('id_kelompok_tani'));
                $this->db->set('tanggal_kendala_tanam', $this->input->post('tanggal_kendala_tanam'));
                $this->db->set('luas_lahan', $this->input->post('luas_lahan_terkendala'));
                $this->db->set('keterangan', $this->input->post('keterangan'));
                $this->db->set('file', $this->input->post('file_txt'));
    
                $this->db->where('tbl_kendala_tanam.id', $id);
                $this->db->update('tbl_kendala_tanam');
                return jsonOutputSuccess();
            } else {
                $this->db->set('id_permohonan', $this->input->post('nomor_registrasi'));
                $this->db->set('id_kelompok_tani', $this->input->post('id_kelompok_tani'));
                $this->db->set('tanggal_kendala_tanam', $this->input->post('tanggal_kendala_tanam'));
                $this->db->set('luas_lahan', $this->input->post('luas_lahan_terkendala'));
                $this->db->set('keterangan', $this->input->post('keterangan'));
                $this->db->set('created_on', date('Y-m-d H:i:s'));
                $this->db->set('file', $this->input->post('file_txt'));
    
                $this->db->insert('tbl_kendala_tanam');
                return jsonOutputSuccess();
            }
            

        }

        
    }

    public function detail_kendala_tanam($id) {
        $this->db->select('
            tbl_kendala_tanam.id,
            tbl_kendala_tanam.tanggal_kendala_tanam,
            tbl_kendala_tanam.luas_lahan,
            tbl_kendala_tanam.keterangan,
            tbl_kendala_tanam.file,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.no_reg_horti,
            tbl_permohonan.tanggal_reg_horti,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.satuan_luas_kebun,
            tbl_permohonan.kelompok_komoditi_nama,
            tbl_permohonan.jenis_komoditi_nama,
            tbl_permohonan.sub_komoditi_nama,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.desa_nama,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
            tbl_kelompok_tani.nama_kelompok,
        ');
        $this->db->join('tbl_permohonan','tbl_kendala_tanam.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->join('tbl_kelompok_tani','tbl_kendala_tanam.id_kelompok_tani = tbl_kelompok_tani.id', 'left');
        $this->db->where('tbl_kendala_tanam.id', $id);
        $this->db->from('tbl_kendala_tanam');
        $data = $this->db->get();
        return $data->row_array();
    }

    public function config_file_upload() {
        $config['upload_path'] = './dist/images/dokumentasi/';
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        return $this->db->delete('tbl_kendala_tanam');
    }

    public function get_kelompok_tani($id_permohonan) {
        $this->db->select(' tbl_kelompok_tani.*');
        $this->db->where('tbl_kelompok_tani.id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $get = $this->db->get();
        return $get->result_array();
    }
    
    public function get_anggota_tani($id_kelompok_tani) {
        $this->db->select(' tbl_anggota_tani.*');
        $this->db->where('tbl_anggota_tani.id_kelompok_tani', $id_kelompok_tani);
        $this->db->from('tbl_anggota_tani');
        $get = $this->db->get();
        return $get->result_array();
    }


    public function exportExcel() {
        $this->db->select('
            tbl_permohonan.nomor_registrasi,
            tbl_kendala_tanam.tanggal_kendala_tanam,
            tbl_kelompok_tani.nama_kelompok,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_kendala_tanam.luas_lahan,
            tbl_permohonan.satuan_luas_kebun,
            tbl_kendala_tanam.keterangan
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->db->where('tbl_permohonan.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->db->where('tbl_permohonan.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->db->join('tbl_permohonan', 'tbl_kendala_tanam.id_permohonan = tbl_permohonan.id', 'left');
        $this->db->join('tbl_kelompok_tani', 'tbl_kendala_tanam.id_kelompok_tani = tbl_kelompok_tani.id', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->order_by('tbl_jenis_komoditi.jenis_komoditi_nama ASC');
        $this->db->from('tbl_kendala_tanam');
        $res = $this->db->get();
        return $res->result_array();
    }
}
