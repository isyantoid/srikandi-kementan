<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends Api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('','');
    }

    public function login_post()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user = $this->db->select('
                tbl_user.*, 
                tbl_user_level.nama as user_level_nama
            ')
            ->join('tbl_user_level', 'tbl_user.user_level_id = tbl_user_level.id','left')
            ->where('username', $username)
            ->where('password', md5($password))
            ->get('tbl_user');
            $thekey = $this->config->item('thekey');
            if ($user->num_rows() > 0) {
                $row = $user->row_array();
                $date = new DateTime();
                $token = array(
                    'userID' => $row['id'],
                    'userEmail' => $row['email'],
                );
                $tokenEncode = JWT::encode($token, $thekey);
                $data = array_merge($row, ['token' => $tokenEncode]);
                $respon = [
                    "message" => "login sukses!",
                    "data" => $data,
                ];
                $this->set_response($respon, REST_Controller::HTTP_OK);
            } else {
                $this->set_response(['message' => 'login salah'], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->set_response(['message' => 'bad request'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
