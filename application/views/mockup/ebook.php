<div class="row">
    <?php for ($i = 1; $i <= 8; $i++) : ?>
    <div class="col-xl-3">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head kt-portlet__head--noborder"></div>
            <div class="kt-portlet__body kt-portlet__body--fit-y">
                <div class="kt-widget kt-widget--user-profile-4">
                    <div class="kt-widget__head">
                        <div class="text-center">
                            <img class="kt-widget__img kt-hidden-" src="{dist_path}images/book.png" alt="image"
                                width="200">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__section">
                                <a href="#" class="kt-widget__username"> Buku Manual Aplikasi <?php echo $i ?> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endfor ?>
</div>