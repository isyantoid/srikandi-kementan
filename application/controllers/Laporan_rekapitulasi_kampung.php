<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_rekapitulasi_kampung extends CI_Controller
{

    private $title = 'Laporam Rekapitulasi Kampung';
    private $view = 'Laporan_rekapitulasi_kampung';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        $this->load->helper('hendra_helper');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index() {
        $data['laporan'] = $this->model->query_laporan();
        $data['provinsi_nama'] = $this->model->provinsi_nama();
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function index_()
    {
        $this->db->select('
            DISTINCT(tbl_permohonan.provinsi_nama),
        ');
        $this->db->order_by('tbl_permohonan.provinsi_nama', 'asc');
        $this->db->join('tbl_kelompok_komoditi', 'tbl_permohonan.kelompok_komoditi_kode = tbl_kelompok_komoditi.kelompok_komoditi_kode', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->join('tbl_sub_komoditi', 'tbl_permohonan.sub_komoditi_kode = tbl_sub_komoditi.sub_komoditi_kode', 'left');
        $this->db->from('tbl_permohonan');

        $query_provinsi = $this->db->get();

        $this->db->select('
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');
        $this->db->order_by('tbl_jenis_komoditi.jenis_komoditi_nama', 'asc');
        $this->db->join('tbl_kelompok_komoditi', 'tbl_permohonan.kelompok_komoditi_kode = tbl_kelompok_komoditi.kelompok_komoditi_kode', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_kode = tbl_jenis_komoditi.jenis_komoditi_kode', 'left');
        $this->db->join('tbl_sub_komoditi', 'tbl_permohonan.sub_komoditi_kode = tbl_sub_komoditi.sub_komoditi_kode', 'left');
        $this->db->from('tbl_permohonan');
        $query_jenis_komoditi = $this->db->get();

        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['show_query_komoditi'] = $query_jenis_komoditi->result();
        $data['show_query_provinsi'] = $query_provinsi->result();
        $data['no_query_provinsi'] = $query_provinsi->num_rows();
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }


    public function explode_testing() {
        $string = 'luaskebun_ACEH';
        $exp = explode('_', $string);
        echo json_encode($exp);
    }
}