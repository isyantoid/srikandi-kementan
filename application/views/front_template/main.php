<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->

<head>
    <base href="{base_url}">
    <meta charset="utf-8" />
    <title>{title}</title>
    <meta name="description" content="Support center home example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{dist_path}assets/css/pages/support-center/home-2.css" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{dist_path}assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{dist_path}assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{dist_path}assets/media/logos/favicon.ico" />

    <link rel="stylesheet" href="{dist_path}carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{dist_path}carousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/4.5.6/css/ionicons.min.css">
    <link rel="stylesheet" href="{dist_path}carousel/css/animate.css">
    <link rel="stylesheet" href="{dist_path}carousel/css/style.css">

    <link rel="stylesheet" href="{dist_path}assets/css/custom.css">




    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{dist_path}assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="{dist_path}assets/js/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->


    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;900&display=swap"
        rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.6.2/animate.min.css" rel="stylesheet">

    <!-- slider css-->
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Montserrat:400,600'>
    {style_css}
    <!-- slider css-->
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

    <!-- begin:: Page -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        <?php $this->load->view('front_template/hero') ?>
        <?php $this->load->view($content) ?>

        <!-- begin:: Footer -->
        <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
            <div class="col-md-3" style="background-color:rgba(41,132,62,0.8) !important; border-radius:10px;">
                <h4 style="color:aliceblue !important;"><strong>TAUTAN SITUS</strong></h4>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3"
                style="background-color:rgba(41,132,62,0.8) !important; border-radius:10px; margin:0px 4%">
                <h4 style="color:aliceblue !important;"><strong>KONTAK KAMI</strong></h3>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="background-color:rgba(41,132,62,0.8) !important; border-radius:10px;">
                <h4 style="color:aliceblue !important;"><strong>HUBUNGI KAMI</strong></h4>
            </div>
        </div>

        <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
            <div class="col-md-3" style="background-color:azure; border-radius:10px; padding:10px 0px;">
                <ul id="menu-tautan-situs" class="menu">
                    <li id="menu-item-474"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-474"><a
                            href="http://pertanian.go.id">Kementan</a></li>
                    <li id="menu-item-477"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-477"><a
                            href="http://bps.go.id">BPS RI</a></li>
                    <li id="menu-item-478"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-478"><a
                            href="http://kemendag.go.id">Kemendag</a></li>
                    <li id="menu-item-479"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-479"><a
                            href="http://bnn.go.id">BNN</a></li>
                    <li id="menu-item-1951"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1951"><a
                            href="http://hortikultura-ppid.pertanian.go.id">PPID Hortikultura</a></li>
                    <li id="menu-item-1952"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1952"><a
                            href="http://simforta.pertanian.go.id">Sistem Informasi Pertanian</a></li>
                </ul>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="background-color:azure; border-radius:10px; padding:10px; margin:0px 4%">
                <h5><strong>Direktorat Jenderal Hortikultura - Kementerian Pertanian</strong></h5>
                <h6><strong>Jl. AUP No. 3, RT/RW 009/010 Pasar Minggu, Jakarta Selatan, DKI Jakarta,
                        12520</strong><br>
                    <strong>Telp 021-7806775</strong><br>
                    <strong>Fax 021-78844037</strong><br>
                    <strong>e mail : hortikultura@pertanian.go.id</strong><br>
                    <strong>web : hortikultura.pertanian.go.id</strong>
                </h6>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3" style="background-color:azure; border-radius:10px; padding:10px 0px;">
                <!--start-->
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <figure class="wpb_wrapper vc_figure">
                            <a href="https://twitter.com/ditjenhorti" target="_blank"
                                class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img "
                                    src="http://hortikultura.pertanian.go.id/wp-content/uploads/2021/02/58e9196deb97430e819064f6-75x75.png"
                                    width="75" height="75" alt="Twitter Hortikultura" title="Twitter Hortikultura"
                                    data-pin-no-hover="true"></a>
                        </figure>
                    </div>
                    <div class="col-md-4">
                        <figure class="wpb_wrapper vc_figure">
                            <a href="https://www.instagram.com/ditjenhorti/" target="_blank"
                                class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img "
                                    src="http://hortikultura.pertanian.go.id/wp-content/uploads/2021/02/dba8217926d57db2a1cad921b5446c9a-75x75.png"
                                    width="75" height="75" alt="Instagram Hortikultura" title="Instagram Hortikultura"
                                    data-pin-no-hover="true"></a>
                        </figure>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <figure class="wpb_wrapper vc_figure">
                            <a href="https://www.youtube.com/channel/UClEt4Ef4NbCB8YferF7Bo7Q" target="_blank"
                                class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img "
                                    src="http://hortikultura.pertanian.go.id/wp-content/uploads/2021/02/2018_social_media_popular_app_logo_youtube-512-75x75.png"
                                    width="75" height="75" alt="Youtube Hortikultura" title="Youtube Hortikultura"
                                    data-pin-no-hover="true"></a>
                        </figure>
                    </div>
                    <div class="col-md-4">
                        <figure class="wpb_wrapper vc_figure">
                            <a href="https://www.facebook.com/DitjenHortikultura" target="_blank"
                                class="vc_single_image-wrapper   vc_box_border_grey"><img class="vc_single_image-img "
                                    src="http://hortikultura.pertanian.go.id/wp-content/uploads/2021/02/facebook-logo-2019-1597680-1350125-75x75.png"
                                    width="75" height="75" alt="Facebook Hortikultura" title="Facebook Hortikultura"
                                    data-pin-no-hover="true"></a>
                        </figure>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <!--end-->
            </div>
        </div>

    </div>
    <!-- end:: Footer -->

    <!-- begin:: Footer -->
    <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer"
        style="background-color:rgba(41,132,62,0.8) !important; border-radius:10px;">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-footer__copyright" style="margin:0px 40% !important">
                <strong style="color:aliceblue !important">
                    2020&nbsp;&copy;&nbsp;<a href="<?php echo base_url(); ?>" target="_blank" class="kt-link"
                        style="color:aliceblue !important">DIREKTORAT JENDERAL HORTIKULTURA</a>
                </strong>
            </div>
        </div>
    </div>
    <!-- end:: Footer -->
    </div>
    <!-- end:: Content -->


    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>



    <!-- end::Scrolltop -->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#22b9ff",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
    </script>

    <!-- end::Global Config -->
</body>


<!-- end::Body -->
<script src="{dist_path}carousel/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</html>