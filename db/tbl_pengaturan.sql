/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 08/06/2023 11:21:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_pengaturan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pengaturan`;
CREATE TABLE `tbl_pengaturan`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `setting_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `aktif` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `html` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  `help_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_pengaturan
-- ----------------------------
INSERT INTO `tbl_pengaturan` VALUES (1, 'konten_beranda', '<h3>Selamat Datang di&nbsp;<img src=\"../dist/assets/media/logos/berdikari3.png\" alt=\"\" width=\"130\" height=\"26\" /><br />Sistem Berbasis Data dan Informasi Kampung Buah dan Flori Direktorat Buah dan Florikultura</h3>\r\n<p>&nbsp;</p>\r\n<p>Dalam rangka pengembangan kawasan buah dan florikultura melalui Kampung Buah dan Kampung Flori, perlu dilakukan kegiatan monitoring dan evaluasi terhadap fasilitasi bantuan yang telah diberikan kepada petani/kelompoktani penerima manfaat sebagai bagian dari tahapan evaluasi kinerja di bidang peningkatan produksi/produktivitas buah dan florikultura. Kegiatan monitoring dan evaluasi fasilitasi bantuan kegiatan Kampung Buah dan Kampung Flori ini perlu dilakukan untuk mengukur sampai sejauh mana azas kemanfaatan dirasakan oleh petani/kelompoktani penerima fasilitasi bantuan, dan juga sebagai bahan evaluasi terhadap efektifitas fasilitasi bantuan serta sebagai acuan dalam penetapan kebijakan/program selanjutnya.</p>\r\n<p>&nbsp;</p>\r\n<p>Salam,<br />Buah dan Florikultura Maju, Mandiri, Modern</p>', '1', '1', NULL);
INSERT INTO `tbl_pengaturan` VALUES (2, 'popup_beranda', '<h3>&nbsp;</h3>\r\n<h3>INFORMASI PENTING !</h3>\r\n<p>Disampaikan kepada seluruh tim teknis, bahwa batas penginputan data Kampung Buah dan Kampung Flori APBN TA 2022 pada sistem BERDIKARI adalah tanggal <strong>23 Desember 2022</strong></p>\r\n<p>Dimohon untuk segera menginput data&nbsp;<span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">Kampung Buah dan Kampung Flori APBN TA 2022</span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Jakarta November 2022,&nbsp;<strong>Direktorat Jenderal Hortikultura</strong></p>', '1', '1', NULL);
INSERT INTO `tbl_pengaturan` VALUES (3, 'format_nomor_registrasi_p2l', 'REG/P2L/{{tahun}}/', '1', '0', 'Variable: {{hari}}, {{bulan}}, {{tahun}}');

SET FOREIGN_KEY_CHECKS = 1;
