<style>
    .gm-style .gm-style-iw-c {
    position: absolute;
    box-sizing: border-box;
    overflow: hidden;
    top: 0;
    left: 0;
    transform: translate3d(-50%,-100%,0);
    background-color: #2786fb;
    color: white;
    border-radius: 8px;
    padding: 12px !important;
    box-shadow: 0 2px 7px 1px rgb(0 0 0 / 30%);
}
.gm-style .gm-style-iw-d {
    overflow: auto !important;
}
</style>

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet" data-ktportlet="true" id="collapse">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon2-list-3"></i>
                        </span>
                        <h3 class="kt-portlet__head-title"> Filter {title}</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form kt-form--fit kt-form--label-right" action="javascript:filter()" id="form1">
                        <div class="kt-portlet__body">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Komoditi:</label>
                                <div class="col-lg-3">
                                    <select class="form-control kelompok_komoditi_id" name="kelompok_komoditi_id"></select>
                                </div>
                                <label class="col-lg-2 col-form-label">Jenis Komoditi:</label>
                                <div class="col-lg-3">
                                    <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id"></select>
                                </div>
                            </div>
                          
                            <?php if(is_admin()): ?>
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label">Provinsi:</label>
                                    <div class="col-lg-3">
                                        <select class="form-control provinsi_kode" name="provinsi_kode"></select>
                                    </div>
                                    <label class="col-lg-2 col-form-label">Kabupaten:</label>
                                    <div class="col-lg-3">
                                        <select class="form-control kabupaten_kode" name="kabupaten_kode"></select>
                                    </div>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="kt-portlet__foot kt-portlet__foot--fit-x">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-success">Tampilkan Hasil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="result"></div>
</div>

<script>
    var portlet = new KTPortlet('collapse');
    // portlet.collapse('hide');

    $('.kabupaten_kode, .jenis_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');
    
    ajaxSelect({
            id: '.kelompok_komoditi_id',
        url: '{site_url}ajax_selectbox/kelompok_komoditi_id',
    });

    $('.kelompok_komoditi_id').change(function() {
        var val = $(this).val();
        $('.jenis_komoditi_id').empty();
        ajaxSelect({
            id: '.jenis_komoditi_id',
            url: '{site_url}ajax_selectbox/jenis_komoditi_id',
            optionalSearch: {
                kelompok_komoditi_id: val
            }
        });
    });

    ajaxSelect({
        allowClear: true,
        id: '.provinsi_kode',
        url: '{site_url}ajax_selectbox/provinsi_select',
    });

    $('.provinsi_kode').change(function() {
        var val = $(this).val();
        $('.kabupaten_kode, .kecamatan_kode, .desa_kode').empty();
        ajaxSelect({
            id: '.kabupaten_kode',
            url: '{site_url}ajax_selectbox/kabupaten_select',
            optionalSearch: {
                provinsi_kode: val
            }
        });
    });

    $('.tanggal_awal, .tanggal_akhir').datepicker({
        todayHighlight: true,
        orientation: "bottom left",
    });

    $(document).on('click', '.result_summary_kabupaten', function(e) {
        e.preventDefault();
        var komoditi = $(this).data('komoditi');
        var provinsi = $(this).data('provinsi');
        var image = $(this).data('image');
        var url = '{site_url}dashboard/result_summary';

        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            $('.result').load(url, {
                komoditi: komoditi,
                image: image,
                provinsi: provinsi,
                tipe: 'kabupaten'
            }, function() {
                unblockPage();
            });
        }, 1000);
    });

    $(document).on('click', '.result_summary_provinsi', function(e) {
        e.preventDefault();
        var komoditi = $(this).data('komoditi');
        var image = $(this).data('image');
        var url = '{site_url}dashboard/result_summary';

        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            $('.result').load(url, {
                komoditi: komoditi,
                image: image,
                tipe: 'provinsi'
            }, function() {
                unblockPage();
            });
        }, 1000);
    });

    function filter() {
        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            portlet.collapse('hide');
            //start edit hendra
            var kelompok_komoditi_id = $(".kelompok_komoditi_id").val();
            var jenis_komoditi_id = $(".jenis_komoditi_id").val();
         
            var provinsi_kode = $(".provinsi_kode").val();
            var kabupaten_kode = $(".kabupaten_kode").val();
            //start edit hendra
             $('.result').load('{site_url}dashboard/map_pusat?kelompok_komoditi_id='+kelompok_komoditi_id+'&jenis_komoditi_id='+jenis_komoditi_id+'&provinsi_kode='+provinsi_kode+'&kabupaten_kode='+kabupaten_kode, function() {
                unblockPage();
            });
            //end edit hendra
            // $('.result').load('{site_url}dashboard/map_pusat, function() {
            //     unblockPage();
            // });
        }, 1000);
    }
    
</script>