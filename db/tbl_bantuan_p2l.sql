/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 08/06/2023 14:52:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_bantuan_p2l
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bantuan_p2l`;
CREATE TABLE `tbl_bantuan_p2l`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `registrasi_p2l_id` int NULL DEFAULT NULL,
  `tanggal_penerima_bantuan` date NULL DEFAULT NULL,
  `realisasi_pemanfaatan_anggaran` decimal(10, 0) NULL DEFAULT NULL,
  `lat` float NULL DEFAULT NULL,
  `lng` float NULL DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  `tanggal_kirim` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_bantuan_p2l
-- ----------------------------
INSERT INTO `tbl_bantuan_p2l` VALUES (1, 1, '2023-06-08', 1000000000, -6.98702, 108.326, '1', '2023-06-08 09:38:08');

SET FOREIGN_KEY_CHECKS = 1;
