<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function path_info()
{

    $ci = &get_instance();
    $user = get_user();

    $data = array(
        'base_url' => base_url(),
        'site_url' => site_url(),
        'segment1' => site_url($ci->uri->segment(1) . '/'),
        'dist_path' => base_url('dist/'),
        'js_path' => base_url('dist/js/'),
        'css_path' => base_url('dist/css/'),
        'images_path' => base_url('dist/images/'),
        'uploads_path' => base_url('uploads/'),
        'url_api' => $ci->config->item('url_api'),
        'url_api_wilayah' => $ci->config->item('url_api_wilayah'),
        // 'user_username' => $user['username'],
        // 'user_nama' => $user['nama'],
        // 'user_level_nama' => $user['user_level_nama'],
        // 'user_provinsi_kode' => $user['provinsi_kode'],
        // 'user_kabupaten_kode' => $user['kabupaten_kode'],
    );

    return $data;
}

function akses_user($method) {
    $ci = &get_instance();

    $module = $ci->uri->segment(1);
    $user_level_id = get_user()['user_level_id'];

    $query = $ci->db
    ->where('url',$module)
    ->get('tbl_menu');

    if($query->num_rows() > 0) {
        $cek_akses = $ci->db
        ->where('menu_id', $query->row_array()['id'])
        ->where('user_level_id', $user_level_id)
        ->get('tbl_user_akses');
        if($cek_akses->num_rows() > 0) {
            $akses = $cek_akses->row_array();
            if($method == 'delete') {
                if($akses['delete'] == '0') return false; else return true;
            } else {
                if($akses[$method] == '0') {
                    return show_error('Anda tidak memiliki izin untuk mengakses halaman ini', 403, 'Izin Akses Dilarang');
                }
            }
        } else {
            return show_error('Anda tidak memiliki izin untuk mengakses halaman ini', 403, 'Izin Akses Dilarang');
        }
    }
}

function is_user() {
    $ci = &get_instance();
    
    $is_user = $ci->session->userdata('session_is_user');
    if($is_user) {
        return true;
    } else {
        return false;
    }
}

function is_admin() {
    $ci = &get_instance();
    
    $user_level_id = $ci->session->userdata('session_user_level_id');
    if($user_level_id == '1') {
        return true;
    } else {
        return false;
    }
}

function get_user($user_id = null) {
    $ci = &get_instance();
    
    $ci->db->select('
        tbl_user.*,
        tbl_user_level.nama as user_level_nama
    ');
    if($user_id) $ci->db->where('tbl_user.id', $user_id);
    else $ci->db->where('tbl_user.id', $ci->session->userdata('session_user_id'));
    $ci->db->join('tbl_user_level', 'tbl_user.user_level_id = tbl_user_level.id', 'left');
    $get = $ci->db->get('tbl_user');
    return $get->row_array();
}


function getImage($image){

    if(file_exists('dist/images/' . $image) == FALSE || $image == null){
        return base_url() . 'dist/images/noimage.png';
    }

    return base_url() . 'dist/images/' . $image;
}


function removeComma($number, $delimiter = ',') {
    return str_replace($delimiter, '', $number);
}

function formatTanggal($date) {
    return date('d M Y', strtotime($date));
}


function getTable($table, $where = array())
{
    $ci = &get_instance();
    if ($where) {
        $ci->db->where($where);
    }
    $get = $ci->db->get($table)->result();
    return $get;
}

function getRowArray($table, $where = array())
{
    $ci = &get_instance();
    if ($where) {
        $ci->db->where($where);
    }
    $get = $ci->db->get($table)->row_array();
    return $get;
}

function getResultArray($table, $where = array(), $select = array())
{
    $ci = &get_instance();
    if($select) {
        $ci->db->select(implode(',', $select));
    }
    if ($where) {
        $ci->db->where($where);
    }
    $get = $ci->db->get($table)->result_array();
    return $get;
}

function getCount($table, $where = array())
{
    $ci = &get_instance();
    if ($where) {
        $ci->db->where($where);
    }
    $get = $ci->db->get($table)->num_rows();
    return $get;
}

function isDuplicate($table, $key, $value)
{
    $ci = &get_instance();
    $ci->db->where($key, $value);
    $checkUnique = $ci->db->get($table);
    if ($checkUnique->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function jsonOutputSuccess($message = null)
{
    $ci = &get_instance();
    if ($message == null) {
        $message = 'Data berhasil disimpan.';
    }
    $data['status'] = true;
    $data['message'] = $message;
    $ci->output->set_content_type('application/json')->set_output(json_encode($data));
}

function jsonOutputError($message = null)
{
    $ci = &get_instance();
    if ($message == null) {
        $message = 'Data gagal disimpan.';
    }
    $data['status'] = false;
    $data['message'] = $message;
    $ci->output->set_content_type('application/json')->set_output(json_encode($data));
}

function unlinkFile($file)
{
    if (file_exists($file)) {
        unlink($file);
    }
}

function getUserById($id = null)
{
    $ci = &get_instance();

    if (!$id) {
        $ci->db->where('id', $ci->session->userdata('id'));
    } else {
        $ci->db->where('id', $id);
    }
    $get = $ci->db->get('m_user');
    return $get->row_array();
}

function dataApiRespon($status, $message, $data = null)
{
    $result['status'] = $status;
    $result['message'] = $message;
    if ($data) {
        $result['data'] = $data;
    }
    return $result;
}

function menuIsOpen($menus = array()) {
    $ci = &get_instance();
    if(in_array($ci->uri->segment(1), $menus)) {
        return 'show';
    }
}

function menuIsActive($stringUri1) {
    $ci = &get_instance();
    if($ci->uri->segment(1) == $stringUri1) {
        return 'active';
    }
}

function menuIsActive2($stringUri2) {
    $ci = &get_instance();
    if($ci->uri->segment(2) == $stringUri2) {
        return 'active';
    }
}

function range_date($date1, $date2){
	$birthDate = new DateTime($date1);
	$today = new DateTime($date2);
	if ($birthDate > $today) { 
	    exit("0 tahun 0 bulan 0 hari");
	}
	$y = $today->diff($birthDate)->y;
	$m = $today->diff($birthDate)->m;
	$d = $today->diff($birthDate)->d;
	return $y." tahun ".$m." bulan ".$d." hari";
}

function getUser($field) {
    $ci = &get_instance();
    $ci->db->where('id', $ci->session->userdata('user'));
    $ci->db->join('registrasi_m', 'user_t.id_registrasi = registrasi_m.registrasi_id', 'left');
    $get = $ci->db->get('user_t');
    return $get->row_array()[$field];
}

function getPengaturan($key) {
    $ci = &get_instance();
    $ci->db->where('setting_key', $key);
    $get = $ci->db->get('tbl_pengaturan');
    return $get->row_array()['setting_desc'];
}

function getNomorRegistrasiP2L() {
    $ci = &get_instance();
    $desc = getPengaturan('format_nomor_registrasi_p2l');
    $desc = str_replace('{{tahun}}', date('Y'), $desc);
    $desc = str_replace('{{bulan}}', date('m'), $desc);

    $count = $ci->db->like('nomor_registrasi_p2l', $desc)
    ->get('tbl_registrasi_p2l')->num_rows();
    $nomorUrut = str_pad($count + 1, 4, '0', STR_PAD_LEFT);;
    return $desc . $nomorUrut;
}

function is_mobile() {
    $ci = &get_instance();
    $ci->load->library('user_agent');
    if($ci->agent->is_mobile()) {
        return 'mobile/';
    } else {
        return '';
    }
}

function site_name() {
    $ci = &get_instance();
    $p2l = [9, 10, 11, 12];
    $champ = [13,14];
    $user_level_id = $ci->session->userdata('session_user_level_id');

    $title = 'SRIKANDI';
    if(in_array($user_level_id, $p2l)) {
        $title = 'PEKARANGAN PANGAN LESTARI (P2L)';
    } else if(in_array($user_level_id, $champ)) {
        $title = 'CHAMPION';
    }
    return $title;
}

function site_logo() {
    $ci = &get_instance();
    
    $p2l = [9, 10, 11, 12];
    $champ = [13,14];
    $user_level_id = $ci->session->userdata('session_user_level_id');
    
    $logo = site_url() . '/dist/images/logo.png';
    if(in_array($user_level_id, $p2l)) {
        $logo = site_url() . '/dist/images/logo_p2l.png';
    } else if(in_array($user_level_id, $champ)) {
        $logo = site_url() . '/dist/images/logo.png';
    }
    return $logo;
}

function champ_user() {
    $ci = &get_instance();
    $user_level_id = $ci->session->userdata('session_user_level_id');
    $data = 'admin_champ';
    if(in_array($user_level_id, [13,14,18])) {
        $data = 'bamer';
    } else if(in_array($user_level_id, [15,16,19])) {        
        $data = 'cabai';
    }
    return $data;
}