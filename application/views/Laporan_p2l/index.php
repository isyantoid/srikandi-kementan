<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <form class="kt-form" action="" id="form1">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">Filter Pencarian</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Provinsi</label>
                        <select class="form-control kt-select2 provinsi_kode" name="provinsi_kode"
                            style="width:100%;"></select>
                    </div>
                    <div class="col-md-6">
                        <label>Kabupaten</label>
                        <select class="form-control kt-select2 kabupaten_kode" name="kabupaten_kode"
                            style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Kelompok Tani</label>
                        <select class="form-control kt-select2 kelompok_tani_id" name="kelompok_tani_id" style="width:100%;"></select>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary submit">Submit</button>
                    <a href="{site_url}kampung" class="btn btn-secondary">Clear</a>
                </div>
            </div>
        </form>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">Hasil Pencarian</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}export_hasil_pencarian?<?= uri_string() ?>" class="btn btn-brand btn-elevate btn-icon-sm">Export Hasil Pencarian</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <th>No</th>
                        <th>Nomor Registrasi P2L</th>
                        <th>Tanggal Registrasi P2L</th>
                        <th>Kelompok Tani</th>
                        <th>Nama Ketua</th>
                        <th>Provinsi</th>
                        <th>Kabupaten</th>
                        <th>Kecamatan</th>
                        <th>Desa</th>
                        <th>Jumlah Penerima Manfaat (orang)</th>
                        <th>Terdaftar Simluhtan</th>
                    </thead>
                    <tbody>
                        <?php if($get_laporan): ?>
                            <?php $no=1; foreach($get_laporan as $row): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $row['nomor_registrasi_p2l'] ?></td>
                                    <td><?= $row['tanggal_registrasi_p2l'] ?></td>
                                    <td><?= $row['nama_kelompok'] ?></td>
                                    <td><?= $row['nama_ketua'] ?></td>
                                    <td><?= $row['provinsi_nama'] ?></td>
                                    <td><?= $row['kabupaten_nama'] ?></td>
                                    <td><?= $row['kecamatan_nama'] ?></td>
                                    <td><?= $row['desa_nama'] ?></td>
                                    <td><?= $row['jumlah_penerima_manfaat'] ?></td>
                                    <td><?= ($row['terdaftar_simluhtan'] == '1') ? 'Ya' : 'Tidak' ?></td>
                                </tr>
                            <?php $no++; endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

    $('.kabupaten_kode, .jenis_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');

    ajaxSelect({
        allowClear: true,
        id: '.provinsi_kode',
        url: '{site_url}ajax_selectbox/provinsi_select',
        selected: '<?= $this->input->get('provinsi_kode') ?>'
    });

    $('.provinsi_kode').change(function() {
        var val = $(this).val();
        if(val) {
            $('.kabupaten_kode').empty();
            ajaxSelect({
                allowClear: true,
                id: '.kabupaten_kode',
                url: '{site_url}ajax_selectbox/kabupaten_select',
                optionalSearch: {
                    provinsi_kode: val
                },
                selected: '<?= $this->input->get('kabupaten_kode') ?>'
            });
        }
    });

    $('.provinsi_kode').on("select2:unselecting", function (e) {
        $('.kabupaten_kode').empty();
        $('.kabupaten_kode, .jenis_komoditi_id').select2({
            placeholder: 'Pilih Opsi'
        }).val('').trigger('change');
    });

    ajaxSelect({
        id: '.kelompok_tani_id',
        url: '{site_url}ajax_selectbox/kelompok_tani_id',
        selected: '<?= $this->input->get('kelompok_tani_id') ?>'
    });
    
</script>