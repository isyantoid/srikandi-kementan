<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Provinsi extends CI_Controller
{

    private $title = 'Provinsi';
    private $view = 'Provinsi';

    public function __construct()
    {
        parent::__construct();
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
}