<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_komitmen_cabai_rawit_model extends CI_Model
{

    private $tableName = 'champ_realisasi_pasar';
    private $primaryKey = 'id';

    

    public function laporan_komitmen($tahun = '')
    {
        $id = $this->uri->segment(3);
        $this->db->select('
            champ_komitmen.id,
            champ_komitmen.tahun,
            sum(champ_komitmen.stok) as stok,
            sum(champ_komitmen.jan) as jan,
            sum(champ_komitmen.feb) as feb,
            sum(champ_komitmen.mar) as mar,
            sum(champ_komitmen.apr) as apr,
            sum(champ_komitmen.may) as may,
            sum(champ_komitmen.jun) as jun,
            sum(champ_komitmen.jul) as jul,
            sum(champ_komitmen.aug) as aug,
            sum(champ_komitmen.sep) as sep,
            sum(champ_komitmen.oct) as oct,
            sum(champ_komitmen.nov) as nov,
            champ_komitmen.dec as dec,
            champ.nama as champion_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            champ_kontak_dinas.nama as kontak_dinas_nama,
            champ_kontak_dinas.nomor_hp as kontak_dinas_nomor_hp,
        ');
        $this->db->join('champ', 'champ_komitmen.user_id = champ.user_id', 'left');
        $this->db->join('tbl_kabupaten', 'champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->db->join('tbl_kecamatan', 'champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->db->join('champ_kontak_dinas', 'champ_kontak_dinas.kabupaten_kode = champ.kabupaten_kode', 'left');
        $this->db->where('champ_komitmen.tahun', $tahun);
        $this->db->where('champ_komitmen.jenis_komoditi_id', '77');
        $this->db->group_by('champ_komitmen.champ_id');
        $this->db->group_by('champ_komitmen.tahun');
        $get = $this->db->get('champ_komitmen');
        return $get->result_array();
    }
}