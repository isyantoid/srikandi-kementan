<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}" class="btn btn-danger btn-elevate btn-icon-sm">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <div class="row">
                <div class="col-6">

                    <table class="table table-hover table-checkable">
                        <tbody>
                            <tr>
                                <td>Nomor Registrasi STO</td>
                                <td>:</td>
                                <td class="font-weight-bold">{nomor_registrasi}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Registrasi STO</td>
                                <td>:</td>
                                <td class="font-weight-bold">{tanggal_permohonan}</td>
                            </tr>
                            <tr>
                                <td>Nomor Registrasi Horti</td>
                                <td>:</td>
                                <td class="font-weight-bold">{no_reg_horti}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Pengesahan Registrasi Horti</td>
                                <td>:</td>
                                <td class="font-weight-bold">{tanggal_reg_horti}</td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>:</td>
                                <td class="font-weight-bold">{provinsi_nama}</td>
                            </tr>
                            <tr>
                                <td>Kabupaten</td>
                                <td>:</td>
                                <td class="font-weight-bold">{kabupaten_nama}</td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td class="font-weight-bold">{kecamatan_nama}</td>
                            </tr>
                            <tr>
                                <td>Desa</td>
                                <td>:</td>
                                <td class="font-weight-bold">{desa_nama}</td>
                            </tr>
                            <tr>
                                <td>Garis Lintang</td>
                                <td>:</td>
                                <td class="font-weight-bold">{lat}</td>
                            </tr>
                            <tr>
                                <td>Garis Bujur</td>
                                <td>:</td>
                                <td class="font-weight-bold">{lng}</td>
                            </tr>
                            <tr>
                                <td>Kelompok Komoditas</td>
                                <td>:</td>
                                <td class="font-weight-bold">{kelompok_komoditi_nama}</td>
                            </tr>
                            <tr>
                                <td>Jenis Komoditas</td>
                                <td>:</td>
                                <td class="font-weight-bold">{jenis_komoditi_nama}</td>
                            </tr>
                            <tr>
                                <td>Sub Komoditi</td>
                                <td>:</td>
                                <td class="font-weight-bold">{sub_komoditi_nama}</td>
                            </tr>
                            <tr>
                                <td>Nama Kelompok Tani</td>
                                <td>:</td>
                                <td class="font-weight-bold">{nama_kelompok}</td>
                            </tr>
                            <tr>
                                <td>Luas Panen</td>
                                <td>:</td>
                                <td class="font-weight-bold">{luas_panen} {satuan_luas_kebun}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Panen</td>
                                <td>:</td>
                                <td class="font-weight-bold">{tanggal_panen}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Hasil Panen</td>
                                <td>:</td>
                                <td class="font-weight-bold">{jumlah_hasil_panen}</td>
                            </tr>
                            <tr>
                                <td>Tanggal Perkiraan Panen</td>
                                <td>:</td>
                                <td class="font-weight-bold">

                                    <?php $tglPerkiraanPanen = json_decode($tanggal_perkiraan_panen) ?>
                                    <?php for($i=0; $i<count($tglPerkiraanPanen); $i++): ?>
                                        <p><?= $tglPerkiraanPanen[$i] ?></p>
                                    <?php endfor ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-6">
                    <img src="{dist_path}images/dokumentasi/{foto_panen}" class="img-fluid rounded" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Content -->
