
<div class="kt-portlet" data-ktportlet="true" id="reload">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title"></h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-group">
                <a href="#" data-ktportlet-tool="reload" class="btn btn-sm btn-icon btn-default btn-pill btn-icon-md reload"><i class="la la-refresh"></i></a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div id="map1" style="height:400px;"></div>
    </div>
</div>




<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&libraries=places&callback=initMap"></script>
<script>
    
    var reload = new KTPortlet('reload');

    reload.on('reload', function(portlet) {
        filter();
    });

    function initMap() {
        const map = new google.maps.Map(document.getElementById("map1"), {
            zoom: 5.25,
            center: { lat: -2.0423904, lng: 122.8983498 },
        });
        setMarkers(map);
    }

    function setMarkers(map) {
        const image = {
            url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
            size: new google.maps.Size(20, 32),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 32),
        };


        $.ajax({
            url: "{site_url}dashboard/get_lat_lng_permohonan?kelompok_komoditi_id=<?php echo $kelompok_komoditi_id;?>&jenis_komoditi_id=<?php echo $jenis_komoditi_id;?>&provinsi_kode=<?php echo $provinsi_kode;?>&kabupaten_kode=<?php echo $kabupaten_kode;?>",
            //url: '{site_url}dashboard/get_lat_lng_permohonan',
            dataType: 'json',
            method: 'post',
            success: function(res) {
                console.log(res)
                for (let i = 0; i < res.length; i++) {
                    
                    const row = res[i];

                    const marker = new google.maps.Marker({
                        position: { lat: parseFloat(row.lat), lng: parseFloat(row.lng) },
                        map,
                        icon: {
                            url: '{dist_path}images/jenis_komoditi/' + row.jenis_komoditi_pin,
                            scaledSize: new google.maps.Size(50, 50),
                            origin: new google.maps.Point(0,0),
                            anchor: new google.maps.Point(0, 0)
                        },
                        title: row.id_pemohon,
                    });


                    const contentString = `
                    <div class="bg-info text-white">
                    <p class="m-0 small">Nomor Registrasi: `+row.nomor_registrasi+`</p>
                    <p class="m-0 small">Kampung: `+row.jenis_komoditi_nama+`</p>
                    <p class="m-0 small">Luas kampung: `+row.luas_kebun+` Ha</p>
                    <p class="m-0 small">Jumlah kelompok tani: `+row.total_kelompok_tani+`</p>
                    <p class="m-0 small">Total Nilai bantuan: Rp. `+numeral(row.total_nilai_bantuan).format()+`</p>
                    <p class="m-0 small">Desa `+row.desa_nama+` kecamatan `+row.kecamatan_nama+` kabupaten `+row.kabupaten_nama+` provinsi `+row.provinsi_nama+`</p>
                    <div class="m-0 mt-2">
                        <span class="m-0 small">`+descriptionScore(row.nilai_rating_persen.toString())+`</span>
                        <span class="m-0 small stars-container stars-`+row.nilai_rating_persen.toString()+`">★★★★★</span>
                    </div>
                    <div class="mt-3">
                        <div class="btn-group">
                            <a href="javascript:map_pusat_detail(`+row.id+`)" class="btn btn-block btn-warning btn-sm p-1">Lihat Detail</a>
                            <a href="{site_url}registrasi_kampung/direction/`+row.id+`" target="_blank" class="btn btn-primary btn-sm p-1">Kampung</a>
                        </div>
                    </div>
                    </div>`;
                    addScore(row.nilai_rating_persen, '#score');

                    var infowindow = new google.maps.InfoWindow();
                    marker.addListener("click", () => {
                        infowindow.setContent(contentString);
                        infowindow.setOptions({maxWidth: 300});
                        infowindow.open({ anchor: marker, map });
                    });
                    
                }
            }
        });
    }

    function map_pusat_detail(id) {
        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            portlet.collapse('hide');
            $('.result').load('{site_url}dashboard/map_pusat_detail/' + id, function() {
                unblockPage();
            });
        }, 1000);
    }
</script>