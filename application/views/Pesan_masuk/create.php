<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1" enctype="multipart/form-data">
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Tanggal</label>
                    <div class="col-10">
                        <input class="form-control tanggal" type="date" value="" name="tanggal" required>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Nama</label>
                    <div class="col-10">
                        <input class="form-control nama" type="text" value="" name="nama" required>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Email</label>
                    <div class="col-10">
                        <input class="form-control email" type="text" value="" name="email" required>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Lampiran</label>
                    <div class="col-10">
                        <input class="form-control lampiran" type="file" name="lampiran">
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Pesan</label>
                    <div class="col-10">
                        <input class="form-control pesan" type="text" value="" name="pesan" required>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Alamat Kantor</label>
                    <div class="col-10">
                        <input class="form-control alamat_kantor" type="text" value="" name="alamat_kantor" required>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->
<script>
function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>