<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />

<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="kt-portlet p-0 kt-iconbox" style="background-color:#fd2758;">
                <div class="kt-portlet__body p-3">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <img src="{dist_path}images/icon/gis.png" alt="" width="60">
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title">
                                <a class="kt-link text-white" href="{site_url}dashboard/gis_pusat">GIS</a>
                            </h3>
                            <div class="kt-iconbox__content text-white"> Geographic Information System </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet p-0 kt-iconbox" style="background-color: #49ce4d;">
                <div class="kt-portlet__body p-3">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <img src="{dist_path}images/icon/summary.png" alt="" width="60">
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title"> <a class="kt-link text-white" href="{site_url}dashboard/summary_pusat">DASHBOARD</a> </h3>
                            <div class="kt-iconbox__content text-white"> Summary Executive Report </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet p-0 kt-iconbox" style="background-color: #8689da;">
                <div class="kt-portlet__body p-3">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <img src="{dist_path}images/icon/chart.png" alt="" width="60">
                        </div>
                        <div class="kt-iconbox__desc">
                            <h3 class="kt-iconbox__title"> <a class="kt-link text-white" href="{site_url}dashboard/chart_pusat">DASHBOARD</a> </h3>
                            <div class="kt-iconbox__content text-white"> Chart Information System </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="kt-portlet p-0 kt-iconbox kt-bg-success">
                <div class="kt-portlet__body p-1">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <img src="{dist_path}images/icon/garden.png" alt="" width="80">
                        </div>
                        <div class="kt-iconbox__desc mt-3 mb-0">
                            <h3 class="kt-iconbox__title font-weight-bold m-0" style="font-size:2.2rem"> <a class="kt-link text-white" href="#"><?php echo number_format($this->model->total_kampung_indo()) ?></a> </h3>
                            <div class="kt-iconbox__content text-white"> Jumlah kampung</span></div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet p-0 kt-iconbox kt-bg-dark">
                <div class="kt-portlet__body p-1">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <img src="{dist_path}images/icon/garden_area.png" alt="" width="80">
                        </div>
                        <div class="kt-iconbox__desc mt-3 mb-0">
                            <h3 class="kt-iconbox__title font-weight-bold m-0" style="font-size:2.2rem"> <a class="kt-link text-white" href="#"><?php echo number_format($this->model->total_luas_indo()) ?></a> Ha </h3>
                            <div class="kt-iconbox__content text-white"> Luas kampung</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="kt-portlet p-0 kt-iconbox kt-bg-warning">
                <div class="kt-portlet__body p-1">
                    <div class="kt-iconbox__body">
                        <div class="kt-iconbox__icon">
                            <img src="{dist_path}images/icon/farmer.png" alt="" width="80">
                        </div>
                        <div class="kt-iconbox__desc mt-3 mb-0">
                            <h3 class="kt-iconbox__title font-weight-bold m-0" style="font-size:2.2rem"> <a class="kt-link text-white" href="#"><?php echo number_format($this->model->total_kelompok_tani_indo()) ?></a> </h3>
                            <div class="kt-iconbox__content text-white"> Kelompok tani</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="result">
        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet" style="background-color: #95e894;">
                    <div class="kt-portlet__body">
                        <div class="row">
                            <?php if($list_index_komoditi): ?>
                                <?php foreach($list_index_komoditi as $row): ?>
                                    <div class="col-md-3">
                                        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--skin-solid p-0" style="box-shadow: 5px 5px 20px 0px rgb(0 0 0)">
                                            <div class="kt-portlet__head kt-portlet__head--noborder">
                                                <div class="kt-portlet__head-label">
                                                    <h3 class="kt-portlet__head-title">
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body p-3">
                                                <div class="kt-widget kt-widget--user-profile-2">
                                                    <div class="kt-widget__head">
                                                        <div class="kt-widget__media">
                                                            <img class="kt-widget__img kt-hidden-" src="{dist_path}images/jenis_komoditi/<?php echo $row['jenis_komoditi_icon'] ?>" alt="image">
                                                        </div>
                                                        <div class="kt-widget__info">
                                                            <span href="#" class="kt-widget__username result_summary_provinsi" data-komoditi="<?php echo $row['jenis_komoditi_kode'] ?>"> <?php echo $row['jenis_komoditi_nama'] ?> </span>
                                                            <span class="kt-widget__desc"> <?php echo $row['kelompok_komoditi_nama'] ?> </span>
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget__body">
                                                        <div class="kt-widget__item mt-5">
                                                            <div class="kt-widget__contact">
                                                                <span class="kt-widget__label">Total Kampung:</span>
                                                                <a href="#" class="kt-widget__data font-weight-bold"><?php echo $row['total_kampung'] ?></a>
                                                            </div>
                                                            <div class="kt-widget__contact">
                                                                <span class="kt-widget__label">Luas Kampung:</span>
                                                                <a href="#" class="kt-widget__data font-weight-bold"><?php echo number_format($row['total_luas']) ?> Ha</a>
                                                            </div>
                                                            <div class="kt-widget__contact">
                                                                <span class="kt-widget__label">Jumlah Kelompok Tani:</span>
                                                                <span class="kt-widget__data font-weight-bold"><?php echo $row['total_kelompok'] ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--begin::Page Vendors(used by this page) -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!--end::Page Vendors -->


<script>
    // $(document).on('click', '.result_summary_kabupaten', function(e) {
    //     e.preventDefault();
    //     var komoditi = $(this).data('komoditi');
    //     var provinsi = $(this).data('provinsi');
    //     var image = $(this).data('image');
    //     var url = '{site_url}dashboard/result_summary';

    //     $('.result').html(null);
    //     blockPage();
    //     setTimeout(function() {
    //         $('.result').load(url, {
    //             komoditi: komoditi,
    //             image: image,
    //             provinsi: provinsi,
    //             tipe: 'kabupaten'
    //         }, function() {
    //             unblockPage();
    //         });
    //     }, 1000);
    // });

    // $(document).on('click', '.result_summary_provinsi', function(e) {
    //     e.preventDefault();
    //     var komoditi = $(this).data('komoditi');
    //     var image = $(this).data('image');
    //     var url = '{site_url}dashboard/result_summary';

    //     $('.result').html(null);
    //     blockPage();
    //     setTimeout(function() {
    //         $('.result').load(url, {
    //             komoditi: komoditi,
    //             image: image,
    //             tipe: 'provinsi'
    //         }, function() {
    //             unblockPage();
    //         });
    //     }, 1000);
    // });

    function filter() {
        $('.result').html(null);
        blockPage();
        setTimeout(function() {
            portlet.collapse('hide');
            $('.result').load('{site_url}dashboard/result_summary', function() {
                unblockPage();
            });
        }, 1000);
    }
    am4core.ready(function() {
        am4core.useTheme(am4themes_animated);
        var chart = am4core.create("kt_amcharts_1", am4charts.XYChart);
        chart.dataSource.url = "{site_url}dashboard/chart_total_kampung";
        chart.exporting.menu = new am4core.ExportMenu();
        chart.exporting.menu.align = "left";
        chart.exporting.menu.verticalAlign = "top";

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
            if (target.dataItem && target.dataItem.index & 2 == 2) {
                return dy + 25;
            }
            return dy;
        });

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "total";
        series.dataFields.categoryX = "month";
        series.name = "Visits";
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .5;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 1;
        columnTemplate.strokeOpacity = .8;
        columnTemplate.fill = am4core.color("green");
        columnTemplate.stroke = am4core.color("black");


    });

    am4core.ready(function() {
        am4core.useTheme(am4themes_animated);
        var chart = am4core.create("kt_amcharts_2", am4charts.XYChart);
        chart.dataSource.url = "{site_url}dashboard/chart_total_luas";
        chart.exporting.menu = new am4core.ExportMenu();
        chart.exporting.menu.align = "left";
        chart.exporting.menu.verticalAlign = "top";

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "month";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;

        categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
            if (target.dataItem && target.dataItem.index & 2 == 2) {
                return dy + 25;
            }
            return dy;
        });

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "total";
        series.dataFields.categoryX = "month";
        series.name = "Visits";
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .5;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 1;
        columnTemplate.strokeOpacity = .8;
        columnTemplate.fill = am4core.color("red");
        columnTemplate.stroke = am4core.color("black");


    });
</script>