/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 07/07/2023 12:38:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_kendala_p2l
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kendala_p2l`;
CREATE TABLE `tbl_kendala_p2l`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `registrasi_p2l_id` int NULL DEFAULT NULL,
  `nama_kelompok_tani` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `komponen` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_kejadian` date NULL DEFAULT NULL,
  `uraian` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `uraian_foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `upaya_tindak_lanjut` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  `upaya_foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_kirim` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_kendala_p2l
-- ----------------------------
INSERT INTO `tbl_kendala_p2l` VALUES (1, 1, 'Aneka Usaha II', 'komponen oke', '2023-07-07', 'ini uraian', 'cd51f5c8ac60652ce29b79b44e241646.jpg', 'ini upaya tindak lanjut', '2a34ce4a449db9bd21ee4ff2082b2f25.jpg', '2023-07-07 04:52:15');

SET FOREIGN_KEY_CHECKS = 1;
