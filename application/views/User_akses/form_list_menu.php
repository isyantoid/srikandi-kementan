<table class="table table-bordered">
    <thead class="bg-primary text-white">
        <th>Menu</th>
        <th class="text-center">Show <br> <input type="checkbox" class="checkAllIndex" onClick="checkAll(this, 'show')">
        </th>
        <th class="text-center">Read <br> <input type="checkbox" class="checkAllIndex" onClick="checkAll(this, 'read')">
        </th>
        <th class="text-center">Create <br> <input type="checkbox" class="checkAllCreate"
                onClick="checkAll(this, 'create')"></th>
        <th class="text-center">Update <br> <input type="checkbox" class="checkAllUpdate"
                onClick="checkAll(this, 'update')"></th>
        <th class="text-center">Delete <br> <input type="checkbox" class="checkAllDelete"
                onClick="checkAll(this, 'delete')"></th>
    </thead>
    <tbody>
        <?php foreach($head_menu as $head): ?>
        <?php if($head['tipe'] == '1'): ?>
        <tr>
            <td><?= $head['nama'] ?></td>
            <td class="text-center">
                <input type="checkbox" <?= $head['show'] == '1' ? 'checked' : '' ?> class="show"
                    value="<?= $head['id'] ?>" name="show[]" onClick="updateAkses(this, '<?= $head['id'] ?>', 'show')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $head['read'] == '1' ? 'checked' : '' ?> class="read"
                    value="<?= $head['id'] ?>" name="read[]" onClick="updateAkses(this, '<?= $head['id'] ?>', 'read')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $head['create'] == '1' ? 'checked' : '' ?> class="create"
                    value="<?= $head['id'] ?>" name="create[]"
                    onClick="updateAkses(this, '<?= $head['id'] ?>', 'create')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $head['update'] == '1' ? 'checked' : '' ?> class="update"
                    value="<?= $head['id'] ?>" name="update[]"
                    onClick="updateAkses(this, '<?= $head['id'] ?>', 'update')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $head['delete'] == '1' ? 'checked' : '' ?> class="delete"
                    value="<?= $head['id'] ?>" name="delete[]"
                    onClick="updateAkses(this, '<?= $head['id'] ?>', 'delete')">
            </td>
        </tr>
        <?php else: ?>
        <tr>
            <td class="font-weight-bold"><?= $head['nama'] ?></td>
            <td class="text-center">
                <input type="checkbox" <?= $head['show'] == '1' ? 'checked' : '' ?> class="show"
                    value="<?= $head['id'] ?>" name="show[]" onClick="updateAkses(this, '<?= $head['id'] ?>', 'show')">
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php endif; ?>
        <?php foreach($this->model->sub_menu($user_level_id, $head['id']) as $row): ?>

        <?php if($row['tipe'] == '1'): ?>
        <tr>
            <td><?= $row['nama'] ?></td>
            <td class="text-center">
                <input type="checkbox" <?= $row['show'] == '1' ? 'checked' : '' ?> class="show"
                    value="<?= $row['id'] ?>" name="show[]" onClick="updateAkses(this, '<?= $row['id'] ?>', 'show')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $row['read'] == '1' ? 'checked' : '' ?> class="read"
                    value="<?= $row['id'] ?>" name="read[]" onClick="updateAkses(this, '<?= $row['id'] ?>', 'read')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $row['create'] == '1' ? 'checked' : '' ?> class="create"
                    value="<?= $row['id'] ?>" name="create[]"
                    onClick="updateAkses(this, '<?= $row['id'] ?>', 'create')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $row['update'] == '1' ? 'checked' : '' ?> class="update"
                    value="<?= $row['id'] ?>" name="update[]"
                    onClick="updateAkses(this, '<?= $row['id'] ?>', 'update')">
            </td>
            <td class="text-center">
                <input type="checkbox" <?= $row['delete'] == '1' ? 'checked' : '' ?> class="delete"
                    value="<?= $row['id'] ?>" name="delete[]"
                    onClick="updateAkses(this, '<?= $row['id'] ?>', 'delete')">
            </td>
        </tr>
        <?php else: ?>
        <tr>
            <td class="font-weight-bold"><?= $row['nama'] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php endif ?>
        <?php endforeach ?>
        <?php endforeach ?>

    </tbody>
</table>

<script>
function checkAll(oInput, className) {
    var aInputs = $('.' + className);

    for (var i = 0; i < aInputs.length; i++) {
        aInputs[i].checked = oInput.checked;
        updateAkses(aInputs[i], aInputs[i].value, className)
    }
}

function updateAkses(oInput, menuID, akses) {
    var levelID = $('.user_level_id :selected').val();
    var valInput = (oInput.checked) ? '1' : '0';

    $.ajax({
        url: '{segment1}update_akses_user',
        dataType: 'json',
        method: 'post',
        data: {
            user_level_id: levelID,
            menu_id: menuID,
            value: valInput,
            akses: akses,
        },
        success: function(res) {
            console.log(res)
        }
    })
}
</script>