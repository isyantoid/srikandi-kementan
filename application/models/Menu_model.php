<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model {

    private $tableName = 'tbl_menu';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);

            $password = $this->input->post('password');
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['username'] != $this->input->post('username')) {
                    if (isDuplicate($this->tableName, 'username', $this->input->post('username'))) {
                        return jsonOutputError('Username sudah ada sebelumnya.');
                    }
                }
                if ($checkDup['email'] != $this->input->post('email')) {
                    if (isDuplicate($this->tableName, 'email', $this->input->post('email'))) {
                        return jsonOutputError('Email sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                if($password) $this->db->set('password', md5($password));
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate($this->tableName, 'username', $this->input->post('username'))) {
                    return jsonOutputError('Username sudah ada sebelumnya.');
                }
                if (isDuplicate($this->tableName, 'email', $this->input->post('email'))) {
                    return jsonOutputError('Email sudah ada sebelumnya.');
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->set('password', md5($password));
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }

        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}
