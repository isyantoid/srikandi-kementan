<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
        data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <?php $appId = get_user()['app_id']; ?>
            <?php $user_level_id = get_user()['user_level_id']; ?>


            <?php
                $menus = $this->db
                ->select('
                tbl_menu.id,
                tbl_menu.nama,
                tbl_menu.tipe,
                tbl_menu.subId,
                tbl_menu.icon,
                tbl_menu.url,
                tbl_menu.nomor_urut
                ')
                ->where('tbl_menu.subId',null)
                ->where('tbl_user_akses.user_level_id',$user_level_id)
                ->where('tbl_user_akses.show','1')
                ->join('tbl_menu', 'tbl_user_akses.menu_id = tbl_menu.id')
                ->order_by('nomor_urut', 'asc')
                ->get('tbl_user_akses')->result_array();
                // $menus = $this->db->where('subId',null)->where('app_id',null)->order_by('nomor_urut', 'asc')->get('tbl_menu')->result_array();
                // if($appId == '4') {
                //     $menus = $this->db->where('subId',null)->where('app_id',4)->order_by('nomor_urut', 'asc')->get('tbl_menu')->result_array();
                // }
            ?>
            <?php foreach($menus as $menu): ?>
            <?php if($menu['tipe'] == '1'): ?>




            <li class="kt-menu__item " aria-haspopup="true"><a href="{site_url}<?= $menu['url'] ?>" class="kt-menu__link "><i
                        class="kt-menu__link-icon <?= $menu['icon'] ?>"></i><span
                        class="kt-menu__link-text"><?= $menu['nama'] ?></span></a></li>
            <?php else: ?>
                <?php if($menu['url'] != null): ?>
                    <li class="kt-menu__item " aria-haspopup="true"><a href="{site_url}<?= $menu['url'] ?>" class="kt-menu__link "><i
                        class="kt-menu__link-icon <?= $menu['icon'] ?>"></i><span
                        class="kt-menu__link-text"><?= $menu['nama'] ?></span></a></li>
                <?php else : ?>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                        <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i
                                class="kt-menu__link-icon <?= $menu['icon'] ?>"></i><span
                                class="kt-menu__link-text"><?= $menu['nama'] ?></span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span
                                        class="kt-menu__link-text"><?= $menu['nama'] ?></span></li>
                                <?php
                                $subMenus = $this->db
                                ->select('
                                tbl_menu.id,
                                tbl_menu.nama,
                                tbl_menu.tipe,
                                tbl_menu.subId,
                                tbl_menu.icon,
                                tbl_menu.url,
                                tbl_menu.nomor_urut
                                ')
                                ->where('tbl_menu.subId',$menu['id'])
                                ->where('tbl_user_akses.user_level_id',$user_level_id)
                                ->where('tbl_user_akses.show','1')
                                ->join('tbl_menu', 'tbl_user_akses.menu_id = tbl_menu.id')
                                ->order_by('tbl_menu.nomor_urut', 'asc')
                                ->get('tbl_user_akses')->result_array();
                                ?>
                                <?php foreach($subMenus as $subMenu): ?>
                                <li class="kt-menu__item " aria-haspopup="true">
                                    <a href="{site_url}<?= $subMenu['url'] ?>" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i>
                                        <span class="kt-menu__link-text"><?= $subMenu['nama'] ?></span>
                                    </a>
                                </li>
                                <?php endforeach ?>
                            </ul>
                    </li>
                <?php endif ?>

            <?php endif ?>
            <?php endforeach ?>
        </ul>
    </div>
</div>

<!-- end:: Aside Menu -->