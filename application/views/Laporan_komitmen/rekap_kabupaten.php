<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">List Data</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}export_rekap_kabupaten/<?= $kabupaten_kode ?>"
                            class="btn btn-brand btn-elevate btn-icon-sm">Export</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Nama Kelompok</th>
                        <th>Desa/Kelurahan</th>
                        <th>Kecamatan</th>
                        <th>Kabupaten/Kota</th>
                        <th>Ketua Kelompok</th>
                    </thead>
                    <tbody>
                        <?php if($get_laporan): ?>
                        <?php $no=1; foreach($get_laporan as $row): ?>
                        <tr>
                            <td class="text-center"><?= $no ?></td>
                            <td><?= $row['nama_kelompok'] ?></td>
                            <td><?= $row['desa_nama'] ?></td>
                            <td><?= $row['kecamatan_nama'] ?></td>
                            <td><?= $row['kabupaten_nama'] ?></td>
                            <td><?= $row['nama_ketua'] ?></td>
                        </tr>
                        <?php $no++; endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCynBKMoc3o3YGdscEYadjoFFyqtXhqjuY&callback=initMap">
</script>

<script>
$('.kabupaten_kode, .jenis_komoditi_id').select2({
    placeholder: 'Pilih Opsi'
}).val('').trigger('change');

ajaxSelect({
    allowClear: true,
    id: '.provinsi_kode',
    url: '{site_url}ajax_selectbox/provinsi_select',
    selected: '<?= $this->input->get('provinsi_kode') ?>'
});

$('.provinsi_kode').change(function() {
    var val = $(this).val();
    if (val) {
        $('.kabupaten_kode').empty();
        ajaxSelect({
            allowClear: true,
            id: '.kabupaten_kode',
            url: '{site_url}ajax_selectbox/kabupaten_select',
            optionalSearch: {
                provinsi_kode: val
            },
            selected: '<?= $this->input->get('kabupaten_kode') ?>'
        });
    }
});

$('.provinsi_kode').on("select2:unselecting", function(e) {
    $('.kabupaten_kode').empty();
    $('.kabupaten_kode, .jenis_komoditi_id').select2({
        placeholder: 'Pilih Opsi'
    }).val('').trigger('change');
});

ajaxSelect({
    id: '.kelompok_tani_id',
    url: '{site_url}ajax_selectbox/kelompok_tani_id',
    selected: '<?= $this->input->get('kelompok_tani_id') ?>'
});
</script>