<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_komitmen_cabai_model extends CI_Model
{

    private $tableName = 'champ_komitmen';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                $check = $this->db
                ->where('user_id', $this->session->userdata('session_user_id'))
                ->where('tahun', $this->input->post('tahun'))
                ->where('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'))
                ->get($this->tableName)->row_array();
                if($check) {
                    return jsonOutputError('Tahun & Komoditi yang diisi sudah ada sebelumnya. Silahkan ubah data yang sebelumnya');
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }

                $userId = $this->session->userdata('session_user_id');
                $champ = getRowArray('champ', ['user_id' => $this->session->userdata('session_user_id')]);
                if($userId == $champ['id']) {
                    $this->db->set('champ_id', $champ['id']);
                } else {
                    $this->db->set('champ_id', $this->input->post('champ_id'));
                }

                $this->db->set('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'));
                $this->db->set('user_id', $this->session->userdata('session_user_id'));
                $this->db->set('created_at', date('Y-m-d H:i:s'));
                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}