<div class="kt-portlet__body">
    <div class="slider-hero">
        <div class="featured-carousel owl-carousel">
            <?php foreach ($query_artikel as $qa) : ?>
            <div class="item">
                <div class="work">
                    <div class="img d-flex align-items-center justify-content-center"
                        style="background-image: url(<?php echo base_url('/uploads/form_artikel/' . $qa->id . '/' . $qa->image); ?>);">
                        <div class="text text-center w-75">
                            <a href="{site_url}mockup/baca_artikel/<?php echo $qa->id; ?>">
                                <h2><?php echo $qa->nama_artikel; ?></h2>
                                <p class="text-white"><?php echo $qa->keterangan; ?>
                                </p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>

</div>


<script>
(function($) {

    "use strict";

    var fullHeight = function() {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function() {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    var owl = $('.featured-carousel');

    $('.featured-carousel').owlCarousel({
        animateOut: 'fadeOut',
        center: false,
        items: 1,
        loop: true,
        stagePadding: 0,
        margin: 0,
        smartSpeed: 1500,
        autoplay: true,
        dots: true,
        nav: false,
        navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
    });

    $('.thumbnail li').each(function(slide_index) {
        $(this).click(function(e) {
            owl.trigger('to.owl.carousel', [slide_index, 1500]);
            e.preventDefault();
        })
    })

    owl.on('changed.owl.carousel', function(event) {
        $('.thumbnail li').removeClass('active');
        $('.thumbnail li').eq(event.item.index - 2).addClass('active');
    })

})(jQuery);

(function($) {

    "use strict";

    var fullHeight = function() {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function() {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    var carousel = function() {
        $('.featured-carousel-2').owlCarousel({
            loop: false,
            autoplay: true,
            margin: 30,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            nav: false,
            dots: false,
            autoplayHoverPause: false,
            items: 1,
            navText: ["<span class='ion-ios-arrow-back'></span>",
                "<span class='ion-ios-arrow-forward'></span>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });

    };
    carousel();

})(jQuery);
</script>