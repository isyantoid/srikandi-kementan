<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->

<head>
    <base href="../../../">
    <meta charset="utf-8" />
    <title>Sistem Informasi Kebun Atau Lahan Usaha Sayur dan Lahan - Kementerian Pertanian RI</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{dist_path}assets/css/pages/login/login-2.css" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{dist_path}assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{dist_path}assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{dist_path}assets/css/custom.css">

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{dist_path}assets/media/logos/favicon.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--home" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-login"
                style="background-image: url({dist_path}images/bg.jpg);">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper" style="padding: 6% 2rem 1rem 2rem;">
                    <div class="kt-login__container">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="{dist_path}images/logo.png" width="600">
                            </a>
                        </div>
                        <div class="kt-login__home">
                            <div class="kt-login__head">
                                <div class="title">
                                    <center>
                                        <b style="font-size:x-large; color:aliceblue;">DIREKTORAT JENDERAL
                                            HORTIKULTURA</b>
                                    </center>
                                </div>
                            </div>
                            <form class="kt-form" action="{site_url}beranda/do_login" method="post">
                                <?php if($this->session->flashdata('error_login')): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <div class="alert-text"><?php echo $this->session->flashdata('error_login') ?></div>
                                    </div>
                                <?php endif ?>
                                <?php if ($this->session->flashdata('gagal_masuk')) : ?>
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <div class="alert-text"><?php echo $this->session->flashdata('gagal_masuk') ?></div>
                                    <div class="alert-close">
                                        <i class="flaticon2-cross kt-icon-sm" data-dismiss="alert"></i>
                                    </div>
                                </div>
                                <?php endif ?>

                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Masukkan Username" name="username" autocomplete="off" required>
                                </div>
                                <div class="input-group">
                                    <input class="form-control" type="password" placeholder="Masukan Password" name="password" autocomplete="off" required>
                                </div>
                                <div class="kt-login__actions">
                                    <input type="submit" id="" class="btn btn-pill kt-login__btn-primary">
                                    <button type="button" id="" class="btn btn-pill kt-login__btn-primary cancel">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#22b9ff",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{dist_path}assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="{dist_path}assets/js/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->

    <script>
    $(document).on('click', '.cancel', function() {
        window.location = '{site_url}';
    });

    $(document).on('click', '.submit', function() {
        window.location = '{site_url}dashboard';
    });
    </script>

</body>

<!-- end::Body -->

</html>