/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 19/06/2023 17:15:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_capaian_kinerja
-- ----------------------------
DROP TABLE IF EXISTS `tbl_capaian_kinerja`;
CREATE TABLE `tbl_capaian_kinerja`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `registrasi_p2l_id` int NULL DEFAULT NULL,
  `nama_kelompok_tani` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_komoditi_id` int NULL DEFAULT NULL,
  `tanggal_capaian_kinerja` date NULL DEFAULT NULL,
  `jumlah` int NULL DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  `tipe` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '1 produksi benih',
  `tanggal_kirim` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_capaian_kinerja
-- ----------------------------
INSERT INTO `tbl_capaian_kinerja` VALUES (1, 1, 'Aneka Usaha II', 61, '2023-06-19', 100, '1', '1', '2023-06-19 10:57:04');
INSERT INTO `tbl_capaian_kinerja` VALUES (2, 1, 'Aneka Usaha II', 62, '2023-06-19', 50, '1', '2', '2023-06-19 11:14:27');

SET FOREIGN_KEY_CHECKS = 1;
