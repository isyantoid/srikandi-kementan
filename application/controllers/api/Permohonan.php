<?php

defined('BASEPATH') or exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Permohonan extends Api
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('','');
    }

    public function registrasi_all_get() {
        $id_registrasi = $this->get('id_registrasi');
        $status_kabupaten = $this->get('status_kabupaten');
        $status_provinsi = $this->get('status_provinsi');
        $sertifikat = $this->get('sertifikat');
        $this->db->select('
            id_pemohon idPermohonan, 
            nomor_registrasi nomorRegistrasi, 
            GAP gap,  
            tanggal_permohonan tanggalPermohonan, 
            tanggal_pengesahan tanggalPengesahan, 
            DATE_FORMAT(tgl_kirim_pelaku_usaha, "%Y-%m-%d") as tanggalKirimPelakuUsaha, 
            CASE WHEN status_pelaku_usaha = "1" THEN CONCAT("TERKIRIM") ELSE CONCAT("BELUM TERKIRIM") END as statusPelakuUsaha, 
            permohonan_m.nama_lengkap as namaLengkapPemohon, 
            permohonan_m.alamat,
            alamat_kebun alamatKebun,
            jenis_pengajuan jenisPengajuan, 
            CONCAT(luas_kebun, " ", permohonan_m.satuan_luas) luasKebun,
            komoditi_jenis_m.komoditi_jenis_nama namaJenisKomoditi, 
            UPPER(status_kabupaten) statusKabupaten,
            kabupaten_m.kabupaten_nama namaKabupaten, 
            kecamatan_m.kecamatan_nama namaKecamatan,
            provinsi_m.provinsi_nama namaProvinsi, 
            UPPER(permohonan_m.status_provinsi) statusProvinsi,
            permohonan_m.tgl_kirim_kabupaten tanggalKirimKabupaten,
            permohonan_m.tgl_kirim_provinsi tanggalKirimProvinsi,
            permohonan_m.noreg noreg,
            permohonan_m.file_sertifikat fileSertifikat,
        ');
        $this->db->join('registrasi_m', 'permohonan_m.id_registrasi = registrasi_m.registrasi_id','left');
        $this->db->join('komoditi_m', 'komoditi_m.komoditi_kode = permohonan_m.id_komoditi', 'left');
        $this->db->join('komoditi_jenis_m', 'komoditi_jenis_m.komoditi_jenis_kode = permohonan_m.id_jenis_komoditi', 'left');
        $this->db->join('user_t', 'user_t.id_registrasi = registrasi_m.registrasi_id', 'left');
        $this->db->join('provinsi_m', 'provinsi_m.provinsi_kode = user_t.id_provinsi');
        $this->db->join('kabupaten_m', 'kabupaten_m.kabupaten_kode = user_t.id_kabupaten AND kabupaten_m.provinsi_kode = user_t.id_provinsi');
        $this->db->join('kecamatan_m', 'kecamatan_m.provinsi_kode = user_t.id_provinsi AND kecamatan_m.kabupaten_kode = user_t.id_kabupaten AND kecamatan_m.kecamatan_kode = user_t.id_kecamatan');
        $this->db->where('permohonan_m.id_registrasi', $id_registrasi);
        if($status_kabupaten) $this->db->where('permohonan_m.status_kabupaten', $status_kabupaten);
        if($status_provinsi) $this->db->where('permohonan_m.status_provinsi', $status_provinsi);
        if($sertifikat == '1') $this->db->where('permohonan_m.noreg !=', '');
        $get = $this->db->get('permohonan_m');
        if($get->num_rows() > 0) {
            $data = $get->result_array();
            $this->set_response(dataApiRespon(true, 'Data permohonan', $data), REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data'), REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function cek_nomor_registrasi_get() {
        
        $nomor_registrasi = $this->get('nomor_registrasi');
        if(substr($nomor_registrasi, 0,4) == 'http') {
            $exp = explode("/", $nomor_registrasi);
            $nomor_registrasi = $exp[5];
        } else {
            $nomor_registrasi = $this->get('nomor_registrasi');
        }

        $status_kabupaten = $this->get('status_kabupaten');
        $status_provinsi = $this->get('status_provinsi');
        $sertifikat = $this->get('sertifikat');
        $this->db->select('
            id_pemohon idPermohonan, 
            nomor_registrasi nomorRegistrasi, 
            GAP gap,  
            tanggal_permohonan tanggalPermohonan, 
            tanggal_pengesahan tanggalPengesahan, 
            DATE_FORMAT(tgl_kirim_pelaku_usaha, "%Y-%m-%d") as tanggalKirimPelakuUsaha, 
            CASE WHEN status_pelaku_usaha = "1" THEN CONCAT("TERKIRIM") ELSE CONCAT("BELUM TERKIRIM") END as statusPelakuUsaha, 
            permohonan_m.nama_lengkap as namaLengkapPemohon, 
            permohonan_m.alamat,
            alamat_kebun alamatKebun,
            jenis_pengajuan jenisPengajuan, 
            CONCAT(luas_kebun, " ", permohonan_m.satuan_luas) luasKebun,
            komoditi_jenis_m.komoditi_jenis_nama namaJenisKomoditi, 
            UPPER(status_kabupaten) statusKabupaten,
            kabupaten_m.kabupaten_nama namaKabupaten, 
            kecamatan_m.kecamatan_nama namaKecamatan,
            provinsi_m.provinsi_nama namaProvinsi, 
            UPPER(permohonan_m.status_provinsi) statusProvinsi,
            permohonan_m.tgl_kirim_kabupaten tanggalKirimKabupaten,
            permohonan_m.tgl_kirim_provinsi tanggalKirimProvinsi,
        ');
        $this->db->join('registrasi_m', 'permohonan_m.id_registrasi = registrasi_m.registrasi_id','left');
        $this->db->join('komoditi_m', 'komoditi_m.komoditi_kode = permohonan_m.id_komoditi', 'left');
        $this->db->join('komoditi_jenis_m', 'komoditi_jenis_m.komoditi_jenis_kode = permohonan_m.id_jenis_komoditi', 'left');
        $this->db->join('user_t', 'user_t.id_registrasi = registrasi_m.registrasi_id', 'left');
        $this->db->join('provinsi_m', 'provinsi_m.provinsi_kode = user_t.id_provinsi', 'left');
        $this->db->join('kabupaten_m', 'kabupaten_m.kabupaten_kode = user_t.id_kabupaten AND kabupaten_m.provinsi_kode = user_t.id_provinsi','left');
        $this->db->join('kecamatan_m', 'kecamatan_m.provinsi_kode = user_t.id_provinsi AND kecamatan_m.kabupaten_kode = user_t.id_kabupaten AND kecamatan_m.kecamatan_kode = user_t.id_kecamatan', 'left');
        $this->db->where('permohonan_m.nomor_registrasi', $nomor_registrasi);
        if($status_kabupaten) $this->db->where('permohonan_m.status_kabupaten', $status_kabupaten);
        if($status_provinsi) $this->db->where('permohonan_m.status_provinsi', $status_provinsi);
        // if($sertifikat == '1') $this->db->where('permohonan_m.noreg !=', '');
        $get = $this->db->get('permohonan_m');
        if($get->num_rows() > 0) {
            $data = $get->row_array();
            $this->set_response(dataApiRespon(true, 'Data permohonan', $data), REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data dengan nomor registrasi ' . $nomor_registrasi), REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function cek_sertifikat_get() {
        $nomor_gap = $this->get('nomor_gap');
        $this->db->select('
            id_pemohon idPermohonan, 
            nomor_registrasi nomorRegistrasi, 
            gap gap,  
            tanggal_permohonan tanggalPermohonan, 
            tanggal_pengesahan tanggalPengesahan, 
            DATE_FORMAT(tgl_kirim_pelaku_usaha, "%Y-%m-%d") as tanggalKirimPelakuUsaha, 
            CASE WHEN status_pelaku_usaha = "1" THEN CONCAT("TERKIRIM") ELSE CONCAT("BELUM TERKIRIM") END as statusPelakuUsaha, 
            view_permohonan.nama_lengkap as namaLengkapPemohon, 
            view_permohonan.alamat,
            alamat_kebun alamatKebun,
            jenis_pengajuan jenisPengajuan, 
            CONCAT(luas_kebun, " ", view_permohonan.satuan_luas) luasKebun,
            view_permohonan.komoditi_jenis_nama namaJenisKomoditi, 
            UPPER(status_kabupaten) statusKabupaten,
            view_permohonan.kabupaten_nama namaKabupaten, 
            view_permohonan.kecamatan_nama namaKecamatan,
            view_permohonan.provinsi_nama namaProvinsi, 
            UPPER(view_permohonan.status_provinsi) statusProvinsi,
            view_permohonan.tgl_kirim_kabupaten tanggalKirimKabupaten,
            view_permohonan.tgl_kirim_provinsi tanggalKirimProvinsi,
        ');
        $this->db->where('view_permohonan.gap', $nomor_gap);
        $this->db->where('view_permohonan.status_provinsi', 'terima');
        $get = $this->db->get('view_permohonan');
        if($get->num_rows() > 0) {
            $data = $get->row_array();
            $this->set_response(dataApiRespon(true, 'Data permohonan', $data), REST_Controller::HTTP_OK);
        } else {
            $this->set_response(dataApiRespon(false, 'Tidak ditemukan data'), REST_Controller::HTTP_NOT_FOUND);
        }
    }
}
