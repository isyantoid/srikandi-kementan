<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_komoditi_model extends CI_Model {

    private $tableName = 'tbl_jenis_komoditi';
    private $primaryKey = 'id';
    
    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }

                $config = $this->config_file_upload();                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                if ( $this->upload->do_upload('jenis_komoditi_icon')){
                    $data_upload = $this->upload->data();
                    $this->db->set('jenis_komoditi_icon', $data_upload['file_name']);
                }
                
                if ( $this->upload->do_upload('jenis_komoditi_pin')){
                    $data_upload = $this->upload->data();
                    $this->db->set('jenis_komoditi_pin', $data_upload['file_name']);
                }

                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {


                $config = $this->config_file_upload();                    
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                
                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                
                if ( $this->upload->do_upload('jenis_komoditi_icon')){
                    $data_upload = $this->upload->data();
                    $this->db->set('jenis_komoditi_icon', $data_upload['file_name']);
                }
                
                if ( $this->upload->do_upload('jenis_komoditi_pin')){
                    $data_upload = $this->upload->data();
                    $this->db->set('jenis_komoditi_pin', $data_upload['file_name']);
                }

                $insert = $this->db->insert($this->tableName);
                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }

        }
    }


    public function config_file_upload() {
        $config['upload_path'] = './dist/images/jenis_komoditi/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}
