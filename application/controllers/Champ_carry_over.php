<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_carry_over extends CI_Controller
{

    private $title = 'Master Carry Over';
    private $view = 'Champ_carry_over';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('
            champ_carry_over.*, 
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');
        $this->datatables->join('tbl_jenis_komoditi','champ_carry_over.jenis_komoditi_id = tbl_jenis_komoditi.id');
        if(champ_user() == 'bamer') {
            $this->datatables->where('champ_carry_over.jenis_komoditi_id', '68');
        } else if(champ_user() == 'cabai') {
            $this->datatables->where('champ_carry_over.jenis_komoditi_id !=', '68');
        }
        $this->datatables->from('champ_carry_over');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('champ_carry_over', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit ' . $this->title;
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}