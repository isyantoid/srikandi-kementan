<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Level</label>
                    <div class="col-6">
                        <select class="form-control user_level_id" name="user_level_id" style="width:100%;"></select>
                    </div>
                </div>

                <div class="mt-5">
                    <div class="form_list_menu"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>


    ajaxSelect({
        id: '.user_level_id',
        url: '{site_url}ajax_selectbox/user_level_id',
    });

    $('.user_level_id').change(function() {
        var val = $(this).val();
        if (val) {
            $.ajax({
                url: '{segment1}get_list_menu',
                method: 'post',
                data: {
                    user_level_id: val,
                },
                success: function(res) {
                    $('.form_list_menu').html(res)
                }
            })
        }
    });
    

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>