<div class="row">
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> Dokumentasi Tanam</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Komoditas</th>
                                <th>Tanggal Tanam</th>
                                <th>Kampung</th>
                                <th>Kelompok</th>
                                <th>Luas tanam</th>
                            </thead>
                            <tbody>
                                <?php $jenis_komoditi_nama = '' ?>
                                <?php if($this->model->view_dokumentasi_tanam()) : ?>
                                    <?php foreach($this->model->view_dokumentasi_tanam() as $row): ?>
                                        
                                        <tr>
                                            <td><?php echo ($jenis_komoditi_nama == $row['jenis_komoditi_nama']) ? '' : $row['jenis_komoditi_nama'] ?></td>
                                            <td><?php echo formatTanggal($row['tanggal_tanam']) ?></td>
                                            <td><?php echo $row['desa_nama'] . ' - ' . $row['kecamatan_nama'] . ' - ' . $row['kabupaten_nama'] . ' - ' . $row['provinsi_nama'] ?></td>
                                            <td><?php echo $row['nama_kelompok'] ?></td>
                                            <td><?php echo number_format($row['luas_tanam']) ?>Ha</td>
                                        </tr>

                                        <?php $jenis_komoditi_nama = $row['jenis_komoditi_nama'] ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> Perkiraan panen</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Komoditas</th>
                                <th>Tanggal Panen</th>
                                <th>Kampung</th>
                                <th>Kelompok</th>
                            </thead>
                            <tbody>
                                <?php $jenis_komoditi_nama = '' ?>
                                <?php if($this->model->view_dokumentasi_perkiraan_panen()) : ?>
                                    <?php foreach($this->model->view_dokumentasi_perkiraan_panen() as $row): ?>
                                        
                                        <tr>
                                            <td><?php echo ($jenis_komoditi_nama == $row['jenis_komoditi_nama']) ? '' : $row['jenis_komoditi_nama'] ?></td>
                                            <td><?php echo formatTanggal($row['tanggal_perkiraan_panen']) ?></td>
                                            <td><?php echo $row['desa_nama'] . ' - ' . $row['kecamatan_nama'] . ' - ' . $row['kabupaten_nama'] . ' - ' . $row['provinsi_nama'] ?></td>
                                            <td><?php echo $row['nama_kelompok'] ?></td>
                                        </tr>

                                        <?php $jenis_komoditi_nama = $row['jenis_komoditi_nama'] ?>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="flaticon2-list-3"></i>
                    </span>
                    <h3 class="kt-portlet__head-title"> Dokumentasi Panen</h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Komoditas</th>
                                <th>Tanggal panen</th>
                                <th>Kampung</th>
                                <th>Kelompok</th>
                                <th>Luas panen</th>
                                <th>Hasil panen</th>
                            </thead>
                            <tbody>
                                <?php $jenis_komoditi_nama = '' ?>
                                <?php if($this->model->view_dokumentasi_panen()) : ?>
                                    <?php foreach($this->model->view_dokumentasi_panen() as $row): ?>
                                        
                                        <tr>
                                            <td><?php echo ($jenis_komoditi_nama == $row['jenis_komoditi_nama']) ? '' : $row['jenis_komoditi_nama'] ?></td>
                                            <td><?php echo formatTanggal($row['tanggal_panen']) ?></td>
                                            <td><?php echo $row['desa_nama'] . ' - ' . $row['kecamatan_nama'] . ' - ' . $row['kabupaten_nama'] . ' - ' . $row['provinsi_nama'] ?></td>
                                            <td><?php echo $row['nama_kelompok'] ?></td>
                                            <td><?php echo number_format($row['luas_panen']) ?>Ha</td>
                                            <td><?php echo number_format($row['jumlah_hasil_panen']) ?></td>
                                        </tr>

                                        <?php $jenis_komoditi_nama = $row['jenis_komoditi_nama'] ?>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="6" class="text-danger text-center">Tidak ditemukan data</td>
                                    </tr>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>