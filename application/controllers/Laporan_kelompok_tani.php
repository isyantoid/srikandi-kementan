<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_kelompok_tani extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_kelompok_tani_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $this->data['content'] = 'Laporan_kelompok_tani/index';
        $this->data['title'] = 'Laporan Kelompok Tani';
        $this->data['subTitle'] = '';
        $this->data['laporan'] = $this->model->get_laporan();
        $data = array_merge($this->data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function export_data() {
        akses_user('read');
        $fileName = 'laporan_kelompok_tani_' . date('d_m_Y') . '.xlsx';
        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();
        $defaultStyle = $objPHPExcel->getDefaultStyle();

        $defaultStyle->getFont()->setName('Times New Roman');
        $defaultStyle->getFont()->setSize(10);
        
        $sheet = $objPHPExcel->setActiveSheetIndex(0);

        $styleArray = [
            'font' => [
                'bold' => false,
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
        
        $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
        $objPHPExcel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        

        $sheet->mergeCells('A1:H3')->SetCellValue('A1', 'Detail Kelompok Tani');
        $sheet->mergeCells('I1:V3')->SetCellValue('I1', 'Bantuan');
        $sheet->mergeCells('W1:AG3')->SetCellValue('W1', 'Capaian Kinerja');
        $sheet->mergeCells('AH1:AO2')->SetCellValue('AH1', 'Kendala');
        $sheet->mergeCells('AH3:AL3')->SetCellValue('AH3', 'Tanam');
        $sheet->mergeCells('AM3:AO3')->SetCellValue('AM3', 'Panen');
        $sheet->mergeCells('AP1:AS3')->SetCellValue('AP1', 'Pemantauan Tanam');
        $sheet->mergeCells('AT1:AZ3')->SetCellValue('AT1', 'Dokumentasi');

        $sheet->mergeCells('A4:A5')->SetCellValue('A4', 'No.');
        $sheet->mergeCells('B4:B5')->SetCellValue('B4', 'Kelompok Tani');
        $sheet->mergeCells('C4:E4')->SetCellValue('C4', 'Alamat');
        $sheet->SetCellValue('C5', 'Desa');
        $sheet->SetCellValue('D5', 'Kecamatan');
        $sheet->SetCellValue('E5', 'Kabupaten');
        $sheet->mergeCells('F4:F5')->SetCellValue('F4', 'Jumlah Peneriman Manfaat (Orang)');
        $sheet->mergeCells('G4:G5')->SetCellValue('G4', 'Terdaftar Simluhtan');
        $sheet->mergeCells('H4:H5')->SetCellValue('H4', 'Titik Koordinat');
        $sheet->mergeCells('I4:I5')->SetCellValue('I4', 'Tanggal Terima Bantuan');
        $sheet->mergeCells('J4:J5')->SetCellValue('J4', 'Realisasi Pemanfaatan Anggaran (Rp)');
        $sheet->mergeCells('K4:M4')->SetCellValue('K4', 'Realisasi Fisik Sarana Perbenihan');
        $sheet->SetCellValue('K5', 'Jenis');
        $sheet->SetCellValue('L5', 'Jumlah');
        $sheet->SetCellValue('M5', 'Foto');
        $sheet->mergeCells('N4:P4')->SetCellValue('N4', 'Realisasi Fisik Demplot');
        $sheet->SetCellValue('N5', 'Jenis');
        $sheet->SetCellValue('O5', 'Jumlah');
        $sheet->SetCellValue('P5', 'Foto');
        $sheet->mergeCells('Q4:S4')->SetCellValue('Q4', 'Realisasi Fisik Pertanaman');
        $sheet->SetCellValue('Q5', 'Jenis');
        $sheet->SetCellValue('R5', 'Jumlah');
        $sheet->SetCellValue('S5', 'Foto');
        $sheet->mergeCells('T4:V4')->SetCellValue('T4', 'Realisasi Fisik Sarana Pascapanen');
        $sheet->SetCellValue('T5', 'Jenis');
        $sheet->SetCellValue('U5', 'Jumlah');
        $sheet->SetCellValue('V5', 'Foto');
        $sheet->mergeCells('W4:X4')->SetCellValue('W4', 'Produksi Benih');
        $sheet->SetCellValue('W5', 'Komoditas');
        $sheet->SetCellValue('X5', 'Jumlah');
        $sheet->mergeCells('Y4:Z4')->SetCellValue('Y4', 'Hasil Panen Demplot');
        $sheet->SetCellValue('Y5', 'Komoditas');
        $sheet->SetCellValue('Z5', 'Jumlah');
        $sheet->mergeCells('AA4:AB4')->SetCellValue('AA4', 'Hasil Panen Pertanaman ');
        $sheet->SetCellValue('AA5', 'Komoditas');
        $sheet->SetCellValue('AB5', 'Jumlah');
        $sheet->mergeCells('AC4:AD4')->SetCellValue('AC4', 'Hasil Penjualan Pendapatan Kelompok');
        $sheet->SetCellValue('AC5', 'Komoditas');
        $sheet->SetCellValue('AD5', 'Jumlah');
        $sheet->mergeCells('AE4:AF4')->SetCellValue('AE4', 'Hasil Panen yang dikonsumsi Anggota');
        $sheet->SetCellValue('AE5', 'Komoditas');
        $sheet->SetCellValue('AF5', 'Jumlah');
        $sheet->mergeCells('AG4:AG5')->SetCellValue('AG4', 'Penghematan Pengeluaran Belanja Pangan (Rp)');
        $sheet->mergeCells('AH4:AK4')->SetCellValue('AH4', 'Keterangan');
        $sheet->SetCellValue('AH5', 'Jenis Komoditas');
        $sheet->SetCellValue('AI5', 'Tanggal Kendala');
        $sheet->SetCellValue('AJ5', 'Kendala');
        $sheet->SetCellValue('AK5', 'Luas Lahan Terkendala (Ha)');
        $sheet->mergeCells('AL4:AL5')->SetCellValue('AL4', 'Foto');
        $sheet->mergeCells('AM4:AN4')->SetCellValue('AN4', 'Keterangan');
        $sheet->SetCellValue('AM5', 'Tanggal Kendala');
        $sheet->SetCellValue('AN5', 'Luas Lahan Terkendala (Ha)');
        $sheet->mergeCells('AO4:AO5')->SetCellValue('AO4', 'Foto');
        $sheet->mergeCells('AP4:AQ4')->SetCellValue('AP4', 'Keterangan');
        $sheet->SetCellValue('AP5', 'Tanggal Pemantauan');
        $sheet->SetCellValue('AQ5', 'Permasalahan');
        $sheet->mergeCells('AR4:AR5')->SetCellValue('AR4', 'Tindak Lanjut');
        $sheet->mergeCells('AS4:AS5')->SetCellValue('AS4', 'Foto');
        $sheet->mergeCells('AT4:AU4')->SetCellValue('AT4', 'Keterangan');
        $sheet->SetCellValue('AT5', 'Tanggal Tanam');
        $sheet->SetCellValue('AU5', 'Perkiraan Panen');
        $sheet->mergeCells('AV4:AV5')->SetCellValue('AV4', 'Foto');
        $sheet->mergeCells('AW4:AY4')->SetCellValue('AW4', 'Keterangan');
        $sheet->SetCellValue('AW5', 'Tanggal Panen');
        $sheet->SetCellValue('AX5', 'Luas Panen (Ha)');
        $sheet->SetCellValue('AY5', 'Jumlah (Kg)');
        $sheet->mergeCells('AZ4:AZ5')->SetCellValue('AZ4', 'Foto');


        
        
        $bantuanKelompokTani = $this->model->get_bantuan_kelompok_tani();
        if($bantuanKelompokTani) {
            $rowCount = 6;
            foreach($bantuanKelompokTani as $row) {
                $sheet->SetCellValue('B' . $rowCount, $row['nama_kelompok'])->getColumnDimension('B')->setAutoSize(true);
                $sheet->SetCellValue('C' . $rowCount, '')->getColumnDimension('C')->setAutoSize(true);
                $sheet->SetCellValue('D' . $rowCount, $row['kecamatan_nama'])->getColumnDimension('D')->setAutoSize(true);
                $sheet->SetCellValue('E' . $rowCount, $row['kabupaten_nama'])->getColumnDimension('E')->setAutoSize(true);
                $simluhtan = $row['terdaftar_simluhtan'] == '1' ? 'Ya' : 'Tidak';
                $sheet->SetCellValue('G' . $rowCount, $simluhtan)->getColumnDimension('G')->setAutoSize(true);
                $sheet->SetCellValue('I' . $rowCount, $row['tanggal_kirim'])->getColumnDimension('I')->setAutoSize(true);
                $sheet->SetCellValue('J' . $rowCount, $row['total_nilai_bantuan'])->getColumnDimension('J')->setAutoSize(true);


                $bantuanDetail = $this->model->get_bantuan_detail($row['id']);
                if($bantuanDetail) {
                    foreach($bantuanDetail as $bandet) {
                        $sheet->SetCellValue('K' . $rowCount, $bandet['nama_bantuan'])->getColumnDimension('K')->setAutoSize(true);
                        $sheet->SetCellValue('L' . $rowCount, $bandet['jumlah'])->getColumnDimension('L')->setAutoSize(true);
                        $linkFoto = base_url('uploads/bantuan/' . $bandet['foto']);
                        $linkFoto = $bandet['foto'] ? $linkFoto : '';
                        if($linkFoto) {
                            $sheet->setCellValue('M' . $rowCount, '=Hyperlink("'.$linkFoto.'","Klik")');
                        }

                        $kendalaTanam = $this->model->get_kendala_tanam($row['kelompok_tani_id']);
                        $kendalaPanen = $this->model->get_kendala_panen($row['kelompok_tani_id']);
                        if($kendalaTanam) {
                            $sheet->SetCellValue('AI' . $rowCount, $kendalaTanam['tanggal_kendala_tanam'])->getColumnDimension('AI')->setAutoSize(true);
                            $sheet->SetCellValue('AJ' . $rowCount, $kendalaTanam['keterangan']);
                            $sheet->SetCellValue('AK' . $rowCount, $kendalaTanam['luas_lahan']);
                            $sheet->SetCellValue('AL' . $rowCount, '');
                        }
                        if($kendalaPanen) {
                            $sheet->SetCellValue('AM' . $rowCount, $kendalaPanen['tanggal_kendala_panen'])->getColumnDimension('AM')->setAutoSize(true);
                            $sheet->SetCellValue('AN' . $rowCount, $kendalaPanen['jumlah_produksi_terkendala']);
                            $sheet->SetCellValue('AO' . $rowCount, '');
                        }

                        $pemantauanTanam = $this->model->get_pemantauan_tanam($row['kelompok_tani_id']);
                        if($pemantauanTanam) {
                            $sheet->SetCellValue('AP' . $rowCount, $pemantauanTanam['tanggal'])->getColumnDimension('AP')->setAutoSize(true);
                            $sheet->SetCellValue('AQ' . $rowCount, $pemantauanTanam['penemuan_masalah']);
                            $sheet->SetCellValue('AR' . $rowCount, $pemantauanTanam['tindakan_penyelesaian']);
                            $sheet->SetCellValue('AS' . $rowCount, '');
                        }

                        $dokumentasi = $this->model->get_dokumentasi($row['kelompok_tani_id'], '1');
                        if($dokumentasi) {
                            $sheet->SetCellValue('AT' . $rowCount, $dokumentasi['tanggal_tanam'])->getColumnDimension('AT')->setAutoSize(true);
                            $sheet->SetCellValue('AU' . $rowCount, $dokumentasi['tanggal_perkiraan_panen']);
                            $sheet->SetCellValue('AV' . $rowCount, '');
                        }
                        $dokumentasi = $this->model->get_dokumentasi($row['kelompok_tani_id'], '2');
                        if($dokumentasi) {
                            $sheet->SetCellValue('AW' . $rowCount, $dokumentasi['tanggal_panen'])->getColumnDimension('AW')->setAutoSize(true);
                            $sheet->SetCellValue('AX' . $rowCount, $dokumentasi['luas_panen']);
                            $sheet->SetCellValue('AY' . $rowCount, $dokumentasi['jumlah_hasil_panen']);
                            $sheet->SetCellValue('AZ' . $rowCount, '');
                        }
                        
                        $rowCount++;
                    }
                }
                
            }
        }

        foreach(range('A','H') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $objPHPExcel->getActiveSheet()->calculateColumnWidths();


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        // $kelompokTani = $this->model->get_bantuan_kelompok_tani();
        // $this->output->set_content_type('application/json')->set_output(json_encode($kelompokTani, JSON_PRETTY_PRINT));
        // echo json_encode($kelompokTani, JSON_PRETTY_PRINT);

    }

    public function export_data_()
    {
        $fileName = 'registrasi_kampung_' . date('d_m_Y') . '.xlsx';
        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $styleForTitle = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 10,
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForBody = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
                'name' => 'Cambria',
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForBodyLeft = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
                'name' => 'Cambria',
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
        );
        $styleForBodyRight = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
                'name' => 'Cambria',
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
        );
        
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        
        $objPHPExcel->getDefaultStyle()->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

        $sheet->mergeCells('A1:H3')
        ->SetCellValue('A1', 'Registrasi Kampung')
        ->getStyle('A1:H3')->applyFromArray($styleForTitle);
        
        $sheet->mergeCells('I1:M3')
        ->SetCellValue('I1', 'Bantuan Kampung')
        ->getStyle('I1:M3')->applyFromArray($styleForTitle);
        
        $sheet->mergeCells('N1:R3')
        ->SetCellValue('N1', 'Penilaian Kampung')
        ->getStyle('N1:R3')->applyFromArray($styleForTitle);
        
        $sheet->mergeCells('S1:Z2')
        ->SetCellValue('S1', 'Kendala')
        ->getStyle('S1:Z2')->applyFromArray($styleForTitle);
        
        $sheet->mergeCells('AA1:AD3')
        ->SetCellValue('AA1', 'Pemantauan Tanam')
        ->getStyle('AA1:AD3')->applyFromArray($styleForTitle);
        
        $sheet->mergeCells('AE1:AK3')
        ->SetCellValue('AE1', 'Dokumentasi')
        ->getStyle('AE1:AK3')->applyFromArray($styleForTitle);
        
        
        $sheet->mergeCells('A4:A5')
        ->SetCellValue('A4', 'No')
        ->getStyle('A4:A5')->applyFromArray($styleForBody);
        $sheet->mergeCells('B4:B5')
        ->SetCellValue('B4', 'No. Registrasi')
        ->getStyle('B4:B5')->applyFromArray($styleForBody);
        $sheet->mergeCells('C4:C5')
        ->SetCellValue('C4', 'Kelompok Tani')
        ->getStyle('C4:C5')->applyFromArray($styleForBody);
        $sheet->mergeCells('D4:D5')
        ->SetCellValue('D4', 'Alamat Kabupaten')
        ->getStyle('D4:D5')->applyFromArray($styleForBody);
        $sheet->mergeCells('E4:E5')
        ->SetCellValue('E4', 'Luas (Ha)')
        ->getStyle('E4:E5')->applyFromArray($styleForBody);
        $sheet->mergeCells('F4:F5')
        ->SetCellValue('F4', 'Terdaftar Simluhtan')
        ->getStyle('F4:F5')->applyFromArray($styleForBody);
        $sheet->mergeCells('G4:G5')
        ->SetCellValue('G4', 'Titik Koordinat Kampung')
        ->getStyle('G4:G5')->applyFromArray($styleForBody);
        $sheet->mergeCells('H4:H5')
        ->SetCellValue('H4', 'Peta Polygon')
        ->getStyle('H4:H5')->applyFromArray($styleForBody);
        $sheet->mergeCells('I4:I5')
        ->SetCellValue('I4', 'Tanggal Terima Bantuan')
        ->getStyle('I4:I5')->applyFromArray($styleForBody);
        $sheet->mergeCells('J4:L4')
        ->SetCellValue('J4', 'Keterangan')
        ->getStyle('J4:L4')->applyFromArray($styleForBody);
        $sheet->SetCellValue('J5', 'Jenis')->getStyle('J5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('K5', 'Jumlah')->getStyle('K5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('L5', 'Harga')->getStyle('L5')->applyFromArray($styleForBody);
        $sheet->mergeCells('M4:M5')
        ->SetCellValue('M4', 'Foto')
        ->getStyle('M4:M5')->applyFromArray($styleForBody);
        $sheet->mergeCells('N4:R4')
        ->SetCellValue('N4', 'Keterangan')
        ->getStyle('N4:R4')->applyFromArray($styleForBody);
        $sheet->SetCellValue('N5', 'UMKM')->getStyle('N5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('O5', 'Sarana')->getStyle('O5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('P5', 'Akses Jalan')->getStyle('P5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('Q5', 'KEP')->getStyle('Q5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('R5', 'AUT')->getStyle('R5')->applyFromArray($styleForBody);
        $sheet->mergeCells('S3:W3')
        ->SetCellValue('S3', 'Tanam')
        ->getStyle('S3:W3')->applyFromArray($styleForBody);
        $sheet->mergeCells('S4:V4')
        ->SetCellValue('S4', 'Keterangan')
        ->getStyle('S4:V4')->applyFromArray($styleForBody);
        $sheet->SetCellValue('S5', 'Jenis Komoditi')->getStyle('S5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('T5', 'Tanggal Kendala')->getStyle('T5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('U5', 'Kendala')->getStyle('U5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('V5', 'Luas Lahan Terkendala')->getStyle('V5')->applyFromArray($styleForBody);
        $sheet->mergeCells('W4:W5')
        ->SetCellValue('W4', 'Foto')
        ->getStyle('W4:W5')->applyFromArray($styleForBody);
        $sheet->mergeCells('X4:Y4')
        ->SetCellValue('X4', 'Keterangan')
        ->getStyle('X4:Y4')->applyFromArray($styleForBody);
        $sheet->mergeCells('Z4:Z5')
        ->SetCellValue('Z4', 'Foto')
        ->getStyle('Z4:Z5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('X5', 'Tanggal Terkendala')->getStyle('X5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('Y5', 'Kendala')->getStyle('Y5')->applyFromArray($styleForBody);
        $sheet->mergeCells('X3:Z3')
        ->SetCellValue('X3', 'Panen')
        ->getStyle('X3:Z3')->applyFromArray($styleForBody);
        $sheet->mergeCells('AA4:AB4')->SetCellValue('AA4', 'Keterangan')->getStyle('AA4:AB4')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AA5', 'Tanggal Pemantauan')->getStyle('AA5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AB5', 'Permasalahan')->getStyle('AB5')->applyFromArray($styleForBody);
        $sheet->mergeCells('AC4:AC5')->SetCellValue('AC4', 'Tindak Lanjut')->getStyle('AC4:AC5')->applyFromArray($styleForBody);
        $sheet->mergeCells('AD4:AD5')->SetCellValue('AD4', 'Foto')->getStyle('AD4:AD5')->applyFromArray($styleForBody);

        $sheet->mergeCells('AE4:AF4')->SetCellValue('AE4', 'Keterangan')->getStyle('AE4:AF4')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AE5', 'Tanggal Tanam')->getStyle('AE5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AF5', 'Perkiraan Panen')->getStyle('AF5')->applyFromArray($styleForBody);
        $sheet->mergeCells('AG4:AG5')->SetCellValue('AG4', 'Foto')->getStyle('AG4:AG5')->applyFromArray($styleForBody);
        $sheet->mergeCells('AH4:AJ4')->SetCellValue('AH4', 'Keterangan')->getStyle('AH4:AJ4')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AH5', 'Tanggal Panen')->getStyle('AH5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AI5', 'Luas Panen')->getStyle('AI5')->applyFromArray($styleForBody);
        $sheet->SetCellValue('AJ5', 'Jumlah')->getStyle('AJ5')->applyFromArray($styleForBody);
        $sheet->mergeCells('AK4:AK5')->SetCellValue('AK4', 'Foto')->getStyle('AK4:AK5')->applyFromArray($styleForBody);

        $registrasiKampung = $this->model->get_laporan();
        if($registrasiKampung) {
            $rowCount = 6;
            foreach($registrasiKampung as $row) {
                
                

                $kelompokTani = $this->model->get_kelompok_tani($row['id']);
                if($kelompokTani) {
                    $countKelTani = count($kelompokTani);
                    $rowKelompokTani = 1;
                    foreach($kelompokTani as $kelTani) {

                        $rowAwalKelTani = $rowCount;

                        
                        
                        
                        

                        $rowBantuan = $sheet->getHighestColumn();
                        $bantuanDetail = $this->model->get_bantuan_detail($kelTani['id']);
                        if($bantuanDetail) {
                            $noBanDet = 1;
                            $rowAkhirKelTani = $rowCount + count($bantuanDetail) - 1;
                            foreach($bantuanDetail as $bandet) {

                                $sheet->mergeCells('G' . $rowAwalKelTani . ':G' . $rowAkhirKelTani)
                                ->SetCellValue('G' . $rowCount, '')
                                ->getStyle('G' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('H' . $rowAwalKelTani . ':H' . $rowAkhirKelTani)
                                ->SetCellValue('H' . $rowCount, '')
                                ->getStyle('H' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $sheet->mergeCells('C' . $rowAwalKelTani . ':C' . $rowAkhirKelTani)
                                ->SetCellValue('C' . $rowCount, $kelTani['nama_kelompok'])
                                ->getStyle('C' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $sheet->mergeCells('D' . $rowAwalKelTani . ':D' . $rowAkhirKelTani)
                                ->SetCellValue('D' . $rowCount, $row['kabupaten_nama'])
                                ->getStyle('D' . $rowCount)->applyFromArray($styleForBodyLeft)
                                ->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

                                $sheet->mergeCells('E' . $rowAwalKelTani . ':E' . $rowAkhirKelTani)
                                ->SetCellValue('E' . $rowCount, $kelTani['luas'])
                                ->getStyle('E' . $rowCount)->applyFromArray($styleForBody)
                                ->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

                                $terdaftarSimluhtan = $kelTani['terdaftar_simluhtan'] == '0' ? 'Tidak' : 'Ya';
                                $sheet->mergeCells('F' . $rowAwalKelTani . ':F' . $rowAkhirKelTani)
                                ->SetCellValue('F' . $rowCount, $terdaftarSimluhtan)
                                ->getStyle('F' . $rowCount)->applyFromArray($styleForBody)
                                ->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                                

                                $sheet->mergeCells('B' . $rowAwalKelTani . ':B' . $rowAkhirKelTani)
                                ->SetCellValue('B' . $rowCount, $row['nomor_registrasi'])
                                ->getStyle('B' . $rowCount)->applyFromArray($styleForBody)
                                ->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                                
                                

                                

                                $sheet->SetCellValue('I' . $rowCount, $bandet['tanggal_bantuan'])
                                ->getStyle('I' . $rowCount)->applyFromArray($styleForBody)
                                ->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                                $sheet->SetCellValue('K' . $rowCount, $bandet['jumlah'])
                                ->getStyle('K' . $rowCount)->applyFromArray($styleForBodyRight)
                                ->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                                $sheet->SetCellValue('J' . $rowCount, $bandet['nama_bantuan'])
                                ->getStyle('J' . $rowCount)->applyFromArray($styleForBodyLeft)
                                ->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                                $sheet->SetCellValue('L' . $rowCount, $rowAwalKelTani . ' - ' . $rowAkhirKelTani)
                                ->getStyle('L' . $rowCount)->applyFromArray($styleForBodyRight)
                                ->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

                                $linkFoto = base_url('uploads/bantuan/' . $bandet['foto']);
                                $linkFoto = $bandet['foto'] ? $linkFoto : '';
                                $sheet->SetCellValue('M' . $rowCount, $linkFoto)
                                ->getStyle('M' . $rowCount)->applyFromArray($styleForBodyLeft)
                                ->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);


                                $sheet->mergeCells('S' . $rowAwalKelTani . ':S' . $rowAkhirKelTani)
                                ->SetCellValue('S' . $rowCount, $row['jenis_komoditi_nama'])
                                ->getStyle('S' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $kendalaTanam = $this->model->get_kendala_tanam($kelTani['id']);
                                $sheet->mergeCells('T' . $rowAwalKelTani . ':T' . $rowAkhirKelTani)
                                ->SetCellValue('T' . $rowCount, $kendalaTanam['tanggal_kendala_tanam'])
                                ->getStyle('T' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('U' . $rowAwalKelTani . ':U' . $rowAkhirKelTani)
                                ->SetCellValue('U' . $rowCount, $kendalaTanam['keterangan'])
                                ->getStyle('U' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('V' . $rowAwalKelTani . ':V' . $rowAkhirKelTani)
                                ->SetCellValue('V' . $rowCount, $kendalaTanam['luas_lahan'])
                                ->getStyle('V' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('W' . $rowAwalKelTani . ':W' . $rowAkhirKelTani)
                                ->SetCellValue('W' . $rowCount, '')
                                ->getStyle('W' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $kendalaPanen = $this->model->get_kendala_panen($kelTani['id']);
                                $sheet->mergeCells('X' . $rowAwalKelTani . ':X' . $rowAkhirKelTani)
                                ->SetCellValue('X' . $rowCount, $kendalaPanen['tanggal_kendala_panen'])
                                ->getStyle('X' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('Y' . $rowAwalKelTani . ':Y' . $rowAkhirKelTani)
                                ->SetCellValue('Y' . $rowCount, $kendalaPanen['jumlah_produksi_terkendala'])
                                ->getStyle('Y' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('Z' . $rowAwalKelTani . ':Z' . $rowAkhirKelTani)
                                ->SetCellValue('Z' . $rowCount, '')
                                ->getStyle('Z' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $pemantauanTanam = $this->model->get_pemantauan_tanam($kelTani['id']);
                                $sheet->mergeCells('AA' . $rowAwalKelTani . ':AA' . $rowAkhirKelTani)
                                ->SetCellValue('AA' . $rowCount, $pemantauanTanam['tanggal'])
                                ->getStyle('AA' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('AB' . $rowAwalKelTani . ':AB' . $rowAkhirKelTani)
                                ->SetCellValue('AB' . $rowCount, $pemantauanTanam['penemuan_masalah'])
                                ->getStyle('AB' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('AC' . $rowAwalKelTani . ':AC' . $rowAkhirKelTani)
                                ->SetCellValue('AC' . $rowCount, $pemantauanTanam['tindakan_penyelesaian'])
                                ->getStyle('AC' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('AD' . $rowAwalKelTani . ':AD' . $rowAkhirKelTani)
                                ->SetCellValue('AD' . $rowCount, '')
                                ->getStyle('AD' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $dokumentasi = $this->model->get_dokumentasi($kelTani['id'], '1');
                                $sheet->mergeCells('AE' . $rowAwalKelTani . ':AE' . $rowAkhirKelTani)
                                ->SetCellValue('AE' . $rowCount, $dokumentasi['tanggal_tanam'])
                                ->getStyle('AE' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('AF' . $rowAwalKelTani . ':AF' . $rowAkhirKelTani)
                                ->SetCellValue('AF' . $rowCount, $dokumentasi['tanggal_perkiraan_panen'])
                                ->getStyle('AF' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $linkFoto = base_url('uploads/dokumentasi/' . $dokumentasi['foto_tanam']);
                                $linkFoto = $dokumentasi['foto_tanam'] ? $linkFoto : '';
                                $sheet->mergeCells('AG' . $rowAwalKelTani . ':AG' . $rowAkhirKelTani)
                                ->SetCellValue('AG' . $rowCount, $linkFoto)
                                ->getStyle('AG' . $rowCount)->applyFromArray($styleForBodyLeft);
                                
                                $dokumentasi = $this->model->get_dokumentasi($kelTani['id'], '2');
                                $sheet->mergeCells('AH' . $rowAwalKelTani . ':AH' . $rowAkhirKelTani)
                                ->SetCellValue('AH' . $rowCount, $dokumentasi['tanggal_panen'])
                                ->getStyle('AH' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('AI' . $rowAwalKelTani . ':AI' . $rowAkhirKelTani)
                                ->SetCellValue('AI' . $rowCount, $dokumentasi['luas_panen'])
                                ->getStyle('AI' . $rowCount)->applyFromArray($styleForBodyLeft);
                                $sheet->mergeCells('AJ' . $rowAwalKelTani . ':AJ' . $rowAkhirKelTani)
                                ->SetCellValue('AJ' . $rowCount, $dokumentasi['jumlah_hasil_panen'])
                                ->getStyle('AJ' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $linkFoto = base_url('uploads/dokumentasi/' . $dokumentasi['foto_panen']);
                                $linkFoto = $dokumentasi['foto_panen'] ? $linkFoto : '';
                                $sheet->mergeCells('AK' . $rowAwalKelTani . ':AK' . $rowAkhirKelTani)
                                ->SetCellValue('AK' . $rowCount, $linkFoto)
                                ->getStyle('AK' . $rowCount)->applyFromArray($styleForBodyLeft);

                                $rowCount++;
                                $noBanDet++;
                            }
                        }
                    }
                }
                
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}

/* End of file Lahan.php */