<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User_akses extends CI_Controller
{

    private $title = 'User Akses';
    private $view = 'User_akses';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function get_list_menu() {
        $data['user_level_id'] = $this->input->post('user_level_id');
        $data['head_menu'] = $this->model->head_menu($this->input->post('user_level_id'));
        $data['list_menu'] = $this->model->list_menu($this->input->post('user_level_id'));
        $data = array_merge($data, path_info());
        $this->parser->parse($this->view . '/form_list_menu', $data);
    }

    public function update_akses_user() {
        echo json_encode($this->input->post());

        $check = $this->db
        ->where('user_level_id', $this->input->post('user_level_id'))
        ->where('menu_id', $this->input->post('menu_id'))
        ->get('tbl_user_akses');

        if($check->num_rows() > 0) {
            $value = $this->input->post('value');
            $akses = $this->input->post('akses');
            $this->db
            ->set('menu_id', $this->input->post('menu_id'))
            ->set('user_level_id', $this->input->post('user_level_id'))
            ->set($akses, $value)
            ->where('id', $check->row_array()['id'])
            ->update('tbl_user_akses');
        } else {
            $value = $this->input->post('value');
            $akses = $this->input->post('akses');
            $this->db
            ->set('menu_id', $this->input->post('menu_id'))
            ->set('user_level_id', $this->input->post('user_level_id'))
            ->set($akses, $value)
            ->insert('tbl_user_akses');
        }
    }

    public function index()
    {
        akses_user('read');
        akses_user('update');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        $this->load->library('Datatables');
        $this->datatables->select('tbl_user_level.*');
        $this->datatables->where('tbl_user_level.app_id','2');
        $this->datatables->from('tbl_user_level');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        if ($id) {
            $data = getRowArray('tbl_user_level', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        $this->model->delete();
    }



}