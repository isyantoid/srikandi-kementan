<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Buku_digital extends CI_Controller
{

    private $title = 'Buku Digital';
    private $view = 'Buku_digital';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('tbl_buku_digital.id, tbl_buku_digital.nama_ebook, tbl_jenis_dokumen.jenis_dokumen, tbl_buku_digital.keterangan, tbl_buku_digital.file_upload');
        $this->datatables->join('tbl_jenis_dokumen', 'tbl_buku_digital.id_jenis_dokumen = tbl_jenis_dokumen.id', 'left');
        $this->datatables->from('tbl_buku_digital');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data['show_jenis_dokumen'] = $this->db->get('tbl_jenis_dokumen')->result();
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('tbl_buku_digital', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data['show_jenis_dokumen'] = $this->db->get('tbl_jenis_dokumen')->result();
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            return $this->model->delete();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}