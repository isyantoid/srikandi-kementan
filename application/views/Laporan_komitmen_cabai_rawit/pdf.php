<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <style>
        <?= $css ?>
    </style>
    <link rel="stylesheet" href="<?= base_url('dist/assets/css/pdf_style.css') ?>">
</head>
<body>
    <h2><?= $title ?></h2>
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th>Champion</th>
                <th>Kecamatan</th>
                <th>Kaubpaten</th>
                <th>Stok</th>
                <th>Jan</th>
                <th>Feb</th>
                <th>Mar</th>
                <th>Apr</th>
                <th>May</th>
                <th>Jun</th>
                <th>Jul</th>
                <th>Aug</th>
                <th>Sep</th>
                <th>Oct</th>
                <th>Nov</th>
                <th>Dec</th>
                <th>Total Kumulatif</th>
                <th>Kontak Dinas</th>
            </tr>
        </thead>
        <tbody>
            <?php if($data): ?>
                <?php $no=1; foreach($data as $row): ?>
                    <tr>
                        <td class="text-center"><?= $no ?></td>
                        <td class="text-center"><?= $row['champion_nama'] ?></td>
                        <td class="text-center"><?= $row['kecamatan_nama'] ?></td>
                        <td class="text-center"><?= $row['kabupaten_nama'] ?></td>
                        <td class="text-center"><?= $row['stok'] ?></td>
                        <td class="text-center"><?= $row['jan'] ?></td>
                        <td class="text-center"><?= $row['feb'] ?></td>
                        <td class="text-center"><?= $row['mar'] ?></td>
                        <td class="text-center"><?= $row['apr'] ?></td>
                        <td class="text-center"><?= $row['may'] ?></td>
                        <td class="text-center"><?= $row['jun'] ?></td>
                        <td class="text-center"><?= $row['jul'] ?></td>
                        <td class="text-center"><?= $row['aug'] ?></td>
                        <td class="text-center"><?= $row['sep'] ?></td>
                        <td class="text-center"><?= $row['oct'] ?></td>
                        <td class="text-center"><?= $row['nov'] ?></td>
                        <td class="text-center"><?= $row['dec'] ?></td>
                        <td class="text-center"><?= $row['stok'] ?></td>
                        <td class="text-center"><?= $row['kontak_dinas_nama'] ?> <br><?= $row['kontak_dinas_nomor_hp'] ?> </td>
                    </tr>
                <?php $no++; endforeach ?>
            <?php else: ?>
                <tr><td colspan="19">Tidak ada data</td></tr>
            <?php endif ?>
        </tbody>
    </table>
</body>
</html>