<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_kontak_dinas_model extends CI_Model
{

    private $tableName = 'champ_kontak_dinas';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['kabupaten_kode'] != $this->input->post('kabupaten_kode')) {
                    if (isDuplicate($this->tableName, 'kabupaten_kode', $this->input->post('kabupaten_kode'))) {
                        return jsonOutputError('Dinas kabupaten yang dipilih sudah ada sebelumnya.');
                    }
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate($this->tableName, 'kabupaten_kode', $this->input->post('kabupaten_kode'))) {
                    return jsonOutputError('Dinas kabupaten yang dipilih sudah ada sebelumnya.');
                }
                if (isDuplicate('tbl_user', 'username', $this->input->post('username'))) {
                    return jsonOutputError('Username login sudah ada sebelumnya.');
                }

                $this->db->set('nama', $this->input->post('nama'));
                $this->db->set('username', $this->input->post('username'));
                $this->db->set('password', md5($this->input->post('password')));
                $this->db->set('user_level_id', 14);
                $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                $this->db->set('provinsi_kode', substr($this->input->post('kabupaten_kode'), 0, 2));
                $this->db->set('app_id', '2');
                $insertUser = $this->db->insert('tbl_user');
                $user_id = $this->db->insert_id();
                if($insertUser) {
                    $this->db->set('nama', $this->input->post('nama'));
                    $this->db->set('nomor_hp', $this->input->post('nomor_hp'));
                    $this->db->set('user_id', $user_id);
                    $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                    $insert = $this->db->insert($this->tableName);
                    if ($insert) {
                        return jsonOutputSuccess();
                    } else {
                        return jsonOutputError();
                    }
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }
}