<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_registrasi_kampung_model extends CI_Model
{


    public function get_laporan()
    {
        $this->db->select('
            tbl_permohonan.id,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.tanggal_permohonan,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_provinsi.nama as provinsi_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');
        $this->db->join('tbl_provinsi', 'tbl_permohonan.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten', 'tbl_permohonan.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->join('tbl_jenis_komoditi', 'tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->where('app_id', '2');
        $this->db->from('tbl_permohonan');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_kelompok_tani($id_permohonan)
    {
        $this->db->select('
            tbl_kelompok_tani.id,
            tbl_kelompok_tani.nama_kelompok,
            tbl_kelompok_tani.nama_ketua,
            tbl_kelompok_tani.nik_ketua,
            tbl_kelompok_tani.no_hp,
            tbl_kelompok_tani.luas,
            tbl_kelompok_tani.terdaftar_simluhtan,
        ');
        $this->db->where('id_permohonan', $id_permohonan);
        $this->db->from('tbl_kelompok_tani');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_bantuan_detail($kelompok_tani_id)
    {
        $this->db->select('
            tbl_bantuan_detail.tanggal_bantuan,
            tbl_bantuan_detail.jumlah,
            tbl_bantuan_detail.harga,
            tbl_bantuan_detail.foto,
            tbl_jenis_bantuan.nama_bantuan,
        ');
        $this->db->join('tbl_bantuan', 'tbl_bantuan_detail.bantuan_id = tbl_bantuan.id');
        $this->db->join('tbl_jenis_bantuan', 'tbl_bantuan_detail.jenis_bantuan_id = tbl_jenis_bantuan.id','left');
        $this->db->where('tbl_bantuan.kelompok_tani_id', $kelompok_tani_id);
        $this->db->from('tbl_bantuan_detail');
        $res = $this->db->get();
        return $res->result_array();
    }

    public function get_kendala_tanam($kelompok_tani_id)
    {
        $this->db->select('
            tbl_kendala_tanam.id,
            tbl_kendala_tanam.keterangan,
            tbl_kendala_tanam.tanggal_kendala_tanam,
            tbl_kendala_tanam.luas_lahan,
        ');
        $this->db->where('id_kelompok_tani', $kelompok_tani_id);
        $this->db->from('tbl_kendala_tanam');
        $res = $this->db->get();
        return $res->row_array();
    }
    
    public function get_kendala_panen($kelompok_tani_id)
    {
        $this->db->select('
            tbl_kendala_panen.id,
            tbl_kendala_panen.keterangan,
            tbl_kendala_panen.tanggal_kendala_panen,
            tbl_kendala_panen.jumlah_produksi_terkendala,
        ');
        $this->db->where('id_kelompok_tani', $kelompok_tani_id);
        $this->db->from('tbl_kendala_panen');
        $res = $this->db->get();
        return $res->row_array();
    }
    
    public function get_pemantauan_tanam($kelompok_tani_id)
    {
        $this->db->select('
            tbl_pemantauan_tanam.id,
            tbl_pemantauan_tanam.penemuan_masalah,
            tbl_pemantauan_tanam.tanggal,
            tbl_pemantauan_tanam.tindakan_penyelesaian,
        ');
        $this->db->where('id_kelompok_tani', $kelompok_tani_id);
        $this->db->from('tbl_pemantauan_tanam');
        $res = $this->db->get();
        return $res->row_array();
    }
    
    public function get_dokumentasi($kelompok_tani_id, $tipe)
    {
        $this->db->select('
            tbl_dokumentasi.id,
            tbl_dokumentasi.tanggal_tanam,
            tbl_dokumentasi.tanggal_perkiraan_panen,
            tbl_dokumentasi.foto_tanam,
            tbl_dokumentasi.tanggal_panen,
            tbl_dokumentasi.luas_panen,
            tbl_dokumentasi.jumlah_hasil_panen,
            tbl_dokumentasi.foto_panen,
        ');
        $this->db->where('kelompok_tani_id', $kelompok_tani_id);
        $this->db->where('tipe', $tipe);
        $this->db->from('tbl_dokumentasi');
        $res = $this->db->get();
        return $res->row_array();
    }
}

/* End of file Lahan_model.php */