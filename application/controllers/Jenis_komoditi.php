<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_komoditi extends CI_Controller
{

    private $title = 'Jenis komoditas';
    private $view = 'Jenis_komoditi';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_jenis_komoditi.id, 
            tbl_jenis_komoditi.jenis_komoditi_kode, 
            tbl_jenis_komoditi.jenis_komoditi_nama, 
            tbl_jenis_komoditi.jenis_komoditi_icon, 
            tbl_jenis_komoditi.jenis_komoditi_pin, 
            tbl_jenis_komoditi.kelompok_komoditi_kode, 
            tbl_kelompok_komoditi.kelompok_komoditi_nama
        ');
        $this->datatables_server_side->join('tbl_kelompok_komoditi', 'tbl_jenis_komoditi.kelompok_komoditi_kode = tbl_kelompok_komoditi.kelompok_komoditi_kode', 'left');
        $this->datatables_server_side->from('tbl_jenis_komoditi');
        $this->datatables_server_side->where_in('tbl_jenis_komoditi.kelompok_komoditi_id', [1,2]);
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('tbl_jenis_komoditi', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            return $this->model->delete();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}