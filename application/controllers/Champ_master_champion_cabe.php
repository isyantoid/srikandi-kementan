<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Champ_master_champion_cabe extends CI_Controller
{

    private $title = 'Champion Cabe';
    private $view = 'Champ_master_champion_cabe';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }
    
    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('champ.*, 
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi2.jenis_komoditi_nama as jenis_komoditi_nama2,
        ');
        $this->datatables->join('tbl_kabupaten','champ.kabupaten_kode = tbl_kabupaten.kode');
        $this->datatables->join('tbl_kecamatan','champ.kecamatan_kode = tbl_kecamatan.kode');
        $this->datatables->join('tbl_jenis_komoditi','champ.jenis_komoditi_id = tbl_jenis_komoditi.id');
        $this->datatables->join('tbl_jenis_komoditi as tbl_jenis_komoditi2','champ.jenis_komoditi_kedua_id = tbl_jenis_komoditi2.id', 'left');
        $this->datatables->where('champ.jenis_komoditi_id !=', 68);
        $this->datatables->from('champ');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('champ', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit ' . $this->title;
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}