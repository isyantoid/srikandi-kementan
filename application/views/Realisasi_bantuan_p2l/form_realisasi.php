<tr class="<?= $uniqid ?>">
    <td>
        <div class="form-group row">
            <div class="col-md-12">
                <label>Jenis Bantuan:</label>
                <select class="form-control jenis_bantuan_id" id="<?= $uniqid ?>" name="jenis_bantuan_id" style="width:100%;"
                    required></select>
            </div>
        </div>
    </td>
    <td>
        <div class="form-group row">
            <div class="col-md-12">
                <label>Jumlah:</label>
                <input type="number" class="form-control jumlah" name="jumlah" value="" required
                    placeholder="Masukan jumlah" data-id="<?= $uniqid ?>">
            </div>
        </div>
    </td>
    <td>
        <div class="form-group row">
            <div class="col-md-12">
                <label>Nilai (Rp):</label>
                <input type="number" class="form-control nilai" name="nilai" value="" required
                    placeholder="Masukan nilai" data-id="<?= $uniqid ?>">
            </div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <div class="col-md-12">
                <label class="form-label">&nbsp;</label>
                <div>
                    <?php if($btnDelete == '1'): ?>
                        <button type="button" class="btn btn-sm btn-danger btn-circle btn-elevate btn-icon btn-delete"
                            data-id="<?= $uniqid ?>"><i class="la la-trash"></i></button>
                    <?php endif ?>
                    <button type="button" class="btn btn-sm btn-success btn-circle btn-elevate btn-icon" id="btn-add"
                        data-id="<?= $uniqid ?>"><i class="la la-plus"></i></button>
                </div>
            </div>
        </div>
    </td>
</tr>

<script>
    ajaxSelect({
        id: '#<?= $uniqid ?>',
        url: '{site_url}ajax_selectbox/jenis_bantuan_id',
    });

    
</script>