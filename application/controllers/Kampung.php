<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kampung extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Kampung_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        // $this->data['data_laporan'] = $this->model->data_laporan();
        // $filter = $this->input->get('filter');
        // if ($filter == '1') {
        //     $this->data['get_laporan'] = $this->model->get_laporan();
        // } else {
        //     $this->data['get_laporan'] = array();
        // }

        akses_user('read');

        $data['provinsi_kode'] = $this->input->get('provinsi_kode');
        $data['kabupaten_kode'] = $this->input->get('kabupaten_kode');

        $this->data['content'] = 'Kampung/index';
        $this->data['title'] = 'Data Lahan';
        $this->data['subTitle'] = '';
        $data = array_merge($this->data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function get_view_filter()
    {
        akses_user('read');
        // $data = $this->input->post();
        $data['id_provinsi'] = $this->input->post('id_provinsi');
        $data['id_kabupaten'] = $this->input->post('id_kabupaten');
        $data['id_komoditi'] = $this->input->post('id_komoditi');
        $data['id_jenis_komoditi'] = $this->input->post('id_jenis_komoditi');
        $data['status'] = $this->input->post('status');
        $data['jenis_registrasi'] = $this->input->post('jenis_registrasi');
        $data = array_merge($data, path_info());
        $this->parser->parse('Kampung/view_filter', $data);
    }

    public function edit($id = null)
    {
        if ($id) {

            $data = getRowArray('permohonan_m', array('id_pemohon' => $id));
            if ($data) {

                if ($this->input->post('submit')) {
                    $create_dir = FCPATH . 'uploads/dokumen/';
                    $config['upload_path'] = $create_dir;
                    $config['encrypt_name'] = true;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|docx';
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);


                    $userfile = array();
                    if ($this->upload->do_upload("userfile")) {
                        $userfile = $this->upload->data();
                    }
                    $denah_lokasi = array();
                    if ($this->upload->do_upload("denah_lokasi")) {
                        $denah_lokasi = $this->upload->data();
                    }

                    $insert_pemohon = array(

                        'tanggal_permohonan'   => date('Y-m-d', strtotime($this->input->post('tanggal_permohonan'))),
                        'nama_pemilik'        => $this->input->post('nama'),
                        'jenis_pengajuan'        => $this->input->post('jenis_pengajuan'),
                        'nama_lengkap'            => $this->input->post('nama'),
                        'nama_kampung'            => $this->input->post('nama_kampung'),
                        'total_kebun'            => $this->input->post('total_kebun'),
                        'id_varietas'            => $this->input->post('id_varietas'),
                        'alamat'                => $this->input->post('alamat'),
                        'telepon'                => $this->input->post('telepon'),
                        'alamat_kebun'            => $this->input->post('alamat_kebun'),
                        'id_komoditi'            => $this->input->post('komoditi'),
                        'id_jenis_komoditi'    => $this->input->post('jenis_komoditi'),
                        'luas_kebun'            => str_replace(',', '.', $this->input->post('luas_lahan')),
                        'satuan_luas'            => $this->input->post('satuan_luas'),
                        'id_registrasi'        => $this->session->userdata('registrasi'),
                        'desa_kode'        => $this->input->post('desa'),
                        'kecamatan_kode'        => $this->input->post('kecamatan'),
                        'kabupaten_kode'        => $this->input->post('kabupaten'),
                        'provinsi_kode'        => $this->input->post('provinsi'),
                        'jenis_registrasi' => $this->input->post('jenis_registrasi'),
                        'longitude' => $this->input->post('longitude'),
                        'latitude' => $this->input->post('latitude'),
                    );

                    if (isset($userfile['file_name'])) {
                        $dok['dokumen_1a'] = $userfile['file_name'];
                        $merge = array_merge($insert_pemohon, $dok);
                    } else {
                        $merge = $insert_pemohon;
                    }

                    if (isset($denah_lokasi['file_name'])) {
                        $dok['dokumen_2a'] = $denah_lokasi['file_name'];
                        $merge = array_merge($insert_pemohon, $dok);
                    } else {
                        $merge = $insert_pemohon;
                    }

                    $this->db->where('id_pemohon', $id);
                    $this->db->update('permohonan_m', $merge);
                    $id_pemohon = $id;

                    $this->db->delete('jawab_kondisi_kebun_t', array('id_pemohon' => $id_pemohon));
                    foreach ($this->input->post('pertanyaan') as $key => $value) {
                        $jawab = array('id_pertanyaan' => $key, 'jawab' => $value, 'id_pemohon' => $id_pemohon);
                        $this->db->insert('jawab_kondisi_kebun_t', $jawab);
                    }


                    $kelompok_tani_nomor = $this->input->post('kelompok_tani_nomor');
                    $count_kelompok_tani_nomor = count($kelompok_tani_nomor);
                    if ($count_kelompok_tani_nomor > 0) {
                        $this->db->delete('detail_kelompok_tani', array('id_permohonan' => $id_pemohon));
                        $this->db->delete('detail_anggota_kelompok_tani', array('id_permohonan' => $id_pemohon));

                        for ($i = 0; $i < $count_kelompok_tani_nomor; $i++) {
                            $insert_kelompok['id_permohonan'] = $id_pemohon;
                            $insert_kelompok['nama_kelompok'] = $this->input->post('kelompok_tani_nama_ketua')[$i];
                            $insert_kelompok['nama_ketua'] = $this->input->post('kelompok_tani_nama_ketua')[$i];
                            $insert_kelompok['nik'] = $this->input->post('kelompok_tani_nik')[$i];
                            $insert_kelompok['no_hp'] = $this->input->post('kelompok_tani_no_hp')[$i];
                            $insert_kelompok['luas'] = $this->input->post('kelompok_tani_luas')[$i];
                            $this->db->insert('detail_kelompok_tani', $insert_kelompok);
                            $id_kelompok_tani = $this->db->insert_id();


                            $anggota_tani = $this->input->post($kelompok_tani_nomor[$i] . '_nomor');
                            $count_anggota_tani = count($anggota_tani);
                            if ($count_anggota_tani > 0) {
                                for ($x = 0; $x < $count_anggota_tani; $x++) {
                                    $insert_anggota['id_kelompok_tani'] = $id_kelompok_tani;
                                    $insert_anggota['nik'] = $this->input->post($kelompok_tani_nomor[$i] . '_nik')[$x];
                                    $insert_anggota['nama_lengkap'] = $this->input->post($kelompok_tani_nomor[$i] . '_nama_lengkap')[$x];
                                    $insert_anggota['no_hp'] = $this->input->post($kelompok_tani_nomor[$i] . '_no_hp')[$x];
                                    $insert_anggota['luas'] = $this->input->post($kelompok_tani_nomor[$i] . '_luas')[$x];
                                    $insert_anggota['longitude'] = $this->input->post($kelompok_tani_nomor[$i] . '_longitude')[$x];
                                    $insert_anggota['latitude'] = $this->input->post($kelompok_tani_nomor[$i] . '_latitude')[$x];
                                    $insert_anggota['komoditi_kode'] = $this->input->post($kelompok_tani_nomor[$i] . '_komoditi_anggota')[$x];
                                    $insert_anggota['komoditi_jenis_kode'] = $this->input->post($kelompok_tani_nomor[$i] . '_jenis_komoditi_anggota')[$x];
                                    $insert_anggota['tanggal_tanam'] = $this->input->post($kelompok_tani_nomor[$i] . '_tanggal_tanam_anggota')[$x];
                                    $this->db->insert('detail_anggota_kelompok_tani', $insert_anggota);
                                    $data_anggota[] = $insert_anggota;
                                }
                            }
                        }
                    }



                    $this->session->set_flashdata('sukses', 'Berhasil menyimpan data');

                    $urlGETS = $this->input->get();
                    $get = '';
                    foreach ($urlGETS as $key => $val) {
                        $get .= $key . '=' . $val . '&';
                    }
                    $get = substr($get, 0, -1);
                    redirect('kampung?' . $get);
                }

                $data['title'] = 'Data Kampung';
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = 'Kampung/edit';
                $data['provinsi'] = $this->db->get('provinsi_m');
                $data['jenis_komoditi'] = $this->db->get('komoditi_jenis_m');
                $data['pertanyaan'] = $this->db->get_where('pertanyaan_kondisi_kebun_m', array('jenis_permohonan' => '1'));
                // $data['pertanyaan'] = $this->db->get_where('pertanyaan_kondisi_kebun_m' , array('jenis_permohonan' => '1' ) )->result();
                $data = array_merge($data, path_info());
                $this->load->view('layout/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function datatable_filter()
    {
        $this->load->library('Datatables_server_side');

        $this->datatables_server_side->select('
            permohonan_m.id_pemohon,
            permohonan_m.nomor_registrasi,
            permohonan_m.noreg,
            permohonan_m.tanggal_permohonan,
            permohonan_m.GAP,
            komoditi_m.komoditi_nama,
            komoditi_jenis_m.komoditi_jenis_nama,
            permohonan_m.luas_kebun,
            permohonan_m.satuan_luas,
            permohonan_m.nama_lengkap,
            permohonan_m.jenis_pengajuan,
            kabupaten_m.kabupaten_nama,
            permohonan_m.tanggal_pengesahan,
            permohonan_m.tanggal_kadaluarsa,
            permohonan_m.jenis_registrasi,
            permohonan_m.nama_kampung,
            provinsi_m.provinsi_nama,
            view_status_sertifikat.status_sertifikat,
        ');


        if ($this->input->post('provinsi')) {
            $this->datatables_server_side->where('permohonan_m.provinsi_kode', $this->input->post('provinsi'));
        }

        if ($this->input->post('kabupaten')) {
            $this->datatables_server_side->where('permohonan_m.kabupaten_kode', $this->input->post('kabupaten'));
        }

        if ($this->input->post('komoditi')) {
            $this->datatables_server_side->where('permohonan_m.id_komoditi', $this->input->post('komoditi'));
        }

        if ($this->input->post('jenis_komoditi')) {
            $this->datatables_server_side->where('permohonan_m.id_jenis_komoditi', $this->input->post('jenis_komoditi'));
        }

        if ($this->input->post('jenis_registrasi')) {
            $this->datatables_server_side->where('permohonan_m.jenis_registrasi', $this->input->post('jenis_registrasi'));
        }

        if ($this->input->post('status')) {
            $status = $this->input->post('status');
            $whereStatus = 'blm_proses';
            if ($status == '2') {
                $whereStatus = 'terima';
            } elseif ($status == '3') {
                $whereStatus = 'tolak';
            }
            $this->datatables_server_side->where('permohonan_m.status_provinsi', $whereStatus);
        }

        $this->datatables_server_side->join('view_status_sertifikat', 'permohonan_m.id_pemohon = view_status_sertifikat.id_pemohon', 'left');
        $this->datatables_server_side->join('provinsi_m', 'permohonan_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        $this->datatables_server_side->join('kabupaten_m', 'permohonan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        // $this->datatables_server_side->join('kecamatan_m', 'permohonan_m.kecamatan_kode = kecamatan_m.kecamatan_kode', 'left');
        $this->datatables_server_side->join('komoditi_m', 'permohonan_m.id_komoditi = komoditi_m.komoditi_kode', 'left');
        $this->datatables_server_side->join('komoditi_jenis_m', 'permohonan_m.id_jenis_komoditi = komoditi_jenis_m.komoditi_jenis_kode', 'left');
        // $this->datatables_server_side->where('permohonan_m.status_kabupaten', 'terima');
        // $this->datatables_server_side->where('permohonan_m.status_provinsi', 'terima');
        $this->datatables_server_side->from('permohonan_m');

        // $this->datatables->select('view_permohonan.*');		






        return print_r($this->datatables_server_side->generate());
    }

    public function id_provinsi($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('provinsi_m.provinsi_kode as id, concat(provinsi_m.provinsi_kode, " - ", provinsi_m.provinsi_nama) as text');
            $data = $this->db->where('provinsi_m.provinsi_kode', $id)->get('provinsi_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('provinsi_m.provinsi_kode as id, concat(provinsi_m.provinsi_kode, " - ", provinsi_m.provinsi_nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('provinsi_m.provinsi_nama', $term);
            }

            $data = $this->db->get('provinsi_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_komoditi($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('komoditi_m.komoditi_kode as id, concat(komoditi_m.komoditi_kode, " - ", komoditi_m.komoditi_nama) as text');
            $data = $this->db->where('komoditi_m.komoditi_kode', $id)->get('komoditi_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('komoditi_m.komoditi_kode as id, concat(komoditi_m.komoditi_kode, " - ", komoditi_m.komoditi_nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('komoditi_m.komoditi_nama', $term);
            }

            $this->db->order_by('komoditi_kode', 'asc');
            $data = $this->db->get('komoditi_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_kabupaten($id = null)
    {
        $term = $this->input->get('q');
        $id_provinsi = $this->input->get('id_provinsi');
        if ($id) {
            $this->db->select('kabupaten_m.kabupaten_kode as id, concat(kabupaten_m.kabupaten_kode, " - ", kabupaten_m.kabupaten_nama) as text');
            $data = $this->db->where('kabupaten_m.kabupaten_kode', $id)->get('kabupaten_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('kabupaten_m.kabupaten_kode as id, concat(kabupaten_m.kabupaten_kode, " - ", kabupaten_m.kabupaten_nama) as text');
            $this->db->limit(25);
            if ($term) {
                $this->db->like('kabupaten_m.kabupaten_nama', $term);
            }
            if ($id_provinsi) $this->db->where('provinsi_kode', $id_provinsi);

            $data = $this->db->get('kabupaten_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_jenis_komoditi($id = null)
    {
        $term = $this->input->get('q');
        $id_komoditi = $this->input->get('id_komoditi');
        if ($id) {
            $this->db->select('komoditi_jenis_m.komoditi_jenis_kode as id, concat(komoditi_jenis_m.komoditi_jenis_kode, " - ", komoditi_jenis_m.komoditi_jenis_nama) as text');
            $data = $this->db->where('komoditi_jenis_m.komoditi_jenis_kode', $id)->get('komoditi_jenis_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('komoditi_jenis_m.komoditi_jenis_kode as id, concat(komoditi_jenis_m.komoditi_jenis_kode, " - ", komoditi_jenis_m.komoditi_jenis_nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('komoditi_jenis_m.komoditi_jenis_nama', $term);
            }
            if ($id_komoditi) $this->db->where('komoditi_kode', $id_komoditi);

            $data = $this->db->get('komoditi_jenis_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_varietas($id = null)
    {
        $term = $this->input->get('q');
        $id_jenis_komositi = $this->input->get('id_jenis_komositi');
        if ($id) {
            $this->db->select('varitas_m.kode as id, concat(varitas_m.kode, " - ", varitas_m.nama) as text');
            $data = $this->db->where('varitas_m.kode', $id)->get('varitas_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('varitas_m.kode as id, concat(varitas_m.kode, " - ", varitas_m.nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('varitas_m.nama', $term);
            }
            if ($id_jenis_komositi) $this->db->where('komoditi_jenis_kode', $id_jenis_komositi);

            $data = $this->db->get('varitas_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}

/* End of file Lahan.php */