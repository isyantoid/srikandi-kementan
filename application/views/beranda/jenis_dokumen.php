<div class="row">
    <?php foreach ($show_jenis_dokumen as $sje) :
    ?>
    <div class="col-xl-4">
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__head kt-portlet__head--noborder"></div>
            <div class="kt-portlet__body kt-portlet__body--fit-y">
                <div class="kt-widget kt-widget--user-profile-4">
                    <div class="kt-widget__head">
                        <div class="text-center">
                            <img class="kt-widget__img kt-hidden-"
                                src="<?php echo base_url('/uploads/jenis_dokumen/' . $sje->id . '/' . $sje->icon); ?>"
                                alt="image" width="200" style="box-shadow:5px 5px grey !important">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__section">
                                <a href="<?php echo base_url('/beranda/ebook/' . $sje->id); ?>"
                                    class="kt-widget__username"> <?php echo $sje->jenis_dokumen; ?> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;
    ?>
</div>