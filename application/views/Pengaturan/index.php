<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body">
                <?php if($list_pengaturan): ?>
                    <?php foreach($list_pengaturan as $row): ?>
                        <div class="form-group row">
                            <label class="col-3 col-form-label"><?= ucwords(str_replace('_',' ', $row['setting_key'])) ?></label>
                            <div class="col-4">
                                <textarea class="form-control" name="<?= $row['setting_key'] ?>" id="<?= $row['setting_key'] ?>" rows="10"><?= $row['setting_desc'] ?></textarea>
                                <span class="form-text text-muted"><?= $row['help_text'] ?></span>
                            </div>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-3"> </div>
                        <div class="col-8">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>

    function save() {
        var form = $('#form1')[0];
        var formData = new FormData(form);

        btnSpinnerShow();

        $.ajax({
            url: '{segment1}save',
            dataType: 'json',
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.status) {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'success',
                        title: 'Sukses',
                        text: data.message,
                    }).then((res) => {
                        redirect('{segment1}');
                    });
                } else {
                    swal.fire({
                        allowOutsideClick: false,
                        type: 'error',
                        title: 'Kesalahan',
                        text: data.message,
                    }).then((res) => {
                        btnSpinnerHide();
                    });
                }
            },
            error: function() {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: 'Internal server error',
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        });
    }
</script>