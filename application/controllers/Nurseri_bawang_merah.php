<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Nurseri_bawang_merah extends CI_Controller
{

    private $title = 'Nurseri';
    private $view = 'Nurseri_bawang_merah';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->view . '_model', 'model');
        if (!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');

        $data['title'] = $this->title . ' Bawang Merah';
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables');
        $this->datatables->select('nurseri.*, 
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');
        $this->datatables->join('tbl_kabupaten', 'nurseri.kabupaten_kode = tbl_kabupaten.kode');
        $this->datatables->join('tbl_kecamatan', 'nurseri.kecamatan_kode = tbl_kecamatan.kode');
        $this->datatables->join('tbl_jenis_komoditi', 'nurseri.jenis_komoditi_id = tbl_jenis_komoditi.id');
        $this->datatables->where('nurseri.jenis_komoditi_id', '68');

        $this->datatables->from('nurseri');
        return print_r($this->datatables->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['jenis_komoditi_id'] = 68;
        $data['title'] = $this->title . ' Bawang Merah';
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = getRowArray('nurseri', array('id' => $id));
            if ($data) {
                $data['title'] = $this->title . ' Bawang Merah';
                $data['subTitle'] = 'Edit ' . $this->title;
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function save()
    {
        $this->model->save();
    }

    public function delete()
    {
        if (akses_user('delete')) {
            $this->model->delete();
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}
