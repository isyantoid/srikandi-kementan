var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5.8,
        center: {
            lat: -2.4833826,
            lng: 117.8902853
        },
        disableDefaultUI: true
    });

    // Muat GeoJSON menggunakan fetch API
    fetch(baseUrl + 'dashboard/json_nasional').then(response => response.json()).then(geojson => { // Buat lapisan GeoJSON
        var geojsonLayer = new google.maps.Data({
            map: map,
            style: {
                fillColor: 'green',
                strokeWeight: 1
            }
        });

        // Tambahkan GeoJSON ke lapisan
        geojsonLayer.addGeoJson(geojson);
    });

    $("#saveButton").click(function () {
        saveMapImage();
    });
}

function saveMapImage() {
    const mapCanvas = map.getDiv();

    html2canvas(mapCanvas, {
        useCORS: true, // Aktifkan CORS jika Anda mengalami masalah dengan izin
    }).then(function (canvas) {
        const dataUrl = canvas.toDataURL("image/png");

        // Kirim dataUrl ke server menggunakan metode POST
        $.ajax({
            type: "POST",
            url: baseUrl + "laporan_rekap_p2l/create_capture_map", // Ganti dengan URL skrip PHP Anda
            data: { image: dataUrl },
            success: function (response) {
                if (response === "success") {
                    // alert("Gambar berhasil disimpan di server.");
                    window.location.href = baseUrl + 'laporan_rekap_p2l/export_rekap_nasional';
                } else {
                    alert("Gagal menyimpan gambar di server.");
                }
            },
            error: function (xhr, status, error) {
                console.error(error); // Tampilkan pesan kesalahan (opsional)
                alert("Terjadi kesalahan saat mengirim permintaan ke server.");
            },
        });


        // const a = document.createElement("a");
        // a.href = dataUrl;
        // a.download = "map_image.png"; // Nama file yang diunduh
        // a.click();
    });
}
