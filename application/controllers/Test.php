<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
	}


	public function index(){
		$totalLuas = "(select sum(luas_kebun) from tbl_permohonan ";
        $totalLuas .= "where tbl_permohonan.app_id = 2 and tbl_permohonan.jenis_komoditi_id = tbl_head.jenis_komoditi_id ";
        if($this->provinsi_kode) {
            $totalLuas .= "and tbl_permohonan.provinsi_kode = " . $this->provinsi_kode . " ";
        }
        if($this->kabupaten_kode) {
            $totalLuas .= "and tbl_permohonan.kabupaten_kode = " . $this->kabupaten_kode . " ";
        }
        $totalLuas .= " ) as total_luas,";

        $totalKampung = "(select count(*) from tbl_permohonan where tbl_permohonan.app_id = 2 and tbl_permohonan.jenis_komoditi_id = tbl_head.jenis_komoditi_id ";
        if($this->provinsi_kode) {
            $totalKampung .= "and tbl_permohonan.provinsi_kode = " . $this->provinsi_kode . " ";
        }
        if($this->kabupaten_kode) {
            $totalKampung .= "and tbl_permohonan.kabupaten_kode = " . $this->kabupaten_kode . " ";
        }
        $totalKampung .= " ) as total_kampung,";
        
        
        $totalKelompok = "(select id from tbl_permohonan where tbl_permohonan.app_id = 2 and tbl_permohonan.jenis_komoditi_id = tbl_head.jenis_komoditi_id ";
        if($this->provinsi_kode) {
            $totalKelompok .= "and tbl_permohonan.provinsi_kode = " . $this->provinsi_kode . " ";
        }
        if($this->kabupaten_kode) {
            $totalKelompok .= "and tbl_permohonan.kabupaten_kode = " . $this->kabupaten_kode . " ";
        }
        $totalKelompok .= " )";

        $this->db->select('tbl_head.kelompok_komoditi_nama,
        tbl_head.jenis_komoditi_kode,
        tbl_head.jenis_komoditi_nama,
        tbl_jenis_komoditi.jenis_komoditi_icon,
        '.$totalKampung.'
        '.$totalLuas.'
        (select count(*) from tbl_kelompok_tani where id_permohonan in '.$totalKelompok.') as total_kelompok,
        (select count(*) from tbl_anggota_tani where id_permohonan = tbl_head.id ) as total_anggota
        ');
        $this->db->join('tbl_jenis_komoditi','tbl_head.jenis_komoditi_id = tbl_jenis_komoditi.id');
        if($this->provinsi_kode) $this->db->where('tbl_head.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_head.kabupaten_kode', $this->kabupaten_kode);
        $this->db->where('tbl_head.app_id', '2');
        $this->db->group_by('tbl_head.jenis_komoditi_id');
        $get = $this->db->get('tbl_permohonan tbl_head');

 		echo $this->db->last_query();       
	}
	

    public function cek(){
        $array_data = array(
            'kelompok_komoditi_id' => 3,
            'jenis_komoditi_id' => 1,
            'provinsi_kode' => '',
            'kabupaten_kode' => ''
            );

        $a = $this->hasil($array_data);
        print_r($a);
    }

	public function hasil($array_data){
		$this->db->select('
            tbl_permohonan.id,
            tbl_permohonan.nomor_registrasi,
            tbl_permohonan.lat,
            tbl_permohonan.lng,
            tbl_permohonan.desa_nama,
            tbl_permohonan.kecamatan_nama,
            tbl_permohonan.kabupaten_nama,
            tbl_permohonan.provinsi_nama,
            tbl_permohonan.luas_kebun,
            tbl_permohonan.satuan_luas_kebun,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_jenis_komoditi.jenis_komoditi_pin,
            (select COALESCE(sum(total_nilai_bantuan),0) from tbl_bantuan where permohonan_id = tbl_permohonan.id) total_nilai_bantuan,
            (select count(*) from tbl_kelompok_tani where id_permohonan = tbl_permohonan.id) total_kelompok_tani,
            (select count(*) from tbl_anggota_tani where id_permohonan = tbl_permohonan.id) total_anggota_tani,
            coalesce((select nilai_rating_persen from tbl_penilaian where permohonan_id = tbl_permohonan.id), 0) as nilai_rating_persen,
        ');

        if(is_array($array_data)):
            if(!empty($array_data['kelompok_komoditi_id'])):
             $this->db->where('tbl_permohonan.kelompok_komoditi_id', $array_data['kelompok_komoditi_id']);
            endif;
            if(!empty($array_data['jenis_komoditi_id'])): 
                $this->db->where('tbl_permohonan.jenis_komoditi_id', $array_data['jenis_komoditi_id']);
            endif;
            if(!empty($array_data['provinsi_kode'])): 
                $this->db->where('tbl_permohonan.provinsi_kode', $array_data['provinsi_kode']);
            endif;
            if(!empty($array_data['kabupaten_kode'])): 
                $this->db->where('tbl_permohonan.kabupaten_kode', $array_data['kabupaten_kode']);
            endif;
        else:
            $this->db->where('lat !=', null);
            $this->db->or_where('lat !=', 0);
            $this->db->where('lng !=', null);
            $this->db->or_where('lng !=', 0);
        endif;

        // $this->db->where('lat !=', null);
        // $this->db->or_where('lat !=', 0);
        // $this->db->where('lng !=', null);
        // $this->db->or_where('lng !=', 0);

        if($this->provinsi_kode) $this->db->where('tbl_permohonan.provinsi_kode', $this->provinsi_kode);
        if($this->kabupaten_kode) $this->db->where('tbl_permohonan.kabupaten_kode', $this->kabupaten_kode);

        
        $this->db->join('tbl_jenis_komoditi','tbl_permohonan.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->where('tbl_permohonan.app_id', '2');
        $get = $this->db->get('tbl_permohonan');
        return $get->result_array();    
	}



}
