<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_rekap_p2l_model extends CI_Model
{

    public function get_count_alokasi($provinsiKode) {
        $this->db->where('tbl_registrasi_p2l.provinsi_kode', $provinsiKode);
        $this->db->from('tbl_registrasi_p2l');
        $res = $this->db->get()->num_rows();
        return $res;
    }

    public function get_laporan() {
        $this->db->select('
            tbl_registrasi_p2l.id,
            tbl_registrasi_p2l.provinsi_kode,
            tbl_provinsi.nama as provinsi_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            count(tbl_registrasi_p2l.id) as alokasi
        ');
        $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->group_by('tbl_registrasi_p2l.kabupaten_kode');
        $this->db->from('tbl_registrasi_p2l');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_rekap_provinsi($provinsiKode) {
        $this->db->select('
            tbl_registrasi_p2l.id,
            tbl_registrasi_p2l.nama_ketua,
            tbl_registrasi_p2l.kabupaten_kode,
            tbl_desa.nama as desa_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_provinsi.nama as provinsi_nama,
            tbl_registrasi_p2l.nama_kelompok
        ');
        $this->db->join('tbl_kelompok_tani', 'tbl_registrasi_p2l.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->join('tbl_kecamatan', 'tbl_registrasi_p2l.kecamatan_kode = tbl_kecamatan.kode', 'left');
        $this->db->join('tbl_desa', 'tbl_registrasi_p2l.desa_kode = tbl_desa.kode', 'left');
        if($provinsiKode) $this->db->where('tbl_registrasi_p2l.provinsi_kode', $provinsiKode);
        $this->db->from('tbl_registrasi_p2l');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_rekap_kabupaten($kabupatenKode) {
        $this->db->select('
            tbl_registrasi_p2l.id,
            tbl_registrasi_p2l.nama_ketua,
            tbl_registrasi_p2l.kabupaten_kode,
            tbl_desa.nama as desa_nama,
            tbl_kecamatan.nama as kecamatan_nama,
            tbl_kabupaten.nama as kabupaten_nama,
            tbl_provinsi.nama as provinsi_nama,
            tbl_registrasi_p2l.nama_kelompok
        ');
        $this->db->join('tbl_kelompok_tani', 'tbl_registrasi_p2l.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->db->join('tbl_provinsi', 'tbl_registrasi_p2l.provinsi_kode = tbl_provinsi.kode', 'left');
        $this->db->join('tbl_kabupaten', 'tbl_registrasi_p2l.kabupaten_kode = tbl_kabupaten.kode', 'left');
        $this->db->join('tbl_kecamatan', 'tbl_registrasi_p2l.kecamatan_kode = tbl_kecamatan.kode', 'left');
        $this->db->join('tbl_desa', 'tbl_registrasi_p2l.desa_kode = tbl_desa.kode', 'left');
        if($kabupatenKode) $this->db->where('tbl_registrasi_p2l.kabupaten_kode', $kabupatenKode);
        $this->db->from('tbl_registrasi_p2l');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_bantuan($registrasiP2lId) {
        $this->db->select('
            tbl_bantuan_p2l.id,
            tbl_bantuan_p2l.tanggal_penerima_bantuan,
            tbl_bantuan_p2l.realisasi_pemanfaatan_anggaran,
        ');
        $this->db->where('tbl_bantuan_p2l.registrasi_p2l_id', $registrasiP2lId);
        $this->db->from('tbl_bantuan_p2l');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_realisasi($bantuanP2lId, $jenis) {
        $this->db->select('
            tbl_bantuan_p2l_realisasi.id,
            tbl_bantuan_p2l_realisasi.nama_realisasi,
            tbl_bantuan_p2l_realisasi.jumlah,
            tbl_bantuan_p2l_realisasi.nilai,
            tbl_bantuan_p2l_realisasi.foto,
        ');
        $this->db->where('tbl_bantuan_p2l_realisasi.bantuan_p2l_id', $bantuanP2lId);
        $this->db->where('tbl_bantuan_p2l_realisasi.jenis', $jenis);
        $this->db->from('tbl_bantuan_p2l_realisasi');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_capaian_kinerja($registrasiP2lId, $tipe) {
        $this->db->select('
            tbl_capaian_kinerja.id,
            tbl_jenis_komoditi.jenis_komoditi_nama,
            tbl_capaian_kinerja.tanggal_capaian_kinerja,
            tbl_capaian_kinerja.jumlah,
        ');
        $this->db->join('tbl_jenis_komoditi', 'tbl_capaian_kinerja.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->db->where('tbl_capaian_kinerja.registrasi_p2l_id', $registrasiP2lId);
        $this->db->where('tbl_capaian_kinerja.tipe', $tipe);
        $this->db->from('tbl_capaian_kinerja');
        $res = $this->db->get();
        return $res->result_array();
    }
    
    public function get_kendala_permasalahan($registrasiP2lId) {
        $this->db->select('
            tbl_kendala_p2l.id,
            tbl_kendala_p2l.komponen,
            tbl_kendala_p2l.tanggal_kejadian,
            tbl_kendala_p2l.uraian,
            tbl_kendala_p2l.uraian_foto,
            tbl_kendala_p2l.upaya_tindak_lanjut,
            tbl_kendala_p2l.upaya_foto,
        ');
        $this->db->where('tbl_kendala_p2l.registrasi_p2l_id', $registrasiP2lId);
        $this->db->from('tbl_kendala_p2l');
        $res = $this->db->get();
        return $res->result_array();
    }
}

/* End of file Lahan_model.php */