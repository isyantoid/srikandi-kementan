<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="{segment1}create" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-plus"></i> New Record</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                    <tr>
                        <th>Record ID</th>
                        <th>#</th>
                        <th>Foto Pengiriman</th>
                        <th>Champion</th>
                        <th>Pasar</th>
                        <th>Bulan</th>
                        <th>Jumlah (Ton)</th>
                        <th>Jumlah Rupiah/Kg</th>
                        <th>Total Rupiah</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

<div class="modal fade" id="modal-gambar" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Lihat Gambar</h5>
      </div>
      <div class="modal-body">
        <div class="text-center">
            <img id="gambar-besar" src="" class="img-fluid" alt="Gambar Besar" style="width:100%">
        </div>
      </div>
    </div>
  </div>
</div>

<script>

function show_image() {
    var gambarKecil = $('.gambar-modal img').attr('src');
    console.log(gambarKecil)
    $('#gambar-besar').attr('src', gambarKecil);
    $('#modal-gambar').modal('show');
}
    
var table;

table = $('#kt_table_1').DataTable({
    ajax: {
        url: '{segment1}datatable'
    },
    responsive: true,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 10,
    order: [
        [0, 'desc']
    ],
    columns: [{
            data: 'id',
            visible: false
        },
        {
            data: 'id',
            width: 25,
            className: 'text-center'
        },
        { 
            data: 'foto',
            width: 25,
            render: function(data, type, row) {
                return '<a class="gambar-modal" href="javascript:show_image()"><img src="{uploads_path}champ/'+data+'" width="40"></a>';
            }
        },
        { data: 'user_nama' },
        { data: 'pasar_nama' },
        { 
            data: 'tahun',
            render: function(data, type, row) {
                return row.bulan + '-' + row.tahun;
            }
        },
        { data: 'jumlah' },
        { data: 'harga' },
        { data: 'harga_total' },
        {
            targets: -1,
            width: 100,
            title: 'Actions',
            className: 'text-center',
            orderable: false,
            render: function(data, type, row, meta) {
                aksi = '<span class="dropdown">';
                aksi +=
                    '<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">';
                aksi += '<i class="la la-ellipsis-h"></i>';
                aksi += '</a>';
                aksi += '<div class="dropdown-menu dropdown-menu-right">';
                aksi += '<a class="dropdown-item" href="{segment1}update/' + row.id +
                    '"><i class="la la-edit"></i> Edit Detail</a>';
                aksi += '<a class="dropdown-item" href="javascript:deleteData(' + row.id +
                    ')"><i class="la la-trash"></i> Delete</a>';
                aksi += '</div>';
                aksi += '</span>';
                return aksi;
            },
        },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        var index = iDisplayIndex + 1;
        $('td:eq(0)', nRow).html(index);
        return nRow;
    }
});

function deleteData(id) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Data yang telah dihapus tidak dapat dikembalikan!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus data!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: '{segment1}delete/' + id,
                success: function(res) {
                    table.ajax.reload();
                    if(res.status) {
                        Swal.fire('Sukses!', 'Data berhasil dihapus.', 'success');
                    } else {
                        Swal.fire('Kesalahan!', res.message, 'error');
                    }
                }
            });
        }
    });
}
</script>