<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="<?= base_url('dist/assets/css/pdf_style.css') ?>">
</head>
<body>
    <h1><?= $title ?></h1>
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">No</th>
                <th>Nama Kelompok</th>
                <th>Desa/Kelurahan</th>
                <th>Kecamatan</th>
                <th>Kabupaten/Kota</th>
                <th>Ketua Kelompok</th>
            </tr>
        </thead>
        <tbody>
            <?php if($get_laporan): ?>
            <?php $no=1; foreach($get_laporan as $row): ?>
            <tr>
                <td class="text-center"><?= $no ?></td>
                <td class="text-center"><?= $row['nama_kelompok'] ?></td>
                <td class="text-center"><?= $row['desa_nama'] ?></td>
                <td class="text-center"><?= $row['kecamatan_nama'] ?></td>
                <td class="text-center"><?= $row['kabupaten_nama'] ?></td>
                <td class="text-center"><?= $row['nama_ketua'] ?></td>
            </tr>
            <?php $no++; endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</body>
</html>