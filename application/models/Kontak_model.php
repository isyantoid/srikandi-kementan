<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kontak_model extends CI_Model
{
    private $tableName = 'tbl_pesan_masuk';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $params = array(
                    'tanggal' => date("Y-m-d H:i:s"),
                    'nama' => $this->input->post('nama', TRUE),
                    'email' => $this->input->post('email', TRUE),
                    'pesan' => $this->input->post('pesan', TRUE),
                );
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName, $params);

                if (!empty($_FILES['lampiran'])) :
                    $this->delete_all_file($id);

                    $data_file = $this->upload_pdf($id);
                    $this->db->where($this->primaryKey, $id);
                    $this->db->update($this->tableName, array('lampiran' => $data_file['nama_file'], 'tipe_file' => $data_file['tipe_file']));
                endif;

                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {
                $params = array(
                    'tanggal' => date("Y-m-d H:i:s"),
                    'nama' => $this->input->post('nama', TRUE),
                    'email' => $this->input->post('email', TRUE),
                    'pesan' => $this->input->post('pesan', TRUE),
                );

                $insert = $this->db->insert($this->tableName, $params);
                $id = $this->db->insert_id();

                if (!empty($_FILES['lampiran'])) :
                    $data_file = $this->upload_pdf($id);
                    $this->db->where($this->primaryKey, $id);
                    $this->db->update($this->tableName, array('lampiran' => $data_file['nama_file'], 'tipe_file' => $data_file['tipe_file']));
                endif;


                if ($insert) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);

        $this->delete_all_file($id);

        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }

    //delete image and folder
    function delete_all_file($id)
    {
        $dir = './uploads/pesan_masuk/' . $id;
        if (!is_dir($dir)) {
            $file = "direktori folder kosong";
        } else {
            $data = scandir($dir);

            $num = count($data) - 1;
            if ($num > 1) {
                foreach (scandir($dir) as $item) {
                    if ($item == '.' || $item == '..') {
                        continue;
                    }

                    unlink($dir . '/' . $item);
                }
                rmdir($dir);
            } else {
                $file = "file kosong";
            }
        }
    }

    public function upload_pdf($id)
    {

        $path = './uploads/pesan_masuk/' . $id;
        if (!is_dir($path)) {
            //Directory does not exist, so lets create it.
            mkdir($path, 0755, true);
        }

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|gif|png|jpeg|pdf';
        $config['max_size'] = 0;
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('lampiran')) {
            $error = array('error' => $this->upload->display_errors());
            $nama_file = '';
            $tipe_file = '';
        } else {
            $data = array('upload_data' => $this->upload->data());
            $nama_file =  $data['upload_data']['file_name'];
            $tipe_file = explode('/', $data['upload_data']['file_type'])[0];
        }

        return array('nama_file' => $nama_file, 'tipe_file' => $tipe_file);
    }
}