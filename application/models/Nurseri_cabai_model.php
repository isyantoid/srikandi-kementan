<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Nurseri_cabai_model extends CI_Model
{

    private $tableName = 'nurseri';
    private $primaryKey = 'id';

    public function save()
    {
        if ($this->input->is_ajax_request()) {
            $id = $this->uri->segment(3);
            if ($id) {
                $checkDup = getRowArray($this->tableName, array($this->primaryKey => $id));
                if ($checkDup['nama'] != $this->input->post('nama')) {
                    if (isDuplicate($this->tableName, 'nama', $this->input->post('nama'))) {
                        return jsonOutputError('Nama nurseri sudah ada sebelumnya.');
                    }
                }

                $config = $this->config_file_upload();
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $data_upload = [];
                if ($this->upload->do_upload('foto')) {
                    $data_upload = $this->upload->data();
                    $this->db->set('foto', $data_upload['file_name']);
                }

                foreach ($this->input->post() as $key => $val) {
                    $this->db->set($key, strip_tags($val));
                }
                $this->db->where($this->primaryKey, $id);
                $update = $this->db->update($this->tableName);
                if ($update) {
                    return jsonOutputSuccess();
                } else {
                    return jsonOutputError();
                }
            } else {

                if (isDuplicate('tbl_user', 'username', $this->input->post('username'))) {
                    return jsonOutputError('Username login sudah ada sebelumnya.');
                }

                if (isDuplicate('champ', 'nama', $this->input->post('nama'))) {
                    return jsonOutputError('Nama nurseri sudah ada sebelumnya.');
                }

                $config = $this->config_file_upload();
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                $data_upload = [];
                if ($this->upload->do_upload('foto')) {
                    $data_upload = $this->upload->data();
                } else {
                    return jsonOutputError($this->upload->display_errors('', ''));
                }

                $this->db->set('nama', $this->input->post('nama'));
                $this->db->set('username', $this->input->post('username'));
                $this->db->set('password', md5($this->input->post('password')));
                $this->db->set('user_level_id', 16);
                $this->db->set('provinsi_kode', $this->input->post('provinsi_kode'));
                $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                $this->db->set('foto', $data_upload['file_name']);
                $this->db->set('app_id', '2');
                $insertUser = $this->db->insert('tbl_user');
                $user_id = $this->db->insert_id();
                if ($insertUser) {
                    $this->db->set('nama', $this->input->post('nama'));
                    $this->db->set('nomor_hp', $this->input->post('nomor_hp'));
                    $this->db->set('alamat', $this->input->post('alamat'));
                    $this->db->set('desa_kode', $this->input->post('desa_kode'));
                    $this->db->set('kecamatan_kode', $this->input->post('kecamatan_kode'));
                    $this->db->set('kabupaten_kode', $this->input->post('kabupaten_kode'));
                    $this->db->set('provinsi_kode', $this->input->post('provinsi_kode'));
                    $this->db->set('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'));
                    $this->db->set('jenis_komoditi_kedua_id', $this->input->post('jenis_komoditi_kedua_id'));
                    $this->db->set('lat', $this->input->post('lat'));
                    $this->db->set('lng', $this->input->post('lng'));
                    $this->db->set('luas', $this->input->post('luas'));
                    $this->db->set('foto', $data_upload['file_name']);
                    $this->db->set('user_id', $user_id);
                    $this->db->set('created_at', date('Y-m-d H:i:s'));
                    $insertChamp = $this->db->insert('champ');


                    if ($insertChamp) {
                        return jsonOutputSuccess();
                    } else {
                        return jsonOutputError();
                    }
                }
            }
        }
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->db->where($this->primaryKey, $id);
        return $this->db->delete($this->tableName);
    }

    public function config_file_upload()
    {
        $config['upload_path'] = './uploads/users/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['encrypt_name']  = true;
        $config['overwrite']  = true;
        return $config;
    }
}
