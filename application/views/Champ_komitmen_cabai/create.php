<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title"><i class="kt-font-brand la la-edit"></i> {subTitle}</h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form kt-form--label-right" action="javascript:save()" id="form1">
            <div class="kt-portlet__body" style="padding-bottom:0px !important">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Tahun</label>
                            <input class="form-control tahun" type="text" value="<?= date('Y') ?>" readonly name="tahun" required>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label>Champion</label>
                            <?php $userId = $this->session->userdata('session_user_id'); ?>
                            <select class="form-control champ_id" name="champ_id" <?= ($userId == $champ_user_id) ? 'disabled' : '' ?> style="width:100%;"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label>Jenis Komoditi</label>
                            <select class="form-control jenis_komoditi_id" name="jenis_komoditi_id" style="width:100%;"></select>
                        </div>
                    </div>
                </div>
                <div class="form_komitmen" hidden>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th class="h3">Bulan</th>
                            <th class="h3 head-table-komitmen">Komitmen Stok Cabai Rawit (Ton)</th>
                        </thead>
                        <tbody>
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <?php $month = date("M", mktime(0, 0, 0, $i, 1)); ?>
                                <tr>
                                    <td class="align-middle h5 font-weight-bold"><?= $month ?></td>
                                    <td class="align-midle">
                                        <input class="form-control" type="number" value="0" min="0" name="<?= strtolower($month) ?>" required>
                                    </td>
                                </tr>
                            <?php endfor ?>
                        </tbody>
                    </table>
                </div>

                
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{segment1}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end:: Content -->

<script>

$(document).on('change', '.champ_id', function(){
    var val = $(this).val();
    $.ajax({
        url: '{segment1}champ_komitmen_cabai',
        data: { champ_id: val },
        success: function(response) {
            $('#output').html(response);
        },
        error: function() {
            $('#output').text('Gagal memuat konten.');
        }
    });
})

ajaxSelect({
    id: '.champ_id',
    url: '{site_url}ajax_selectbox/champ_cabai',
    selected: '{champ_id}'
});

ajaxSelect({
    id: '.jenis_komoditi_id',
    url: '{site_url}ajax_selectbox/cabe_id',
});

$(".tahun").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
});

$(document).on('change', '.jenis_komoditi_id', function() {
    var val = $(this).val();
    var title = '';
    if(val == 76) {
        title = 'Cabai Besar';
        $('.form_komitmen').attr('hidden', false);
        $('.head-table-komitmen').text('Komitmen Stok ' + title);
    } else {
        title = 'Cabai Rawit';
        $('.form_komitmen').attr('hidden', false);
        $('.head-table-komitmen').text('Komitmen Stok ' + title);
    }
});

function save() {
    var form = $('#form1')[0];
    var formData = new FormData(form);

    btnSpinnerShow();

    $.ajax({
        url: '{segment1}save',
        dataType: 'json',
        method: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function(data) {
            if (data.status) {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'success',
                    title: 'Sukses',
                    text: data.message,
                }).then((res) => {
                    redirect('{segment1}');
                });
            } else {
                swal.fire({
                    allowOutsideClick: false,
                    type: 'error',
                    title: 'Kesalahan',
                    text: data.message,
                }).then((res) => {
                    btnSpinnerHide();
                });
            }
        },
        error: function() {
            swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title: 'Kesalahan',
                text: 'Internal server error',
            }).then((res) => {
                btnSpinnerHide();
            });
        }
    });
}
</script>