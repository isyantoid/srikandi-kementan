<?php

defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Laporan_p2l extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_p2l_model', 'model');
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {

        akses_user('read');

        $data['provinsi_kode'] = $this->input->get('provinsi_kode');
        $data['kabupaten_kode'] = $this->input->get('kabupaten_kode');
        $data['kelompok_tani_id'] = $this->input->get('kelompok_tani_id');

        $this->data['get_laporan'] = $this->model->get_laporan(
            $data['provinsi_kode'],
            $data['kabupaten_kode'],
            $data['kelompok_tani_id']
        );
        $this->data['content'] = 'Laporan_p2l/index';
        $this->data['title'] = 'Laporan P2L';
        $this->data['subTitle'] = '';
        $data = array_merge($this->data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function export_hasil_pencarian() {
        $fileName = 'laporan_registrasi_p2l_' . date('d_m_Y') . '.xlsx';
        $this->load->library('excel');

        $objPHPExcel = new PHPExcel();

        $styleForTitle = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 14,
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );
        
        $styleForSubTitle = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 10,
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForHeader = array(
            'font'  => [
                'bold'  => true,
                'color' => ['rgb' => '000000'],
                'size'  => 10,
            ],
            'borders' => [
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000']
                ]
            ],
            'fill' => [
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'DDDDDD'],
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $styleForBody = array(
            'font'  => [
                'color' => ['rgb' => '000000'],
                'size'  => 9,
            ],
            'borders' => [
                'allBorders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THICK,
                    'color' => ['rgb' => '444444']
                ]
            ],
            'aligment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_GENERAL,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],

        );

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->getStyle('A2:AX5')->applyFromArray($styleForSubTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A2:K3')->getStyle('A2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('A4:A5');
        $objPHPExcel->getActiveSheet()->mergeCells('B4:B5');
        $objPHPExcel->getActiveSheet()->mergeCells('C4:C5');
        $objPHPExcel->getActiveSheet()->mergeCells('D4:D5');
        $objPHPExcel->getActiveSheet()->mergeCells('E4:E5');
        $objPHPExcel->getActiveSheet()->mergeCells('F4:H4');
        $objPHPExcel->getActiveSheet()->mergeCells('I4:I5');
        $objPHPExcel->getActiveSheet()->mergeCells('J4:J5');
        $objPHPExcel->getActiveSheet()->mergeCells('K4:K5');
        $objPHPExcel->getActiveSheet()->mergeCells('K4:K5');
        $objPHPExcel->getActiveSheet()->mergeCells('L4:L5');
        $objPHPExcel->getActiveSheet()->mergeCells('M4:M5');
        $objPHPExcel->getActiveSheet()->mergeCells('M2:AC3')->getStyle('M2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('N4:Q4')->getStyle('N4')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '1E90FF')
                )
            )
        );
        $objPHPExcel->getActiveSheet()->mergeCells('R4:U4')->getStyle('R4')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FF7417')
                )
            )
        );
        $objPHPExcel->getActiveSheet()->mergeCells('V4:Y4')->getStyle('V4')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'FFAE42')
                )
            )
        );
        $objPHPExcel->getActiveSheet()->mergeCells('Z4:AC4')->getStyle('Z4')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '32CD32')
                )
            )
        );
        $objPHPExcel->getActiveSheet()->mergeCells('AD2:AR3')->getStyle('AD2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->mergeCells('AD4:AF4');
        $objPHPExcel->getActiveSheet()->mergeCells('AG4:AI4');
        $objPHPExcel->getActiveSheet()->mergeCells('AJ4:AL4');
        $objPHPExcel->getActiveSheet()->mergeCells('AM4:AO4');
        $objPHPExcel->getActiveSheet()->mergeCells('AP4:AR4');
        $objPHPExcel->getActiveSheet()->mergeCells('AS2:AX4')->getStyle('AS2')->applyFromArray($styleForTitle)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'DATA KELOMPOK TANI');
        $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'NO.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Nomor Registrasi P2L');
        $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Tanggal Registrasi P2L');
        $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Kelompok Tani');
        $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Nama Ketua');
        $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Alamat');
        $objPHPExcel->getActiveSheet()->SetCellValue('F5', 'Desa');
        $objPHPExcel->getActiveSheet()->SetCellValue('G5', 'Kecamatan');
        $objPHPExcel->getActiveSheet()->SetCellValue('H5', 'Kabupaten');
        $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Jumlah Penerima Manfaat/Anggota (Orang)');
        $objPHPExcel->getActiveSheet()->SetCellValue('J4', 'Terdaftar Simluhtan');
        $objPHPExcel->getActiveSheet()->SetCellValue('K4', 'Titik Koordinat Rumah Benih/Demplot');
        $objPHPExcel->getActiveSheet()->SetCellValue('L4', 'Tanggal Terima Bantuan (Transfer Uang)');
        $objPHPExcel->getActiveSheet()->SetCellValue('M4', 'Realisasi Pemanfaatan Anggaran (Rp)');
        $objPHPExcel->getActiveSheet()->SetCellValue('M2', 'BANTUAN');
        $objPHPExcel->getActiveSheet()->SetCellValue('N4', 'Realisasi Fisik Sarana Perbenihan');
        $objPHPExcel->getActiveSheet()->SetCellValue('N5', 'Jenis');
        $objPHPExcel->getActiveSheet()->SetCellValue('O5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('P5', 'Nilai (Rp)');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q5', 'Foto');
        $objPHPExcel->getActiveSheet()->SetCellValue('R4', 'Realisasi Fisik Demplot');
        $objPHPExcel->getActiveSheet()->SetCellValue('R5', 'Jenis');
        $objPHPExcel->getActiveSheet()->SetCellValue('S5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('T5', 'Nilai (Rp)');
        $objPHPExcel->getActiveSheet()->SetCellValue('U5', 'Foto');
        $objPHPExcel->getActiveSheet()->SetCellValue('V4', 'Realisasi Fisik Pertanaman');
        $objPHPExcel->getActiveSheet()->SetCellValue('V5', 'Jenis');
        $objPHPExcel->getActiveSheet()->SetCellValue('W5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('X5', 'Nilai (Rp)');
        $objPHPExcel->getActiveSheet()->SetCellValue('Y5', 'Foto');
        $objPHPExcel->getActiveSheet()->SetCellValue('Z4', 'Realisasi Fisik Sarana Pascapanen');
        $objPHPExcel->getActiveSheet()->SetCellValue('Z5', 'Jenis');
        $objPHPExcel->getActiveSheet()->SetCellValue('AA5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('AB5', 'Nilai (Rp)');
        $objPHPExcel->getActiveSheet()->SetCellValue('AC5', 'Foto');
        $objPHPExcel->getActiveSheet()->SetCellValue('AD2', 'CAPAIAN KINERJA');
        $objPHPExcel->getActiveSheet()->SetCellValue('AD4', 'Produksi Benih');
        $objPHPExcel->getActiveSheet()->SetCellValue('AD5', 'Komoditas');
        $objPHPExcel->getActiveSheet()->SetCellValue('AE5', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('AF5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('AG4', 'Hasil Panen Demplot');
        $objPHPExcel->getActiveSheet()->SetCellValue('AG5', 'Komoditas');
        $objPHPExcel->getActiveSheet()->SetCellValue('AH5', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('AI5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ4', 'Hasil Panen Pertanaman');
        $objPHPExcel->getActiveSheet()->SetCellValue('AJ5', 'Komoditas');
        $objPHPExcel->getActiveSheet()->SetCellValue('AK5', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('AL5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('AM4', 'Hasil Penjualan (Pendapatan Kelompok)');
        $objPHPExcel->getActiveSheet()->SetCellValue('AM5', 'Komoditas');
        $objPHPExcel->getActiveSheet()->SetCellValue('AN5', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('AO5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('AP4', 'Hasil Panen yang Dikonsumsi Anggota');
        $objPHPExcel->getActiveSheet()->SetCellValue('AP5', 'Komoditas');
        $objPHPExcel->getActiveSheet()->SetCellValue('AQ5', 'Tanggal');
        $objPHPExcel->getActiveSheet()->SetCellValue('AR5', 'Jumlah');
        $objPHPExcel->getActiveSheet()->SetCellValue('AS2', 'PERMASALAH / KENDALA');
        $objPHPExcel->getActiveSheet()->SetCellValue('AS5', 'Komponen');
        $objPHPExcel->getActiveSheet()->SetCellValue('AT5', 'Tanggal Kejadian');
        $objPHPExcel->getActiveSheet()->SetCellValue('AU5', 'Uraian');
        $objPHPExcel->getActiveSheet()->SetCellValue('AV5', 'Foto');
        $objPHPExcel->getActiveSheet()->SetCellValue('AW5', 'Upaya Tindak Lanjut');
        $objPHPExcel->getActiveSheet()->SetCellValue('AX5', 'Foto');

        $objPHPExcel->getActiveSheet()->getStyle('A4:AX5')->getAlignment()->setWrapText(true); 
        $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(40);
        $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(40);

        $data['provinsi_kode'] = $this->input->get('provinsi_kode');
        $data['kabupaten_kode'] = $this->input->get('kabupaten_kode');
        $data['kelompok_tani_id'] = $this->input->get('kelompok_tani_id');

        $laporan = $this->model->get_laporan(
            $data['provinsi_kode'],
            $data['kabupaten_kode'],
            $data['kelompok_tani_id']
        );


        if($laporan) {
            $rowCount = 6;
            $highesRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow()+1;
            $no = 1;
            foreach($laporan as $row) {
                $rowCount = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow() + 1;

                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no)->getStyle('A' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $row['nomor_registrasi_p2l'])->getStyle('B' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $row['tanggal_registrasi_p2l'])->getStyle('C' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $row['nama_kelompok'])->getStyle('D' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $row['nama_ketua'])->getStyle('E' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $row['desa_nama'])->getStyle('F' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $row['kecamatan_nama'])->getStyle('G' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $row['kabupaten_nama'])->getStyle('H' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $row['jumlah_penerima_manfaat'])->getStyle('I' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $terdaftarSimluhtan = $row['terdaftar_simluhtan'] == '1' ? 'Ya' : 'Tidak';
                $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $terdaftarSimluhtan)->getStyle('J' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $row['lat'].','.$row['lng'])->getStyle('K' . $rowCount)->applyFromArray($styleForBody);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

                $bantuan = $this->model->get_bantuan($row['id']);
                if($bantuan) {
                    foreach($bantuan as $btn) {
                        
                        $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $btn['tanggal_penerima_bantuan'])->getStyle('L' . $rowCount)->applyFromArray($styleForBody);
                        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
                        $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $btn['realisasi_pemanfaatan_anggaran'])->getStyle('M' . $rowCount)->applyFromArray($styleForBody);
                        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
                        
                        $realisasi = $this->model->get_realisasi($btn['id'],'1');
                        if($realisasi) {
                            $rowRealisasi = 0;
                            foreach($realisasi as $rls) {
                                $newRowRealisasi = $rowCount + $rowRealisasi; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('N' . $newRowRealisasi, $rls['nama_realisasi'])->getStyle('N' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('O' . $newRowRealisasi, $rls['jumlah'])->getStyle('O' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
                                $objPHPExcel->getActiveSheet()->SetCellValue('P' . $newRowRealisasi, $rls['nilai'])->getStyle('P' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
                                $link = base_url('uploads/realisasi_bantuan/' . $rls['foto']);
                                $url = str_replace('https://', '', $link);
                                $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $newRowRealisasi, '=Hyperlink("https://'.$url.'","Klik Foto")')->getStyle('Q' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
                                $rowRealisasi++;
                            }
                        }
                        
                        $realisasi = $this->model->get_realisasi($btn['id'],'2');
                        if($realisasi) {
                            $rowRealisasi = 0;
                            foreach($realisasi as $rls) {
                                $newRowRealisasi = $rowCount + $rowRealisasi; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('R' . $newRowRealisasi, $rls['nama_realisasi'])->getStyle('R' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('S' . $newRowRealisasi, $rls['jumlah'])->getStyle('S' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
                                $objPHPExcel->getActiveSheet()->SetCellValue('T' . $newRowRealisasi, $rls['nilai'])->getStyle('T' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
                                $link = base_url('uploads/realisasi_bantuan/' . $rls['foto']);
                                $url = str_replace('https://', '', $link);
                                $objPHPExcel->getActiveSheet()->SetCellValue('U' . $newRowRealisasi, '=Hyperlink("https://'.$url.'","Klik Foto")')->getStyle('U' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
                                $rowRealisasi++;
                            }
                        }

                        $realisasi = $this->model->get_realisasi($btn['id'],'3');
                        if($realisasi) {
                            $rowRealisasi = 0;
                            foreach($realisasi as $rls) {
                                $newRowRealisasi = $rowCount + $rowRealisasi; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('V' . $newRowRealisasi, $rls['nama_realisasi'])->getStyle('V' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('W' . $newRowRealisasi, $rls['jumlah'])->getStyle('W' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
                                $objPHPExcel->getActiveSheet()->SetCellValue('X' . $newRowRealisasi, $rls['nilai'])->getStyle('X' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
                                $link = base_url('uploads/realisasi_bantuan/' . $rls['foto']);
                                $url = str_replace('https://', '', $link);
                                $objPHPExcel->getActiveSheet()->SetCellValue('Y' . $newRowRealisasi, '=Hyperlink("https://'.$url.'","Klik Foto")')->getStyle('Y' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(15);
                                $rowRealisasi++;
                            }
                        }
                        
                        $realisasi = $this->model->get_realisasi($btn['id'],'4');
                        if($realisasi) {
                            $rowRealisasi = 0;
                            foreach($realisasi as $rls) {
                                $newRowRealisasi = $rowCount + $rowRealisasi; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('Z' . $newRowRealisasi, $rls['nama_realisasi'])->getStyle('Z' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AA' . $newRowRealisasi, $rls['jumlah'])->getStyle('AA' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(15);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AB' . $newRowRealisasi, $rls['nilai'])->getStyle('AB' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
                                $link = base_url('uploads/realisasi_bantuan/' . $rls['foto']);
                                $url = str_replace('https://', '', $link);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AC' . $newRowRealisasi, '=Hyperlink("https://'.$url.'","Klik Foto")')->getStyle('AC' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
                                $rowRealisasi++;
                            }
                        }
                        
                        $capaianKinerja = $this->model->get_capaian_kinerja($row['id'],'1');
                        if($capaianKinerja) {
                            $rowCK = 0;
                            foreach($capaianKinerja as $ck) {
                                $newRowRealisasi = $rowCount + $rowCK; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('AD' . $newRowRealisasi, $ck['jenis_komoditi_nama'])->getStyle('AD' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AE' . $newRowRealisasi, $ck['tanggal_capaian_kinerja'])->getStyle('AE' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AF' . $newRowRealisasi, $ck['jumlah'])->getStyle('AF' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);

                                $newRowRealisasi++;
                            }
                        }
                        
                        $capaianKinerja = $this->model->get_capaian_kinerja($row['id'],'2');
                        if($capaianKinerja) {
                            $rowCK = 0;
                            foreach($capaianKinerja as $ck) {
                                $newRowRealisasi = $rowCount + $rowCK; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('AG' . $newRowRealisasi, $ck['jenis_komoditi_nama'])->getStyle('AG' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AH' . $newRowRealisasi, $ck['tanggal_capaian_kinerja'])->getStyle('AH' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AI' . $newRowRealisasi, $ck['jumlah'])->getStyle('AI' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);

                                $newRowRealisasi++;
                            }
                        }

                        $capaianKinerja = $this->model->get_capaian_kinerja($row['id'],'3');
                        if($capaianKinerja) {
                            $rowCK = 0;
                            foreach($capaianKinerja as $ck) {
                                $newRowRealisasi = $rowCount + $rowCK; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('AJ' . $newRowRealisasi, $ck['jenis_komoditi_nama'])->getStyle('AJ' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AK' . $newRowRealisasi, $ck['tanggal_capaian_kinerja'])->getStyle('AK' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AL' . $newRowRealisasi, $ck['jumlah'])->getStyle('AL' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);

                                $newRowRealisasi++;
                            }
                        }
                        
                        $capaianKinerja = $this->model->get_capaian_kinerja($row['id'],'4');
                        if($capaianKinerja) {
                            $rowCK = 0;
                            foreach($capaianKinerja as $ck) {
                                $newRowRealisasi = $rowCount + $rowCK; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('AM' . $newRowRealisasi, $ck['jenis_komoditi_nama'])->getStyle('AM' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AN' . $newRowRealisasi, $ck['tanggal_capaian_kinerja'])->getStyle('AN' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AO' . $newRowRealisasi, $ck['jumlah'])->getStyle('AO' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);

                                $newRowRealisasi++;
                            }
                        }
                        
                        $capaianKinerja = $this->model->get_capaian_kinerja($row['id'],'5');
                        if($capaianKinerja) {
                            $rowCK = 0;
                            foreach($capaianKinerja as $ck) {
                                $newRowRealisasi = $rowCount + $rowCK; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('AP' . $newRowRealisasi, $ck['jenis_komoditi_nama'])->getStyle('AP' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AQ' . $newRowRealisasi, $ck['tanggal_capaian_kinerja'])->getStyle('AQ' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AR' . $newRowRealisasi, $ck['jumlah'])->getStyle('AR' . $newRowRealisasi)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);

                                $newRowRealisasi++;
                            }
                        }
                        
                        $kendala = $this->model->get_kendala_permasalahan($row['id']);
                        if($kendala) {
                            $rowCK = 0;
                            foreach($kendala as $kdl) {
                                $newRowKendala = $rowCount + $rowCK; 
                                $objPHPExcel->getActiveSheet()->SetCellValue('AS' . $newRowKendala, $kdl['komponen'])->getStyle('AS' . $newRowKendala)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AT' . $newRowKendala, $kdl['tanggal_kejadian'])->getStyle('AT' . $newRowKendala)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AU' . $newRowKendala, $kdl['uraian'])->getStyle('AU' . $newRowKendala)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
                                $link = base_url('uploads/kendala_p2l/' . $kdl['uraian_foto']);
                                $url = str_replace('https://', '', $link);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AV' . $newRowKendala, '=Hyperlink("https://'.$url.'","Klik Foto")')->getStyle('AV' . $newRowKendala)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(15);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AW' . $newRowKendala, $kdl['upaya_tindak_lanjut'])->getStyle('AW' . $newRowKendala)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
                                $link = base_url('uploads/kendala_p2l/' . $kdl['upaya_foto']);
                                $url = str_replace('https://', '', $link);
                                $objPHPExcel->getActiveSheet()->SetCellValue('AX' . $newRowKendala, '=Hyperlink("https://'.$url.'","Klik Foto")')->getStyle('AX' . $newRowKendala)->applyFromArray($styleForBody);
                                $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(15);

                                $newRowKendala++;
                            }
                        }

                        
                        
                    }
                }
                
                
                
                $rowCount++;
                
                $no++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    public function get_view_filter()
    {
        akses_user('read');
        // $data = $this->input->post();
        $data['id_provinsi'] = $this->input->post('id_provinsi');
        $data['id_kabupaten'] = $this->input->post('id_kabupaten');
        $data['id_komoditi'] = $this->input->post('id_komoditi');
        $data['id_jenis_komoditi'] = $this->input->post('id_jenis_komoditi');
        $data['status'] = $this->input->post('status');
        $data['jenis_registrasi'] = $this->input->post('jenis_registrasi');
        $data = array_merge($data, path_info());
        $this->parser->parse('Laporan_p2l/view_filter', $data);
    }

    public function datatable_filter()
    {
        $this->load->library('Datatables_server_side');

        $this->datatables_server_side->select('
            permohonan_m.id_pemohon,
            permohonan_m.nomor_registrasi,
            permohonan_m.noreg,
            permohonan_m.tanggal_permohonan,
            permohonan_m.GAP,
            komoditi_m.komoditi_nama,
            komoditi_jenis_m.komoditi_jenis_nama,
            permohonan_m.luas_kebun,
            permohonan_m.satuan_luas,
            permohonan_m.nama_lengkap,
            permohonan_m.jenis_pengajuan,
            kabupaten_m.kabupaten_nama,
            permohonan_m.tanggal_pengesahan,
            permohonan_m.tanggal_kadaluarsa,
            permohonan_m.jenis_registrasi,
            permohonan_m.nama_kampung,
            provinsi_m.provinsi_nama,
            view_status_sertifikat.status_sertifikat,
        ');


        if ($this->input->post('provinsi')) {
            $this->datatables_server_side->where('permohonan_m.provinsi_kode', $this->input->post('provinsi'));
        }

        if ($this->input->post('kabupaten')) {
            $this->datatables_server_side->where('permohonan_m.kabupaten_kode', $this->input->post('kabupaten'));
        }

        if ($this->input->post('komoditi')) {
            $this->datatables_server_side->where('permohonan_m.id_komoditi', $this->input->post('komoditi'));
        }

        if ($this->input->post('jenis_komoditi')) {
            $this->datatables_server_side->where('permohonan_m.id_jenis_komoditi', $this->input->post('jenis_komoditi'));
        }

        if ($this->input->post('jenis_registrasi')) {
            $this->datatables_server_side->where('permohonan_m.jenis_registrasi', $this->input->post('jenis_registrasi'));
        }

        if ($this->input->post('status')) {
            $status = $this->input->post('status');
            $whereStatus = 'blm_proses';
            if ($status == '2') {
                $whereStatus = 'terima';
            } elseif ($status == '3') {
                $whereStatus = 'tolak';
            }
            $this->datatables_server_side->where('permohonan_m.status_provinsi', $whereStatus);
        }

        $this->datatables_server_side->join('view_status_sertifikat', 'permohonan_m.id_pemohon = view_status_sertifikat.id_pemohon', 'left');
        $this->datatables_server_side->join('provinsi_m', 'permohonan_m.provinsi_kode = provinsi_m.provinsi_kode', 'left');
        $this->datatables_server_side->join('kabupaten_m', 'permohonan_m.kabupaten_kode = kabupaten_m.kabupaten_kode', 'left');
        // $this->datatables_server_side->join('kecamatan_m', 'permohonan_m.kecamatan_kode = kecamatan_m.kecamatan_kode', 'left');
        $this->datatables_server_side->join('komoditi_m', 'permohonan_m.id_komoditi = komoditi_m.komoditi_kode', 'left');
        $this->datatables_server_side->join('komoditi_jenis_m', 'permohonan_m.id_jenis_komoditi = komoditi_jenis_m.komoditi_jenis_kode', 'left');
        // $this->datatables_server_side->where('permohonan_m.status_kabupaten', 'terima');
        // $this->datatables_server_side->where('permohonan_m.status_provinsi', 'terima');
        $this->datatables_server_side->from('permohonan_m');

        // $this->datatables->select('view_permohonan.*');		






        return print_r($this->datatables_server_side->generate());
    }

    public function id_provinsi($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('provinsi_m.provinsi_kode as id, concat(provinsi_m.provinsi_kode, " - ", provinsi_m.provinsi_nama) as text');
            $data = $this->db->where('provinsi_m.provinsi_kode', $id)->get('provinsi_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('provinsi_m.provinsi_kode as id, concat(provinsi_m.provinsi_kode, " - ", provinsi_m.provinsi_nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('provinsi_m.provinsi_nama', $term);
            }

            $data = $this->db->get('provinsi_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_komoditi($id = null)
    {
        $term = $this->input->get('q');
        if ($id) {
            $this->db->select('komoditi_m.komoditi_kode as id, concat(komoditi_m.komoditi_kode, " - ", komoditi_m.komoditi_nama) as text');
            $data = $this->db->where('komoditi_m.komoditi_kode', $id)->get('komoditi_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('komoditi_m.komoditi_kode as id, concat(komoditi_m.komoditi_kode, " - ", komoditi_m.komoditi_nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('komoditi_m.komoditi_nama', $term);
            }

            $this->db->order_by('komoditi_kode', 'asc');
            $data = $this->db->get('komoditi_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_kabupaten($id = null)
    {
        $term = $this->input->get('q');
        $id_provinsi = $this->input->get('id_provinsi');
        if ($id) {
            $this->db->select('kabupaten_m.kabupaten_kode as id, concat(kabupaten_m.kabupaten_kode, " - ", kabupaten_m.kabupaten_nama) as text');
            $data = $this->db->where('kabupaten_m.kabupaten_kode', $id)->get('kabupaten_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('kabupaten_m.kabupaten_kode as id, concat(kabupaten_m.kabupaten_kode, " - ", kabupaten_m.kabupaten_nama) as text');
            $this->db->limit(25);
            if ($term) {
                $this->db->like('kabupaten_m.kabupaten_nama', $term);
            }
            if ($id_provinsi) $this->db->where('provinsi_kode', $id_provinsi);

            $data = $this->db->get('kabupaten_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_jenis_komoditi($id = null)
    {
        $term = $this->input->get('q');
        $id_komoditi = $this->input->get('id_komoditi');
        if ($id) {
            $this->db->select('komoditi_jenis_m.komoditi_jenis_kode as id, concat(komoditi_jenis_m.komoditi_jenis_kode, " - ", komoditi_jenis_m.komoditi_jenis_nama) as text');
            $data = $this->db->where('komoditi_jenis_m.komoditi_jenis_kode', $id)->get('komoditi_jenis_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('komoditi_jenis_m.komoditi_jenis_kode as id, concat(komoditi_jenis_m.komoditi_jenis_kode, " - ", komoditi_jenis_m.komoditi_jenis_nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('komoditi_jenis_m.komoditi_jenis_nama', $term);
            }
            if ($id_komoditi) $this->db->where('komoditi_kode', $id_komoditi);

            $data = $this->db->get('komoditi_jenis_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function id_varietas($id = null)
    {
        $term = $this->input->get('q');
        $id_jenis_komositi = $this->input->get('id_jenis_komositi');
        if ($id) {
            $this->db->select('varitas_m.kode as id, concat(varitas_m.kode, " - ", varitas_m.nama) as text');
            $data = $this->db->where('varitas_m.kode', $id)->get('varitas_m')->row_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        } else {
            $this->db->select('varitas_m.kode as id, concat(varitas_m.kode, " - ", varitas_m.nama) as text');
            $this->db->limit(100);
            if ($term) {
                $this->db->like('varitas_m.nama', $term);
            }
            if ($id_jenis_komositi) $this->db->where('komoditi_jenis_kode', $id_jenis_komositi);

            $data = $this->db->get('varitas_m')->result_array();
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }
}

/* End of file Lahan.php */