<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Capaian_kinerja_hasil_penjualan extends CI_Controller
{

    private $title = 'Capaian Kinerja Hasil Penjualan';
    private $view = 'Capaian_kinerja_hasil_penjualan';

    public function __construct()
    {
        parent::__construct();
        if(!is_user()) {
            redirect('beranda/login');
        }
    }

    public function index()
    {
        akses_user('read');
        $data['title'] = $this->title;
        $data['subTitle'] = 'List';
        $data['content'] = $this->view . '/index';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }

    public function datatable()
    {
        akses_user('read');
        $this->load->library('Datatables_server_side');
        $this->datatables_server_side->select('
            tbl_capaian_kinerja.id,
            tbl_capaian_kinerja.tanggal_capaian_kinerja,
            tbl_capaian_kinerja.jumlah,
            tbl_registrasi_p2l.nomor_registrasi_p2l,
            tbl_registrasi_p2l.tanggal_registrasi_p2l,
            tbl_capaian_kinerja.nama_kelompok_tani as nama_kelompok,
            tbl_jenis_komoditi.jenis_komoditi_nama,
        ');

        if ($this->session->userdata('session_provinsi_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.provinsi_kode', $this->session->userdata('session_provinsi_kode'));
        if ($this->session->userdata('session_kabupaten_kode')) $this->datatables_server_side->where('tbl_registrasi_p2l.kabupaten_kode', $this->session->userdata('session_kabupaten_kode'));
        $this->datatables_server_side->join('tbl_registrasi_p2l', 'tbl_capaian_kinerja.registrasi_p2l_id = tbl_registrasi_p2l.id');
        $this->datatables_server_side->join('tbl_kelompok_tani', 'tbl_registrasi_p2l.kelompok_tani_id = tbl_kelompok_tani.id', 'left');
        $this->datatables_server_side->join('tbl_jenis_komoditi', 'tbl_capaian_kinerja.jenis_komoditi_id = tbl_jenis_komoditi.id', 'left');
        $this->datatables_server_side->where('tbl_capaian_kinerja.tipe','4');
        $this->datatables_server_side->from('tbl_capaian_kinerja');
        return print_r($this->datatables_server_side->generate());
    }

    public function create()
    {
        akses_user('create');
        $data['provinsi_kode'] = $this->session->userdata('provinsi_kode');
        $data['title'] = $this->title;
        $data['subTitle'] = 'New Record';
        $data['content'] = $this->view . '/create';
        $data = array_merge($data, path_info());
        $this->parser->parse('admin_template/main', $data);
    }
    

    public function detail_registrasi($id) {
        akses_user('read');
        if($id) {
            $registrasi = $this->db
            ->select('tbl_registrasi_p2l.*')
            ->where('tbl_registrasi_p2l.id', $id)
            ->get('tbl_registrasi_p2l')->row_array();
            if($registrasi) {
                return $this->output->set_content_type('application/json')->set_output(json_encode($registrasi));
            } else {
                return jsonOutputError('not found');
            }
        }
    }

    public function save($id = null)
    {

        $this->db->set('registrasi_p2l_id', $this->input->post('registrasi_p2l_id'));
        $this->db->set('nama_kelompok_tani', $this->input->post('nama_kelompok_tani'));
        $this->db->set('jenis_komoditi_id', $this->input->post('jenis_komoditi_id'));
        $this->db->set('tanggal_capaian_kinerja', $this->input->post('tanggal_capaian_kinerja'));
        $this->db->set('jumlah', $this->input->post('jumlah'));
        $this->db->set('status', '1');
        $this->db->set('tipe', '4');
        $this->db->set('tanggal_kirim', date('Y-m-d H:i:s'));
        $insert = $this->db->insert('tbl_capaian_kinerja');
        if($insert) {
            return jsonOutputSuccess();
        } else {
            echo 'error';
        }

        
    }

    public function update($id = null)
    {
        akses_user('update');
        if ($id) {
            $data = $this->db
            ->select('tbl_capaian_kinerja.*')
            ->where('tbl_capaian_kinerja.id', $id)
            ->get('tbl_capaian_kinerja')->row_array();
            if ($data) {
                $data['title'] = $this->title;
                $data['subTitle'] = 'Edit Detail';
                $data['content'] = $this->view . '/update';
                $data = array_merge($data, path_info());
                $this->parser->parse('admin_template/main', $data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function delete()
    {
        if(akses_user('delete')) {
            $id = $this->uri->segment(3);
            $this->db->where('id', $id);
            $this->db->delete('tbl_capaian_kinerja');
            return jsonOutputSuccess();
        } else {
            return jsonOutputError('Anda tidak memiliki izin untuk mengakses halaman ini');
        }
    }
}