<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">{subTitle}</h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                    <tr>
                        <th>Record ID</th>
                        <th>#</th>
                        <th>Provinsi Kode</th>
                        <th>Provinsi Nama</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!-- end:: Content -->

<script>
var table;

table = $('#kt_table_1').DataTable({
    ajax: {
        url: '{site_url}API_wilayah/list_provinsi_datatable',
        method: 'post'
    },
    responsive: true,
    lengthMenu: [5, 10, 25, 50],
    pageLength: 50,
    order: [
        [0, 'desc']
    ],
    columns: [{
            data: 'id',
            visible: false
        },
        { data: 'id', width: 25, className: 'text-center' },
        { data: 'kode', width: 100, className: 'text-center' },
        {
            data: 'nama',
            render: function(data, type, row) {
                var link = '<a href="'+ row.url +'">' + data + '</a>';
                return link;
            }
        },
    ],
    fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        var index = iDisplayIndex + 1;
        $('td:eq(0)', nRow).html(index);
        return nRow;
    }
});
</script>