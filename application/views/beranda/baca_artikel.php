<div class="row">
    <div class="col-md-8">
        <div class="kt-portlet kt-margin-top-30">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title"> {nama_artikel} </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div style="overflow:hidden;">
                    <img src="<?php echo base_url('/uploads/form_artikel/'); ?>{id}/{image}" alt="images"
                        class="img-fluid w-100">
                </div>
                <div class="mt-5 text-justify">
                    {keterangan}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="kt-portlet kt-margin-top-30">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title"> Berita Kampung </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <?php foreach ($show_artikel as $sa) : ?>
                <div class="kt-widget kt-widget--user-profile-3">
                    <div class="kt-widget__top pt-3 pb-3">
                        <div class="kt-widget__media kt-hidden-">
                            <a href="{site_url}beranda/baca_artikel/<?php echo $sa->id; ?>" class="kt-widget__username">
                                <img src="<?php echo base_url('/uploads/form_artikel/' . $sa->id . '/' . $sa->image); ?>"
                                    alt="image">
                            </a>
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__head">
                                <a href="{site_url}beranda/baca_artikel/<?php echo $sa->id; ?>"
                                    class="kt-widget__username">
                                    <?php echo $sa->nama_artikel ?></a>
                            </div>
                            <div class="kt-widget__info">
                                <div class="kt-widget__desc">
                                    <?php echo word_limiter($sa->keterangan, 5); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__bottom mt-3"></div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>