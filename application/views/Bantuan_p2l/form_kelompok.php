<table class="table table-borderless table_kelompok_tani table_kelompok_tani_<?php echo $nomor ?>">
    <tr class="bg-warning">
        <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Nama Kelompok"> </td>
        <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Nama Ketua"> </td>
        <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" placeholder="NIK" required> </td>
        <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" placeholder="No. HP" required> </td>
        <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Luas" readonly> </td>
        <td>
            <div class="btn-group btn-group-sm button_aksi">
                <button type="button" class="btn btn-danger btn_delete_kelompok_tani" title="delete kelompok tani"><i class="fa fa-trash"></i></button>
                <button type="button" class="btn btn-success btn_add_anggota" title="Tambah anggota kelompok tani"><i class="fa fa-users"></i></button>
            </div>
        </td>
    </tr>
</table>

<script>
    var tableKelompokTani = $('.table_kelompok_tani_<?php echo $nomor ?>');
    $(document).on('click','.btn_add_anggota', function(){

        nomor = new Date().getTime();

        var html = `<tr class="">
            <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="NIK"> </td>
            <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Nama anggota"> </td>
            <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="No. HP"> </td>
            <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Luas kebun"> </td>
            <td> &nbsp; </td>
            <td> <button type="button" class="btn btn-danger btn-sm btn_delete_anggota_tani" title="delete anggota tani"><i class="fa fa-trash"></i></button> </td>
            </tr><tr>
            <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Latitude" readonly> </td>
            <td> <input type="text" class="form-control luas_kebun" name="luas_kebun" value="" required placeholder="Longitude" readonly> </td>
            <td> <button type="button" class="btn btn-sm btn-info form-control btn_modal_map1"><i class="fa fa-globe"></i> Open maps</button> </td>
            </tr><tr class="kt-separator kt-separator--border-dashed kt-separator--space-md">
            <td> <select class="form-control jenis_komoditi_kode_anggota" name="jenis_komoditi_kode_anggota" style="width:100%;"></select> </td>
            <td> <select class="form-control komoditi_kode_anggota" name="komoditi_kode_anggota" style="width:100%;"></select> </td>
            </tr>`;
            tableKelompokTani.find('tr:first').after(html);

            ajaxSelect({
                placeholder: 'Pilih Jenis Komoditi',
                id: '.jenis_komoditi_kode_anggota',
                url: 'https://registrasilahanhorti.id/api/master_data/komoditi_select',
            });
            $('.komoditi_kode_anggota').select2({
                placeholder: 'Pilih Opsi'
            }).val('').trigger('change');
    });
</script>