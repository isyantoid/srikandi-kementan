/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100424 (10.4.24-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : srikandi

 Target Server Type    : MySQL
 Target Server Version : 100424 (10.4.24-MariaDB)
 File Encoding         : 65001

 Date: 08/06/2023 14:52:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_registrasi_p2l
-- ----------------------------
DROP TABLE IF EXISTS `tbl_registrasi_p2l`;
CREATE TABLE `tbl_registrasi_p2l`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nomor_registrasi_p2l` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_registrasi_p2l` date NULL DEFAULT NULL,
  `kelompok_tani_id` int NULL DEFAULT NULL,
  `nama_ketua` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `provinsi_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kabupaten_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kecamatan_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `desa_kode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jumlah_penerima_manfaat` int NULL DEFAULT NULL,
  `terdaftar_simluhtan` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0',
  `tanggal_penerima_bantuan` date NULL DEFAULT NULL,
  `realisasi_pemanfaatan_anggaran` decimal(10, 0) NULL DEFAULT NULL,
  `lat` float NULL DEFAULT NULL,
  `lng` float NULL DEFAULT NULL,
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1',
  `tanggal_kirim` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_registrasi_p2l
-- ----------------------------
INSERT INTO `tbl_registrasi_p2l` VALUES (1, 'REG/P2L/2023/0001', '2023-06-08', 3, 'Testing Ketua', '12', '12.01', '12.01.01', '12.01.01.2003', 10, '1', NULL, NULL, NULL, NULL, '1', '2023-06-08 09:26:21');

SET FOREIGN_KEY_CHECKS = 1;
