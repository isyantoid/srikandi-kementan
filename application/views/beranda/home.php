<div class="kt-portlet__body">
    <!-- partial:index.partial.html -->
    <!-- Content Source: https://www.farmflavor.com/at-home/cooking/10-fun-facts-about-apples/  -->
    <div class="slider__wrapper">

        <div class="slider">
            <?php $no = 1;
            foreach ($query_artikel_aktif as $qaa) :
            ?>
            <div class="slider__content">
                <div class="slider__text">

                    <h3><a href="<?php echo base_url('beranda/baca_artikel/' . $qaa->id) ?>">
                            <?php echo $qaa->nama_artikel; ?></a>
                    </h3>
                </div>
                <figure class="slider__image">
                    <a href="<?php echo base_url('beranda/baca_artikel/' . $qaa->id) ?>">
                        <img src="<?php echo base_url('/uploads/form_artikel/' . $qaa->id . '/' . $qaa->image) ?>">
                    </a>

                </figure>
            </div>

            <?php $no++;
            endforeach;
            ?>
        </div>
    </div>

    <div class="row" style="margin-top:8%">
        <div class="col-xs-4" style="border-bottom:2px solid grey;">
        </div>
        <div class="col-xs-4">
            <center>
                <h2>BERITA <strong>KAMPUNG</strong></h2>
            </center>
        </div>
        <div class="col-xs-4" style="border-bottom:2px solid grey;">
        </div>
    </div>

    <div class="row" style="margin-top:1%">
        <?php foreach ($query_artikel as $qa) : ?>
        <div class="col-md-6">
            <div class="kt-section__content">
                <div class="row">
                    <div class="col-md-4">
                        <a href="<?php echo base_url('beranda/baca_artikel/' . $qa->id); ?>">
                            <div
                                style="background-image:url('<?php echo base_url('/uploads/form_artikel/' . $qa->id . '/' . $qa->image); ?>'); width:180px; height:160px; background-size: cover; background-size: 100% auto; background-position:center; background-repeat:no-repeat;">
                                &nbsp;</div>
                        </a>
                    </div>
                    <div class="col-md-8">
                        <h5 class="kt-font-boldest">
                            <a class="text-muted" href="<?php echo base_url('beranda/baca_artikel/' . $qa->id); ?>">
                                <?php echo $qa->nama_artikel; ?> </a>
                        </h5>
                        <div class="kt-section__info text-muted">
                            <i class="la la-calendar"></i> <?php echo date('d-m-Y', strtotime($qa->tanggal)); ?> <i
                                class="la la-user"></i> Admin
                        </div>
                        <?php echo word_limiter($qa->keterangan, 10); ?>
                    </div>
                </div>
                <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
            </div>
        </div>
        <?php endforeach; ?>

    </div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js'></script>
<script>
$('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    autoplay: true,
    autoplaySpeed: 1000,
    dots: false,
    infinite: false,
    speed: 1000,
    fade: true,
    slide: 'div',
    cssEase: 'linear'
});
</script>